﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchIO : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        OrderStatusRepository orderStatusRepository;
        BranchRepository branchRepository;
        ModalityRepository modalityRepository;
        IOHeaderRepository iOHeaderRepository;

        internal IssueOrderControl m_issueOrderControl;
        public frmSearchIO(IssueOrderControl issueOrderControl)
        {
            InitializeComponent();
            m_issueOrderControl = issueOrderControl;
            this.Load += frmSearchIO_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            iOHeaderRepository = new IOHeaderRepository();
            orderStatusRepository = new OrderStatusRepository();
            modalityRepository = new ModalityRepository();
        }

        private void frmSearchIO_Load(object sender, EventArgs e)
        {
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            popupMenuSelectModality();
            sbLoadDataForGridIOHeaderAsync();
            gridViewIOHeader.DoubleClick += dataGridView_CellDoubleClick;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridIOHeaderAsync();
                    break;
                case "close":
                    this.Close();
                    break;
                case "getIOByPO":
                    List<IOHeader> iOHeaders = await iOHeaderRepository.GetAll();
                    var querys = iOHeaders.Where(a => a.IODate >= DateTime.Parse(dateEditFromDate.Text) && a.IODate <= DateTime.Parse(dateEditToDate.Text) && a.IONumber.Contains(searchControl11.Text))
                               .ToList();


                    m_issueOrderControl.gridControl1.DataSource = querys;
                    this.Close();
                    break;
            }

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridIOHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private async Task popupMenuSelectModality()
        {
            List<Modality> modalities = await modalityRepository.GetAll();
            foreach (Modality modality in modalities)
            {
                comboBoxEditModality.Properties.Items.Add(modality.ModalityName);
            }
            comboBoxEditModality.SelectedIndex = 38;
        }

        private async Task sbLoadDataForGridIOHeaderAsync()
        {
            List<IOHeader> iOHeaders = await iOHeaderRepository.GetAll();
            gridControlIOHeader.DataSource =iOHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewIOHeader.RowCount > 0)
            {
                IssueOrderControl.selectedIONumber = (string)(sender as GridView).GetFocusedRowCellValue("IONumber");
                this.Close();
            }
        }

        private async void filter()
        {
            List<IOHeader> iOHeaders = await iOHeaderRepository.GetAll();
            if (iOHeaders.Count > 0)
            {
                if (comboBoxEditHandlingStatus.SelectedIndex==0)
                {
                    if (comboBoxEditBranch.SelectedItem.ToString().Equals("<>"))
                    {
                        var querys = iOHeaders.Where(a =>a.ModalityName.Equals(comboBoxEditModality.Text)&&a.IODate >= DateTime.Parse(dateEditFromDate.Text) && a.IODate <= DateTime.Parse(dateEditToDate.Text))
                            .ToList();
                        gridControlIOHeader.DataSource = querys;
                    }
                    else
                    {
                        var querys = iOHeaders.Where(a => a.ModalityName.Equals(comboBoxEditModality.Text) && a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") &&a.IODate >= DateTime.Parse(dateEditFromDate.Text) && a.IODate <= DateTime.Parse(dateEditToDate.Text))
                       .ToList();
                        gridControlIOHeader.DataSource = querys;
                    }                  
                }
                else
                {
                    if (comboBoxEditBranch.SelectedItem.ToString().Equals("<>"))
                    {
                        var querys = iOHeaders.Where(a => a.ModalityName.Equals(comboBoxEditModality.Text) &&a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex-1+"")&&a.IODate >= DateTime.Parse(dateEditFromDate.Text) && a.IODate <= DateTime.Parse(dateEditToDate.Text))
                            .ToList();
                        gridControlIOHeader.DataSource = querys;
                    }
                    else
                    {
                        var querys = iOHeaders.Where(a =>a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.ModalityName.Equals(comboBoxEditModality.Text) && a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex-1 + "") && a.IODate >= DateTime.Parse(dateEditFromDate.Text) && a.IODate <= DateTime.Parse(dateEditToDate.Text))
                       .ToList();
                        gridControlIOHeader.DataSource = querys;
                    }
                }
            }
        }

        private void comboBoxEditBranch_SelectedValueChanged(object sender, EventArgs e)
        {
            filter();
        }

        private void comboBoxEditHandlingStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            filter();
        }

        private void dateEditFromDate_EditValueChanged(object sender, EventArgs e)
        {
            filter();
        }

        private void dateEditToDate_EditValueChanged(object sender, EventArgs e)
        {
            filter();
        }

        private void comboBoxEditModality_SelectedValueChanged(object sender, EventArgs e)
        {
            filter();
        }
    }
}
