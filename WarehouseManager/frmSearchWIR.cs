﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchWIR : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        BranchRepository branchRepository;
        WIRHeaderRepository wIRHeaderRepository;
        public frmSearchWIR()
        {
            InitializeComponent();
            this.Load += frmSearchWIR_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wIRHeaderRepository = new WIRHeaderRepository();


        }

        private async  void frmSearchWIR_Load(object sender, EventArgs e)
        {
            
            gridViewWIRHeader.DoubleClick += dataGridView_CellDoubleClick;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);
            windowsUIButtonPanel1.ButtonClick += button_Click;

            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

            await popupMenuSelectBranch();
            await popupMenuSelectHandlingStatus();
            filter();
        }

        private async void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                await sbLoadDataForGridWIRHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridWIRHeaderAsync();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private async Task sbLoadDataForGridWIRHeaderAsync()
        {
            List<WIRHeader> wIRHeaders = await wIRHeaderRepository.GetAll();
            gridControlWIRHeader.DataSource = wIRHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewWIRHeader.RowCount > 0)
            {
                List<WIRHeader> wIRHeaders = await wIRHeaderRepository.GetUnderID((string)(sender as GridView).GetFocusedRowCellValue("WIRNumber"));
                IssueRequisitionControl.selectedWIRNumber = (string)(sender as GridView).GetFocusedRowCellValue("WIRNumber");
                this.Close();
            }
        }

        private async void filter()
        {
            List<WIRHeader> wIRHeaders = await wIRHeaderRepository.GetAll();
            if (wIRHeaders.Count > 0)
            {
                try
                {
                    if (comboBoxEditHandlingStatus.SelectedIndex==0)
                    {
                        if (comboBoxEditBranch.SelectedIndex==0)
                        {
                            var querys = wIRHeaders.Where(a => a.WIRDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIRDate <= DateTime.Parse(dateEditToDate.Text))
                                .ToList();
                            gridControlWIRHeader.DataSource = querys;
                        }
                        else
                        {
                            var querys = wIRHeaders.Where(a => a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.WIRDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIRDate <= DateTime.Parse(dateEditToDate.Text))
                                .ToList();
                            gridControlWIRHeader.DataSource = querys;
                        }
                    }
                    else
                    {
                        if (comboBoxEditBranch.SelectedIndex == 0)
                        {
                            var querys = wIRHeaders.Where(a => a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex - 1 + "") && a.WIRDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIRDate <= DateTime.Parse(dateEditToDate.Text))
                                .ToList();
                            gridControlWIRHeader.DataSource = querys;
                        }
                        else
                        {
                            var querys = wIRHeaders.Where(a => a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex - 1 + "") && a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.WIRDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIRDate <= DateTime.Parse(dateEditToDate.Text))
                                .ToList();
                            gridControlWIRHeader.DataSource = querys;
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }


        private void comboBoxEditBranch_SelectedValueChanged(object sender, EventArgs e)
        {
            filter();
        }

        private void comboBoxEditHandlingStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            filter();
        }

        private void dateEditFromDate_EditValueChanged(object sender, EventArgs e)
        {
            filter();
        }

        private void dateEditToDate_EditValueChanged(object sender, EventArgs e)
        {
            filter();
        }
    }
}
