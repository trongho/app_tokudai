﻿using DevExpress.XtraBars.Docking2010;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using OfficeOpenXml;
using LicenseContext = OfficeOpenXml.LicenseContext;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Label = WarehouseManager.Models.Label;

namespace WarehouseManager
{
    public partial class LabelManagerControl : DevExpress.XtraEditors.XtraUserControl
    {

        LabelRepository labelRepository = null;
        public LabelManagerControl()
        {
            InitializeComponent();
            labelRepository = new LabelRepository();
            this.Load += LabelManagerControl_Load;

        }

        private void LabelManagerControl_Load(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_Click;
            gridViewGoods.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            gridViewGoods.RowClick += gridView_RowClick;
            sbLoadDataForGridGoodsAsync();
        }

        private void gridView_RowClick(Object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            textEditPalletID.Text = (string)(sender as GridView).GetFocusedRowCellValue("PalletID");
            textEditCurrentIDCode.Text = (string)(sender as GridView).GetFocusedRowCellValue("CurrentIDCode").ToString();
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewGoods.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        public async Task updateLabels()
        {

            Label label = await labelRepository.GetUnderID(textEditPalletID.Text);
            label.PalletID = textEditPalletID.Text.ToString();
            label.CurrentIDCode = 0;

            if(await labelRepository.Update(label, label.PalletID) > 0)
            {
                sbLoadDataForGridGoodsAsync();
                MessageBox.Show("Reset success!!!");
            }
           
        }


        private async void sbLoadDataForGridGoodsAsync()
        {
            gridControlGoods.DataSource = await labelRepository.getAll();
        }
       

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    
                    break;
                case "save":
                   
                    break;
                case "delete":
                    
                    break;
                case "search":
                    break;
                case "refesh":
                    sbLoadDataForGridGoodsAsync();
                    break;
                case "import":
                    
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":

                    break;
            }

        }

        private async void Reset_Click(object sender, EventArgs e)
        {
            await updateLabels();
        }
    }
}
