﻿
namespace WarehouseManager
{
    partial class IssueRequisitionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IssueRequisitionControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition10 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition6 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition7 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditWIRNumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditWIRDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditTotalQuantityByItem = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalQuantityByPack = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditPONumber = new DevExpress.XtraEditors.TextEdit();
            this.textEditPalletID = new DevExpress.XtraEditors.MemoEdit();
            this.textEditQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditNote = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemWRRNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRRDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityByItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityByPack = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.wRRDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.wRRDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPartNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLotID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIRQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMFGDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXPDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInputDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPalletID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlWIRDetail = new DevExpress.XtraGrid.GridControl();
            this.gridViewWIRDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWIRNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPalletID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductFamily = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLotID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByItem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByPack = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMFGDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXPDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInputDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.collNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEditTotalIRQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalClosingStockQuantity = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.colQuantity1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWIRNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWIRDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWIRDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByPack.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPONumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPalletID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByPack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWIRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWIRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalIRQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalClosingStockQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(400, 144, 812, 500);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1240, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1236, 32);
            this.windowsUIButtonPanel1.TabIndex = 4;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1240, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1240, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.label3);
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEditWIRNumber);
            this.layoutControl2.Controls.Add(this.dateEditWIRDate);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityByItem);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityByPack);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl2.Controls.Add(this.textEditPONumber);
            this.layoutControl2.Controls.Add(this.textEditPalletID);
            this.layoutControl2.Controls.Add(this.textEditQuantity);
            this.layoutControl2.Controls.Add(this.textEditNote);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(515, 298, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1240, 135);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(270, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 25);
            this.label3.TabIndex = 32;
            this.label3.Text = "(*)";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(270, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "(*)";
            // 
            // textEditWIRNumber
            // 
            this.textEditWIRNumber.Location = new System.Drawing.Point(99, 12);
            this.textEditWIRNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditWIRNumber.Name = "textEditWIRNumber";
            this.textEditWIRNumber.Properties.ReadOnly = true;
            this.textEditWIRNumber.Properties.UseReadOnlyAppearance = false;
            this.textEditWIRNumber.Size = new System.Drawing.Size(167, 20);
            this.textEditWIRNumber.StyleController = this.layoutControl2;
            this.textEditWIRNumber.TabIndex = 4;
            // 
            // dateEditWIRDate
            // 
            this.dateEditWIRDate.EditValue = null;
            this.dateEditWIRDate.Location = new System.Drawing.Point(438, 12);
            this.dateEditWIRDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateEditWIRDate.Name = "dateEditWIRDate";
            this.dateEditWIRDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWIRDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWIRDate.Properties.CalendarTimeProperties.DisplayFormat.FormatString = "d";
            this.dateEditWIRDate.Properties.CalendarTimeProperties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWIRDate.Properties.CalendarTimeProperties.EditFormat.FormatString = "d";
            this.dateEditWIRDate.Properties.CalendarTimeProperties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWIRDate.Size = new System.Drawing.Size(128, 20);
            this.dateEditWIRDate.StyleController = this.layoutControl2;
            this.dateEditWIRDate.TabIndex = 7;
            // 
            // textEditTotalQuantityByItem
            // 
            this.textEditTotalQuantityByItem.Location = new System.Drawing.Point(1159, 12);
            this.textEditTotalQuantityByItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityByItem.Name = "textEditTotalQuantityByItem";
            this.textEditTotalQuantityByItem.Properties.DisplayFormat.FormatString = "G29";
            this.textEditTotalQuantityByItem.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.textEditTotalQuantityByItem.Size = new System.Drawing.Size(69, 20);
            this.textEditTotalQuantityByItem.StyleController = this.layoutControl2;
            this.textEditTotalQuantityByItem.TabIndex = 10;
            // 
            // textEditTotalQuantityByPack
            // 
            this.textEditTotalQuantityByPack.Location = new System.Drawing.Point(1159, 40);
            this.textEditTotalQuantityByPack.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityByPack.Name = "textEditTotalQuantityByPack";
            this.textEditTotalQuantityByPack.Properties.DisplayFormat.FormatString = "G29";
            this.textEditTotalQuantityByPack.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.textEditTotalQuantityByPack.Size = new System.Drawing.Size(69, 20);
            this.textEditTotalQuantityByPack.StyleController = this.layoutControl2;
            this.textEditTotalQuantityByPack.TabIndex = 11;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(958, 12);
            this.comboBoxEditHandlingStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(69, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 13;
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(698, 12);
            this.comboBoxEditBranch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(128, 20);
            this.comboBoxEditBranch.StyleController = this.layoutControl2;
            this.comboBoxEditBranch.TabIndex = 14;
            // 
            // textEditPONumber
            // 
            this.textEditPONumber.Location = new System.Drawing.Point(140, 69);
            this.textEditPONumber.Name = "textEditPONumber";
            this.textEditPONumber.Size = new System.Drawing.Size(126, 20);
            this.textEditPONumber.StyleController = this.layoutControl2;
            this.textEditPONumber.TabIndex = 24;
            this.textEditPONumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textEditPONumber_KeyDown);
            // 
            // textEditPalletID
            // 
            this.textEditPalletID.Location = new System.Drawing.Point(438, 98);
            this.textEditPalletID.Name = "textEditPalletID";
            this.textEditPalletID.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textEditPalletID.Size = new System.Drawing.Size(388, 25);
            this.textEditPalletID.StyleController = this.layoutControl2;
            this.textEditPalletID.TabIndex = 29;
            // 
            // textEditQuantity
            // 
            this.textEditQuantity.Location = new System.Drawing.Point(140, 98);
            this.textEditQuantity.Name = "textEditQuantity";
            this.textEditQuantity.Size = new System.Drawing.Size(126, 20);
            this.textEditQuantity.StyleController = this.layoutControl2;
            this.textEditQuantity.TabIndex = 31;
            // 
            // textEditNote
            // 
            this.textEditNote.Location = new System.Drawing.Point(438, 40);
            this.textEditNote.Name = "textEditNote";
            this.textEditNote.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textEditNote.Size = new System.Drawing.Size(388, 54);
            this.textEditNote.StyleController = this.layoutControl2;
            this.textEditNote.TabIndex = 33;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemWRRNumber,
            this.layoutControlItemWRRDate,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem13,
            this.layoutControlItemTotalQuantityByItem,
            this.layoutControlItemTotalQuantityByPack,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItemBranch,
            this.layoutControlItem3,
            this.layoutControlItem14,
            this.layoutControlItem6,
            this.simpleLabelItem1});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 19.405592722871681D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 2.9720363856415997D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 19.405592722871681D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 19.405592722871681D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 15D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 15D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6});
            rowDefinition1.Height = 25D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 25D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 25D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 25D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1240, 135);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemWRRNumber
            // 
            this.layoutControlItemWRRNumber.Control = this.textEditWIRNumber;
            this.layoutControlItemWRRNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemWRRNumber.Name = "layoutControlItemWRRNumber";
            this.layoutControlItemWRRNumber.Size = new System.Drawing.Size(258, 28);
            this.layoutControlItemWRRNumber.Text = "Số phiếu yêu cầu";
            this.layoutControlItemWRRNumber.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemWRRNumber.TextSize = new System.Drawing.Size(82, 13);
            this.layoutControlItemWRRNumber.TextToControlDistance = 5;
            // 
            // layoutControlItemWRRDate
            // 
            this.layoutControlItemWRRDate.Control = this.dateEditWIRDate;
            this.layoutControlItemWRRDate.Location = new System.Drawing.Point(298, 0);
            this.layoutControlItemWRRDate.Name = "layoutControlItemWRRDate";
            this.layoutControlItemWRRDate.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemWRRDate.Size = new System.Drawing.Size(260, 28);
            this.layoutControlItemWRRDate.Text = "Ngày yêu cầu";
            this.layoutControlItemWRRDate.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.label3;
            this.layoutControlItem9.Location = new System.Drawing.Point(258, 86);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem9.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem9.Size = new System.Drawing.Size(40, 29);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEditQuantity;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 86);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem8.Size = new System.Drawing.Size(258, 29);
            this.layoutControlItem8.Text = "SL cần xuất";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.textEditPONumber;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 57);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem13.Size = new System.Drawing.Size(258, 29);
            this.layoutControlItem13.Text = "PO Number";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItemTotalQuantityByItem
            // 
            this.layoutControlItemTotalQuantityByItem.Control = this.textEditTotalQuantityByItem;
            this.layoutControlItemTotalQuantityByItem.Location = new System.Drawing.Point(1019, 0);
            this.layoutControlItemTotalQuantityByItem.Name = "layoutControlItemTotalQuantityByItem";
            this.layoutControlItemTotalQuantityByItem.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalQuantityByItem.Size = new System.Drawing.Size(201, 28);
            this.layoutControlItemTotalQuantityByItem.Text = "∑ SL yêu cầu";
            this.layoutControlItemTotalQuantityByItem.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItemTotalQuantityByPack
            // 
            this.layoutControlItemTotalQuantityByPack.Control = this.textEditTotalQuantityByPack;
            this.layoutControlItemTotalQuantityByPack.CustomizationFormText = "Σ Mã yêu cầu";
            this.layoutControlItemTotalQuantityByPack.Location = new System.Drawing.Point(1019, 28);
            this.layoutControlItemTotalQuantityByPack.Name = "layoutControlItemTotalQuantityByPack";
            this.layoutControlItemTotalQuantityByPack.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalQuantityByPack.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalQuantityByPack.Size = new System.Drawing.Size(201, 29);
            this.layoutControlItemTotalQuantityByPack.Text = "Σ Thùng yêu cầu";
            this.layoutControlItemTotalQuantityByPack.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(818, 0);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(201, 28);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.Location = new System.Drawing.Point(558, 0);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItemBranch.Size = new System.Drawing.Size(260, 28);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(258, 57);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem3.Size = new System.Drawing.Size(40, 29);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.textEditNote;
            this.layoutControlItem14.Location = new System.Drawing.Point(298, 28);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem14.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem14.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem14.OptionsTableLayoutItem.RowSpan = 2;
            this.layoutControlItem14.Size = new System.Drawing.Size(520, 58);
            this.layoutControlItem14.Text = "Ghi chú";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(124, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEditPalletID;
            this.layoutControlItem6.Location = new System.Drawing.Point(298, 86);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem6.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem6.Size = new System.Drawing.Size(520, 29);
            this.layoutControlItem6.Text = "Danh sách Pallet cần xuất";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(124, 13);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem1.Location = new System.Drawing.Point(258, 0);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.OptionsTableLayoutItem.ColumnIndex = 1;
            this.simpleLabelItem1.Size = new System.Drawing.Size(40, 28);
            this.simpleLabelItem1.Text = "(*)";
            this.simpleLabelItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(14, 13);
            // 
            // wRRDetailBindingSource
            // 
            this.wRRDetailBindingSource.DataMember = "WRRDetail";
            this.wRRDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wRRDetailTableAdapter
            // 
            this.wRRDetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.simpleButton1);
            this.layoutControl3.Controls.Add(this.gridControl2);
            this.layoutControl3.Controls.Add(this.gridControl1);
            this.layoutControl3.Controls.Add(this.gridControlWIRDetail);
            this.layoutControl3.Controls.Add(this.textEditTotalIRQuantity);
            this.layoutControl3.Controls.Add(this.textEditTotalClosingStockQuantity);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 172);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(622, 480, 650, 400);
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1240, 474);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.simpleButton1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopLeft;
            this.simpleButton1.Location = new System.Drawing.Point(116, 419);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.simpleButton1.Size = new System.Drawing.Size(126, 22);
            this.simpleButton1.StyleController = this.layoutControl3;
            this.simpleButton1.TabIndex = 27;
            this.simpleButton1.Text = "LƯU( Ctrl+S)";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Location = new System.Drawing.Point(12, 12);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(100, 373);
            this.gridControl2.TabIndex = 10;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPartNumber1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView2_FocusedRowChanged);
            // 
            // colPartNumber1
            // 
            this.colPartNumber1.Caption = "Part Number";
            this.colPartNumber1.FieldName = "PartNumber";
            this.colPartNumber1.Name = "colPartNumber1";
            this.colPartNumber1.Visible = true;
            this.colPartNumber1.VisibleIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(116, 12);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(256, 373);
            this.gridControl1.TabIndex = 6;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLotID1,
            this.colQuantity,
            this.colIRQuantity,
            this.colMFGDate1,
            this.colEXPDate1,
            this.colInputDate1,
            this.colPalletID1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            // 
            // colLotID1
            // 
            this.colLotID1.Caption = "Lot ID";
            this.colLotID1.FieldName = "LotID";
            this.colLotID1.Name = "colLotID1";
            this.colLotID1.OptionsColumn.ReadOnly = true;
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "SL Tồn Kho";
            this.colQuantity.DisplayFormat.FormatString = "G29";
            this.colQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.OptionsColumn.ReadOnly = true;
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 1;
            // 
            // colIRQuantity
            // 
            this.colIRQuantity.Caption = "SL xuất";
            this.colIRQuantity.DisplayFormat.FormatString = "G29";
            this.colIRQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colIRQuantity.FieldName = "IRQuantity";
            this.colIRQuantity.Name = "colIRQuantity";
            this.colIRQuantity.Visible = true;
            this.colIRQuantity.VisibleIndex = 2;
            // 
            // colMFGDate1
            // 
            this.colMFGDate1.Caption = "MFG Date";
            this.colMFGDate1.FieldName = "MFGDate";
            this.colMFGDate1.Name = "colMFGDate1";
            // 
            // colEXPDate1
            // 
            this.colEXPDate1.Caption = "EXP Date";
            this.colEXPDate1.FieldName = "EXPDate";
            this.colEXPDate1.Name = "colEXPDate1";
            // 
            // colInputDate1
            // 
            this.colInputDate1.Caption = "Input Date";
            this.colInputDate1.FieldName = "InputDate";
            this.colInputDate1.Name = "colInputDate1";
            // 
            // colPalletID1
            // 
            this.colPalletID1.Caption = "Pallet Number";
            this.colPalletID1.FieldName = "PalletID";
            this.colPalletID1.Name = "colPalletID1";
            this.colPalletID1.Visible = true;
            this.colPalletID1.VisibleIndex = 0;
            // 
            // gridControlWIRDetail
            // 
            this.gridControlWIRDetail.DataSource = this.wRRDetailBindingSource;
            this.gridControlWIRDetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWIRDetail.Location = new System.Drawing.Point(376, 12);
            this.gridControlWIRDetail.MainView = this.gridViewWIRDetail;
            this.gridControlWIRDetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWIRDetail.Name = "gridControlWIRDetail";
            this.gridControlWIRDetail.Size = new System.Drawing.Size(932, 433);
            this.gridControlWIRDetail.TabIndex = 5;
            this.gridControlWIRDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWIRDetail});
            // 
            // gridViewWIRDetail
            // 
            this.gridViewWIRDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWIRNumber,
            this.colOrdinal,
            this.colPalletID,
            this.colPartNumber,
            this.colPartName,
            this.colProductFamily,
            this.colLotID,
            this.colQuantityByItem,
            this.colQuantityByPack,
            this.colPackingVolume,
            this.colTotalQuantity,
            this.colUnit,
            this.colMFGDate,
            this.colEXPDate,
            this.colInputDate,
            this.colPONumber,
            this.colStatus,
            this.colRemark,
            this.collNo,
            this.colMaterial,
            this.colMold});
            this.gridViewWIRDetail.DetailHeight = 284;
            this.gridViewWIRDetail.GridControl = this.gridControlWIRDetail;
            this.gridViewWIRDetail.Name = "gridViewWIRDetail";
            this.gridViewWIRDetail.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewWIRDetail.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridViewWIRDetail.OptionsCustomization.AllowSort = false;
            this.gridViewWIRDetail.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewWIRDetail.OptionsView.RowAutoHeight = true;
            this.gridViewWIRDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPackingVolume, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewWIRDetail.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridViewWIRDetail_RowCellClick);
            this.gridViewWIRDetail.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewWIRDetail_FocusedRowChanged);
            // 
            // colWIRNumber
            // 
            this.colWIRNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colWIRNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colWIRNumber.Caption = "WIRNumber";
            this.colWIRNumber.FieldName = "WIRNumber";
            this.colWIRNumber.MinWidth = 21;
            this.colWIRNumber.Name = "colWIRNumber";
            this.colWIRNumber.Width = 81;
            // 
            // colOrdinal
            // 
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.MinWidth = 21;
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Width = 81;
            // 
            // colPalletID
            // 
            this.colPalletID.Caption = "Pallet Number";
            this.colPalletID.FieldName = "PalletID";
            this.colPalletID.MinWidth = 86;
            this.colPalletID.Name = "colPalletID";
            this.colPalletID.Visible = true;
            this.colPalletID.VisibleIndex = 4;
            this.colPalletID.Width = 86;
            // 
            // colPartNumber
            // 
            this.colPartNumber.Caption = "Part Number";
            this.colPartNumber.FieldName = "PartNumber";
            this.colPartNumber.MinWidth = 86;
            this.colPartNumber.Name = "colPartNumber";
            this.colPartNumber.Visible = true;
            this.colPartNumber.VisibleIndex = 2;
            this.colPartNumber.Width = 117;
            // 
            // colPartName
            // 
            this.colPartName.Caption = "Part Name";
            this.colPartName.FieldName = "PartName";
            this.colPartName.MinWidth = 129;
            this.colPartName.Name = "colPartName";
            this.colPartName.Visible = true;
            this.colPartName.VisibleIndex = 3;
            this.colPartName.Width = 215;
            // 
            // colProductFamily
            // 
            this.colProductFamily.Caption = "Product Family";
            this.colProductFamily.FieldName = "ProductFamily";
            this.colProductFamily.MinWidth = 86;
            this.colProductFamily.Name = "colProductFamily";
            this.colProductFamily.Visible = true;
            this.colProductFamily.VisibleIndex = 1;
            this.colProductFamily.Width = 86;
            // 
            // colLotID
            // 
            this.colLotID.Caption = "Lot ID";
            this.colLotID.CustomizationCaption = "Lot ID";
            this.colLotID.FieldName = "LotID";
            this.colLotID.MinWidth = 86;
            this.colLotID.Name = "colLotID";
            this.colLotID.Width = 90;
            // 
            // colQuantityByItem
            // 
            this.colQuantityByItem.Caption = "Qty";
            this.colQuantityByItem.DisplayFormat.FormatString = "G29";
            this.colQuantityByItem.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityByItem.FieldName = "QuantityByItem";
            this.colQuantityByItem.MinWidth = 86;
            this.colQuantityByItem.Name = "colQuantityByItem";
            this.colQuantityByItem.Visible = true;
            this.colQuantityByItem.VisibleIndex = 6;
            this.colQuantityByItem.Width = 86;
            // 
            // colQuantityByPack
            // 
            this.colQuantityByPack.Caption = "Cartons";
            this.colQuantityByPack.DisplayFormat.FormatString = "G29";
            this.colQuantityByPack.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityByPack.FieldName = "QuantityByPack";
            this.colQuantityByPack.MinWidth = 86;
            this.colQuantityByPack.Name = "colQuantityByPack";
            this.colQuantityByPack.Visible = true;
            this.colQuantityByPack.VisibleIndex = 5;
            this.colQuantityByPack.Width = 129;
            // 
            // colPackingVolume
            // 
            this.colPackingVolume.Caption = "Qty per Lot";
            this.colPackingVolume.DisplayFormat.FormatString = "G29";
            this.colPackingVolume.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPackingVolume.FieldName = "PackingVolume";
            this.colPackingVolume.MinWidth = 86;
            this.colPackingVolume.Name = "colPackingVolume";
            this.colPackingVolume.Visible = true;
            this.colPackingVolume.VisibleIndex = 7;
            this.colPackingVolume.Width = 129;
            // 
            // colTotalQuantity
            // 
            this.colTotalQuantity.Caption = "Tổng lượng";
            this.colTotalQuantity.DisplayFormat.FormatString = "G29";
            this.colTotalQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalQuantity.FieldName = "TotalQuantity";
            this.colTotalQuantity.MinWidth = 86;
            this.colTotalQuantity.Name = "colTotalQuantity";
            this.colTotalQuantity.Width = 86;
            // 
            // colUnit
            // 
            this.colUnit.Caption = "Unit";
            this.colUnit.FieldName = "Unit";
            this.colUnit.MinWidth = 50;
            this.colUnit.Name = "colUnit";
            this.colUnit.Visible = true;
            this.colUnit.VisibleIndex = 8;
            this.colUnit.Width = 64;
            // 
            // colMFGDate
            // 
            this.colMFGDate.Caption = "MFG Date";
            this.colMFGDate.FieldName = "MFGDate";
            this.colMFGDate.MinWidth = 86;
            this.colMFGDate.Name = "colMFGDate";
            this.colMFGDate.Width = 86;
            // 
            // colEXPDate
            // 
            this.colEXPDate.Caption = "EXP Date";
            this.colEXPDate.FieldName = "EXPDate";
            this.colEXPDate.MinWidth = 86;
            this.colEXPDate.Name = "colEXPDate";
            this.colEXPDate.Width = 86;
            // 
            // colInputDate
            // 
            this.colInputDate.Caption = "Ngày nhập";
            this.colInputDate.FieldName = "InputDate";
            this.colInputDate.MinWidth = 86;
            this.colInputDate.Name = "colInputDate";
            this.colInputDate.Width = 86;
            // 
            // colPONumber
            // 
            this.colPONumber.Caption = "PO Number";
            this.colPONumber.FieldName = "PONumber";
            this.colPONumber.MinWidth = 80;
            this.colPONumber.Name = "colPONumber";
            this.colPONumber.Visible = true;
            this.colPONumber.VisibleIndex = 9;
            this.colPONumber.Width = 80;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 21;
            this.colStatus.Name = "colStatus";
            this.colStatus.Width = 100;
            // 
            // colRemark
            // 
            this.colRemark.Caption = "Remark";
            this.colRemark.FieldName = "Remark";
            this.colRemark.MinWidth = 86;
            this.colRemark.Name = "colRemark";
            this.colRemark.Visible = true;
            this.colRemark.VisibleIndex = 10;
            this.colRemark.Width = 86;
            // 
            // collNo
            // 
            this.collNo.Caption = "No.";
            this.collNo.FieldName = "No";
            this.collNo.MinWidth = 43;
            this.collNo.Name = "collNo";
            this.collNo.Visible = true;
            this.collNo.VisibleIndex = 0;
            this.collNo.Width = 52;
            // 
            // colMaterial
            // 
            this.colMaterial.Caption = "Material";
            this.colMaterial.FieldName = "Material";
            this.colMaterial.Name = "colMaterial";
            // 
            // colMold
            // 
            this.colMold.Caption = "Mold";
            this.colMold.FieldName = "Mold";
            this.colMold.MinWidth = 75;
            this.colMold.Name = "colMold";
            // 
            // textEditTotalIRQuantity
            // 
            this.textEditTotalIRQuantity.Location = new System.Drawing.Point(312, 389);
            this.textEditTotalIRQuantity.Name = "textEditTotalIRQuantity";
            this.textEditTotalIRQuantity.Properties.DisplayFormat.FormatString = "G29";
            this.textEditTotalIRQuantity.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.textEditTotalIRQuantity.Size = new System.Drawing.Size(60, 20);
            this.textEditTotalIRQuantity.StyleController = this.layoutControl3;
            this.textEditTotalIRQuantity.TabIndex = 7;
            // 
            // textEditTotalClosingStockQuantity
            // 
            this.textEditTotalClosingStockQuantity.Location = new System.Drawing.Point(182, 389);
            this.textEditTotalClosingStockQuantity.Name = "textEditTotalClosingStockQuantity";
            this.textEditTotalClosingStockQuantity.Properties.DisplayFormat.FormatString = "G29";
            this.textEditTotalClosingStockQuantity.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.textEditTotalClosingStockQuantity.Size = new System.Drawing.Size(60, 20);
            this.textEditTotalClosingStockQuantity.StyleController = this.layoutControl3;
            this.textEditTotalClosingStockQuantity.TabIndex = 9;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem10,
            this.layoutControlItem18,
            this.layoutControlItem12,
            this.layoutControlItem5});
            this.layoutControlGroup2.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup2.Name = "Root";
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 8D;
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 10D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 10D;
            columnDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition10.Width = 72D;
            this.layoutControlGroup2.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition7,
            columnDefinition8,
            columnDefinition9,
            columnDefinition10});
            rowDefinition5.Height = 88D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition6.Height = 7D;
            rowDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition7.Height = 7D;
            rowDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup2.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition5,
            rowDefinition6,
            rowDefinition7});
            this.layoutControlGroup2.Size = new System.Drawing.Size(1320, 457);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlWIRDetail;
            this.layoutControlItem2.Location = new System.Drawing.Point(364, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem2.OptionsTableLayoutItem.RowSpan = 3;
            this.layoutControlItem2.Size = new System.Drawing.Size(936, 437);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControl1;
            this.layoutControlItem4.Location = new System.Drawing.Point(104, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem4.Size = new System.Drawing.Size(260, 377);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.textEditTotalIRQuantity;
            this.layoutControlItem10.Location = new System.Drawing.Point(234, 377);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem10.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem10.Size = new System.Drawing.Size(130, 30);
            this.layoutControlItem10.Text = "∑ SL đã chọn";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(62, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.gridControl2;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(104, 377);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEditTotalClosingStockQuantity;
            this.layoutControlItem12.Location = new System.Drawing.Point(104, 377);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem12.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem12.Size = new System.Drawing.Size(130, 30);
            this.layoutControlItem12.Text = "∑ SL tồn";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(62, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButton1;
            this.layoutControlItem5.Location = new System.Drawing.Point(104, 407);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem5.Size = new System.Drawing.Size(130, 30);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // colQuantity1
            // 
            this.colQuantity1.Name = "colQuantity1";
            // 
            // IssueRequisitionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "IssueRequisitionControl";
            this.Size = new System.Drawing.Size(1240, 646);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditWIRNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWIRDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWIRDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByPack.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPONumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPalletID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByPack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWIRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWIRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalIRQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalClosingStockQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit textEditWIRNumber;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRRNumber;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.DateEdit dateEditWIRDate;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityByItem;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityByPack;
        private System.Windows.Forms.BindingSource wRRDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter wRRDetailTableAdapter;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.GridControl gridControlWIRDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWIRDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colWIRNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colPartNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPartName;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByItem;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByPack;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn collNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRRDate;
        private DevExpress.XtraGrid.Columns.GridColumn colProductFamily;
        private DevExpress.XtraGrid.Columns.GridColumn colLotID;
        private DevExpress.XtraGrid.Columns.GridColumn colPalletID;
        private DevExpress.XtraGrid.Columns.GridColumn colMFGDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEXPDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemark;
        private DevExpress.XtraGrid.Columns.GridColumn colInputDate;
        private DevExpress.XtraGrid.Columns.GridColumn colUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colPONumber;
        private DevExpress.XtraEditors.TextEdit textEditPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterial;
        private DevExpress.XtraGrid.Columns.GridColumn colMold;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity1;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.MemoEdit textEditPalletID;
        private DevExpress.XtraEditors.TextEdit textEditQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colLotID1;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit textEditTotalIRQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colIRQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalClosingStockQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colPartNumber1;
        private DevExpress.XtraEditors.MemoEdit textEditNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityByItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityByPack;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraGrid.Columns.GridColumn colMFGDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colEXPDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colInputDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colPalletID1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
    }
}
