﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;

namespace WarehouseManager
{
    public partial class ProcesTallyControl : DevExpress.XtraEditors.XtraUserControl
    {
        TSDataGeneralRepository tSDataGeneralRepository = null;
        TSHeaderRepository tSHeaderRepository = null;
        TSDetailRepository tSDetailRepository = null;
        InventoryDetailRepository inventoryDetailRepository = null;
        InventoryRepository inventoryRepository = null;
        BranchRepository branchRepository = null;
        WarehouseRepository warehouseRepository = null;
        List<TSDataGeneral> tSDataGenerals = null;
        public ProcesTallyControl()
        {
            InitializeComponent();
            tSDataGeneralRepository = new TSDataGeneralRepository();
            tSHeaderRepository = new TSHeaderRepository();
            tSDetailRepository = new TSDetailRepository();
            inventoryDetailRepository = new InventoryDetailRepository();
            inventoryRepository = new InventoryRepository();
            branchRepository = new BranchRepository();
            warehouseRepository = new WarehouseRepository();
            tSDataGenerals = new List<TSDataGeneral>();

            popupMenuSelectBranch();
            popupMenuSelectWarehouse();
            loadProcesseData();
        }

        private async void popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 1;
        }

        private async void popupMenuSelectWarehouse()
        {
            List<Warehouse> warehouses = await warehouseRepository.getAll();
            foreach (Warehouse warehouse in warehouses)
            {
                comboBoxEditWarehouse.Properties.Items.Add(warehouse.WarehouseName);
            }
            comboBoxEditWarehouse.SelectedIndex = 1;
        }



        private void windowsUIButtonPanel1_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            String tag = e.Button.Properties.Tag.ToString();
            switch (tag)
            {
                
                case "refesh":
                    loadProcesseData();
                    break;
                case "collect":
                    collectData();
                    break;
                case "process":
                    ProcessData();
                    break;
                case "close":
                    break;
            }
        }

        private async void collectData()
        {
            List<TSHeader> tSHeaders = await tSHeaderRepository.GetByTSDate(dateEditTallyDate.DateTime);
            if (tSHeaders.Count == 0)
            {
                MessageBox.Show("Không có dữ liệu kiểm kê trong ngày " + dateEditTallyDate.Text);
                return;
            }


            int i = 0;
            foreach (TSHeader tSHeader in tSHeaders) {
                List<TSDetail> tSDetails = await tSDetailRepository.GetUnderID(tSHeader.TSNumber);
                foreach(TSDetail tSDetail in tSDetails)
                {
                    TSDataGeneral tSDataGeneral = new TSDataGeneral();
                    tSDataGeneral.TallyDate = tSHeader.TSDate;
                    tSDataGeneral.WarehouseID = tSHeader.WarehouseID;
                    tSDataGeneral.WarehouseName = tSHeader.WarehouseName;
                    tSDataGeneral.BranchID = tSHeader.BranchID;
                    tSDataGeneral.Ordinal = i + 1;
                    tSDataGeneral.PartNumber = tSDetail.PartNumber;
                    tSDataGeneral.PartName = tSDetail.PartName;
                    tSDataGeneral.StockUnitID = tSDetail.StockUnitID;
                    tSDataGeneral.TallyQuantity = tSDetail.Quantity;
                    tSDataGeneral.LedgerQuantity =null;
                    tSDataGeneral.DifferenceQuantity =null;
                    tSDataGeneral.Status = "0";
                    tSDataGenerals.Add(tSDataGeneral);
                    i++;
                }
            }

            loadCollecteData(tSDataGenerals);
        }

        private void loadCollecteData(List<TSDataGeneral> tSDataGenerals)
        {
            gridControlData.DataSource = tSDataGenerals;
        }

        private async void ProcessData()
        {
            List<TSDataGeneral> tSDataGeneralsProcessed = new List<TSDataGeneral>();
            if (tSDataGenerals.Count == 0)
            {
                MessageBox.Show("Tổng hợp dữ liệu trước !!!!");
                return;
            }
            foreach (TSDataGeneral tSDataGeneral in tSDataGenerals)
            {
                List<Inventory> inventories = await inventoryRepository.GetByPartNumber("1",tSDataGeneral.PartNumber);
                if (inventories.Count > 0)
                {
                    tSDataGeneral.LedgerQuantity = inventories[0].ClosingStockQuantity;
                    tSDataGeneral.DifferenceQuantity = tSDataGeneral.LedgerQuantity - tSDataGeneral.TallyQuantity;
                }

                List<TSDataGeneral> tSDataGeneralsCheckExist = await tSDataGeneralRepository.GetUnderID("1", dateEditTallyDate.DateTime);
                if (tSDataGeneralsCheckExist.Count > 0) {
                    await tSDataGeneralRepository.Update(tSDataGeneral, "1", tSDataGeneral.PartNumber, tSDataGeneral.Ordinal, (DateTime)tSDataGeneral.TallyDate);
                }
                else
                {
                    await tSDataGeneralRepository.Create(tSDataGeneral);
                }
                
                tSDataGeneralsProcessed.Add(tSDataGeneral);
            }

            MessageBox.Show("Đã xử lý xong dữ liệu kiểm kê ngày " + dateEditTallyDate.Text);
            gridControlData.DataSource = tSDataGeneralsProcessed;
        }

        private async void loadProcesseData()
        {
            List<TSDataGeneral> tSDataGenerals = await tSDataGeneralRepository.GetUnderID("1", dateEditTallyDate.DateTime);
            gridControlData.DataSource = tSDataGenerals;
        }
    }
}
