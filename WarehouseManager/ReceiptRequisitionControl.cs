﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraLayout.Filtering.Templates;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;
using LicenseContext = OfficeOpenXml.LicenseContext;
using DevExpress.XtraEditors.Repository;
using System.Globalization;
using DevExpress.XtraTab;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using System.Diagnostics;

namespace WarehouseManager
{
    public partial class ReceiptRequisitionControl : DevExpress.XtraEditors.XtraUserControl
    {
        WRRDetailRepository WRRDetailRepository;
        WRRHeaderRepository wRRHeaderRepository;
        HandlingStatusRepository handlingStatusRepository;
        BranchRepository branchRepository;
        WRDataHeaderRepository wRDataHeaderRepository;
        WRDataGeneralRepository wRDataGeneralRepository;
        GoodsRepository goodsRepository;
        InventoryDetailRepository inventoryDetailRepository = null;
        public static string SelectedTable = string.Empty;
        public static String selectedPartNumber;
        public static String selectedGoodsName;
        public static String selectedWRRNumber;
        public static String selectedReferenceNumber;
        public static DateTime selectedWRRDate;
        public static String selectedNote;
        public static String selectedBranchID;
        public static String selectedBranchName;
        public static String selectedHandlingID;
        public static String selectedHandlingName;
        public static String selectedGoodsID;

        String lastWRRNumber = "";
        Boolean isWRRNumberExist = false;


        AutoCompleteStringCollection dataPalletID = new AutoCompleteStringCollection();

        Int16 SaveNewRight = 1;
        Int16 SaveChangeRight = 1;
        int findIndex = WMMessage.UserRightOnScreens.FindIndex(x => x.ScreenID.Equals("frmReceiptRequisition"));

        public ReceiptRequisitionControl()
        {
            InitializeComponent();
            this.Load += ReceiptRequisitionControl_Load;



            WRRDetailRepository = new WRRDetailRepository();
            wRRHeaderRepository = new WRRHeaderRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wRDataHeaderRepository = new WRDataHeaderRepository();
            wRDataGeneralRepository = new WRDataGeneralRepository();
            goodsRepository = new GoodsRepository();
            inventoryDetailRepository = new InventoryDetailRepository();
        }

        private async void ReceiptRequisitionControl_Load(Object sender, EventArgs e)
        {
            textEditWRRNumber.Focus();
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            popupMenuSelectHandlingStatus();
            popupMenuSelectBranch();
            popupMenuSelectUnits();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            gridViewWRRDetail.CustomColumnDisplayText += wRRHeaderGridView_CustomColumnDisplayText;
            gridViewWRRDetail.CellValueChanged += YourDGV_CellValueChanged;

            gridViewWRRDetail.ValidatingEditor += gridView1_ValidatingEditor;
            textEditWRRNumber.DoubleClick += textEditWRRNumer_CellDoubleClick;

            gridViewWRRDetail.ClearSorting();
            sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);

            autoCompletePartNumberAsync();


            textEditPartNumber.KeyDown += textEditPartNumber_KeyDown;
            textEditPalletID.KeyDown += textEditPalletID_KeyDown;
            textEditQuantity.KeyDown += textEditQuantity_KeyDown;
            textEditQuantityByPack.KeyDown += textEditQuantityByPack_KeyDown;
            //dateEditMFGDate.MouseLeave += dateEditMFG_MouseLeave;

            checkRightOnScreen();
        }

        private void checkRightOnScreen()
        {
            
            if (WMMessage.UserRightOnScreens[findIndex].SaveNewRight==0&& WMMessage.UserRightOnScreens[findIndex].SaveChangeRight==0)
            {
                SaveNewRight = 0;
                SaveChangeRight = 0;
                ((WindowsUIButton)windowsUIButtonPanel1.Buttons[2]).Enabled = false;
            }
            else if (WMMessage.UserRightOnScreens[findIndex].SaveNewRight == 0 && WMMessage.UserRightOnScreens[findIndex].SaveChangeRight == 1)
            {
                SaveNewRight = 0;
                SaveChangeRight = 1;
            }
            else if (WMMessage.UserRightOnScreens[findIndex].SaveNewRight == 1&& WMMessage.UserRightOnScreens[findIndex].SaveChangeRight == 0)
            {
                SaveChangeRight = 0;
                SaveNewRight = 1;
            }

            if (WMMessage.UserRightOnScreens[findIndex].DeleteRight == 0)
            {
                ((WindowsUIButton)windowsUIButtonPanel1.Buttons[4]).Enabled = false;
            }
            if (WMMessage.UserRightOnScreens[findIndex].ImportRight == 0)
            {
                ((WindowsUIButton)windowsUIButtonPanel1.Buttons[10]).Enabled = false;
            }
            if (WMMessage.UserRightOnScreens[findIndex].PrintRight == 0)
            {
                ((WindowsUIButton)windowsUIButtonPanel1.Buttons[14]).Enabled = false;
            }

            if (WMMessage.User.AdjustWRR == 0)
            {
                simpleButtonSave.Enabled = false;
                simpleButtonDelete.Enabled = false;
                simpleButtonClear.Enabled = false;
            }
        }

        //private void checkRight()
        //{
        //    int findIndexAdjustWRR = WMMessage.UserRights.FindIndex(x => x.RightID.Equals("AdjustWRR"));
        //    int findIndexApproveWRRLevel1 = WMMessage.UserRights.FindIndex(x => x.RightID.Equals("ApproveWRRLevel1"));
        //    int findIndexApproveWRRLevel2 = WMMessage.UserRights.FindIndex(x => x.RightID.Equals("ApproveWRRLevel2"));
        //    int findIndexCancelWRR = WMMessage.UserRights.FindIndex(x => x.RightID.Equals("CancelWRR"));
        //    if (WMMessage.UserRights[findIndexAdjustWRR].GrantRight == 0)
        //    {
        //        AdjustWRR = 0;
        //    }
        //    if (WMMessage.UserRights[findIndexApproveWRRLevel1].GrantRight == 0)
        //    {
        //        ApproveWRRLevel1= 0;
        //    }
        //    if (WMMessage.UserRights[findIndexApproveWRRLevel2].GrantRight == 0)
        //    {
        //        ApproveWRRLevel2 = 0;
        //    }
        //    if (WMMessage.UserRights[findIndexCancelWRR].GrantRight == 0)
        //    {
        //        CancelWRR = 0;
        //    }
        //}

        private void popupMenuSelectUnits()
        {
            List<string> list = Helpers.Constant.units;
            foreach (string s in list)
            {
                comboBoxEditUnit.Properties.Items.Add(s);
            }
            comboBoxEditUnit.SelectedIndex = 0;
        }

        private void popupMenuSelectHandlingStatus()
        {
            List<string> list = Helpers.Constant.handlingStatuss;
            foreach (string s in list)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(s);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private void popupMenuSelectBranch()
        {
            List<string> list = Helpers.Constant.branchs;
            foreach (string s in list)
            {
                comboBoxEditBranch.Properties.Items.Add(s);
            }
            comboBoxEditBranch.SelectedIndex = 1;
        }

        private async void autoCompletePartNumberAsync()
        {
            AutoCompleteStringCollection data = new AutoCompleteStringCollection();
            List<Goods> goodss = await goodsRepository.getAll();
            foreach (Goods goods in goodss)
            {
                data.Add(goods.PartNumber);
            }
            textEditPartNumber.MaskBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            textEditPartNumber.MaskBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            textEditPartNumber.MaskBox.AutoCompleteCustomSource = data;
        }

        private void autoCompletePalletIDAsync()
        {

            textEditPalletID.MaskBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            textEditPalletID.MaskBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            textEditPalletID.MaskBox.AutoCompleteCustomSource = dataPalletID;
        }

        private async void textEditPartNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dataPalletID.Clear();

                List<Goods> goodss = await goodsRepository.GetUnderPartNumber(textEditPartNumber.Text);
                if (goodss.Count == 0)
                {
                    MessageBox.Show("Part Number không tồn tại trong hệ thống");
                    textEditPartNumber.Text = "";
                    textEditPartNumber.Focus();
                    return;
                }

                foreach (Goods goods in goodss)
                {
                    dataPalletID.Add(goods.PalletID);
                }
                textEditPalletID.Text = goodss[0].PalletID;
                List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams5(2022, 6, "1", textEditPartNumber.Text, textEditPalletID.Text);
                if (inventoryDetails.Count > 0)
                {
                    MessageBox.Show("Pallet "+textEditPalletID.Text+" ko trống,ko thể chọn");
                    textEditPartNumber.Text = "";
                    textEditPalletID.Text = "";
                    textEditPartName.Text = "";
                    textEditProductFamily.Text = "";
                    return;
                }
                

                textEditPartName.Text = goodss[0].PartName;
                comboBoxEditUnit.Text = goodss[0].Unit;
                textEditProductFamily.Text = goodss[0].ProductFamily;
                autoCompletePalletIDAsync();

                textEditLotID.Focus();
            }
        }

        private async void textEditPalletID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Goods goods = await goodsRepository.GetUnderID(textEditPartNumber.Text.ToString(), textEditPalletID.Text.ToString());

                textEditPartName.Text = goods.PartName;
                comboBoxEditUnit.Text = goods.Unit;
                textEditProductFamily.Text = goods.ProductFamily;
            }
        }

        private async void textEditQuantity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!string.IsNullOrEmpty(textEditQuantity.Text))
                {
                    List<Goods> goodss = await goodsRepository.getAll();
                    foreach (Goods goods in goodss)
                    {
                        if (goods.PalletID.Equals(textEditPalletID.Text))
                        {
                            textEditQuantityByPack.Text = (decimal.Parse(textEditQuantity.Text) / goods.Quantity).ToString();
                            break;
                        }
                    }
                }
            }
        }

        private async void textEditQuantityByPack_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!string.IsNullOrEmpty(textEditQuantityByPack.Text))
                {
                    List<Goods> goodss = await goodsRepository.getAll();
                    foreach (Goods goods in goodss)
                    {
                        if (goods.PalletID.Equals(textEditPalletID.Text))
                        {
                            textEditQuantity.Text = (decimal.Parse(textEditQuantityByPack.Text)* goods.Quantity).ToString();
                            break;
                        }
                    }
                }
            }
        }




        private void YourDGV_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName.Equals("QuantityByItem"))
            {
                getTotalQuantityByItem();
            }
            if (e.Column.FieldName.Equals("QuantityByPack"))
            {
                getTotalQuantityByPack();
            }
        }

        private void gridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = sender as GridView;

            object val = e.Value;

            if (view.FocusedColumn.FieldName == "PartNumber")
            {
                if (val == null || string.IsNullOrWhiteSpace(e.Value.ToString()))
                {
                    e.Valid = false;
                    e.ErrorText = "Bạn nhập mã hàng null";
                }
                if (checkExistPartNumber(e.Value.ToString()) == true)
                {

                    e.Valid = false;
                    e.ErrorText = "Mã hàng hóa trùng";
                }
            }
        }

        private Boolean checkExistPartNumber(String PartNumber)
        {
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                String PartNumber1 = (String)gridViewWRRDetail.GetRowCellValue(rowHandle, "PartNumber");
                if (PartNumber.Equals(PartNumber1))
                {
                    return true;
                }
                i++;
            }
            return false;
        }

        private Boolean checkRowBeforeNull()
        {
            int rowHandle = gridViewWRRDetail.FocusedRowHandle;
            String PartNumber1 = (String)gridViewWRRDetail.GetRowCellValue(rowHandle - 1, "PartNumber");
            if (rowHandle > 0 && PartNumber1 == null)
            {
                return true;
            }
            return false;
        }

        private void gridView1_ShownEditor(object sender, EventArgs e)
        {
            GridView view = sender as GridView;

            view.ActiveEditor.IsModified = true;
        }

        private void getTotalQuantityByItem()
        {
            Decimal totalQuantityByItem = 0;
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                if (gridViewWRRDetail.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantityByItem = (Decimal)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                totalQuantityByItem += quantityByItem;
                i++;
            }
            textEditTotalQuantityByItem.Text = totalQuantityByItem.ToString();
        }

        private void getTotalQuantityByPack()
        {
            Decimal totalQuantityByPack = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                if (gridViewWRRDetail.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantityByPack = (Decimal)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                totalQuantityByPack += quantityByPack;
                i++;
            }
            textEditTotalQuantityByPack.Text = totalQuantityByPack.ToString();
        }

        private void wRRHeaderGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == collNo)
            {
                int rowHandle = gridViewWRRDetail.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async void sbLoadDataForGridWRRDetailAsync(String wRRNumber)
        {
            List<WRRDetail> wRRDetails = await WRRDetailRepository.GetUnderID(wRRNumber);
            int mMaxRow = 100;
            if (wRRDetails.Count < mMaxRow)
            {
                int num = mMaxRow - wRRDetails.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    wRRDetails.Add(new WRRDetail());
                    i++;
                }
            }
            gridControlWRRDetail.DataSource = wRRDetails;
            getTotalQuantityByItem();
            getTotalQuantityByPack();
        }

        public async void getlastWRRNumberAsync()
        {
            lastWRRNumber = await wRRHeaderRepository.GetLastWRRNumber();
            String wRRNumberYear = DateTime.Now.Year.ToString().Substring(2, 2);
            if (lastWRRNumber == "")
            {
                textEditWRRNumber.Text = wRRNumberYear + "000" + "-" + $"{1:D5}";
            }
            else
            {
                string[] arrListStr = lastWRRNumber.Split('-');
                String wRRNumberPartOne = arrListStr[0];
                String wRRNumberPartTwo = arrListStr[1];
                wRRNumberPartTwo = (int.Parse(wRRNumberPartTwo) + 1).ToString();
                textEditWRRNumber.Text = wRRNumberYear + "000" + "-" + $"{int.Parse(wRRNumberPartTwo):D5}";
            }


        }

        public async void createWRRHeaderAsync()
        {
            WRRHeader wRRHeader = new WRRHeader();
            wRRHeader.WRRNumber = textEditWRRNumber.Text;
            wRRHeader.WRRDate = DateTime.Parse(dateEditWRRDate.DateTime.ToString("yyyy-MM-dd"));
            wRRHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex).ToString();
            wRRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wRRHeader.BranchID = (comboBoxEditBranch.SelectedIndex).ToString();
            wRRHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            wRRHeader.Note ="";
            wRRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wRRHeader.CreatedUserID = WMMessage.User.UserID;
            wRRHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            wRRHeader.Status = "1";

            if (await wRRHeaderRepository.Create(wRRHeader) > 0)
            {
                await createWRRDetailAsync();
                WMPublic.sbMessageSaveNewSuccess(this);
            }
        }

        public async Task createWRRDetailAsync()
        {
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                WRRDetail wRRDetail = new WRRDetail();
                wRRDetail.WRRNumber = textEditWRRNumber.Text;
                wRRDetail.PartNumber = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "PartNumber");
                wRRDetail.PartName = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "PartName");
                wRRDetail.Ordinal = i + 1;
                wRRDetail.TotalQuantity = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "TotalQuantity");
                wRRDetail.QuantityByItem = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                wRRDetail.QuantityByPack = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                wRRDetail.PackingVolume = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "PackingVolume");
                wRRDetail.LotID = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "LotID");
                wRRDetail.PalletID = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "PalletID");
                wRRDetail.MFGDate = (DateTime?)gridViewWRRDetail.GetRowCellValue(rowHandle, "MFGDate");
                wRRDetail.EXPDate = (DateTime?)gridViewWRRDetail.GetRowCellValue(rowHandle, "EXPDate");
                wRRDetail.Remark = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Remark");
                wRRDetail.Unit = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Unit");
                wRRDetail.PONumber = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "PONumber");
                wRRDetail.Status = "0";
                wRRDetail.ProductFamily = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "ProductFamily");
                wRRDetail.QuantityByPrinted = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByPrinted");
                wRRDetail.RemainingPrintQuantity = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "RemainingPrintQuantity");

                if (await WRRDetailRepository.Create(wRRDetail) > 0)
                {
                    i++;
                }
            }
        }

        public async void createWRDataHeaderAsync()
        {
            WRRHeader wRRHeader = await wRRHeaderRepository.GetUnderID(textEditWRRNumber.Text);
            WRDataHeader wRDataHeader = new WRDataHeader();
            wRDataHeader.WRDNumber = textEditWRRNumber.Text;
            wRDataHeader.WRDDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            wRDataHeader.WRRNumber = textEditWRRNumber.Text;
            wRDataHeader.BranchID = wRRHeader.BranchID;
            wRDataHeader.BranchName = wRRHeader.BranchName;
            wRDataHeader.HandlingStatusID = "1";
            wRDataHeader.HandlingStatusName = "Chưa duyệt";
            wRDataHeader.Note = "";
            wRDataHeader.Status = "0";
            wRDataHeader.TotalQuantity = 0m;
            wRDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wRDataHeader.CreatedUserID = WMMessage.User.UserID;
            wRDataHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wRDataHeaderRepository.Create(wRDataHeader) > 0)
            {
                createWRDataGeneralAsync();
                MessageBox.Show("Đã tạo dữ liệu nhập");
            }
        }

        public async void createWRDataGeneralAsync()
        {
            int num = gridViewWRRDetail.RowCount - 1;
            for (int i = 0; i < gridViewWRRDetail.RowCount - 1; i++)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                WRDataGeneral wRDataGeneral = new WRDataGeneral();
                wRDataGeneral.WRDNumber = textEditWRRNumber.Text;
                wRDataGeneral.PartNumber = (string)gridViewWRRDetail.GetRowCellValue(i, "PartNumber");
                wRDataGeneral.PartName = (string)gridViewWRRDetail.GetRowCellValue(i, "PartName");
                wRDataGeneral.Ordinal = i + 1;
                wRDataGeneral.Quantity = 0m;
                wRDataGeneral.TotalQuantity = 0m;
                wRDataGeneral.TotalGoods = 0m;
                wRDataGeneral.PackingQuantity = 0m;
                wRDataGeneral.QuantityOrg = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByItem");
                wRDataGeneral.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);

                List<WRRDetail> wRRDetails = await WRRDetailRepository.GetUnderID(textEditWRRNumber.Text);
                wRDataGeneral.TotalGoodsOrg = Convert.ToDecimal(wRRDetails.Count);

                wRDataGeneral.Status = "0";
                wRDataGeneral.PackingVolume = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "PackingVolume");
                wRDataGeneral.QuantityByPack = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByPack");
                wRDataGeneral.QuantityByItem = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByItem");
                wRDataGeneral.LotID = (string)gridViewWRRDetail.GetRowCellValue(i, "LotID");
                wRDataGeneral.PalletID = (string)gridViewWRRDetail.GetRowCellValue(i, "PalletID");
                wRDataGeneral.PONumber = (string)gridViewWRRDetail.GetRowCellValue(i, "PONumber");
                wRDataGeneral.ProductFamily = (string)gridViewWRRDetail.GetRowCellValue(i, "ProductFamily");
               
                wRDataGeneral.IDCode = "";

                if (((DateTime?)gridViewWRRDetail.GetRowCellValue(i, "MFGDate")).ToString() != "")
                {
                    wRDataGeneral.MFGDate = (DateTime?)gridViewWRRDetail.GetRowCellValue(i, "MFGDate");
                }
                if (((DateTime?)gridViewWRRDetail.GetRowCellValue(i, "EXPDate")).ToString() != "")
                {
                    wRDataGeneral.EXPDate = (DateTime?)gridViewWRRDetail.GetRowCellValue(i, "EXPDate");
                }

                wRDataGeneral.Remark = (string)gridViewWRRDetail.GetRowCellValue(i, "Remark");
                await wRDataGeneralRepository.Create(wRDataGeneral);
            }
        }


        public async void updateWRRHeaderAsync()
        {
            WRRHeader wRRHeader = await wRRHeaderRepository.GetUnderID(textEditWRRNumber.Text);
            wRRHeader.WRRNumber = textEditWRRNumber.Text;
            wRRHeader.WRRDate = DateTime.Parse(dateEditWRRDate.DateTime.ToString("yyyy-MM-dd"));
            wRRHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex).ToString();
            wRRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wRRHeader.BranchID = (comboBoxEditBranch.SelectedIndex).ToString();
            wRRHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            wRRHeader.Note = "";
            wRRHeader.Status = "1";
            wRRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wRRHeader.UpdatedUserID = WMMessage.User.UserID;
            wRRHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (comboBoxEditHandlingStatus.SelectedIndex ==2)
            {
                if (WMMessage.User.ApproveWRRLevel1 == 0)
                {
                    MessageBox.Show("Không có quyền duyệt mức 1 yêu cầu nhập");
                    return;
                }
            }
            if (comboBoxEditHandlingStatus.SelectedIndex ==3)
            {
                if (WMMessage.User.ApproveWRRLevel2== 0)
                {
                    MessageBox.Show("Không có quyền duyệt mức 2 yêu cầu nhập");
                    return;
                }
            }

            if (comboBoxEditHandlingStatus.SelectedIndex ==4)
            {
                if (WMMessage.User.CancelWRR == 0)
                {
                    MessageBox.Show("Không có quyền hủy phiếu yêu cầu nhập");
                    return;
                }
            }

            if (await wRRHeaderRepository.Update(wRRHeader, textEditWRRNumber.Text) > 0)
            {
                updateWRRDetailAsync();
                WMPublic.sbMessageSaveChangeSuccess(this);
                if (comboBoxEditHandlingStatus.SelectedIndex == 3)
                {
                    createWRDataHeaderAsync();
                }
            }

        }

        public async void updateWRRDetailAsync()
        {
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                WRRDetail wRRDetail = new WRRDetail();
                wRRDetail.WRRNumber = textEditWRRNumber.Text;
                wRRDetail.PartNumber = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "PartNumber");
                wRRDetail.PartName = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "PartName");
                wRRDetail.PackingVolume = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "PackingVolume");
                wRRDetail.TotalQuantity = decimal.Parse(textEditTotalQuantityByItem.Text);
                wRRDetail.QuantityByItem = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                wRRDetail.QuantityByPack = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                wRRDetail.LotID = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "LotID");
                wRRDetail.PalletID = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "PalletID");
                wRRDetail.MFGDate = (DateTime?)gridViewWRRDetail.GetRowCellValue(rowHandle, "MFGDate");
                wRRDetail.EXPDate = (DateTime?)gridViewWRRDetail.GetRowCellValue(rowHandle, "EXPDate");
                wRRDetail.Remark = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Remark");
                wRRDetail.PONumber = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "PONumber");
                wRRDetail.Unit = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "Unit");
                wRRDetail.ProductFamily = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "ProductFamily");
                wRRDetail.Status = "1";
                wRRDetail.QuantityByPrinted = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByPrinted");
                wRRDetail.IDCodeRange = (string)gridViewWRRDetail.GetRowCellValue(rowHandle, "IDCodeRange");
                wRRDetail.RemainingPrintQuantity = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "RemainingPrintQuantity");
                List<WRRDetail> list = await WRRDetailRepository.GetByWRRNumberAndPalletID(textEditWRRNumber.Text, wRRDetail.PalletID);

                if (list.Count > 0)
                {
                    wRRDetail.Ordinal = i + 1;
                    if (await WRRDetailRepository.Update(wRRDetail, textEditWRRNumber.Text, wRRDetail.PalletID, wRRDetail.Ordinal, wRRDetail.LotID) > 0)
                    {
                        i++;
                    }

                }
                else
                {
                    wRRDetail.Ordinal = i + 1;
                    if (await WRRDetailRepository.Create(wRRDetail) > 0)
                    {
                        i++;
                    }
                }

            }
        }

        public async void DeleteWRRHeaderAsync()
        {
            if ((await wRRHeaderRepository.Delete(textEditWRRNumber.Text)) > 0)
            {
                await DeleteWRRDetailAsync();
                await DeleteWRDataHeaderAsync();
                sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);
                WMPublic.sbMessageDeleteSuccess(this);
                textEditWRRNumber.Text = "";
                comboBoxEditHandlingStatus.SelectedIndex = 0;
                comboBoxEditBranch.SelectedIndex = 0;
                dateEditWRRDate.DateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            }
        }

        public async Task DeleteWRRDetailAsync()
        {
            await WRRDetailRepository.Delete(textEditWRRNumber.Text);
            clearAsync();
            gridControlWRRDetail.DataSource = null;
        }

        public async Task DeleteWRDataHeaderAsync()
        {
            if (await wRDataHeaderRepository.GetUnderID(textEditWRRNumber.Text) != null){
                if ((await wRDataHeaderRepository.Delete(textEditWRRNumber.Text)) > 0)
                {
                    await DeleteWRDataGeneralAsync();
                }
            }
        }

        public async Task DeleteWRDataGeneralAsync()
        {
            await wRDataGeneralRepository.Delete(textEditWRRNumber.Text);
        }


        private void clearAsync()
        {
            dateEditWRRDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            textEditTotalQuantityByItem.Text = "0";
            textEditTotalQuantityByPack.Text = "0";
            textEditPalletID.Text = "";
            textEditPartNumber.Text = "";
            textEditPartName.Text = "";
            textEditProductFamily.Text = "";
            textEditLotID.Text = "";
            comboBoxEditUnit.SelectedIndex = 0;
            textEditQuantity.Text = "";
            textEditPONumber.Text = "";
            dateEditMFGDate.Text = "";
            dateEditEXPDate.Text = "";
            List<WRRDetail> wRRDetails = new List<WRRDetail>();

            for (int i = 0; i < 100; i++)
            {
                wRRDetails.Add(new WRRDetail());
            }
            gridControlWRRDetail.DataSource = wRRDetails;
        }

        private async Task  checkExistWRRNumber(string wRRNumber)
        {
            if (!wRRNumber.Equals("") && (await wRRHeaderRepository.checkExistAsync(wRRNumber)) == true)
            {
                isWRRNumberExist = true;
            }
        }


        private async void textEditWRRNumber_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {

                frmSearchWRR frmSearchWRR = new frmSearchWRR();
                frmSearchWRR.ShowDialog(this);
                frmSearchWRR.Dispose();
                if (selectedWRRNumber != null)
                {
                    textEditWRRNumber.Text = selectedWRRNumber;
                    dateEditWRRDate.Text = selectedWRRDate.ToString("dd/MM/yyyy");
                    comboBoxEditBranch.SelectedIndex = int.Parse(selectedBranchID);
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(selectedHandlingID);
                    sbLoadDataForGridWRRDetailAsync(selectedWRRNumber);
                }
                await checkExistWRRNumber(textEditWRRNumber.Text);
            }
        }



        private async void textEditWRRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchWRR frmSearchWRR = new frmSearchWRR();
            frmSearchWRR.ShowDialog(this);
            frmSearchWRR.Dispose();
            if (selectedWRRNumber != null)
            {
                textEditWRRNumber.Text = selectedWRRNumber;
                dateEditWRRDate.Text = selectedWRRDate.ToString("dd/MM/yyyy");
                comboBoxEditBranch.SelectedIndex = int.Parse(selectedBranchID);
                comboBoxEditHandlingStatus.SelectedIndex = int.Parse(selectedHandlingID);
                sbLoadDataForGridWRRDetailAsync(selectedWRRNumber);
            }
            await checkExistWRRNumber(textEditWRRNumber.Text);
        }

        private async void selectFileExcelAsync()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"d:\";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                List<WRRDetail> wRRDetails = await ExcelToListAsync(fdlg.FileName);
                int mMaxRow = 100;
                if (wRRDetails.Count < mMaxRow)
                {
                    int num = mMaxRow - wRRDetails.Count;
                    int i = 1;
                    while (true)
                    {
                        int num2 = i;
                        int num3 = num;
                        if (num2 > num3)
                        {
                            break;
                        }
                        wRRDetails.Add(new WRRDetail());
                        i++;
                    }
                }
                gridControlWRRDetail.DataSource = wRRDetails;
                getTotalQuantityByItem();
                getTotalQuantityByPack();
                Application.DoEvents();
            }
        }


        private async Task<List<WRRDetail>> ExcelToListAsync(String fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                dateEditWRRDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

                int totalRows = workSheet.Dimension.Rows;
                List<WRRDetail> wRRDetails = new List<WRRDetail>();
                for (int i = 6; i <= totalRows + 4; i++)
                {
                    int firstDotMFGDate = workSheet.Cells[i, 9].Text.Trim().IndexOf("/");
                    int secondDotMFGDate = workSheet.Cells[i, 9].Text.Trim().LastIndexOf("/");
                    String dateMFGDate = workSheet.Cells[i, 9].Text.Trim().Substring((firstDotMFGDate + 1), 2);
                    String monthMFGDate = workSheet.Cells[i, 9].Text.Trim().Substring(0, 2);
                    String yearMFGDate = workSheet.Cells[i, 9].Text.Trim().Substring((secondDotMFGDate + 1), 2);


                    int firstDotInputDate = workSheet.Cells[i, 11].Text.Trim().IndexOf("/");
                    int secondDotInputDate = workSheet.Cells[i, 11].Text.Trim().LastIndexOf("/");
                    String dateInputDate = workSheet.Cells[i, 11].Text.Trim().Substring((firstDotInputDate + 1), 2);
                    String monthInputDate = workSheet.Cells[i, 11].Text.Trim().Substring(0, 2);
                    String yearInputDate = workSheet.Cells[i, 11].Text.Trim().Substring((secondDotInputDate + 1), 2);

                    wRRDetails.Add(new WRRDetail
                    {
                        PalletID = workSheet?.Cells[i, 2].Text.Trim(),
                        PartNumber = workSheet?.Cells[i, 3].Text.Trim(),
                        PartName = workSheet?.Cells[i, 4].Text.Trim(),
                        ProductFamily = workSheet?.Cells[i, 5].Text.Trim(),
                        LotID = workSheet?.Cells[i, 6].Text.Trim(),
                        QuantityByItem = Decimal.Parse(workSheet?.Cells[i, 7].Text.Trim()),
                        //QuantityByPack = Decimal.Parse(workSheet?.Cells[i, 8].Text.Trim()),
                        TotalQuantity = Decimal.Parse(workSheet?.Cells[i, 7].Text.Trim()),
                        //PackingVolume = Math.Floor(Decimal.Parse(workSheet?.Cells[i, 7].Text.Trim()) / Decimal.Parse(workSheet?.Cells[i, 8].Text.Trim())),
                        Unit = workSheet?.Cells[i, 8].Text.Trim(),
                        MFGDate = DateTime.Parse(dateMFGDate + "/" + monthMFGDate + "/" + yearMFGDate),
                        EXPDate = (DateTime.Parse(dateMFGDate + "/" + monthMFGDate + "/" + yearMFGDate)).AddYears(2).AddDays(-1),
                        PONumber = workSheet?.Cells[i, 13].Text.Trim(),
                        Remark = workSheet?.Cells[i, 14].Text.Trim(),
                        QuantityByPrinted = 0m
                    });
                  
                }
                List<Goods> goodss = await goodsRepository.getAll();
                for (int i = 0; i < wRRDetails.Count; i++)
                {
                    foreach (Goods goods in goodss)
                    {
                        if (goods.PartNumber.Equals(wRRDetails[i].PartNumber))
                        {
                            wRRDetails[i].PackingVolume = goods.Quantity;
                            wRRDetails[i].QuantityByPack = wRRDetails[i].QuantityByItem / wRRDetails[i].PackingVolume;
                            wRRDetails[i].RemainingPrintQuantity = wRRDetails[i].QuantityByItem / wRRDetails[i].PackingVolume;
                        }
                    }
                }
                return wRRDetails;
            }
        }


        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        getlastWRRNumberAsync();
                        clearAsync();
                    }
                    break;
                case "save":
                    await checkExistWRRNumber(textEditWRRNumber.Text);

                    if (isWRRNumberExist == true)
                    {
                        if (SaveChangeRight == 0)
                        {
                            MessageBox.Show("Không có quyền chỉnh sửa yêu cầu nhập");
                            return;
                        }
                        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                        {
                            updateWRRHeaderAsync();
                        }
                    }
                    else
                    {
                        if (SaveNewRight == 0)
                        {
                            MessageBox.Show("Không có quyền tạo yêu cầu nhập");
                            return;
                        }
                        if (WMPublic.sbMessageSaveNewRequest(this) == true)
                        {
                            getlastWRRNumberAsync();
                            createWRRHeaderAsync();
                        }
                    }
                    break;
                case "delete":
                    if (WMMessage.UserRightOnScreens[findIndex].DeleteRight == 0)
                    {
                        MessageBox.Show("Không có quyền xóa yêu cầu nhập");
                        return;
                    }
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {

                        DeleteWRRHeaderAsync();
                    }
                    break;
                case "search":
                    break;
                case "refesh":
                    sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);
                    break;
                case "import":
                    if (WMMessage.UserRightOnScreens[findIndex].ImportRight == 0)
                    {
                        MessageBox.Show("Không có quyền nhập dữ liệu");
                        return;
                    }
                    try
                    {
                        Application.UseWaitCursor = true;
                        selectFileExcelAsync();
                    }
                    finally
                    {
                        MessageBox.Show("Tải thành công file excel");
                        Application.UseWaitCursor = false;
                    }
                    break;
                case "export":
                    String mCurrentDateTime = DateTime.Now.ToString("MMddyyyy");
                    gridViewWRRDetail.BeginUpdate();
                    gridViewWRRDetail.Columns["No"].Visible = false;
                    string path = "d:\\tokudai\\"+ String.Format(mCurrentDateTime, "MMddyyyy")+".xlsx";
                    ExportSettings.DefaultExportType = ExportType.DataAware;
                    XlsxExportOptionsEx advOptions = new XlsxExportOptionsEx();
                    advOptions.AllowGrouping = DevExpress.Utils.DefaultBoolean.False;
                    advOptions.ShowTotalSummaries = DevExpress.Utils.DefaultBoolean.False;
                    advOptions.SheetName = String.Format(mCurrentDateTime, "MMddyyyy");
                    gridControlWRRDetail.ExportToXlsx(path);
                    // Open the created XLSX file with the default application.
                    Process.Start(path);
                    gridViewWRRDetail.Columns["No"].Visible = true;
                    gridViewWRRDetail.EndUpdate();
                    break;
                case "print":
                    if (WMMessage.UserRightOnScreens[findIndex].PrintRight == 0)
                    {
                        MessageBox.Show("Không có quyền in yêu cầu nhập");
                        return;
                    }


                    //frmPrintLabel frmPrintLabel = new frmPrintLabel(this);
                    //frmPrintLabel.wrrNumber = textEditWRRNumber.Text;
                    //frmPrintLabel.ShowDialog(this);

                    frmExportPrintLabel frmExportPrintLabel = new frmExportPrintLabel();
                    frmExportPrintLabel.wrrNumber = textEditWRRNumber.Text;
                    frmExportPrintLabel.ShowDialog(this);

                    break;
                case "print_automation":

                    frmPrintLabel frmPrintLabel = new frmPrintLabel(this);
                    frmPrintLabel.wrrNumber = textEditWRRNumber.Text;
                    frmPrintLabel.ShowDialog(this);

                    break;
                case "close":
                    break;
            }

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {

            switch (keyData)
            {
                case Keys.F2:
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        getlastWRRNumberAsync();
                        clearAsync();
                    }
                    return true;
                case Keys.F3:
                    if (SaveChangeRight == 0&&SaveNewRight==0)
                    {
                        MessageBox.Show("Không có quyền tạo và chỉnh sửa yêu cầu nhập");
                        return false;
                    }
                    if (isWRRNumberExist == true)
                    {
                        if (SaveChangeRight == 0)
                        {
                            MessageBox.Show("Không có quyền chỉnh sửa yêu cầu nhập");
                            return false;
                        }
                        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                        {
                            updateWRRHeaderAsync();
                        }
                    }
                    else
                    {
                        if (SaveNewRight == 0)
                        {
                            MessageBox.Show("Không có quyền tạo yêu cầu nhập");
                            return false;
                        }
                        if (WMPublic.sbMessageSaveNewRequest(this) == true)
                        {
                            getlastWRRNumberAsync();
                            createWRRHeaderAsync();
                        }
                    }
                    return true;
                case Keys.F4:
                    if (WMMessage.UserRightOnScreens[findIndex].DeleteRight == 0)
                    {
                        MessageBox.Show("Không có quyền xóa yêu cầu nhập");
                        return false;
                    }
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {

                        DeleteWRRHeaderAsync();
                    }
                    return true;
                case Keys.F5:
                    sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);
                    return true;
                case Keys.F7:
                    if (WMMessage.UserRightOnScreens[findIndex].ImportRight == 0)
                    {
                        MessageBox.Show("Không có quyền nhập dữ liệu");
                        return false;
                    }
                    try
                    {
                        Application.UseWaitCursor = true;
                        selectFileExcelAsync();
                    }
                    finally
                    {
                        MessageBox.Show("Tải thành công file excel");
                        Application.UseWaitCursor = false;
                    }
                    return true;
                case Keys.F9:
                    if (WMMessage.UserRightOnScreens[findIndex].PrintRight == 0)
                    {
                        MessageBox.Show("Không có quyền in yêu cầu nhập");
                        return false;
                    }
                    //frmPrintLabel frmPrintLabel = new frmPrintLabel(this);

                    //frmPrintLabel.ShowDialog(this);
                    //frmPrintLabel.wrrNumber = textEditWRRNumber.Text;
                    frmExportPrintLabel frmExportPrintLabel = new frmExportPrintLabel();
                    frmExportPrintLabel.wrrNumber = textEditWRRNumber.Text;
                    frmExportPrintLabel.ShowDialog(this);
                    return true;
                case Keys.F10:
                    if (WMPublic.sbMessageCloseTab(this) == true)
                    {
                        XtraTabPage tabPage;
                        tabPage = (XtraTabPage)(this.Parent);
                        XtraTabControl tabControl;
                        tabControl = (XtraTabControl)tabPage.Parent;
                        tabControl.TabPages.Remove(tabPage);
                        tabPage.Dispose();
                    }
                    return true;
                case Keys.S | Keys.Control:
                    if (WMMessage.User.AdjustWRR == 0)
                    {
                        MessageBox.Show("Không có quyền điều chỉnh yêu cầu nhập");
                        return false;
                    }
                    createWRRHeaderFromForm();
                    return true;
                case Keys.D | Keys.Control:
                    if (WMMessage.User.AdjustWRR == 0)
                    {
                        MessageBox.Show("Không có quyền điều chỉnh yêu cầu nhập");
                        return false;
                    }
                    deleteWRRDetailFromForm();
                    return true;
                case Keys.R | Keys.Control:
                    if (WMMessage.User.AdjustWRR == 0)
                    {
                        MessageBox.Show("Không có quyền điều chỉnh yêu cầu nhập");
                        return false;
                    }
                    clearForm();
                    return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        public async void createWRRHeaderFromForm()
        {
            WRRHeader wRRHeader = new WRRHeader();
            wRRHeader.WRRNumber = textEditWRRNumber.Text;
            wRRHeader.WRRDate = DateTime.Parse(dateEditWRRDate.DateTime.ToString("yyyy-MM-dd"));
            wRRHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex).ToString();
            wRRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wRRHeader.BranchID = (comboBoxEditBranch.SelectedIndex).ToString();
            wRRHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            wRRHeader.Note = "";
            wRRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wRRHeader.CreatedUserID = WMMessage.User.UserID;
            wRRHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            wRRHeader.Status = "1";

            WRRHeader wRRHeader1 = await wRRHeaderRepository.GetUnderID(textEditWRRNumber.Text);
            if (wRRHeader1 != null)
            {
                

                if (await wRRHeaderRepository.Update(wRRHeader, textEditWRRNumber.Text) > 0)
                {
                    createWRRDetailFromForm();

                }
            }
            else
            {
                if (await wRRHeaderRepository.Create(wRRHeader) > 0)
                {
                    createWRRDetailFromForm();

                }
            }

            await checkExistWRRNumber(textEditWRRNumber.Text);
        }

        private async void createWRRDetailFromForm()
        {
            if (textEditWRRNumber.Text.Equals("") || textEditPalletID.Text.Equals("")||textEditLotID.Text.Equals("")||textEditQuantity.Text.Equals(""))
            {
                MessageBox.Show("Nhập thiếu dữ liệu");
                return;
            }


            List<WRRDetail> wRRDetails = await WRRDetailRepository.GetByPalletIDAndLotID(textEditWRRNumber.Text, textEditPalletID.Text, textEditLotID.Text);
            List<WRRDetail> wRRDetails1 = await WRRDetailRepository.GetUnderID(textEditWRRNumber.Text);

            if (wRRDetails.Count > 0)
            {
                WRRDetail wRRDetail = wRRDetails[0];
                wRRDetail.WRRNumber = textEditWRRNumber.Text;
                wRRDetail.PartNumber = textEditPartNumber.Text;
                wRRDetail.PartName = textEditPartName.Text;


                wRRDetail.TotalQuantity = Decimal.Parse(textEditQuantity.Text);
                wRRDetail.QuantityByItem = Decimal.Parse(textEditQuantity.Text);

                List<Goods> goodss = await goodsRepository.getAll();
                foreach (Goods goods in goodss)
                {
                    if (goods.PalletID.Equals(textEditPalletID.Text))
                    {
                        wRRDetail.PackingVolume = goods.Quantity;
                        wRRDetail.QuantityByPack = wRRDetail.QuantityByItem / wRRDetail.PackingVolume;
                        break;
                    }
                }


                wRRDetail.LotID = textEditLotID.Text;
                wRRDetail.PalletID = textEditPalletID.Text;
                wRRDetail.MFGDate = DateTime.Parse(dateEditMFGDate.DateTime.ToString("yyyy-MM-dd"));
                wRRDetail.EXPDate = DateTime.Parse(dateEditEXPDate.DateTime.ToString("yyyy-MM-dd"));
                wRRDetail.Remark = textEditRemark.Text;
                wRRDetail.Unit = comboBoxEditUnit.Text;
                wRRDetail.PONumber = textEditPONumber.Text;
                wRRDetail.Status = "0";
                wRRDetail.ProductFamily = textEditProductFamily.Text;
                wRRDetail.QuantityByPrinted =wRRDetails[0].QuantityByPrinted;
                wRRDetail.RemainingPrintQuantity = wRRDetail.QuantityByPack;
                wRRDetail.IDCodeRange = "";
                if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                {
                    if (await WRRDetailRepository.Update(wRRDetail, textEditWRRNumber.Text, textEditPalletID.Text, wRRDetails[0].Ordinal, wRRDetail.LotID) > 0)
                    {

                        sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);
                        WMPublic.sbMessageSaveChangeSuccess(this);
                    }
                }
            }
            else
            {

                WRRDetail wRRDetail = new WRRDetail();
                wRRDetail.Ordinal = wRRDetails1.Count + 1;
                wRRDetail.WRRNumber = textEditWRRNumber.Text;
                wRRDetail.PartNumber = textEditPartNumber.Text;
                wRRDetail.PartName = textEditPartName.Text;


                wRRDetail.TotalQuantity = Decimal.Parse(textEditQuantity.Text);
                wRRDetail.QuantityByItem = Decimal.Parse(textEditQuantity.Text);

                List<Goods> goodss = await goodsRepository.getAll();
                foreach (Goods goods in goodss)
                {
                    if (goods.PalletID.Equals(textEditPalletID.Text))
                    {
                        wRRDetail.PackingVolume = goods.Quantity;
                        wRRDetail.QuantityByPack = wRRDetail.QuantityByItem / wRRDetail.PackingVolume;
                        break;
                    }
                }


                wRRDetail.LotID = textEditLotID.Text;
                wRRDetail.PalletID = textEditPalletID.Text;
                wRRDetail.MFGDate = DateTime.Parse(dateEditMFGDate.DateTime.ToString("yyyy-MM-dd"));
                wRRDetail.EXPDate = DateTime.Parse(dateEditEXPDate.DateTime.ToString("yyyy-MM-dd"));
                wRRDetail.Remark = textEditRemark.Text;
                wRRDetail.Unit = comboBoxEditUnit.Text;
                wRRDetail.PONumber = textEditPONumber.Text;
                wRRDetail.Status = "0";
                wRRDetail.ProductFamily = textEditProductFamily.Text;
                wRRDetail.QuantityByPrinted = 0m;
                wRRDetail.RemainingPrintQuantity = wRRDetail.QuantityByPack;
                wRRDetail.IDCodeRange = "";
                if (WMPublic.sbMessageSaveNewRequest(this) == true)
                {
                    if (await WRRDetailRepository.Create(wRRDetail) > 0)
                    {

                        sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);
                        WMPublic.sbMessageSaveNewSuccess(this);
                    }
                }
            }

        }

        private async void deleteWRRDetailFromForm()
        {
            List<WRRDetail> wRRDetails = await WRRDetailRepository.GetByWRRNumberAndPalletID(textEditWRRNumber.Text, textEditPalletID.Text);
            if (wRRDetails.Count > 0)
            {
                if (WMPublic.sbMessageDeleteRequest(this) == true)
                {
                    if (await WRRDetailRepository.Delete3(textEditWRRNumber.Text, textEditPalletID.Text, wRRDetails[0].Ordinal) > 0)
                    {
                        WMPublic.sbMessageDeleteSuccess(this);
                        sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);
                    }
                }
            }
        }

        private void clearForm()
        {
            textEditPalletID.Text = "";
            textEditPartNumber.Text = "";
            textEditPartName.Text = "";
            textEditProductFamily.Text = "";
            textEditLotID.Text = "";
            comboBoxEditUnit.SelectedIndex = 0;
            textEditQuantity.Text = "";
            textEditPONumber.Text = "";
            dateEditMFGDate.Text = "";
            dateEditEXPDate.Text = "";
            textEditPartNumber.Focus();
        }

        private  void simpleButtonSave_Click(object sender, EventArgs e)
        {

            createWRRHeaderFromForm();
        }

        private void simpleButtonDelete_Click(object sender, EventArgs e)
        {
            deleteWRRDetailFromForm();
        }

        private void simpleButtonClear_Click(object sender, EventArgs e)
        {
            clearForm();
        }



        private void dateEditMFGDate_Properties_Closed(object sender, DevExpress.XtraEditors.Controls.ClosedEventArgs e)
        {
            dateEditEXPDate.Text = (DateTime.Parse(dateEditMFGDate.DateTime.ToString("yyyy-MM-dd")).AddYears(2).AddDays(-1)).ToString("yyyy-MM-dd");
        }

        private void textEditLotID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string lotID = textEditLotID.Text;
                if (lotID.Length == 8)
                {
                    string dateTime = lotID.Substring(2, 5);
                    string year = dateTime.Substring(3, 2);
                    string month = dateTime.Substring(0, 1);
                    string date = dateTime.Substring(1, 2);

                    if (month.Equals("O"))
                    {
                        month = "10";
                    }
                    if (month.Equals("N"))
                    {
                        month = "11";
                    }
                    if (month.Equals("D"))
                    {
                        month = "12";
                    }

                    string mfgDate = date + "/" + month + "/" + year;
                    try
                    {
                        dateEditMFGDate.DateTime = DateTime.Parse(mfgDate);
                        dateEditEXPDate.DateTime = DateTime.Parse(mfgDate).AddYears(2).AddDays(-1);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Sai Lot ID");
                }
            }
        }

        private void gridViewWRRDetail_RowClick(object sender, RowClickEventArgs e)
        {
            if ((string)(sender as GridView).GetFocusedRowCellValue("PartNumber") != null)
            {
                textEditPartNumber.Text = (string)(sender as GridView).GetFocusedRowCellValue("PartNumber");
                textEditPartName.Text = (string)(sender as GridView).GetFocusedRowCellValue("PartName");
                comboBoxEditUnit.Text = (string)(sender as GridView).GetFocusedRowCellValue("Unit");
                textEditProductFamily.Text = (string)(sender as GridView).GetFocusedRowCellValue("ProductFamily");
                textEditQuantity.Text = (string)(sender as GridView).GetFocusedRowCellValue("QuantityByItem").ToString();
                textEditPalletID.Text = (string)(sender as GridView).GetFocusedRowCellValue("PalletID");
                textEditLotID.Text = (string)(sender as GridView).GetFocusedRowCellValue("LotID");
                textEditPONumber.Text = (string)(sender as GridView).GetFocusedRowCellValue("PONumber");
                textEditRemark.Text = (string)(sender as GridView).GetFocusedRowCellValue("Remark");
                dateEditMFGDate.Text = DateTime.Parse((sender as GridView).GetFocusedRowCellValue("MFGDate").ToString()).ToString("yyyy-MM-dd");
                dateEditEXPDate.Text = DateTime.Parse((sender as GridView).GetFocusedRowCellValue("EXPDate").ToString()).ToString("yyyy-MM-dd");
            }
        }

        
    }
}
