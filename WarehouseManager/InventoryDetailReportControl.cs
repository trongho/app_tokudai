﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using WarehouseManager.Repository;

namespace WarehouseManager
{
    public partial class InventoryDetailReportControl : DevExpress.XtraReports.UI.XtraReport
    {
        InventoryDetailRepository inventoryDetailRepository = null;
        public InventoryDetailReportControl(string partNumber)
        {
            InitializeComponent();
            inventoryDetailRepository = new InventoryDetailRepository();
            Load(partNumber);
        }

        private async void Load(string partNumber)
        {
            this.DataSource = await inventoryDetailRepository.GetByPartNumber(partNumber);
        }

    }
}
