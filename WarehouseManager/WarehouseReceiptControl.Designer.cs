﻿
namespace WarehouseManager
{
    partial class WarehouseReceiptControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WarehouseReceiptControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDataDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.wRDataDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlWRDetail = new DevExpress.XtraGrid.GridControl();
            this.wRDetailBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewWRDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWRNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiptQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPalletID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMFGDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXPDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLotID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInputDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wRDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRDetailTableAdapter();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.label1 = new System.Windows.Forms.Label();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEditWRNumber = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditModality = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateEditWRDate = new DevExpress.XtraEditors.DateEdit();
            this.comboBoxEditWarehouse = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditTotalQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditNote = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemModality = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWarehouse = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNote = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditModality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNote)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(444, 166, 812, 495);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1298, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Làm mới", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Tìm kiếm", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9 )In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1294, 32);
            this.windowsUIButtonPanel1.TabIndex = 4;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1298, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1298, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // wRDataDetailBindingSource
            // 
            this.wRDataDetailBindingSource.DataMember = "WRDataDetail";
            this.wRDataDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wRDataDetailTableAdapter
            // 
            this.wRDataDetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlWRDetail);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 155);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1298, 471);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlWRDetail
            // 
            this.gridControlWRDetail.DataSource = this.wRDetailBindingSource1;
            this.gridControlWRDetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRDetail.Location = new System.Drawing.Point(12, 12);
            this.gridControlWRDetail.MainView = this.gridViewWRDetail;
            this.gridControlWRDetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRDetail.Name = "gridControlWRDetail";
            this.gridControlWRDetail.Size = new System.Drawing.Size(1274, 447);
            this.gridControlWRDetail.TabIndex = 5;
            this.gridControlWRDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWRDetail});
            // 
            // wRDetailBindingSource1
            // 
            this.wRDetailBindingSource1.DataMember = "WRDetail";
            this.wRDetailBindingSource1.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // gridViewWRDetail
            // 
            this.gridViewWRDetail.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewWRDetail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewWRDetail.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.gridViewWRDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWRNumber,
            this.colOrdinal,
            this.colPartNumber,
            this.colPartName,
            this.colQuantityReceived,
            this.colReceiptQuantity,
            this.colStatus,
            this.colPalletID,
            this.colUnit,
            this.colMFGDate,
            this.colEXPDate,
            this.colIDCode,
            this.colLotID,
            this.colInputDate,
            this.colPackingQuantity,
            this.colPackingVolume});
            this.gridViewWRDetail.DetailHeight = 284;
            this.gridViewWRDetail.GridControl = this.gridControlWRDetail;
            this.gridViewWRDetail.Name = "gridViewWRDetail";
            this.gridViewWRDetail.OptionsCustomization.AllowSort = false;
            this.gridViewWRDetail.OptionsPrint.AutoWidth = false;
            this.gridViewWRDetail.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridViewWRDetail.OptionsPrint.EnableAppearanceOddRow = true;
            this.gridViewWRDetail.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            // 
            // colWRNumber
            // 
            this.colWRNumber.FieldName = "WRNumber";
            this.colWRNumber.Name = "colWRNumber";
            // 
            // colOrdinal
            // 
            this.colOrdinal.Caption = "No.";
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.MinWidth = 30;
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Visible = true;
            this.colOrdinal.VisibleIndex = 0;
            this.colOrdinal.Width = 30;
            // 
            // colPartNumber
            // 
            this.colPartNumber.Caption = "Part Number";
            this.colPartNumber.FieldName = "PartNumber";
            this.colPartNumber.MinWidth = 150;
            this.colPartNumber.Name = "colPartNumber";
            this.colPartNumber.Visible = true;
            this.colPartNumber.VisibleIndex = 1;
            this.colPartNumber.Width = 150;
            // 
            // colPartName
            // 
            this.colPartName.Caption = "Part Name";
            this.colPartName.FieldName = "PartName";
            this.colPartName.MinWidth = 200;
            this.colPartName.Name = "colPartName";
            this.colPartName.Visible = true;
            this.colPartName.VisibleIndex = 4;
            this.colPartName.Width = 200;
            // 
            // colQuantityReceived
            // 
            this.colQuantityReceived.Caption = "Lượng yêu cầu";
            this.colQuantityReceived.FieldName = "QuantityReceived";
            this.colQuantityReceived.MinWidth = 120;
            this.colQuantityReceived.Name = "colQuantityReceived";
            this.colQuantityReceived.Visible = true;
            this.colQuantityReceived.VisibleIndex = 7;
            this.colQuantityReceived.Width = 120;
            // 
            // colReceiptQuantity
            // 
            this.colReceiptQuantity.Caption = "Lượng nhập";
            this.colReceiptQuantity.FieldName = "ReceiptQuantity";
            this.colReceiptQuantity.MinWidth = 120;
            this.colReceiptQuantity.Name = "colReceiptQuantity";
            this.colReceiptQuantity.Visible = true;
            this.colReceiptQuantity.VisibleIndex = 8;
            this.colReceiptQuantity.Width = 120;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            // 
            // colPalletID
            // 
            this.colPalletID.Caption = "Pallet ID";
            this.colPalletID.FieldName = "PalletID";
            this.colPalletID.Name = "colPalletID";
            this.colPalletID.Visible = true;
            this.colPalletID.VisibleIndex = 2;
            this.colPalletID.Width = 44;
            // 
            // colUnit
            // 
            this.colUnit.Caption = "Unit";
            this.colUnit.FieldName = "Unit";
            this.colUnit.Name = "colUnit";
            this.colUnit.Visible = true;
            this.colUnit.VisibleIndex = 6;
            this.colUnit.Width = 20;
            // 
            // colMFGDate
            // 
            this.colMFGDate.Caption = "MFG Date";
            this.colMFGDate.FieldName = "MFGDate";
            this.colMFGDate.MinWidth = 70;
            this.colMFGDate.Name = "colMFGDate";
            this.colMFGDate.Visible = true;
            this.colMFGDate.VisibleIndex = 10;
            this.colMFGDate.Width = 70;
            // 
            // colEXPDate
            // 
            this.colEXPDate.Caption = "EXP Date";
            this.colEXPDate.FieldName = "EXPDate";
            this.colEXPDate.MinWidth = 70;
            this.colEXPDate.Name = "colEXPDate";
            this.colEXPDate.Visible = true;
            this.colEXPDate.VisibleIndex = 11;
            this.colEXPDate.Width = 70;
            // 
            // colIDCode
            // 
            this.colIDCode.Caption = "ID Code";
            this.colIDCode.FieldName = "IDCode";
            this.colIDCode.Name = "colIDCode";
            this.colIDCode.Visible = true;
            this.colIDCode.VisibleIndex = 13;
            this.colIDCode.Width = 20;
            // 
            // colLotID
            // 
            this.colLotID.Caption = "Lot ID";
            this.colLotID.FieldName = "LotID";
            this.colLotID.MinWidth = 70;
            this.colLotID.Name = "colLotID";
            this.colLotID.Visible = true;
            this.colLotID.VisibleIndex = 3;
            this.colLotID.Width = 70;
            // 
            // colInputDate
            // 
            this.colInputDate.Caption = "Input Date";
            this.colInputDate.FieldName = "InputDate";
            this.colInputDate.MinWidth = 70;
            this.colInputDate.Name = "colInputDate";
            this.colInputDate.Visible = true;
            this.colInputDate.VisibleIndex = 12;
            this.colInputDate.Width = 70;
            // 
            // colPackingQuantity
            // 
            this.colPackingQuantity.Caption = "Qty Cartons nhập";
            this.colPackingQuantity.FieldName = "PackingQuantity";
            this.colPackingQuantity.MinWidth = 70;
            this.colPackingQuantity.Name = "colPackingQuantity";
            this.colPackingQuantity.Visible = true;
            this.colPackingQuantity.VisibleIndex = 9;
            this.colPackingQuantity.Width = 70;
            // 
            // colPackingVolume
            // 
            this.colPackingVolume.Caption = "Quy cách";
            this.colPackingVolume.FieldName = "PackingVolume";
            this.colPackingVolume.MinWidth = 70;
            this.colPackingVolume.Name = "colPackingVolume";
            this.colPackingVolume.Visible = true;
            this.colPackingVolume.VisibleIndex = 5;
            this.colPackingVolume.Width = 70;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1298, 471);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlWRDetail;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1278, 451);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // wRDetailBindingSource
            // 
            this.wRDetailBindingSource.DataMember = "WRDetail";
            this.wRDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wRDetailTableAdapter
            // 
            this.wRDetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEdit4);
            this.layoutControl2.Controls.Add(this.textEditWRNumber);
            this.layoutControl2.Controls.Add(this.comboBoxEditModality);
            this.layoutControl2.Controls.Add(this.dateEditWRDate);
            this.layoutControl2.Controls.Add(this.comboBoxEditWarehouse);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantity);
            this.layoutControl2.Controls.Add(this.textEditNote);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(389, 386, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1298, 118);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(218, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "(*)";
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(2540, 10);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(53, 20);
            this.textEdit4.StyleController = this.layoutControl2;
            this.textEdit4.TabIndex = 11;
            // 
            // textEditWRNumber
            // 
            this.textEditWRNumber.Location = new System.Drawing.Point(97, 12);
            this.textEditWRNumber.Name = "textEditWRNumber";
            this.textEditWRNumber.Size = new System.Drawing.Size(117, 20);
            this.textEditWRNumber.StyleController = this.layoutControl2;
            this.textEditWRNumber.TabIndex = 12;
            // 
            // comboBoxEditModality
            // 
            this.comboBoxEditModality.Location = new System.Drawing.Point(97, 36);
            this.comboBoxEditModality.Name = "comboBoxEditModality";
            this.comboBoxEditModality.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditModality.Size = new System.Drawing.Size(117, 20);
            this.comboBoxEditModality.StyleController = this.layoutControl2;
            this.comboBoxEditModality.TabIndex = 13;
            // 
            // dateEditWRDate
            // 
            this.dateEditWRDate.EditValue = null;
            this.dateEditWRDate.Location = new System.Drawing.Point(344, 36);
            this.dateEditWRDate.Name = "dateEditWRDate";
            this.dateEditWRDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWRDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWRDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRDate.Size = new System.Drawing.Size(117, 20);
            this.dateEditWRDate.StyleController = this.layoutControl2;
            this.dateEditWRDate.TabIndex = 14;
            // 
            // comboBoxEditWarehouse
            // 
            this.comboBoxEditWarehouse.Location = new System.Drawing.Point(344, 12);
            this.comboBoxEditWarehouse.Name = "comboBoxEditWarehouse";
            this.comboBoxEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditWarehouse.Size = new System.Drawing.Size(323, 20);
            this.comboBoxEditWarehouse.StyleController = this.layoutControl2;
            this.comboBoxEditWarehouse.TabIndex = 17;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(756, 12);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(117, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 25;
            // 
            // textEditTotalQuantity
            // 
            this.textEditTotalQuantity.Location = new System.Drawing.Point(1168, 12);
            this.textEditTotalQuantity.Name = "textEditTotalQuantity";
            this.textEditTotalQuantity.Size = new System.Drawing.Size(118, 20);
            this.textEditTotalQuantity.StyleController = this.layoutControl2;
            this.textEditTotalQuantity.TabIndex = 27;
            // 
            // textEditNote
            // 
            this.textEditNote.Location = new System.Drawing.Point(97, 60);
            this.textEditNote.Name = "textEditNote";
            this.textEditNote.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textEditNote.Size = new System.Drawing.Size(1189, 46);
            this.textEditNote.StyleController = this.layoutControl2;
            this.textEditNote.TabIndex = 16;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItemWRNumber,
            this.layoutControlItemModality,
            this.layoutControlItemWarehouse,
            this.layoutControlItemTotalQuantity,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItemWRDate,
            this.layoutControlItemNote});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 16.129032258064516D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 3.225806451612903D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 16.129032258064516D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 16.129032258064516D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 16.129032258064516D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 16.129032258064516D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 16.129032258064516D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6,
            columnDefinition7});
            rowDefinition1.Height = 25D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 25D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 25D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 25D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1298, 118);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(206, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(41, 24);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemWRNumber
            // 
            this.layoutControlItemWRNumber.Control = this.textEditWRNumber;
            this.layoutControlItemWRNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemWRNumber.Name = "layoutControlItemWRNumber";
            this.layoutControlItemWRNumber.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemWRNumber.Text = "Số phiếu nhập";
            this.layoutControlItemWRNumber.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemModality
            // 
            this.layoutControlItemModality.Control = this.comboBoxEditModality;
            this.layoutControlItemModality.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemModality.Name = "layoutControlItemModality";
            this.layoutControlItemModality.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemModality.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemModality.Text = "Phương thức";
            this.layoutControlItemModality.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemWarehouse
            // 
            this.layoutControlItemWarehouse.Control = this.comboBoxEditWarehouse;
            this.layoutControlItemWarehouse.Location = new System.Drawing.Point(247, 0);
            this.layoutControlItemWarehouse.Name = "layoutControlItemWarehouse";
            this.layoutControlItemWarehouse.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemWarehouse.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemWarehouse.Size = new System.Drawing.Size(412, 24);
            this.layoutControlItemWarehouse.Text = "Kho nhập hàng";
            this.layoutControlItemWarehouse.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemTotalQuantity
            // 
            this.layoutControlItemTotalQuantity.Control = this.textEditTotalQuantity;
            this.layoutControlItemTotalQuantity.Location = new System.Drawing.Point(1071, 0);
            this.layoutControlItemTotalQuantity.Name = "layoutControlItemTotalQuantity";
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalQuantity.Size = new System.Drawing.Size(207, 24);
            this.layoutControlItemTotalQuantity.Text = "Tổng lượng nhập";
            this.layoutControlItemTotalQuantity.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(659, 0);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemWRDate
            // 
            this.layoutControlItemWRDate.Control = this.dateEditWRDate;
            this.layoutControlItemWRDate.Location = new System.Drawing.Point(247, 24);
            this.layoutControlItemWRDate.Name = "layoutControlItemWRDate";
            this.layoutControlItemWRDate.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemWRDate.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemWRDate.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemWRDate.Text = "Ngày phiếu nhập";
            this.layoutControlItemWRDate.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemNote
            // 
            this.layoutControlItemNote.Control = this.textEditNote;
            this.layoutControlItemNote.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemNote.Name = "layoutControlItemNote";
            this.layoutControlItemNote.OptionsTableLayoutItem.ColumnSpan = 7;
            this.layoutControlItemNote.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemNote.OptionsTableLayoutItem.RowSpan = 2;
            this.layoutControlItemNote.Size = new System.Drawing.Size(1278, 50);
            this.layoutControlItemNote.Text = "Ghi chú";
            this.layoutControlItemNote.TextSize = new System.Drawing.Size(81, 13);
            // 
            // WarehouseReceiptControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "WarehouseReceiptControl";
            this.Size = new System.Drawing.Size(1298, 626);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditModality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNote)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource wRDataDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter wRDataDetailTableAdapter;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlWRDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWRDetail;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.BindingSource wRDetailBindingSource1;
        private DevExpress.XtraGrid.Columns.GridColumn colWRNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colPartNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPartName;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiptQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private System.Windows.Forms.BindingSource wRDetailBindingSource;
        private WARHOUSE_HPDataSet1TableAdapters.WRDetailTableAdapter wRDetailTableAdapter;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEditWRNumber;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditModality;
        private DevExpress.XtraEditors.DateEdit dateEditWRDate;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditWarehouse;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantity;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemModality;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWarehouse;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colPalletID;
        private DevExpress.XtraGrid.Columns.GridColumn colUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colMFGDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEXPDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIDCode;
        private DevExpress.XtraGrid.Columns.GridColumn colLotID;
        private DevExpress.XtraGrid.Columns.GridColumn colInputDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingVolume;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraEditors.MemoEdit textEditNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNote;
    }
}
