﻿
namespace WarehouseManager
{
    partial class InventoryProcessControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InventoryProcessControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition10 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition11 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition12 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition13 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition14 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            this.gridViewDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPalletID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLotID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMFGDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXPDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInputDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlInventory = new DevExpress.XtraGrid.GridControl();
            this.inventoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet2 = new WarehouseManager.WARHOUSE_HPDataSet2();
            this.gridViewInventory = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMonth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOpeningStockQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiptQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosingStockQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.wRRDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.wRRDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditWarehouse = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditProcessingPeriod = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditMessage = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWarehouse = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemProcessingPeriod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditTotalOpenStockQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalOpenStockCarton = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalReceiptQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalReceiptCarton = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalIssueQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalIssueCarton = new DevExpress.XtraEditors.TextEdit();
            this.textEditCloseStockQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditCloseStockCarton = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.inventoryTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet2TableAdapters.InventoryTableAdapter();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditProcessingPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMessage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemProcessingPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalOpenStockQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalOpenStockCarton.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalReceiptQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalReceiptCarton.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalIssueQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalIssueCarton.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCloseStockQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCloseStockCarton.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewDetail
            // 
            this.gridViewDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPalletID,
            this.colLotID,
            this.colIDCode,
            this.colMFGDate,
            this.colEXPDate,
            this.colInputDate,
            this.colQuantity});
            this.gridViewDetail.GridControl = this.gridControlInventory;
            this.gridViewDetail.Name = "gridViewDetail";
            // 
            // colPalletID
            // 
            this.colPalletID.Caption = "Pallet ID";
            this.colPalletID.FieldName = "PalletID";
            this.colPalletID.Name = "colPalletID";
            this.colPalletID.Visible = true;
            this.colPalletID.VisibleIndex = 0;
            // 
            // colLotID
            // 
            this.colLotID.Caption = "Lot ID";
            this.colLotID.FieldName = "LotID";
            this.colLotID.Name = "colLotID";
            this.colLotID.Visible = true;
            this.colLotID.VisibleIndex = 1;
            // 
            // colIDCode
            // 
            this.colIDCode.Caption = "Mã hàng";
            this.colIDCode.FieldName = "IDCode";
            this.colIDCode.Name = "colIDCode";
            this.colIDCode.Visible = true;
            this.colIDCode.VisibleIndex = 2;
            // 
            // colMFGDate
            // 
            this.colMFGDate.Caption = "MFG Date";
            this.colMFGDate.FieldName = "MFGDate";
            this.colMFGDate.Name = "colMFGDate";
            this.colMFGDate.Visible = true;
            this.colMFGDate.VisibleIndex = 3;
            // 
            // colEXPDate
            // 
            this.colEXPDate.Caption = "EXP Date";
            this.colEXPDate.FieldName = "EXPDate";
            this.colEXPDate.Name = "colEXPDate";
            this.colEXPDate.Visible = true;
            this.colEXPDate.VisibleIndex = 4;
            // 
            // colInputDate
            // 
            this.colInputDate.Caption = "Ngày nhập kho";
            this.colInputDate.FieldName = "InputDate";
            this.colInputDate.Name = "colInputDate";
            this.colInputDate.Visible = true;
            this.colInputDate.VisibleIndex = 5;
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "Quantity";
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 6;
            // 
            // gridControlInventory
            // 
            this.gridControlInventory.DataSource = this.inventoryBindingSource;
            this.gridControlInventory.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            gridLevelNode1.LevelTemplate = this.gridViewDetail;
            gridLevelNode1.RelationName = "Level1";
            this.gridControlInventory.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlInventory.Location = new System.Drawing.Point(12, 12);
            this.gridControlInventory.MainView = this.gridViewInventory;
            this.gridControlInventory.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlInventory.Name = "gridControlInventory";
            this.gridControlInventory.Size = new System.Drawing.Size(1216, 468);
            this.gridControlInventory.TabIndex = 4;
            this.gridControlInventory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInventory,
            this.gridViewDetail});
            // 
            // inventoryBindingSource
            // 
            this.inventoryBindingSource.DataMember = "Inventory";
            this.inventoryBindingSource.DataSource = this.wARHOUSE_HPDataSet2;
            // 
            // wARHOUSE_HPDataSet2
            // 
            this.wARHOUSE_HPDataSet2.DataSetName = "WARHOUSE_HPDataSet2";
            this.wARHOUSE_HPDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewInventory
            // 
            this.gridViewInventory.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colYear,
            this.colMonth,
            this.colWarehouseID,
            this.colPartNumber,
            this.colOpeningStockQuantity,
            this.colReceiptQuantity,
            this.colIssueQuantity,
            this.colClosingStockQuantity,
            this.colStatus,
            this.colPackingVolume,
            this.colMaterial,
            this.colMold,
            this.colNo});
            this.gridViewInventory.DetailHeight = 284;
            this.gridViewInventory.GridControl = this.gridControlInventory;
            this.gridViewInventory.Name = "gridViewInventory";
            this.gridViewInventory.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewInventory_CustomColumnDisplayText);
            // 
            // colYear
            // 
            this.colYear.FieldName = "Year";
            this.colYear.MinWidth = 21;
            this.colYear.Name = "colYear";
            // 
            // colMonth
            // 
            this.colMonth.FieldName = "Month";
            this.colMonth.MinWidth = 21;
            this.colMonth.Name = "colMonth";
            // 
            // colWarehouseID
            // 
            this.colWarehouseID.FieldName = "WarehouseID";
            this.colWarehouseID.MinWidth = 21;
            this.colWarehouseID.Name = "colWarehouseID";
            this.colWarehouseID.Width = 100;
            // 
            // colPartNumber
            // 
            this.colPartNumber.Caption = "Part Number";
            this.colPartNumber.FieldName = "PartNumber";
            this.colPartNumber.MinWidth = 21;
            this.colPartNumber.Name = "colPartNumber";
            this.colPartNumber.Visible = true;
            this.colPartNumber.VisibleIndex = 1;
            this.colPartNumber.Width = 161;
            // 
            // colOpeningStockQuantity
            // 
            this.colOpeningStockQuantity.Caption = "Lượng tồn đầu";
            this.colOpeningStockQuantity.DisplayFormat.FormatString = "G29";
            this.colOpeningStockQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colOpeningStockQuantity.FieldName = "OpeningStockQuantity";
            this.colOpeningStockQuantity.MinWidth = 21;
            this.colOpeningStockQuantity.Name = "colOpeningStockQuantity";
            this.colOpeningStockQuantity.Visible = true;
            this.colOpeningStockQuantity.VisibleIndex = 2;
            this.colOpeningStockQuantity.Width = 161;
            // 
            // colReceiptQuantity
            // 
            this.colReceiptQuantity.Caption = "Lượng nhập";
            this.colReceiptQuantity.DisplayFormat.FormatString = "G29";
            this.colReceiptQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colReceiptQuantity.FieldName = "ReceiptQuantity";
            this.colReceiptQuantity.MinWidth = 21;
            this.colReceiptQuantity.Name = "colReceiptQuantity";
            this.colReceiptQuantity.Visible = true;
            this.colReceiptQuantity.VisibleIndex = 3;
            this.colReceiptQuantity.Width = 161;
            // 
            // colIssueQuantity
            // 
            this.colIssueQuantity.Caption = "Lượng xuất";
            this.colIssueQuantity.DisplayFormat.FormatString = "G29";
            this.colIssueQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colIssueQuantity.FieldName = "IssueQuantity";
            this.colIssueQuantity.MinWidth = 21;
            this.colIssueQuantity.Name = "colIssueQuantity";
            this.colIssueQuantity.Visible = true;
            this.colIssueQuantity.VisibleIndex = 4;
            this.colIssueQuantity.Width = 161;
            // 
            // colClosingStockQuantity
            // 
            this.colClosingStockQuantity.Caption = "Lượng tồn cuối";
            this.colClosingStockQuantity.DisplayFormat.FormatString = "G29";
            this.colClosingStockQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colClosingStockQuantity.FieldName = "ClosingStockQuantity";
            this.colClosingStockQuantity.MinWidth = 21;
            this.colClosingStockQuantity.Name = "colClosingStockQuantity";
            this.colClosingStockQuantity.Visible = true;
            this.colClosingStockQuantity.VisibleIndex = 5;
            this.colClosingStockQuantity.Width = 161;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Trạng thái";
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 21;
            this.colStatus.Name = "colStatus";
            this.colStatus.Width = 133;
            // 
            // colPackingVolume
            // 
            this.colPackingVolume.Caption = "Packing Volume";
            this.colPackingVolume.DisplayFormat.FormatString = "G29";
            this.colPackingVolume.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPackingVolume.FieldName = "PackingVolume";
            this.colPackingVolume.Name = "colPackingVolume";
            this.colPackingVolume.Visible = true;
            this.colPackingVolume.VisibleIndex = 6;
            this.colPackingVolume.Width = 161;
            // 
            // colMaterial
            // 
            this.colMaterial.Caption = "Material";
            this.colMaterial.FieldName = "Material";
            this.colMaterial.Name = "colMaterial";
            // 
            // colMold
            // 
            this.colMold.Caption = "Mold";
            this.colMold.FieldName = "Mold";
            this.colMold.Name = "colMold";
            this.colMold.Visible = true;
            this.colMold.VisibleIndex = 7;
            this.colMold.Width = 131;
            // 
            // colNo
            // 
            this.colNo.Caption = "No.";
            this.colNo.FieldName = "No";
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            // 
            // wRRDetailBindingSource
            // 
            this.wRRDetailBindingSource.DataMember = "WRRDetail";
            this.wRRDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wRRDetailTableAdapter
            // 
            this.wRRDetailTableAdapter.ClearBeforeFill = true;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1240, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1240, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Mở khóa chứng từ", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, false, "unlock", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(null, false, -1),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xử lý tồn kho", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, false, "process", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(null, false, -1),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Làm mới", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Print", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator()});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1236, 32);
            this.windowsUIButtonPanel1.TabIndex = 4;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(639, 159, 812, 500);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1240, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl2.Controls.Add(this.comboBoxEditWarehouse);
            this.layoutControl2.Controls.Add(this.comboBoxEditProcessingPeriod);
            this.layoutControl2.Controls.Add(this.textEditMessage);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(134, 182, 812, 551);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1240, 80);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(68, 12);
            this.comboBoxEditBranch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(306, 20);
            this.comboBoxEditBranch.StyleController = this.layoutControl2;
            this.comboBoxEditBranch.TabIndex = 4;
            this.comboBoxEditBranch.SelectedValueChanged += new System.EventHandler(this.comboBoxEditBranch_SelectedValueChanged);
            // 
            // comboBoxEditWarehouse
            // 
            this.comboBoxEditWarehouse.Location = new System.Drawing.Point(68, 42);
            this.comboBoxEditWarehouse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditWarehouse.Name = "comboBoxEditWarehouse";
            this.comboBoxEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditWarehouse.Size = new System.Drawing.Size(306, 20);
            this.comboBoxEditWarehouse.StyleController = this.layoutControl2;
            this.comboBoxEditWarehouse.TabIndex = 5;
            this.comboBoxEditWarehouse.SelectedValueChanged += new System.EventHandler(this.comboBoxEditWarehouse_SelectedValueChanged);
            // 
            // comboBoxEditProcessingPeriod
            // 
            this.comboBoxEditProcessingPeriod.Location = new System.Drawing.Point(434, 12);
            this.comboBoxEditProcessingPeriod.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditProcessingPeriod.Name = "comboBoxEditProcessingPeriod";
            this.comboBoxEditProcessingPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditProcessingPeriod.Size = new System.Drawing.Size(184, 20);
            this.comboBoxEditProcessingPeriod.StyleController = this.layoutControl2;
            this.comboBoxEditProcessingPeriod.TabIndex = 6;
            this.comboBoxEditProcessingPeriod.SelectedValueChanged += new System.EventHandler(this.comboBoxEditProcessingPeriod_SelectedValueChanged);
            // 
            // textEditMessage
            // 
            this.textEditMessage.Location = new System.Drawing.Point(866, 12);
            this.textEditMessage.Name = "textEditMessage";
            this.textEditMessage.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textEditMessage.Size = new System.Drawing.Size(362, 56);
            this.textEditMessage.StyleController = this.layoutControl2;
            this.textEditMessage.TabIndex = 7;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemBranch,
            this.layoutControlItemWarehouse,
            this.layoutControlItemProcessingPeriod,
            this.layoutControlItem2});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 30D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 20D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 10D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 10D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 30D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5});
            rowDefinition1.Height = 50D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 50D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1240, 80);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.Size = new System.Drawing.Size(366, 30);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItemWarehouse
            // 
            this.layoutControlItemWarehouse.Control = this.comboBoxEditWarehouse;
            this.layoutControlItemWarehouse.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItemWarehouse.Name = "layoutControlItemWarehouse";
            this.layoutControlItemWarehouse.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemWarehouse.Size = new System.Drawing.Size(366, 30);
            this.layoutControlItemWarehouse.Text = "Kho hàng";
            this.layoutControlItemWarehouse.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItemProcessingPeriod
            // 
            this.layoutControlItemProcessingPeriod.Control = this.comboBoxEditProcessingPeriod;
            this.layoutControlItemProcessingPeriod.Location = new System.Drawing.Point(366, 0);
            this.layoutControlItemProcessingPeriod.Name = "layoutControlItemProcessingPeriod";
            this.layoutControlItemProcessingPeriod.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItemProcessingPeriod.Size = new System.Drawing.Size(244, 30);
            this.layoutControlItemProcessingPeriod.Text = "Kỳ khóa sổ";
            this.layoutControlItemProcessingPeriod.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEditMessage;
            this.layoutControlItem2.Location = new System.Drawing.Point(854, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem2.OptionsTableLayoutItem.RowSpan = 2;
            this.layoutControlItem2.Size = new System.Drawing.Size(366, 60);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            this.layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlInventory);
            this.layoutControl3.Controls.Add(this.textEditTotalOpenStockQuantity);
            this.layoutControl3.Controls.Add(this.textEditTotalOpenStockCarton);
            this.layoutControl3.Controls.Add(this.textEditTotalReceiptQuantity);
            this.layoutControl3.Controls.Add(this.textEditTotalReceiptCarton);
            this.layoutControl3.Controls.Add(this.textEditTotalIssueQuantity);
            this.layoutControl3.Controls.Add(this.textEditTotalIssueCarton);
            this.layoutControl3.Controls.Add(this.textEditCloseStockQuantity);
            this.layoutControl3.Controls.Add(this.textEditCloseStockCarton);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 117);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(770, 280, 650, 400);
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1240, 544);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // textEditTotalOpenStockQuantity
            // 
            this.textEditTotalOpenStockQuantity.Location = new System.Drawing.Point(235, 510);
            this.textEditTotalOpenStockQuantity.Name = "textEditTotalOpenStockQuantity";
            this.textEditTotalOpenStockQuantity.Size = new System.Drawing.Size(121, 20);
            this.textEditTotalOpenStockQuantity.StyleController = this.layoutControl3;
            this.textEditTotalOpenStockQuantity.TabIndex = 5;
            // 
            // textEditTotalOpenStockCarton
            // 
            this.textEditTotalOpenStockCarton.Location = new System.Drawing.Point(360, 510);
            this.textEditTotalOpenStockCarton.Name = "textEditTotalOpenStockCarton";
            this.textEditTotalOpenStockCarton.Size = new System.Drawing.Size(83, 20);
            this.textEditTotalOpenStockCarton.StyleController = this.layoutControl3;
            this.textEditTotalOpenStockCarton.TabIndex = 6;
            // 
            // textEditTotalReceiptQuantity
            // 
            this.textEditTotalReceiptQuantity.Location = new System.Drawing.Point(477, 510);
            this.textEditTotalReceiptQuantity.Name = "textEditTotalReceiptQuantity";
            this.textEditTotalReceiptQuantity.Size = new System.Drawing.Size(140, 20);
            this.textEditTotalReceiptQuantity.StyleController = this.layoutControl3;
            this.textEditTotalReceiptQuantity.TabIndex = 7;
            // 
            // textEditTotalReceiptCarton
            // 
            this.textEditTotalReceiptCarton.Location = new System.Drawing.Point(621, 510);
            this.textEditTotalReceiptCarton.Name = "textEditTotalReceiptCarton";
            this.textEditTotalReceiptCarton.Size = new System.Drawing.Size(83, 20);
            this.textEditTotalReceiptCarton.StyleController = this.layoutControl3;
            this.textEditTotalReceiptCarton.TabIndex = 8;
            // 
            // textEditTotalIssueQuantity
            // 
            this.textEditTotalIssueQuantity.Location = new System.Drawing.Point(735, 510);
            this.textEditTotalIssueQuantity.Name = "textEditTotalIssueQuantity";
            this.textEditTotalIssueQuantity.Size = new System.Drawing.Size(143, 20);
            this.textEditTotalIssueQuantity.StyleController = this.layoutControl3;
            this.textEditTotalIssueQuantity.TabIndex = 9;
            // 
            // textEditTotalIssueCarton
            // 
            this.textEditTotalIssueCarton.Location = new System.Drawing.Point(882, 510);
            this.textEditTotalIssueCarton.Name = "textEditTotalIssueCarton";
            this.textEditTotalIssueCarton.Size = new System.Drawing.Size(83, 20);
            this.textEditTotalIssueCarton.StyleController = this.layoutControl3;
            this.textEditTotalIssueCarton.TabIndex = 10;
            // 
            // textEditCloseStockQuantity
            // 
            this.textEditCloseStockQuantity.Location = new System.Drawing.Point(1014, 510);
            this.textEditCloseStockQuantity.Name = "textEditCloseStockQuantity";
            this.textEditCloseStockQuantity.Size = new System.Drawing.Size(125, 20);
            this.textEditCloseStockQuantity.StyleController = this.layoutControl3;
            this.textEditCloseStockQuantity.TabIndex = 11;
            // 
            // textEditCloseStockCarton
            // 
            this.textEditCloseStockCarton.Location = new System.Drawing.Point(1143, 510);
            this.textEditCloseStockCarton.Name = "textEditCloseStockCarton";
            this.textEditCloseStockCarton.Size = new System.Drawing.Size(85, 20);
            this.textEditCloseStockCarton.StyleController = this.layoutControl3;
            this.textEditCloseStockCarton.TabIndex = 12;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.simpleLabelItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem9,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem15});
            this.layoutControlGroup2.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup2.Name = "Root";
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 10D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 10D;
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 5D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 10D;
            columnDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition10.Width = 5D;
            columnDefinition11.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition11.Width = 10D;
            columnDefinition12.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition12.Width = 5D;
            columnDefinition13.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition13.Width = 10D;
            columnDefinition14.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition14.Width = 5D;
            this.layoutControlGroup2.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition6,
            columnDefinition7,
            columnDefinition8,
            columnDefinition9,
            columnDefinition10,
            columnDefinition11,
            columnDefinition12,
            columnDefinition13,
            columnDefinition14});
            rowDefinition3.Height = 90D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 5D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition5.Height = 5D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup2.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition3,
            rowDefinition4,
            rowDefinition5});
            this.layoutControlGroup2.Size = new System.Drawing.Size(1240, 544);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.gridControlInventory;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnSpan = 9;
            this.layoutControlItem6.Size = new System.Drawing.Size(1220, 472);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 498);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.OptionsTableLayoutItem.RowIndex = 2;
            this.simpleLabelItem1.Size = new System.Drawing.Size(174, 26);
            this.simpleLabelItem1.Text = "Σ Lượng/Carton";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEditTotalOpenStockQuantity;
            this.layoutControlItem3.Location = new System.Drawing.Point(174, 498);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem3.Size = new System.Drawing.Size(174, 26);
            this.layoutControlItem3.Text = "Tồn đầu";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(39, 13);
            this.layoutControlItem3.TextToControlDistance = 10;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEditTotalOpenStockCarton;
            this.layoutControlItem4.Location = new System.Drawing.Point(348, 498);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem4.Size = new System.Drawing.Size(87, 26);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEditTotalReceiptQuantity;
            this.layoutControlItem5.Location = new System.Drawing.Point(435, 498);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem5.Size = new System.Drawing.Size(174, 26);
            this.layoutControlItem5.Text = "Nhập";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(25, 13);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEditTotalReceiptCarton;
            this.layoutControlItem7.Location = new System.Drawing.Point(609, 498);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem7.Size = new System.Drawing.Size(87, 26);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEditTotalIssueQuantity;
            this.layoutControlItem9.Location = new System.Drawing.Point(696, 498);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem9.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem9.Size = new System.Drawing.Size(174, 26);
            this.layoutControlItem9.Text = "Xuất";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(22, 13);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEditTotalIssueCarton;
            this.layoutControlItem12.Location = new System.Drawing.Point(870, 498);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem12.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem12.Size = new System.Drawing.Size(87, 26);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.textEditCloseStockQuantity;
            this.layoutControlItem13.Location = new System.Drawing.Point(957, 498);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.OptionsTableLayoutItem.ColumnIndex = 7;
            this.layoutControlItem13.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem13.Size = new System.Drawing.Size(174, 26);
            this.layoutControlItem13.Text = "Tồn cuối";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(40, 13);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.textEditCloseStockCarton;
            this.layoutControlItem15.Location = new System.Drawing.Point(1131, 498);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.OptionsTableLayoutItem.ColumnIndex = 8;
            this.layoutControlItem15.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem15.Size = new System.Drawing.Size(89, 26);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // inventoryTableAdapter
            // 
            this.inventoryTableAdapter.ClearBeforeFill = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // InventoryProcessControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "InventoryProcessControl";
            this.Size = new System.Drawing.Size(1240, 661);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditProcessingPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMessage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemProcessingPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalOpenStockQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalOpenStockCarton.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalReceiptQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalReceiptCarton.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalIssueQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalIssueCarton.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCloseStockQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCloseStockCarton.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource wRRDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter wRRDetailTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditWarehouse;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditProcessingPeriod;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWarehouse;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemProcessingPeriod;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlInventory;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInventory;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.BindingSource inventoryBindingSource;
        private WARHOUSE_HPDataSet2 wARHOUSE_HPDataSet2;
        private DevExpress.XtraGrid.Columns.GridColumn colYear;
        private DevExpress.XtraGrid.Columns.GridColumn colMonth;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseID;
        private DevExpress.XtraGrid.Columns.GridColumn colPartNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOpeningStockQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiptQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colClosingStockQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private WARHOUSE_HPDataSet2TableAdapters.InventoryTableAdapter inventoryTableAdapter;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private DevExpress.XtraEditors.MemoEdit textEditMessage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterial;
        private DevExpress.XtraGrid.Columns.GridColumn colMold;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colPalletID;
        private DevExpress.XtraGrid.Columns.GridColumn colLotID;
        private DevExpress.XtraGrid.Columns.GridColumn colIDCode;
        private DevExpress.XtraGrid.Columns.GridColumn colMFGDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEXPDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInputDate;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraEditors.TextEdit textEditTotalOpenStockQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalOpenStockCarton;
        private DevExpress.XtraEditors.TextEdit textEditTotalReceiptQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalReceiptCarton;
        private DevExpress.XtraEditors.TextEdit textEditTotalIssueQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalIssueCarton;
        private DevExpress.XtraEditors.TextEdit textEditCloseStockQuantity;
        private DevExpress.XtraEditors.TextEdit textEditCloseStockCarton;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
    }
}
