﻿
namespace WarehouseManager
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barHeaderUserInfo = new DevExpress.XtraBars.BarHeaderItem();
            this.barButtonItemVietnam = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEnglish = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLogin = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemChangePass = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAddUser = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemGood = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemGoods = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemWarehouseReceipt = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemIssueOrder = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTallySheet = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemProcessTally = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemProcessInventory = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTools = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu2 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemReceiptRequisition = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemReceiptData = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemIssueRequisition = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemIssueData = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPalletManager = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemColor = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUnit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemBackupRestore = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemMaterial = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemChina = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItemMessage = new DevExpress.XtraBars.BarStaticItem();
            this.ribbonPageSystem = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupUser = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupUserManager = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageCategory = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupGoodsManager = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageWarehouse = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupWarehouseManager = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupReport = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageHelp = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.CaptionBarItemLinks.Add(this.barHeaderUserInfo);
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barHeaderUserInfo,
            this.ribbon.ExpandCollapseItem,
            this.ribbon.SearchEditItem,
            this.barButtonItemVietnam,
            this.barButtonItemEnglish,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItemLogin,
            this.barButtonItemChangePass,
            this.barButtonItemAddUser,
            this.barButtonItemGood,
            this.barButtonItemGoods,
            this.barButtonItemWarehouseReceipt,
            this.barButtonItemIssueOrder,
            this.barButtonItemTallySheet,
            this.barButtonItemProcessTally,
            this.barButtonItemProcessInventory,
            this.barButtonItemTools,
            this.barButtonItemReceiptRequisition,
            this.barButtonItemIssueRequisition,
            this.barButtonItemReceiptData,
            this.barButtonItemIssueData,
            this.barButtonItemReport,
            this.barButtonItemColor,
            this.barButtonItemUnit,
            this.barButtonItemBackupRestore,
            this.barButtonItemMaterial,
            this.barButtonItemPalletManager,
            this.barButtonItemChina,
            this.barStaticItemMessage});
            resources.ApplyResources(this.ribbon, "ribbon");
            this.ribbon.MaxItemId = 38;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageSystem,
            this.ribbonPageCategory,
            this.ribbonPageWarehouse,
            this.ribbonPageHelp});
            this.ribbon.QuickToolbarItemLinks.Add(this.barButtonItemVietnam);
            this.ribbon.QuickToolbarItemLinks.Add(this.barButtonItemEnglish);
            this.ribbon.QuickToolbarItemLinks.Add(this.barButtonItemChina);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // barHeaderUserInfo
            // 
            resources.ApplyResources(this.barHeaderUserInfo, "barHeaderUserInfo");
            this.barHeaderUserInfo.Id = 17;
            this.barHeaderUserInfo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barHeaderUserInfo.ImageOptions.SvgImage")));
            this.barHeaderUserInfo.Name = "barHeaderUserInfo";
            // 
            // barButtonItemVietnam
            // 
            resources.ApplyResources(this.barButtonItemVietnam, "barButtonItemVietnam");
            this.barButtonItemVietnam.Id = 1;
            this.barButtonItemVietnam.ImageOptions.Image = global::WarehouseManager.Properties.Resources.vietnamese;
            this.barButtonItemVietnam.Name = "barButtonItemVietnam";
            this.barButtonItemVietnam.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemVietnam_ItemClick);
            // 
            // barButtonItemEnglish
            // 
            resources.ApplyResources(this.barButtonItemEnglish, "barButtonItemEnglish");
            this.barButtonItemEnglish.Id = 2;
            this.barButtonItemEnglish.ImageOptions.Image = global::WarehouseManager.Properties.Resources.usa;
            this.barButtonItemEnglish.Name = "barButtonItemEnglish";
            this.barButtonItemEnglish.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEnglish_ItemClick);
            // 
            // barButtonItem3
            // 
            resources.ApplyResources(this.barButtonItem3, "barButtonItem3");
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            resources.ApplyResources(this.barButtonItem4, "barButtonItem4");
            this.barButtonItem4.Id = 4;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            resources.ApplyResources(this.barButtonItem5, "barButtonItem5");
            this.barButtonItem5.Id = 5;
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItem6
            // 
            resources.ApplyResources(this.barButtonItem6, "barButtonItem6");
            this.barButtonItem6.Id = 6;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemLogin
            // 
            resources.ApplyResources(this.barButtonItemLogin, "barButtonItemLogin");
            this.barButtonItemLogin.Id = 7;
            this.barButtonItemLogin.ImageOptions.Image = global::WarehouseManager.Properties.Resources.login;
            this.barButtonItemLogin.Name = "barButtonItemLogin";
            this.barButtonItemLogin.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemChangePass
            // 
            resources.ApplyResources(this.barButtonItemChangePass, "barButtonItemChangePass");
            this.barButtonItemChangePass.Id = 10;
            this.barButtonItemChangePass.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemChangePass.ImageOptions.Image")));
            this.barButtonItemChangePass.Name = "barButtonItemChangePass";
            this.barButtonItemChangePass.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemAddUser
            // 
            this.barButtonItemAddUser.AllowHtmlText = DevExpress.Utils.DefaultBoolean.False;
            resources.ApplyResources(this.barButtonItemAddUser, "barButtonItemAddUser");
            this.barButtonItemAddUser.Id = 11;
            this.barButtonItemAddUser.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemAddUser.ImageOptions.Image")));
            this.barButtonItemAddUser.Name = "barButtonItemAddUser";
            this.barButtonItemAddUser.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.barButtonItemAddUser.Tag = "barButtonItemAddUser";
            // 
            // barButtonItemGood
            // 
            resources.ApplyResources(this.barButtonItemGood, "barButtonItemGood");
            this.barButtonItemGood.Id = 12;
            this.barButtonItemGood.ImageOptions.Image = global::WarehouseManager.Properties.Resources.good;
            this.barButtonItemGood.Name = "barButtonItemGood";
            this.barButtonItemGood.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemGoods
            // 
            this.barButtonItemGoods.ActAsDropDown = true;
            this.barButtonItemGoods.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            resources.ApplyResources(this.barButtonItemGoods, "barButtonItemGoods");
            this.barButtonItemGoods.DropDownControl = this.popupMenu1;
            this.barButtonItemGoods.Id = 16;
            this.barButtonItemGoods.ImageOptions.Image = global::WarehouseManager.Properties.Resources.good;
            this.barButtonItemGoods.Name = "barButtonItemGoods";
            this.barButtonItemGoods.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // popupMenu1
            // 
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.Ribbon = this.ribbon;
            // 
            // barButtonItemWarehouseReceipt
            // 
            resources.ApplyResources(this.barButtonItemWarehouseReceipt, "barButtonItemWarehouseReceipt");
            this.barButtonItemWarehouseReceipt.Id = 18;
            this.barButtonItemWarehouseReceipt.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemWarehouseReceipt.ImageOptions.SvgImage")));
            this.barButtonItemWarehouseReceipt.Name = "barButtonItemWarehouseReceipt";
            this.barButtonItemWarehouseReceipt.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.barButtonItemWarehouseReceipt.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemIssueOrder
            // 
            resources.ApplyResources(this.barButtonItemIssueOrder, "barButtonItemIssueOrder");
            this.barButtonItemIssueOrder.Id = 19;
            this.barButtonItemIssueOrder.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemIssueOrder.ImageOptions.SvgImage")));
            this.barButtonItemIssueOrder.Name = "barButtonItemIssueOrder";
            this.barButtonItemIssueOrder.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemTallySheet
            // 
            resources.ApplyResources(this.barButtonItemTallySheet, "barButtonItemTallySheet");
            this.barButtonItemTallySheet.Id = 20;
            this.barButtonItemTallySheet.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemTallySheet.ImageOptions.SvgImage")));
            this.barButtonItemTallySheet.Name = "barButtonItemTallySheet";
            this.barButtonItemTallySheet.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemProcessTally
            // 
            resources.ApplyResources(this.barButtonItemProcessTally, "barButtonItemProcessTally");
            this.barButtonItemProcessTally.Id = 21;
            this.barButtonItemProcessTally.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemProcessTally.ImageOptions.SvgImage")));
            this.barButtonItemProcessTally.Name = "barButtonItemProcessTally";
            this.barButtonItemProcessTally.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemProcessInventory
            // 
            resources.ApplyResources(this.barButtonItemProcessInventory, "barButtonItemProcessInventory");
            this.barButtonItemProcessInventory.Id = 22;
            this.barButtonItemProcessInventory.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemProcessInventory.ImageOptions.SvgImage")));
            this.barButtonItemProcessInventory.Name = "barButtonItemProcessInventory";
            // 
            // barButtonItemTools
            // 
            this.barButtonItemTools.ActAsDropDown = true;
            this.barButtonItemTools.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            resources.ApplyResources(this.barButtonItemTools, "barButtonItemTools");
            this.barButtonItemTools.DropDownControl = this.popupMenu2;
            this.barButtonItemTools.Id = 23;
            this.barButtonItemTools.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemTools.ImageOptions.SvgImage")));
            this.barButtonItemTools.Name = "barButtonItemTools";
            this.barButtonItemTools.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // popupMenu2
            // 
            this.popupMenu2.ItemLinks.Add(this.barButtonItemReceiptRequisition);
            this.popupMenu2.ItemLinks.Add(this.barButtonItemReceiptData);
            this.popupMenu2.Name = "popupMenu2";
            this.popupMenu2.Ribbon = this.ribbon;
            // 
            // barButtonItemReceiptRequisition
            // 
            resources.ApplyResources(this.barButtonItemReceiptRequisition, "barButtonItemReceiptRequisition");
            this.barButtonItemReceiptRequisition.Id = 24;
            this.barButtonItemReceiptRequisition.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemReceiptRequisition.ImageOptions.Image")));
            this.barButtonItemReceiptRequisition.Name = "barButtonItemReceiptRequisition";
            this.barButtonItemReceiptRequisition.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemReceiptData
            // 
            resources.ApplyResources(this.barButtonItemReceiptData, "barButtonItemReceiptData");
            this.barButtonItemReceiptData.Id = 26;
            this.barButtonItemReceiptData.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemReceiptData.ImageOptions.Image")));
            this.barButtonItemReceiptData.Name = "barButtonItemReceiptData";
            this.barButtonItemReceiptData.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemIssueRequisition
            // 
            resources.ApplyResources(this.barButtonItemIssueRequisition, "barButtonItemIssueRequisition");
            this.barButtonItemIssueRequisition.Id = 25;
            this.barButtonItemIssueRequisition.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemIssueRequisition.ImageOptions.Image")));
            this.barButtonItemIssueRequisition.Name = "barButtonItemIssueRequisition";
            this.barButtonItemIssueRequisition.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemIssueData
            // 
            resources.ApplyResources(this.barButtonItemIssueData, "barButtonItemIssueData");
            this.barButtonItemIssueData.Id = 27;
            this.barButtonItemIssueData.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemIssueData.ImageOptions.Image")));
            this.barButtonItemIssueData.Name = "barButtonItemIssueData";
            this.barButtonItemIssueData.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonItemPalletManager
            // 
            resources.ApplyResources(this.barButtonItemPalletManager, "barButtonItemPalletManager");
            this.barButtonItemPalletManager.Id = 35;
            this.barButtonItemPalletManager.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemPalletManager.ImageOptions.SvgImage")));
            this.barButtonItemPalletManager.ImageOptions.SvgImageSize = new System.Drawing.Size(30, 30);
            this.barButtonItemPalletManager.Name = "barButtonItemPalletManager";
            // 
            // barButtonItemReport
            // 
            resources.ApplyResources(this.barButtonItemReport, "barButtonItemReport");
            this.barButtonItemReport.Id = 28;
            this.barButtonItemReport.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemReport.ImageOptions.SvgImage")));
            this.barButtonItemReport.Name = "barButtonItemReport";
            // 
            // barButtonItemColor
            // 
            resources.ApplyResources(this.barButtonItemColor, "barButtonItemColor");
            this.barButtonItemColor.Id = 29;
            this.barButtonItemColor.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemColor.ImageOptions.SvgImage")));
            this.barButtonItemColor.Name = "barButtonItemColor";
            this.barButtonItemColor.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemUnit
            // 
            resources.ApplyResources(this.barButtonItemUnit, "barButtonItemUnit");
            this.barButtonItemUnit.Id = 30;
            this.barButtonItemUnit.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemUnit.ImageOptions.SvgImage")));
            this.barButtonItemUnit.Name = "barButtonItemUnit";
            // 
            // barButtonItemBackupRestore
            // 
            resources.ApplyResources(this.barButtonItemBackupRestore, "barButtonItemBackupRestore");
            this.barButtonItemBackupRestore.Id = 31;
            this.barButtonItemBackupRestore.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemBackupRestore.ImageOptions.SvgImage")));
            this.barButtonItemBackupRestore.Name = "barButtonItemBackupRestore";
            this.barButtonItemBackupRestore.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemMaterial
            // 
            resources.ApplyResources(this.barButtonItemMaterial, "barButtonItemMaterial");
            this.barButtonItemMaterial.Id = 33;
            this.barButtonItemMaterial.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemMaterial.ImageOptions.SvgImage")));
            this.barButtonItemMaterial.Name = "barButtonItemMaterial";
            this.barButtonItemMaterial.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemChina
            // 
            resources.ApplyResources(this.barButtonItemChina, "barButtonItemChina");
            this.barButtonItemChina.Id = 36;
            this.barButtonItemChina.ImageOptions.Image = global::WarehouseManager.Properties.Resources.china;
            this.barButtonItemChina.Name = "barButtonItemChina";
            this.barButtonItemChina.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemChina_ItemClick);
            // 
            // barStaticItemMessage
            // 
            resources.ApplyResources(this.barStaticItemMessage, "barStaticItemMessage");
            this.barStaticItemMessage.Id = 37;
            this.barStaticItemMessage.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barStaticItemMessage.ImageOptions.SvgImage")));
            this.barStaticItemMessage.Name = "barStaticItemMessage";
            this.barStaticItemMessage.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // ribbonPageSystem
            // 
            this.ribbonPageSystem.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupUser,
            this.ribbonPageGroupUserManager});
            this.ribbonPageSystem.Name = "ribbonPageSystem";
            resources.ApplyResources(this.ribbonPageSystem, "ribbonPageSystem");
            // 
            // ribbonPageGroupUser
            // 
            this.ribbonPageGroupUser.ItemLinks.Add(this.barButtonItemLogin);
            this.ribbonPageGroupUser.Name = "ribbonPageGroupUser";
            resources.ApplyResources(this.ribbonPageGroupUser, "ribbonPageGroupUser");
            // 
            // ribbonPageGroupUserManager
            // 
            this.ribbonPageGroupUserManager.ItemLinks.Add(this.barButtonItemAddUser);
            this.ribbonPageGroupUserManager.Name = "ribbonPageGroupUserManager";
            resources.ApplyResources(this.ribbonPageGroupUserManager, "ribbonPageGroupUserManager");
            // 
            // ribbonPageCategory
            // 
            this.ribbonPageCategory.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupGoodsManager});
            this.ribbonPageCategory.Name = "ribbonPageCategory";
            resources.ApplyResources(this.ribbonPageCategory, "ribbonPageCategory");
            // 
            // ribbonPageGroupGoodsManager
            // 
            this.ribbonPageGroupGoodsManager.ItemLinks.Add(this.barButtonItemGoods);
            this.ribbonPageGroupGoodsManager.ItemLinks.Add(this.barButtonItemUnit);
            this.ribbonPageGroupGoodsManager.ItemLinks.Add(this.barButtonItemMaterial);
            this.ribbonPageGroupGoodsManager.ItemLinks.Add(this.barButtonItemColor);
            this.ribbonPageGroupGoodsManager.Name = "ribbonPageGroupGoodsManager";
            resources.ApplyResources(this.ribbonPageGroupGoodsManager, "ribbonPageGroupGoodsManager");
            // 
            // ribbonPageWarehouse
            // 
            this.ribbonPageWarehouse.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupWarehouseManager,
            this.ribbonPageGroupReport});
            this.ribbonPageWarehouse.Name = "ribbonPageWarehouse";
            resources.ApplyResources(this.ribbonPageWarehouse, "ribbonPageWarehouse");
            // 
            // ribbonPageGroupWarehouseManager
            // 
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemPalletManager);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemIssueRequisition);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemIssueData);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemWarehouseReceipt);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemIssueOrder);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemTallySheet);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemProcessTally);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemProcessInventory);
            this.ribbonPageGroupWarehouseManager.ItemLinks.Add(this.barButtonItemTools);
            this.ribbonPageGroupWarehouseManager.Name = "ribbonPageGroupWarehouseManager";
            resources.ApplyResources(this.ribbonPageGroupWarehouseManager, "ribbonPageGroupWarehouseManager");
            // 
            // ribbonPageGroupReport
            // 
            this.ribbonPageGroupReport.ItemLinks.Add(this.barButtonItemReport);
            this.ribbonPageGroupReport.Name = "ribbonPageGroupReport";
            resources.ApplyResources(this.ribbonPageGroupReport, "ribbonPageGroupReport");
            this.ribbonPageGroupReport.Visible = false;
            // 
            // ribbonPageHelp
            // 
            this.ribbonPageHelp.Name = "ribbonPageHelp";
            resources.ApplyResources(this.ribbonPageHelp, "ribbonPageHelp");
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.barStaticItemMessage);
            resources.ApplyResources(this.ribbonStatusBar, "ribbonStatusBar");
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "logout.png");
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemBackupRestore);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            resources.ApplyResources(this.ribbonPageGroup2, "ribbonPageGroup2");
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.xtraTabControl1);
            resources.ApplyResources(this.layoutControl1, "layoutControl1");
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1140, 321, 650, 400);
            this.layoutControl1.Root = this.Root;
            // 
            // xtraTabControl1
            // 
            resources.ApplyResources(this.xtraTabControl1, "xtraTabControl1");
            this.xtraTabControl1.Name = "xtraTabControl1";
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1089, 466);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.xtraTabControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1069, 446);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.IconOptions.Image = global::WarehouseManager.Properties.Resources.logo1;
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageSystem;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUser;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem barButtonItemVietnam;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEnglish;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageCategory;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGoodsManager;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageWarehouse;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupWarehouseManager;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageHelp;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLogin;
        private DevExpress.XtraBars.BarButtonItem barButtonItemChangePass;
        private DevExpress.XtraBars.BarButtonItem barButtonItemAddUser;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUserManager;
        private DevExpress.XtraBars.BarButtonItem barButtonItemGood;
        private DevExpress.XtraBars.BarButtonItem barButtonItemGoods;
        private DevExpress.XtraBars.BarHeaderItem barHeaderUserInfo;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemWarehouseReceipt;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupReport;
        private DevExpress.XtraBars.BarButtonItem barButtonItemIssueOrder;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTallySheet;
        private DevExpress.XtraBars.BarButtonItem barButtonItemProcessTally;
        private DevExpress.XtraBars.BarButtonItem barButtonItemProcessInventory;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTools;
        private DevExpress.XtraBars.PopupMenu popupMenu2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemReceiptRequisition;
        private DevExpress.XtraBars.BarButtonItem barButtonItemReceiptData;
        private DevExpress.XtraBars.BarButtonItem barButtonItemIssueRequisition;
        private DevExpress.XtraBars.BarButtonItem barButtonItemIssueData;
        private DevExpress.XtraBars.BarButtonItem barButtonItemReport;
        private DevExpress.XtraBars.BarButtonItem barButtonItemColor;
        private DevExpress.XtraBars.BarButtonItem barButtonItemUnit;
        private DevExpress.XtraBars.BarButtonItem barButtonItemBackupRestore;
        private DevExpress.XtraBars.BarButtonItem barButtonItemMaterial;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPalletManager;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemChina;
        public DevExpress.XtraBars.BarStaticItem barStaticItemMessage;
    }
}