﻿using DevExpress.XtraCharts;
using DevExpress.XtraReports.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using WarehouseManager.Models;
using WarehouseManager.Repository;

namespace WarehouseManager
{
    public partial class InventoryReportControl : DevExpress.XtraReports.UI.XtraReport
    {
        InventoryRepository inventoryRepository = null;
        InventoryDetailRepository inventoryDetailRepository = null;
        public InventoryReportControl()
        {
            InitializeComponent();
            inventoryRepository = new InventoryRepository();
            inventoryDetailRepository = new InventoryDetailRepository();
            Load();
        }

        private async void Load()
        {
            //Detail1.Report.DataSource= await inventoryRepository.GetAll();
            //Detail2.Report.DataSource = await inventoryDetailRepository.GetAll();

            DataSet dataSet = new DataSet();

            DataTable inventoryTable = new DataTable();
            inventoryTable.Columns.Add("Year", typeof(Int16));
            inventoryTable.Columns.Add("Month", typeof(Int16));
            inventoryTable.Columns.Add("WarehouseID", typeof(string));
            inventoryTable.Columns.Add("PartNumber", typeof(string));
            inventoryTable.Columns.Add("OpeningStockQuantity", typeof(decimal));
            inventoryTable.Columns.Add("ReceiptQuantity", typeof(decimal));
            inventoryTable.Columns.Add("IssueQuantity", typeof(decimal));
            inventoryTable.Columns.Add("ClosingStockQuantity", typeof(decimal));
            inventoryTable.Columns.Add("PackingVolume", typeof(decimal));
            List<Inventory> inventories = await inventoryRepository.GetAll();
            foreach (Inventory inventory in inventories)
            {
                inventoryTable.Rows.Add(new object[]
                {
                    inventory.Year,
                    inventory.Month,
                    inventory.WarehouseID,
                    inventory.PartNumber,
                    inventory.OpeningStockQuantity,
                    inventory.ReceiptQuantity,
                    inventory.IssueQuantity,
                    inventory.ClosingStockQuantity,
                    inventory.PackingVolume,
                });
            }
            inventoryTable.TableName = "Inventory";

            DataTable inventoryDetailTable = new DataTable();
            inventoryDetailTable.Columns.Add("PartNumber", typeof(string));
            inventoryDetailTable.Columns.Add("PalletID", typeof(string));
            inventoryDetailTable.Columns.Add("LotID", typeof(string));
            inventoryDetailTable.Columns.Add("IDCode", typeof(string));
            inventoryDetailTable.Columns.Add("Quantity", typeof(decimal));

            List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetAll();
            foreach (InventoryDetail inventoryDetail in inventoryDetails)
            {
                inventoryDetailTable.Rows.Add(new object[]
                {
                    inventoryDetail.PartNumber,
                    inventoryDetail.PalletID,
                    inventoryDetail.LotID,
                    inventoryDetail.IDCode,
                    inventoryDetail.Quantity,
                });
            }
            inventoryDetailTable.TableName = "InventoryDetail";

            dataSet.Tables.Add(inventoryTable);
            dataSet.Tables.Add(inventoryDetailTable);
            dataSet.Relations.Add("InventoryRelationship", inventoryTable.Columns["PartNumber"], inventoryDetailTable.Columns["PartNumber"]);
            Detail1.Report.DataSource = dataSet;
            Detail.Report.DataSource = dataSet;
            ReportFooter.Report.DataSource = dataSet;
            
            Detail2.Report.DataMember = "InventoryRelationship";

            xrChart1.DataSource = dataSet;
            ChartTitle chartTitle = new ChartTitle();
            chartTitle.Text = "Biểu đồ tồn kho";
            xrChart1.Titles.Add(chartTitle);
            Series series1 = new Series("OpeningStockQuantity", ViewType.Bar);
            Series series2 = new Series("ClosingStockQuantity", ViewType.Bar);
            series1.SetDataMembers("PartNumber", "OpeningStockQuantity");
            series2.SetDataMembers("PartNumber", "ClosingStockQuantity");
            xrChart1.Series.Add(series1);
            xrChart1.Series.Add(series2);
            //xrChart1.SeriesTemplate.SeriesDataMember = "OpeningStockQuantity";
            //xrChart1.SeriesTemplate.SeriesDataMember = "ClosingStockQuantity";
            //xrChart1.SeriesTemplate.ArgumentDataMember = "PartNumber";
            //xrChart1.SeriesTemplate.ValueDataMembers.AddRange("ClosingStockQuantity", "OpeningStockQuantity");
            XYDiagram diagram = xrChart1.Diagram as XYDiagram;
            if (diagram != null)
            {
                // Access diagram properties, for example rotate the diagram.
                diagram.Rotated = true;
                // Access properties of objects that belong to the diagram, 
                // for example axes and panes.
                diagram.AxisY.Title.Text = "Population mid-year, millions";
                diagram.DefaultPane.Weight = 2;
            }
        }

    }
}
