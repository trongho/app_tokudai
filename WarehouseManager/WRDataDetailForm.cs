﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;

namespace WarehouseManager
{
    public partial class WRDataDetailForm : DevExpress.XtraEditors.XtraForm
    {
        internal ReceiptDataControl receiptDataControl;
        WRDataGeneralRepository wRDataGeneralRepository = null;

        public decimal quantityByPack
        {
            get
            {
                return decimal.Parse(textEditQuantityByPack.Text);
            }
            set
            {
                textEditQuantityByPack.Text = value+"";
            }
        }

        public string palletID
        {
            get
            {
                return textEditPalletID.Text;
            }
            set
            {
                textEditPalletID.Text = value;
            }
        }

        public int ordinal
        {
            get
            {
                return int.Parse(textEditOrdinal.Text);
            }
            set
            {
                textEditOrdinal.Text = value+"";
            }
        }

        public string wrdNumber
        {
            get
            {
                return textEditWRDNumber.Text;
            }
            set
            {
                textEditWRDNumber.Text = value;
            }
        }


        public WRDataDetailForm(ReceiptDataControl _receiptDataControl)
        {
            
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            receiptDataControl = _receiptDataControl;

           

            wRDataGeneralRepository = new WRDataGeneralRepository();

            loadGridData();
            
        }

        private async void loadGridData()
        {

            List<WRDataGeneral> wRDataGenerals = await wRDataGeneralRepository.GetUnderParams(wrdNumber, palletID,5);
            if (wRDataGenerals.Count > 0)
            {
                string[] idcodes = wRDataGenerals[0].IDCode.Split(',');
                List<WRDataDetail> wRDataDetails = new List<WRDataDetail>();

                foreach (string s in idcodes)
                {
                    WRDataDetail wRDataDetail = new WRDataDetail();
                    wRDataDetail.IDCode = s;
                    wRDataDetails.Add(wRDataDetail);
                }
                wRDataDetails.Remove(wRDataDetails[wRDataDetails.Count - 1]);
                gridControl1.DataSource = wRDataDetails;
            }
        }
    }
}