﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class WRDataDetailRepository
    {
        public async Task<List<WRDataDetail>> GetByWRDNumberAndPalletID(String wRDNumber, String palletID)
        {
            string URI = Constant.BaseURL + "wrdatadetail/GetByWRDNumberAndPalletID/" + wRDNumber + "/" + palletID;
            List<WRDataDetail> entrys = new List<WRDataDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRDataDetail>> GetByID(String wRDNumber)
        {
            string URI = Constant.BaseURL + "wrdatadetail/GetByID/" + wRDNumber;
            List<WRDataDetail> entrys = new List<WRDataDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }
    }
}
