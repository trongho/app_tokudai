﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class ReportRepository
    {
        public ReportRepository()
        {

        }

        public async Task<List<Report>> getAllReportAsync()
        {
            string URI = Constant.BaseURL+"report";
            List<Report> reports = new List<Report>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            reports = JsonConvert.DeserializeObject<Report[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            foreach (Report report in reports)
            {
                report.ViewRight = 0;
                report.PrintRight = 0;
            }
            return reports;
        }
    }
}
