﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class GoodsRepository
    {
        public GoodsRepository()
        {
        }

        public async Task<List<Goods>> getAll()
        {
            string URI = Constant.BaseURL+"goods";
            List<Goods> list = new List<Goods>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            list = JsonConvert.DeserializeObject<Goods[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }          
            return list;
        }

        public async Task<Goods> GetUnderID(String partNumber,String palletID)
        {
            string URI = Constant.BaseURL + "goods/GetByParams/" + partNumber+"/"+palletID;
            List<Goods> entrys = new List<Goods>();
            Goods goods = new Goods();

            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Goods[]>(fileJsonString).ToList();
                            if (entrys.Count>0)
                            {
                                goods = entrys[0];
                            }
                            else
                            {
                                goods = null;
                            }
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }

            return goods;
        }

        public async Task<List<Goods>> GetUnderID(String palletID)
        {
            string URI = Constant.BaseURL + "goods/GetByPalletID/" + palletID;
            List<Goods> entrys = new List<Goods>();

            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Goods[]>(fileJsonString).ToList();
                           
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }

            return entrys;
        }
        public async Task<List<Goods>> GetUnderPartNumber(String partNumber)
        {
            string URI = Constant.BaseURL + "goods/GetByPartNumber/" +partNumber;
            List<Goods> entrys = new List<Goods>();

            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Goods[]>(fileJsonString).ToList();

                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }

            return entrys;
        }



        public async Task<long> Create(Goods entry)
        {
            long result = -1;
            string URI = Constant.BaseURL + "goods/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<long> Update(Goods entry, String partNumber,String palletID)
        {
            long result = -1;
            string URI = Constant.BaseURL + "goods/Put/" +partNumber+"/"+palletID;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String partNumber,String palletID)
        {
            long result = -1;
            string URI = Constant.BaseURL + "goods/Delete/" +partNumber+"/"+palletID;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete()
        {
            long result = -1;
            string URI = Constant.BaseURL + "goods/Delete";
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }
    }

}
