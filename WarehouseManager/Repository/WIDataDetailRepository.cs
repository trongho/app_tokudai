﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class WIDataDetailRepository
    {
        public WIDataDetailRepository()
        {
        }

        public async Task<long> Create(WIDataDetail entry)
        {
            long result = -1;
            string URI = Constant.BaseURL + "widatadetail/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<WIDataDetail>> GetUnderID(String id)
        {
            string URI = Constant.BaseURL + "widatadetail/GetByID/" + id;
            List<WIDataDetail> entrys = new List<WIDataDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<WIDataDetail> GetByParams3(String wIDNumber, String partNumber, int ordinal)
        {
            string URI = Constant.BaseURL + "widatadetail/GetByParams3/" + wIDNumber + "/" + partNumber + "/" + ordinal;
            List<WIDataDetail> entrys = new List<WIDataDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys[0];
        }

        public async Task<WIDataDetail> GetByParams5(String wIDNumber, String partNumber, int ordinal, String lotID, String idCode)
        {
            string URI = Constant.BaseURL + "widatadetail/GetByParams5/" + wIDNumber + "/" + partNumber + "/" + ordinal + "/" + lotID + "/" + idCode;
            List<WIDataDetail> entrys = new List<WIDataDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys[0];
        }

        public async Task<long> Update(WIDataDetail entry, String wIDNumber, String partNumber, int ordinal, String lotID, String idCode)
        {
            long result = -1;
            string URI = Constant.BaseURL + "widatadetail/Put/" + wIDNumber + "/" + partNumber + "/" + ordinal + "/" + lotID + "/" + idCode;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String id)
        {
            long result = -1;
            string URI = Constant.BaseURL + "widatadetail/Delete/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String widNumber, String partNumber, int ordinal, String lotID, String idCode)
        {
            long result = -1;
            string URI = Constant.BaseURL + "widatadetail/DeleteByParams5/" + widNumber + "/" + partNumber + "/" + ordinal + "/" + lotID + "/" + idCode;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }
    }
}
