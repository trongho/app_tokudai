﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class IODetailRepository
    {
        public IODetailRepository()
        {
        }
        public async Task<long> Create(IODetail entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"iodetail/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }


        public async Task<List<IODetail>> GetUnderID(String id)
        {
            string URI = Constant.BaseURL+"iodetail/GetByID/" + id;
            List<IODetail> entrys = new List<IODetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<IODetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<IODetail>> GetByParams3(String IONumber,String palletID, String partNumber)
        {
            string URI = Constant.BaseURL+"iodetail/GetByParams3/" + IONumber + "/" +palletID + "/" +partNumber;
            List<IODetail> entrys = new List<IODetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<IODetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<long> Update(IODetail entry, String IONumber,String palletID, String partNumber, int ordinal)
        {
            long result = -1;
            string URI = Constant.BaseURL+"iodetail/Put/" + IONumber+"/"+palletID + "/" + partNumber + "/" + ordinal;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"iodetail/Delete/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String ioNumber,String palletID,String partNumber,int ordinal)
        {
            long result = -1;
            string URI = Constant.BaseURL + "iodetail/DeleteByParams4/" +ioNumber+"/"+palletID+"/"+partNumber+"/"+ordinal;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }
    }
}
