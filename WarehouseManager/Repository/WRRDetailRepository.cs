﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using webservice.Entities;

namespace WarehouseManager.Repository
{
    class WRRDetailRepository
    {
        public WRRDetailRepository()
        {
        }

        public async Task<long> Create(WRRDetail entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wrrdetail/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<WRRDetail>> GetAll()
        {
            string URI = Constant.BaseURL+"wrrdetail";
            List<WRRDetail> entrys = new List<WRRDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRRDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRRDetail>> GetUnderID(String id)
        {
            string URI = Constant.BaseURL+"wrrdetail/GetByID/" + id;
            List<WRRDetail> entrys = new List<WRRDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRRDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRRDetail>> GetUnderParams(String wRRNumber,String palletID,int ordinal)
        {
            string URI = Constant.BaseURL+"wrrdetail/GetByParams/" + wRRNumber+"/"+palletID+"/"+ordinal;
            List<WRRDetail> entrys = new List<WRRDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRRDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRRDetail>> GetByWRRNumberAndPalletID(String wRRNumber, String palletID)
        {
            string URI = Constant.BaseURL + "wrrdetail/GetByWRRNumberAndPalletID/" + wRRNumber + "/" + palletID;
            List<WRRDetail> entrys = new List<WRRDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRRDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRRDetail>> GetByPalletIDAndLotID(String wRRNumber, String palletID,string lotID)
        {
            string URI = Constant.BaseURL + "wrrdetail/GetByPalletIDAndLotID/" + wRRNumber + "/" + palletID + "/" +lotID;
            List<WRRDetail> entrys = new List<WRRDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRRDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<long> Update(WRRDetail entry, String wRRNumber, String palletID, int ordinal,String lotID)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wrrdetail/Put/"  +wRRNumber + "/" + palletID + "/" + ordinal+"/"+lotID;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wrrdetail/Delete/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete3(String wrrNumber,String palletID,int ordinal)
        {
            long result = -1;
            string URI = Constant.BaseURL + "wrrdetail/Delete3/" +wrrNumber+"/"+palletID+"/"+ordinal;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }
    }
}
