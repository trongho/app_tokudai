﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class InventoryOfLastMonthDetailRepository
    {
        public async Task<long> Create(InventoryOfLastMonthDetail entry)
        {
            long result = -1;
            string URI = Constant.BaseURL + "inventoryoflastmonthdetail/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<InventoryOfLastMonthDetail>> GetAll()
        {
            string URI = Constant.BaseURL + "InventoryOfLastMonthDetail";
            List<InventoryOfLastMonthDetail> entrys = new List<InventoryOfLastMonthDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<InventoryOfLastMonthDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }
        public async Task<List<InventoryOfLastMonthDetail>> GetByParams3(Int16 year, Int16 month, String warehouseID)
        {
            string URI = Constant.BaseURL + "inventoryoflastmonthdetail/GetByParams3/" + year + "/" + month + "/" + warehouseID;
            List<InventoryOfLastMonthDetail> entrys = new List<InventoryOfLastMonthDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<InventoryOfLastMonthDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }


        public async Task<List<InventoryOfLastMonthDetail>> GetByParams4(Int16 year, Int16 month, String warehouseID, String partNumber)
        {
            string URI = Constant.BaseURL + "inventoryoflastmonthdetail/GetByParams4/" + year + "/" + month + "/" + warehouseID + "/" + partNumber;
            List<InventoryOfLastMonthDetail> entrys = new List<InventoryOfLastMonthDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<InventoryOfLastMonthDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<InventoryOfLastMonthDetail>> GetByParams5(Int16 year, Int16 month, String warehouseID, String partNumber, String palletID)
        {
            string URI = Constant.BaseURL + "inventoryoflastmonthdetail/GetByParams5/" + year + "/" + month + "/" + warehouseID + "/" + partNumber + "/" + palletID;
            List<InventoryOfLastMonthDetail> entrys = new List<InventoryOfLastMonthDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<InventoryOfLastMonthDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<InventoryOfLastMonthDetail>> GetByParams6(Int16 year, Int16 month, String warehouseID, String partNumber, String palletID, String IDCode)
        {
            string URI = Constant.BaseURL + "inventoryoflastmonthdetail/GetByParams6/" + year + "/" + month + "/" + warehouseID + "/" + partNumber + "/" + palletID + "/" + IDCode;
            List<InventoryOfLastMonthDetail> entrys = new List<InventoryOfLastMonthDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<InventoryOfLastMonthDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<InventoryOfLastMonthDetail>> GetByParams62(Int16 year, Int16 month, String warehouseID, String partNumber, String palletID, String LotID)
        {
            string URI = Constant.BaseURL + "inventoryoflastmonthdetail/GetByParams62/" + year + "/" + month + "/" + warehouseID + "/" + partNumber + "/" + palletID + "/" + LotID;
            List<InventoryOfLastMonthDetail> entrys = new List<InventoryOfLastMonthDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<InventoryOfLastMonthDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<long> Update(InventoryOfLastMonthDetail entry, Int16 year, Int16 month, String warehouseID, String PartNumber, String palletID)
        {
            long result = -1;
            string URI = Constant.BaseURL + "inventoryoflastmonthdetail/Put/" + year + "/" + month + "/" + warehouseID + "/" + PartNumber + "/" + palletID;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete3(Int16 year, Int16 month, String warehouseID)
        {
            long result = -1;
            string URI = Constant.BaseURL + "inventoryoflastmonthdetail/Delete3/" + year + "/" + month + "/" + warehouseID;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete4(Int16 year, Int16 month, String warehouseID, String partNumber)
        {
            long result = -1;
            string URI = Constant.BaseURL + "inventoryoflastmonthdetail/Delete4/" + year + "/" + month + "/" + warehouseID + "/" + partNumber;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete5(Int16 year, Int16 month, String warehouseID, String partNumber, String palletID)
        {
            long result = -1;
            string URI = Constant.BaseURL + "inventoryoflastmonthdetail/Delete5/" + year + "/" + month + "/" + warehouseID + "/" + partNumber + "/" + palletID;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete6(Int16 year, Int16 month, String warehouseID, String partNumber, String palletID, String IDCode)
        {
            long result = -1;
            string URI = Constant.BaseURL + "inventoryoflastmonthdetail/Delete6/" + year + "/" + month + "/" + warehouseID + "/" + partNumber + "/" + palletID + "/" + IDCode;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> DeleteOldDate(String warehouseID)
        {
            long result = -1;
            string URI = Constant.BaseURL + "inventoryoflastmonthdetail/DeleteOldDate/" + warehouseID;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }
    }
}
