﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class LabelPrintRepository
    {

        public async Task<List<LabelPrint>> GetUnderID(string wrrNumber, string palletID, string partNumber, string lotID,string idCode)
        {
            string URI = Constant.BaseURL + "labelprint/GetByParams6/" + wrrNumber + "/" + palletID + "/" + partNumber + "/" + lotID+"/"+idCode;
            List<LabelPrint> entrys = new List<LabelPrint>();

            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<LabelPrint[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }

            return entrys;
        }

        public async Task<List<LabelPrint>> GetUnderID(string wrrNumber, string palletID, string partNumber, string lotID)
        {
            string URI = Constant.BaseURL + "labelprint/GetByParams5/" + wrrNumber + "/" + palletID + "/" + partNumber + "/" + lotID;
            List<LabelPrint> entrys = new List<LabelPrint>();

            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<LabelPrint[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }

            return entrys;
        }

        public async Task<List<LabelPrint>> GetUnderID(string wrrNumber)
        {
            string URI = Constant.BaseURL + "labelprint/GetByWRRNumber/" + wrrNumber;
            List<LabelPrint> entrys = new List<LabelPrint>();

            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<LabelPrint[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }

            return entrys;
        }

        public async Task<long> Create(LabelPrint entry)
        {
            long result = -1;
            string URI = Constant.BaseURL + "labelprint/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<long> Update(LabelPrint entry, string wrrNumber, string palletID, string partNumber, string lotID, string idCode)
        {
            long result = -1;
            string URI = Constant.BaseURL + "labelprint/Put/" + wrrNumber+"/"+palletID+"/"+partNumber+"/"+lotID+"/"+idCode;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(string wrrNumber, string palletID, string partNumber, string lotID)
        {
            long result = -1;
            string URI = Constant.BaseURL + "labelprint/Delete/" +wrrNumber+"/"+palletID+"/"+partNumber+"/"+lotID;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }
    }
}
