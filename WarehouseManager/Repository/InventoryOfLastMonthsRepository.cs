﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class InventoryOfLastMonthsRepository
    {
        public InventoryOfLastMonthsRepository()
        {
        }

        public async Task<long> Create(InventoryOfLastMonths entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"inventoryoflastmonths/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<InventoryOfLastMonths>> GetAll()
        {
            string URI = Constant.BaseURL+"inventoryoflastmonths";
            List<InventoryOfLastMonths> entrys = new List<InventoryOfLastMonths>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<InventoryOfLastMonths[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<InventoryOfLastMonths>> GetUnderMultiID(Int16 year, Int16 month, String warehouseID)
        {
            string URI = Constant.BaseURL+"inventoryoflastmonths/GetByMultiID/" + year + "/" + month + "/" + warehouseID;
            List<InventoryOfLastMonths> entrys = new List<InventoryOfLastMonths>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<InventoryOfLastMonths[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<InventoryOfLastMonths>> GetUnderMultiPartNumber(Int16 year, Int16 month, String warehouseID, String PartNumber)
        {
            string URI = Constant.BaseURL+"inventoryoflastmonths/GetByMultiPartNumber/" + year + "/" + month + "/" + warehouseID + "/" + PartNumber;
            List<InventoryOfLastMonths> entrys = new List<InventoryOfLastMonths>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<InventoryOfLastMonths[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<InventoryOfLastMonths>> GetByPartNumber(string warehouseID, string partNumber)
        {
            string URI = Constant.BaseURL + "inventoryoflastmonths/GetByPartNumber/" + warehouseID + "/" + partNumber;
            List<InventoryOfLastMonths> entrys = new List<InventoryOfLastMonths>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<InventoryOfLastMonths[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<long> Update(InventoryOfLastMonths entry, Int16 year, Int16 month, String warehouseID, String PartNumber)
        {
            long result = -1;
            string URI = Constant.BaseURL+"inventoryoflastmonths/Put/" + year + "/" + month + "/" + warehouseID + "/" + PartNumber;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(Int16 year, Int16 month, String warehouseID)
        {
            long result = -1;
            string URI = Constant.BaseURL+"inventoryoflastmonths/Delete/" + year + "/" + month + "/" + warehouseID;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }
        public async Task<long> Delete()
        {
            long result = -1;
            string URI = Constant.BaseURL + "inventoryoflastmonths/DeleteAll";
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> DeleteOldDate(String warehouseID)
        {
            long result = -1;
            string URI = Constant.BaseURL + "inventoryoflastmonths/DeleteOldDate/" + warehouseID;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }


        public async Task<Boolean> checkExistAsync(Int16 year, Int16 month, String warehouseID)
        {
            Boolean result = false;
            string URI = Constant.BaseURL+"inventoryoflastmonths/CheckExist/" + year + "/" + month + "/" + warehouseID;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<Boolean> checkExistAsync(Int16 year, Int16 month, String warehouseID, String PartNumber)
        {
            Boolean result = false;
            string URI = Constant.BaseURL+"inventoryoflastmonths/CheckExistWithGoods/" + year + "/" + month + "/" + warehouseID + "/" + PartNumber;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }
    }
}
