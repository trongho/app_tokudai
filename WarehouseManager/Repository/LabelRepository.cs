﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class LabelRepository
    {
        public async Task<List<Label>> getAll()
        {
            string URI = Constant.BaseURL + "label";
            List<Label> list = new List<Label>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            list = JsonConvert.DeserializeObject<Label[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return list;
        }

        public async Task<Label> GetUnderID(String palletID)
        {
            string URI = Constant.BaseURL + "label/GetByParams/"+ palletID;
            List<Label> entrys = new List<Label>();
            Label Label = new Label();

            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Label[]>(fileJsonString).ToList();
                            if (entrys.Count>0)
                            {
                                Label = entrys[0];
                            }
                            else
                            {
                                Label = null;
                            }
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }

            return Label;
        }


        public async Task<long> Create(Label entry)
        {
            long result = -1;
            string URI = Constant.BaseURL + "label/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<long> Update(Label entry,String palletID)
        {
            long result = -1;
            string URI = Constant.BaseURL + "label/Put/"+ palletID;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String palletID)
        {
            long result = -1;
            string URI = Constant.BaseURL + "label/Delete/"+palletID;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete()
        {
            long result = -1;
            string URI = Constant.BaseURL + "label/Delete";
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }
    }
}
