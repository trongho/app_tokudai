﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;
using webservice.Entities;

namespace WarehouseManager.Repository
{
    class WRDataGeneralRepository
    {
        public WRDataGeneralRepository()
        {
        }

        public async Task<long> Create(WRDataGeneral entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wrdatageneral/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<WRDataGeneral>> GetAll()
        {
            string URI = Constant.BaseURL+"wrdatageneral";
            List<WRDataGeneral> entrys = new List<WRDataGeneral>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataGeneral[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRDataGeneral>> GetUnderID(String id)
        {
            string URI = Constant.BaseURL+"wrdatageneral/GetByID/" + id;
            List<WRDataGeneral> entrys = new List<WRDataGeneral>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataGeneral[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRDataGeneral>> GetUnderParams(String wRDNumber, String palletID, int ordinal)
        {
            string URI = Constant.BaseURL+"wrdatageneral/GetByParams/" + wRDNumber + "/" +palletID + "/" + ordinal;
            List<WRDataGeneral> entrys = new List<WRDataGeneral>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataGeneral[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }
        public async Task<List<WRDataGeneral>> GetByParams4(String wRDNumber, String palletID, int ordinal,String lotID)
        {
            string URI = Constant.BaseURL + "wrdatageneral/GetByParams4/" + wRDNumber + "/" + palletID + "/" + ordinal+"/"+lotID;
            List<WRDataGeneral> entrys = new List<WRDataGeneral>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataGeneral[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }



        public async Task<long> Update(WRDataGeneral entry, String wRDNumber,String palletID,int ordinal,String lotID)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wrdatageneral/Put/" + wRDNumber + "/"+palletID + "/" + ordinal+"/"+lotID;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wrdatageneral/Delete/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }
    }
}
