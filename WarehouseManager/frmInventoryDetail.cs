﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;

namespace WarehouseManager
{
    public partial class frmInventoryDetail : DevExpress.XtraEditors.XtraForm
    {
        InventoryDetailRepository inventoryDetailRepository = null;
        InventoryOfLastMonthDetailRepository InventoryOfLastMonthDetailRepository = null;
        GridColumn currentColumn;
        public string partNumber
        {
            get
            {
                return textEditPartNumber.Text;
            }
            set
            {
                textEditPartNumber.Text = value;
            }
        }

        public string warehouseID
        {
            get
            {
                return textEditWarehouseID.Text;
            }
            set
            {
                textEditWarehouseID.Text = value;
            }
        }

        public Int16 year
        {
            get
            {
                return Int16.Parse(textEditYear.Text);
            }
            set
            {
                textEditYear.Text = value + "";
            }
        }

        public Int16 month
        {
            get
            {
                return Int16.Parse(textEditMonth.Text);
            }
            set
            {
                textEditMonth.Text = value + "";
            }
        }
        public frmInventoryDetail()
        {
            InitializeComponent();
            inventoryDetailRepository = new InventoryDetailRepository();
            InventoryOfLastMonthDetailRepository = new InventoryOfLastMonthDetailRepository();
            this.Load += frmInventoryDetail_Load;

        }

        private void frmInventoryDetail_Load(Object sender, EventArgs e)
        {
            popupMenuPalletID();
            popupMenuLotID();
            loadDataGrid();  
        }

        private async void popupMenuPalletID()
        {
            List<InventoryDetail> inventoryDetails= await inventoryDetailRepository.GetByParams4(year, month, warehouseID, partNumber);
            var querys= inventoryDetails.GroupBy(x => new { x.PartNumber,x.PalletID,})
                         .Select(x => new InventoryDetail
                         {
                             PartNumber=x.Key.PartNumber,
                             PalletID=x.Key.PalletID,
                         })
                        .ToList();
            comboBoxEditPalletID.Properties.Items.Add("<>");
            foreach (InventoryDetail inventoryDetail in querys)
            {
                comboBoxEditPalletID.Properties.Items.Add(inventoryDetail.PalletID);
            }
            comboBoxEditPalletID.SelectedIndex =0;
        }

        private async void popupMenuLotID()
        {
            List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams4(year, month, warehouseID, partNumber);
            var querys = inventoryDetails.GroupBy(x => new { x.PartNumber, x.LotID, })
                         .Select(x => new InventoryDetail
                         {
                             PartNumber = x.Key.PartNumber,
                             LotID = x.Key.LotID,
                         })
                        .ToList();
            comboBoxEditLotID.Properties.Items.Add("<>");
            foreach (InventoryDetail inventoryDetail in querys)
            {
                comboBoxEditLotID.Properties.Items.Add(inventoryDetail.LotID);
            }
            comboBoxEditLotID.SelectedIndex = 0;
        }

        private void getTotalQuantity()
        {
            Decimal totalQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridView1.GetRowHandle(i);
                if (gridView1.GetRowCellValue(rowHandle, "PalletID") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridView1.GetRowCellValue(rowHandle, "Quantity");
                totalQuantity += quantity;
                i++;
            }
            textEditTotalQuantity.Text = totalQuantity.ToString("0.#####");
        }

        private async void loadDataGrid()
        {
            List<InventoryDetail> inventoryDetails= await inventoryDetailRepository.GetByParams4(year, month, warehouseID, partNumber);
            List<InventoryOfLastMonthDetail> inventoryOfLastMonthDetails = await InventoryOfLastMonthDetailRepository.GetByParams4(year, month, warehouseID, partNumber);
            if (inventoryDetails.Count >0)
            {
                var querys = inventoryDetails.GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID, x.IDCode })
                      .Select(x => new InventoryDetail
                      {
                          PartNumber = x.Key.PartNumber,
                          PalletID = x.Key.PalletID,
                          LotID = x.Key.LotID,
                          Year = x.First().Year,
                          Month = x.First().Month,
                          WarehouseID = x.First().WarehouseID,
                          Quantity = x.First().Quantity,
                          Status = x.First().Status,
                          IDCode = x.First().IDCode,
                          MFGDate = x.First().MFGDate,
                          EXPDate = x.First().EXPDate,
                          InputDate = x.First().InputDate
                      })
                     .ToList();
                gridControl1.DataSource = querys;
            }
            else
            {
                var querys = inventoryOfLastMonthDetails.GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID, x.IDCode })
                     .Select(x => new InventoryOfLastMonthDetail
                     {
                         PartNumber = x.Key.PartNumber,
                         PalletID = x.Key.PalletID,
                         LotID = x.Key.LotID,
                         Year = x.First().Year,
                         Month = x.First().Month,
                         WarehouseID = x.First().WarehouseID,
                         Quantity = x.First().Quantity,
                         Status = x.First().Status,
                         IDCode = x.First().IDCode,
                         MFGDate = x.First().MFGDate,
                         EXPDate = x.First().EXPDate,
                         InputDate = x.First().InputDate
                     })
                    .ToList();
                gridControl1.DataSource = querys;
            }
            
            getTotalQuantity();
        }

        private async void loadDataGrid(string palletID,string lotID)
        {
            List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams4(year, month, warehouseID, partNumber);
           

            if (comboBoxEditPalletID.SelectedIndex == 0&&comboBoxEditLotID.SelectedIndex==0)
            {
                var querys = inventoryDetails.GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID, x.IDCode })
                      .Select(x => new InventoryDetail
                      {
                          PartNumber = x.Key.PartNumber,
                          PalletID = x.Key.PalletID,
                          LotID = x.Key.LotID,
                          Year = x.First().Year,
                          Month = x.First().Month,
                          WarehouseID = x.First().WarehouseID,
                          Quantity = x.First().Quantity,
                          Status = x.First().Status,
                          IDCode = x.First().IDCode,
                          MFGDate = x.First().MFGDate,
                          EXPDate = x.First().EXPDate,
                          InputDate = x.First().InputDate
                      })
                     .ToList();
                gridControl1.DataSource = querys;
            }
            else if(comboBoxEditPalletID.SelectedIndex == 0 && comboBoxEditLotID.SelectedIndex != 0)
            {
                var querys = inventoryDetails.Where(a => a.LotID.Equals(lotID)).GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID, x.IDCode })
                      .Select(x => new InventoryDetail
                      {
                          PartNumber = x.Key.PartNumber,
                          PalletID = x.Key.PalletID,
                          LotID = x.Key.LotID,
                          Year = x.First().Year,
                          Month = x.First().Month,
                          WarehouseID = x.First().WarehouseID,
                          Quantity = x.First().Quantity,
                          Status = x.First().Status,
                          IDCode = x.First().IDCode,
                          MFGDate = x.First().MFGDate,
                          EXPDate = x.First().EXPDate,
                          InputDate = x.First().InputDate
                      })
                     .ToList();
                gridControl1.DataSource = querys;
            }
            else if (comboBoxEditPalletID.SelectedIndex != 0 && comboBoxEditLotID.SelectedIndex == 0)
            {
                var querys = inventoryDetails.Where(a => a.PalletID.Equals(palletID)).GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID, x.IDCode })
                      .Select(x => new InventoryDetail
                      {
                          PartNumber = x.Key.PartNumber,
                          PalletID = x.Key.PalletID,
                          LotID = x.Key.LotID,
                          Year = x.First().Year,
                          Month = x.First().Month,
                          WarehouseID = x.First().WarehouseID,
                          Quantity = x.First().Quantity,
                          Status = x.First().Status,
                          IDCode = x.First().IDCode,
                          MFGDate = x.First().MFGDate,
                          EXPDate = x.First().EXPDate,
                          InputDate = x.First().InputDate
                      })
                     .ToList();
                gridControl1.DataSource = querys;
            }
            else
            {
                var querys = inventoryDetails.Where(a => a.PalletID.Equals(palletID) && a.LotID.Equals(lotID)).GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID, x.IDCode })
                      .Select(x => new InventoryDetail
                      {
                          PartNumber = x.Key.PartNumber,
                          PalletID = x.Key.PalletID,
                          LotID = x.Key.LotID,
                          Year = x.First().Year,
                          Month = x.First().Month,
                          WarehouseID = x.First().WarehouseID,
                          Quantity = x.First().Quantity,
                          Status = x.First().Status,
                          IDCode = x.First().IDCode,
                          MFGDate = x.First().MFGDate,
                          EXPDate = x.First().EXPDate,
                          InputDate = x.First().InputDate
                      })
                     .ToList();
                gridControl1.DataSource = querys;
            }
            
            getTotalQuantity();
        }

        private void comboBoxEditPalletID_SelectedValueChanged(object sender, EventArgs e)
        {
            EnumConverter ec = TypeDescriptor.GetConverter(typeof(FontStyle)) as EnumConverter;
            var cBox = sender as ComboBoxEdit;
            if (cBox.SelectedIndex != -1)
            {
                string palletID = cBox.SelectedText;
                loadDataGrid(palletID,comboBoxEditLotID.Text);
            }
        }

        private void comboBoxEditLotID_SelectedValueChanged(object sender, EventArgs e)
        {
            EnumConverter ec = TypeDescriptor.GetConverter(typeof(FontStyle)) as EnumConverter;
            var cBox = sender as ComboBoxEdit;
            if (cBox.SelectedIndex != -1)
            {
                string lotID = cBox.SelectedText;
                loadDataGrid(comboBoxEditPalletID.Text, lotID);
            }
        }

        private void simpleButtonPrint_Click(object sender, EventArgs e)
        {
            ReportPrintTool reportPrintTool = new ReportPrintTool(new InventoryDetailReportControl(partNumber));
            reportPrintTool.ShowPreviewDialog();
        }
    }
}