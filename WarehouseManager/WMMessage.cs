﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Models;

namespace WarehouseManager
{
    class WMMessage
    {
        public static Boolean newButtonClick = false;
        public static Boolean saveButtonClick = false;
        public static Boolean deleteButtonClick = false;
        public static Boolean searchButtonClick = false;
        public static User User;
        public static List<UserRight> UserRights;
        public static List<UserRightOnReport> UserRightOnReports;
        public static List<UserRightOnScreen> UserRightOnScreens;

        public static string msgLanguage = "vi-VN";
        public static string msgLanguageEN = "en-US";
        public static string msgLanguageCN = "zh-CHS";

        public static string msgCaption = "Thông báo";
        public static string msgCaptionEN = "Notice";
        public static string msgCaptionCN = "通知";

        public static string msgCloseTabpageRequest = "Đóng các cửa sổ trước khi thay đổi ngôn ngữ";
        public static string msgCloseTabpageRequestEN = "Please close tabpages before change language";
        public static string msgCloseTabpageRequestCN = "更改语言前关闭窗口";

        public static string msgEmptyFieldError = "Nhập thiếu dữ liệu";
        public static string msgEmptyFieldErrorEN = "Field empty";
        public static string msgEmptyFieldErrorCN= "字段为空";

        public static string msgCreateUserSuccess = "Tạo người dùng thành công.";
        public static string msgCreateUserSuccessEN = "Create user success.";
        public static string msgCreateUserSuccessCN = "创造用户成功.";


        public static string msgCreateGoodsLineSuccess = "Tạo ngành hàng thành công.";

        public static string msgUpdateUserSuccess = "Cập nhật người dùng thành công.";
        public static string msgUpdateUserSuccessEN = "Update user success.";
        public static string msgUpdateUserSuccessCN = "更新用户成功.";

        public static string msgDeleteUserSuccess = "Xóa người dùng thành công.";
        public static string msgDeleteUserSuccessEN = "Delete user success.";
        public static string msgDeleteUserSuccessCN = "删除用户成功.";

        public static string msgCreateGoodsLineSuccessEN = "Create goods line success.";
        public static string msgUpdateGoodsLineSuccess = "Cập nhật ngành hàng thành công.";

        public static string msgUpdateGoodsLineSuccessEN = "Update goods line success.";
         public static string msgDeleteGoodsLineSuccess = "Xóa ngành hàng thành công.";

        public static string msgDeleteGoodsLineSuccessEN = "Delete goods line success.";

        public static string msgSaveNewSuccess = "Lưu mới dữ liệu thành công";
        public static string msgSaveNewSuccessEN = "Create data success.";
        public static string msgSaveNewSuccessCN = "创建数据成功.";

        public static string msgSaveChangeSuccess = "Điều chỉnh dữ liệu thành công.";
        public static string msgSaveChangeSuccessEN = "Update data success.";
        public static string msgSaveChangeSuccessCN = "更新数据成功.";

        public static string msgDeleteSuccess = "Xóa dữ liệu thành công.";
        public static string msgDeleteSuccessEN = "Delete data success.";
        public static string msgDeleteSuccessCN = "删除数据成功.";

        public static string msgCloseTab = "Bạn có muốn đóng cửa sổ này";
        public static string msgCloseTabEN = "Do you want to close this tab";
        public static string msgCloseTabCN = "是否要关闭此选项卡";

        public static string msgAddNewRequest = "Bạn có muốn thêm mới?";
        public static string msgAddNewRequestEN = "Are you want to add new?";
        public static string msgAddNewRequestCN = "是否要添加新的?";

        public static string msgSaveNewRequest = "Bạn có muốn lưu mới không ?";
        public static string msgSaveNewRequestEN = "Are you want to save new?";
        public static string msgSaveNewRequestCN = "是否要保存新的?";

        public static string msgSaveChangeRequest = "Bạn có muốn lưu thay đổi không ?";
        public static string msgSaveChangeRequestEN = "Are you want to save change?";
        public static string msgSaveChangeRequestCN = "您要保存更改吗?";

        public static string msgDeleteRequest = "Bạn có muốn xóa không?";
        public static string msgDeleteRequestEN = "Are you want to delete?";
        public static string msgDeleteRequestCN = "你要删除吗?";
    }
}
