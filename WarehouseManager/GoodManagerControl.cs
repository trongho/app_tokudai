﻿using DevExpress.XtraBars.Docking2010;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using OfficeOpenXml;
using LicenseContext = OfficeOpenXml.LicenseContext;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using System.Diagnostics;
using System.Resources;
using WarehouseManager.Helpers;

namespace WarehouseManager
{
    public partial class GoodManagerControl : DevExpress.XtraEditors.XtraUserControl
    {
        GoodsRepository goodsRepository;
        int createdNum = 0;
        int updatedNum = 0;

        internal MainForm m_MainForm;

        public GoodManagerControl(MainForm mainForm)
        {
            InitializeComponent();
            m_MainForm = mainForm;
            BottomBarHelper.normalMessage(m_MainForm.barStaticItemMessage, "!!!");
            updateLanguage();
            this.Load += GoodsManagerControl_Load;

        }

        private void GoodsManagerControl_Load(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_Click;
            gridViewGoods.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            gridViewGoods.CustomDrawCell += grdData_CustomDrawCell;


            goodsRepository = new GoodsRepository();

            popupMenuSelectUnit();
            popupMenuSelectMold();
            popupMenuSelectCartonSize();
            popupMenuSelectPads();
            popupMenuSelectNilon();
            popupMenuSelectLabelSize();

            sbLoadDataForGridGoodsAsync();
        }

        private void updateLanguage()
        {
            ResourceManager rm;
            String cultureName = WMMessage.msgLanguage;
            if (cultureName.Equals("vi-VN"))
            {
                rm = new
                   ResourceManager("WarehouseManager.Resource.vi_local", typeof(MainForm).Assembly);
            }
            else if (cultureName.Equals("en-US"))
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.en_local", typeof(MainForm).Assembly);
            }
            else
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.cn_local", typeof(MainForm).Assembly);
            }

            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[0]).Caption = rm.GetString("addnew");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[2]).Caption = rm.GetString("save");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[4]).Caption = rm.GetString("delete");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[6]).Caption = rm.GetString("search");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[8]).Caption = rm.GetString("refesh");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[10]).Caption = rm.GetString("import_data");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[12]).Caption = rm.GetString("export_data");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[14]).Caption = rm.GetString("print");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[16]).Caption = rm.GetString("exit");

            gridViewGoods.Columns["Quantity"].Caption = rm.GetString("quantity");
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewGoods.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void popupMenuSelectUnit()
        {
            foreach (string s in Helpers.Constant.units)
            {
                comboBoxEditUnit.Properties.Items.Add(s);
            }
            comboBoxEditUnit.SelectedIndex = 0;
        }

        private void popupMenuSelectMold()
        {
            foreach (string s in Helpers.Constant.molds)
            {
                comboBoxEditMold.Properties.Items.Add(s);
            }
            comboBoxEditMold.SelectedIndex = 0;
        }

        private void popupMenuSelectCartonSize()
        {
            foreach (string s in Helpers.Constant.cartonSizes)
            {
                comboBoxEditCartonSize.Properties.Items.Add(s);
            }
            comboBoxEditCartonSize.SelectedIndex = 0;
        }

        private void popupMenuSelectPads()
        {
            foreach (string s in Helpers.Constant.pads)
            {
                comboBoxEditPads.Properties.Items.Add(s);
            }
            comboBoxEditPads.SelectedIndex = 0;
        }

        private void popupMenuSelectNilon()
        {
            foreach (string s in Helpers.Constant.nilons)
            {
                comboBoxEditNilon.Properties.Items.Add(s);
            }
            comboBoxEditNilon.SelectedIndex = 0;
        }

        private void popupMenuSelectLabelSize()
        {
            foreach (string s in Helpers.Constant.labelSizes)
            {
                comboBoxEditLabelSize.Properties.Items.Add(s);
            }
            comboBoxEditLabelSize.SelectedIndex = 0;
        }

        private async Task getTotalGoods()
        {
            Decimal totalGoodsOrg = 0;
            List<Goods> goods = await goodsRepository.getAll();
            totalGoodsOrg += Convert.ToDecimal(goods.Count);
            textEditTotalGoods.Text = totalGoodsOrg.ToString("0.#####");
        }

        private async void sbLoadDataForGridGoodsAsync()
        {
            gridControlGoods.DataSource = await goodsRepository.getAll();
            await getTotalGoods();
        }

        private void grdData_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "No", false) == 0))
            {
                string partNumber = ((gridViewGoods.GetRowCellValue(i, gridViewGoods.Columns["PartNumber"]).ToString() == "") ? "" : Convert.ToString(gridViewGoods.GetRowCellValue(i, gridViewGoods.Columns["PartNumber"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (partNumber.Contains("SW") || partNumber.Contains("LW") || partNumber.Contains("WT"))
                {
                    e.Appearance.ForeColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#000000");
                    e.Appearance.BackColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#FFFFFF");
                }
                else
                {
                    e.Appearance.ForeColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#FFFFFF");
                    e.Appearance.BackColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#000000");
                }
            }
        }

        private void clear()
        {
            textEditPartNumber.Text = "";
            textEditPartName.Text = "";
            comboBoxEditUnit.SelectedIndex = 0;
            textEditProductFamily.Text = "";
            textEditQuantity.Text = "";
            textEditPalletID.Text = "";
            comboBoxEditMold.SelectedIndex = 0;
            textEditCartonName.Text = "";
            comboBoxEditCartonSize.SelectedIndex = 0;
            comboBoxEditPads.SelectedIndex = 0;
            comboBoxEditNilon.SelectedIndex = 0;
            comboBoxEditLabelSize.SelectedIndex = 0;
            textEditMaterial.Text = "";
            textEditStatus.Text = "New";
        }

        public async Task createOrUpdateGoods()
        {
            createdNum = 0;
            int num = gridViewGoods.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewGoods.GetRowCellValue(i, gridViewGoods.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewGoods.GetRowHandle(i);
               

                if ((await goodsRepository.GetUnderID((string)gridViewGoods.GetRowCellValue(rowHandle, "PartNumber"), (string)gridViewGoods.GetRowCellValue(rowHandle, "PalletID")) != null))
                {
                    Goods goods = await goodsRepository.GetUnderID((string)gridViewGoods.GetRowCellValue(rowHandle, "PartNumber"), (string)gridViewGoods.GetRowCellValue(rowHandle, "PalletID"));
                    goods.PartNumber = (string)gridViewGoods.GetRowCellValue(rowHandle, "PartNumber");
                    goods.PartName = (string)gridViewGoods.GetRowCellValue(rowHandle, "PartName");
                    goods.Unit = (string)gridViewGoods.GetRowCellValue(rowHandle, "Unit");
                    goods.ProductFamily = (string)gridViewGoods.GetRowCellValue(rowHandle, "ProductFamily");
                    goods.Quantity = (decimal?)gridViewGoods.GetRowCellValue(rowHandle, "Quantity");
                    goods.PalletID = (string)gridViewGoods.GetRowCellValue(rowHandle, "PalletID");
                    goods.Mold = (string)gridViewGoods.GetRowCellValue(rowHandle, "Mold");
                    goods.CartonName = (string)gridViewGoods.GetRowCellValue(rowHandle, "CartonName");
                    goods.CartonSize = (string)gridViewGoods.GetRowCellValue(rowHandle, "CartonSize");
                    goods.Pads = (string)gridViewGoods.GetRowCellValue(rowHandle, "Pads");
                    goods.Nilon = (string)gridViewGoods.GetRowCellValue(rowHandle, "Nilon");
                    goods.LabelSize = (string)gridViewGoods.GetRowCellValue(rowHandle, "LabelSize");
                    goods.Material = (string)gridViewGoods.GetRowCellValue(rowHandle, "Material");
                    goods.Status = "Updated";
                    goods.UpdatedUserID = WMMessage.User.UserID;
                    goods.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                    if (await goodsRepository.Update(goods, goods.PartNumber, goods.PalletID) > 0)
                    {
                        i++;
                        updatedNum++;
                    }
                }
                else
                {
                    Goods goods = new Goods();
                    goods.PartNumber = (string)gridViewGoods.GetRowCellValue(rowHandle, "PartNumber");
                    goods.Ordinal = i + 1;
                    goods.PartName = (string)gridViewGoods.GetRowCellValue(rowHandle, "PartName");
                    goods.Unit = (string)gridViewGoods.GetRowCellValue(rowHandle, "Unit");
                    goods.ProductFamily = (string)gridViewGoods.GetRowCellValue(rowHandle, "ProductFamily");
                    goods.Quantity = (decimal?)gridViewGoods.GetRowCellValue(rowHandle, "Quantity");
                    goods.PalletID = (string)gridViewGoods.GetRowCellValue(rowHandle, "PalletID");
                    goods.Mold = (string)gridViewGoods.GetRowCellValue(rowHandle, "Mold");
                    goods.CartonName = (string)gridViewGoods.GetRowCellValue(rowHandle, "CartonName");
                    goods.CartonSize = (string)gridViewGoods.GetRowCellValue(rowHandle, "CartonSize");
                    goods.Pads = (string)gridViewGoods.GetRowCellValue(rowHandle, "Pads");
                    goods.Nilon = (string)gridViewGoods.GetRowCellValue(rowHandle, "Nilon");
                    goods.LabelSize = (string)gridViewGoods.GetRowCellValue(rowHandle, "LabelSize");
                    goods.Material = (string)gridViewGoods.GetRowCellValue(rowHandle, "Material");
                    goods.Status = "New";
                    goods.CreatedUserID = WMMessage.User.UserID;
                    goods.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                    goods.CurrentIDCode = 0;
                    if (await goodsRepository.Create(goods) > 0)
                    {
                        i++;
                        createdNum++;
                    }
                }
                
            }
            BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Tạo thành công dữ liệu hàng hóa");
        }

        public async Task createGoods()
        {

            Goods goods = new Goods();
            goods.PartNumber = textEditPartNumber.Text.ToString();
            goods.Ordinal = (int)gridViewGoods.RowCount + 1;
            goods.PartName = textEditPartName.Text.ToString();
            goods.Unit = comboBoxEditUnit.Text.ToString();
            goods.ProductFamily = textEditProductFamily.Text.ToString();
            goods.Quantity = decimal.Parse(textEditQuantity.Text.ToString());
            goods.PalletID = textEditPalletID.Text.ToString();
            goods.Mold = comboBoxEditMold.Text.ToString();
            goods.CartonName = textEditCartonName.Text.ToString();
            goods.CartonSize = comboBoxEditCartonSize.Text.ToString();
            goods.Pads = comboBoxEditPads.Text.ToString();
            goods.Nilon = comboBoxEditNilon.Text.ToString();
            goods.LabelSize = comboBoxEditLabelSize.Text.ToString();
            goods.Material = textEditMaterial.Text.ToString();
            goods.Status = "New";
            goods.CreatedUserID = WMMessage.User.UserID;
            goods.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            goods.CurrentIDCode = int.Parse(textEditCurrentIDCode.Text.ToString());

            await goodsRepository.Create(goods);
            BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Tạo thành công dữ liệu hàng hóa");
        }

        public async Task updateGoods()
        {

            Goods goods = await goodsRepository.GetUnderID(textEditPartNumber.Text, textEditPalletID.Text);
            goods.PartNumber = textEditPartNumber.Text.ToString();
            goods.PartName = textEditPartName.Text.ToString();
            goods.Unit = comboBoxEditUnit.Text.ToString();
            goods.ProductFamily = textEditProductFamily.Text.ToString();
            goods.Quantity = decimal.Parse(textEditQuantity.Text.ToString());
            goods.PalletID = textEditPalletID.Text.ToString();
            goods.Mold = comboBoxEditMold.Text.ToString();
            goods.CartonName = textEditCartonName.Text.ToString();
            goods.CartonSize = comboBoxEditCartonSize.Text.ToString();
            goods.Pads = comboBoxEditPads.Text.ToString();
            goods.Nilon = comboBoxEditNilon.Text.ToString();
            goods.LabelSize = comboBoxEditLabelSize.Text.ToString();
            goods.Material = textEditMaterial.Text.ToString();
            goods.Status = "Updated";
            goods.CreatedUserID = WMMessage.User.UserID;
            goods.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            goods.CurrentIDCode = int.Parse(textEditCurrentIDCode.Text.ToString());

            await goodsRepository.Update(goods, goods.PartNumber, goods.PalletID);
            BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Cập nhật thành công dữ liệu hàng hóa");
        }

        private async Task DeleteAll()
        {
            await goodsRepository.Delete();
            sbLoadDataForGridGoodsAsync();
        }


        private async Task selectFileExcelAsync()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"d:\";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                List<Goods> goods = ExcelToList(fdlg.FileName);
                gridControlGoods.DataSource = goods;
                Application.DoEvents();
            }
        }

        private List<Goods> ExcelToList(String fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            List<Goods> goods = new List<Goods>();
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                var lastRow = workSheet.Dimension.Rows;

                for (int i = 2; i <= lastRow; i++)
                {
                    if (workSheet.Cells[i, 2].Text.Trim() != "")
                    {
                        goods.Add(new Goods
                        {
                            PartNumber = workSheet.Cells[i, 2].Text.Trim().Replace("-", "").Replace(" ", ""),
                            PartName = workSheet.Cells[i, 3].Text.Trim().Replace("-", "").Replace(" ", ""),
                            Unit = workSheet.Cells[i, 4].Text.Trim().Replace("-", "").Replace(" ", ""),
                            ProductFamily = workSheet.Cells[i, 5].Text.Trim().Replace("-", "").Replace(" ", ""),
                            Quantity = decimal.Parse(workSheet.Cells[i, 6].Text.Trim().Replace("-", "").Replace(" ", "")),
                            PalletID = workSheet.Cells[i, 7].Text.Trim().Replace("-", "").Replace(" ", ""),
                            Mold = GetCellValueFromPossiblyMergedCell(workSheet, i, 8),
                            CartonName = GetCellValueFromPossiblyMergedCell(workSheet, i, 9),
                            CartonSize = GetCellValueFromPossiblyMergedCell(workSheet, i, 10),
                            Pads = GetCellValueFromPossiblyMergedCell(workSheet, i, 11),
                            Nilon = GetCellValueFromPossiblyMergedCell(workSheet, i, 12),
                            LabelSize = GetCellValueFromPossiblyMergedCell(workSheet, i, 13),
                            CurrentIDCode = 0
                        });
                    }
                }
            }
            BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, goods[0].PalletID);
            return goods;
        }

        private async Task selectFileExcelAsync2()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"d:\";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                List<Goods> goods = ExcelToList2(fdlg.FileName);
                List<Goods> goods1 = await goodsRepository.getAll();
                for (int i = 0; i < goods.Count; i++)
                {
                    for (int j = 0; j < goods1.Count; j++)
                    {
                        if (goods[i].PartNumber.Equals(goods1[j].PartNumber))
                        {
                            goods1[j].Material = goods[i].Material;
                            goods1[j].Mold = goods[i].Mold;
                        }
                    }
                }

                gridControlGoods.DataSource = goods1;
                Application.DoEvents();
            }
        }

        private List<Goods> ExcelToList2(String fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            List<Goods> goods = new List<Goods>();
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                var lastRow = workSheet.Dimension.Rows;

                for (int i = 3; i <= lastRow; i++)
                {
                    if (workSheet.Cells[i, 2].Text.Trim() != "")
                    {
                        goods.Add(new Goods
                        {
                            PartNumber = workSheet.Cells[i, 2].Text,
                            Material = workSheet.Cells[i, 10].Text + workSheet.Cells[i, 11].Text,
                            Mold = workSheet.Cells[i, 12].Text,
                        });
                    }
                }
            }
            return goods;
        }

        static string GetCellValueFromPossiblyMergedCell(ExcelWorksheet wks, int row, int col)
        {
            var cell = wks?.Cells[row, col];
            if (cell.Merge)
            {
                var mergedId = wks.MergedCells[row, col];
                return wks?.Cells[mergedId].First().Text.ToString().Trim().Replace("-", "").Replace(" ", "") ?? string.Empty;
            }
            else
            {
                return cell.Text.ToString().Trim().Replace("-", "").Replace(" ", "") ?? string.Empty;
            }
        }


        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    clear();
                    break;
                case "save":
                    if (WMPublic.sbMessageSaveRequest(this) == true)
                    {
                        await createOrUpdateGoods();
                        BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Đã thêm: " + createdNum + " ,cập nhật: " + updatedNum);
                        sbLoadDataForGridGoodsAsync();
                    }
                   
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        await DeleteAll();
                    }
                    break;
                case "search":
                    break;
                case "refesh":
                    sbLoadDataForGridGoodsAsync();
                    break;
                case "import":
                    try
                    {
                        Application.UseWaitCursor = true;
                        await selectFileExcelAsync();
                    }
                    finally
                    {
                        BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Tải thành công file excel");
                        Application.UseWaitCursor = false;
                    }
                    break;
                case "export":
                    gridViewGoods.BeginUpdate();
                    gridViewGoods.Columns["Material"].Visible = false;
                    string path = "d:\\tokudai\\labeldata.xlsx";
                    ExportSettings.DefaultExportType = ExportType.DataAware;
                    XlsxExportOptionsEx advOptions = new XlsxExportOptionsEx();
                    advOptions.AllowGrouping = DevExpress.Utils.DefaultBoolean.False;
                    advOptions.ShowTotalSummaries = DevExpress.Utils.DefaultBoolean.False;
                    advOptions.SheetName = "LabelData";
                    gridControlGoods.ExportToXlsx(path);
                    // Open the created XLSX file with the default application.
                    Process.Start(path);
                    gridViewGoods.Columns["Material"].Visible = true;
                    gridViewGoods.EndUpdate();
                    break;
                case "print":
                    gridViewGoods.BeginUpdate();
                    gridViewGoods.Columns["Material"].Visible = false;
                    // Check whether the GridControl can be previewed.
                    if (!gridControlGoods.IsPrintingAvailable)
                    {
                        MessageBox.Show("The 'DevExpress.XtraPrinting' library is not found", "Error");
                        return;
                    }

                    // Open the Preview window.
                    gridControlGoods.ShowPrintPreview();

                    // Check whether the GridControl can be printed.
                    if (!gridControlGoods.IsPrintingAvailable)
                    {
                        MessageBox.Show("The 'DevExpress.XtraPrinting' library is not found", "Error");
                        return;
                    }

                    // Print.
                    gridControlGoods.Print();
                    gridViewGoods.Columns["Material"].Visible = true;
                    gridViewGoods.EndUpdate();
                    break;
                case "close":

                    break;
            }

        }

        private async void simpleButtonSave_Click(object sender, EventArgs e)
        {

            if (!textEditPalletID.Text.Equals("") && !textEditPartNumber.Text.Equals("") && (await goodsRepository.GetUnderID(textEditPartNumber.Text, textEditPalletID.Text)) != null)
            {
                if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                {
                    await updateGoods();
                }
            }
            else
            {
                if (WMPublic.sbMessageSaveNewRequest(this) == true)
                {
                    await createGoods();
                }
            }
            sbLoadDataForGridGoodsAsync();

        }

        private async void simpleButtonDelete_ClickAsync(object sender, EventArgs e)
        {
            if (WMPublic.sbMessageDeleteRequest(this) == true)
            {
                if (await goodsRepository.Delete(textEditPartNumber.Text, textEditPalletID.Text) > 0)
                {
                    BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage,WMMessage.msgDeleteSuccess);
                    sbLoadDataForGridGoodsAsync();
                }
            }

        }

        private void gridViewGoods_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            textEditPartNumber.Text = (string)(sender as GridView).GetFocusedRowCellValue("PartNumber");
            textEditPartName.Text = (string)(sender as GridView).GetFocusedRowCellValue("PartName");
            comboBoxEditUnit.Text = (string)(sender as GridView).GetFocusedRowCellValue("Unit");
            textEditProductFamily.Text = (string)(sender as GridView).GetFocusedRowCellValue("ProductFamily");
            textEditQuantity.Text = (string)(sender as GridView).GetFocusedRowCellValue("Quantity").ToString();
            textEditPalletID.Text = (string)(sender as GridView).GetFocusedRowCellValue("PalletID");
            comboBoxEditMold.Text = (string)(sender as GridView).GetFocusedRowCellValue("Mold");
            textEditCartonName.Text = (string)(sender as GridView).GetFocusedRowCellValue("CartonName");
            comboBoxEditCartonSize.Text = (string)(sender as GridView).GetFocusedRowCellValue("CartonSize");
            comboBoxEditPads.Text = (string)(sender as GridView).GetFocusedRowCellValue("Pads");
            comboBoxEditNilon.Text = (string)(sender as GridView).GetFocusedRowCellValue("Nilon");
            comboBoxEditLabelSize.Text = (string)(sender as GridView).GetFocusedRowCellValue("LabelSize");
            textEditMaterial.Text = (string)(sender as GridView).GetFocusedRowCellValue("Material");
            textEditStatus.Text = (string)(sender as GridView).GetFocusedRowCellValue("Status");
            textEditCurrentIDCode.Text= ((int)(sender as GridView).GetFocusedRowCellValue("CurrentIDCode")).ToString();
        }
    }
}
