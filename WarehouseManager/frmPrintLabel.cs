﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Seagull.BarTender.Print;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmPrintLabel : Form
    {
        internal ReceiptRequisitionControl receiptRequisitionControl;
        WRRDetailRepository wRRDetailRepository = null;
        GoodsRepository goodsRepository = null;

        int labelCount = 0;

        Int32[] selectedRowHandles = null;
        string jobStatus = "";

        private bool isClosing = false; // Set to true if we are closing. This helps discontinue thumbnail loading.
        private const string appName = "Label Print";
        private const string dataSourced = "Data Sourced";
        private Engine engine = null; // The BarTender Print Engine
        private LabelFormatDocument format = null; // The currently open Format
        private string[] browsingFormats; // The list of filenames in the current folder
        Hashtable listItems = new Hashtable();
        Queue<int> generationQueue; // A queue containing indexes into browsingFormats
                                    // to facilitate the generation of thumbnails

        // Label browser indexes.
        int topIndex; // The top visible index in the lstLabelBrowser
        int selectedIndex; // The selected index in the lstLabelBrowser
        delegate void DelegateShowMessageBox(string message);
        private string thumbnailFile = "";
        

        public string wrrNumber
        {
            get
            {
                return textEditWRRNumber.Text;
            }
            set
            {
                textEditWRRNumber.Text = value + "";
            }
        }
        public frmPrintLabel(ReceiptRequisitionControl _receiptRequisitionControl)
        {
            InitializeComponent();
            receiptRequisitionControl = _receiptRequisitionControl;
            wRRDetailRepository = new WRRDetailRepository();
            goodsRepository = new GoodsRepository();
            this.Load += frmPrintLabel_Load;
            this.FormClosed += form_FormClosed;
        }

        private void frmPrintLabel_Load(object sender, EventArgs e)
        {
            loadBartenderEnginer();
            gridViewWRRHeader.OptionsSelection.MultiSelect = true;
            gridViewWRRHeader.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CheckBoxRowSelect;
            gridViewWRRHeader.OptionsSelection.UseIndicatorForSelection = true;
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            sbLoadDataForGridWRRHeaderAsync();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown +=new KeyEventHandler(Form1_KeyDown);
            gridViewWRRHeader.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            gridViewWRRHeader.CustomDrawCell += grdData_CustomDrawCell;
            //gridViewWRRHeader.Columns["colQuantityByPack"].OptionsColumn.AllowEdit=true;

            // Create a uniquely named, temporary file on disk for our thumbnails.
            thumbnailFile = Path.GetTempFileName();

            
        }

        private void form_FormClosed(object sender, FormClosedEventArgs e)
        {
            isClosing = true;

            // Make sure the thumbnail worker is stopped before we try closing BarTender or 
            // there might be problems in the worker.
            // Quit the BarTender Print Engine, but do not save changes to any open formats.
           
            if (engine != null)
                engine.Stop(SaveOptions.DoNotSaveChanges);
            // Remove our temporary thumbnail file.
            if (thumbnailFile.Length != 0)
                File.Delete(thumbnailFile);
        }




        private void loadBartenderEnginer()
        {
            // Create and start a new BarTender Print Engine.
            try
            {
                engine = new Engine(true);

            }
            catch (PrintEngineException exception)
            {
                // If the engine is unable to start, a PrintEngineException will be thrown.
                MessageBox.Show(this, exception.Message, appName);
                this.Close(); // Close this app. We cannot run without connection to an engine.
                return;
            }

            // Get the list of printers
            Printers printers = new Printers();
            foreach (Printer printer in printers)
            {
                comboBoxEditPrinter.Properties.Items.Add(printer.PrinterName);
            }

            if (printers.Count > 0)
            {
                // Automatically select the default printer.
                comboBoxEditPrinter.SelectedItem = printers.Default.PrinterName;
            }

            // Initialize a list and a queue.
            listItems = new System.Collections.Hashtable();
            generationQueue = new Queue<int>();

            //browsingFormats = System.IO.Directory.GetFiles(@"D:\tokudai", "HUB424WT.btw");

            browsingFormats = System.IO.Directory.GetFiles(@"D:\tokudai", "HUB424WT.btw");

            BackgroundWorker formatLoader = new BackgroundWorker();
            formatLoader.DoWork += new DoWorkEventHandler(formatLoader_DoWork);
            formatLoader.RunWorkerCompleted += new RunWorkerCompletedEventHandler(formatLoader_RunWorkerCompleted);
            formatLoader.RunWorkerAsync(selectedIndex);
        }

        void formatLoader_DoWork(object sender, DoWorkEventArgs e)
        {
            
            int index = (int)e.Argument;
            string errorMessage = "";

            // We lock the engine here because the engine might still be printing a format.
            lock (engine)
            {
                // Make sure this is still the label the user has selected in case they are clicking around fast.
                if (selectedIndex == index)
                {
                    try
                    {
                        if (format != null)
                        {
                            format.Close(SaveOptions.DoNotSaveChanges);
                            
                        }
                       
                        format = engine.Documents.Open(browsingFormats[index]);
                    }
                    catch (System.Runtime.InteropServices.COMException comException)
                    {
                        errorMessage = String.Format("Unable to open format: {0}\nReason: {1}", browsingFormats[index], comException.Message);
                        format = null;
                    }
                }
            }
            // We are in a non-UI thread so we need to use Invoke to show our message properly.
            if (errorMessage.Length != 0)
                Invoke(new DelegateShowMessageBox(ShowMessageBox), errorMessage);
        }

        void formatLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // We lock on the engine here so that if we have a valid format object
            // we don't lose it while we are using it.
            lock (engine)
            {
                if (format != null)
                {
                    try
                    {
                        comboBoxEditPrinter.SelectedItem = format.PrintSetup.PrinterName;
                        format.ExportImageToFile(thumbnailFile, ImageType.JPEG, Seagull.BarTender.Print.ColorDepth.ColorDepth24bit, new Resolution(picThumbnail.Width, picThumbnail.Height), OverwriteOptions.Overwrite);
                        picThumbnail.ImageLocation = thumbnailFile;

                    }
                    catch (PrintEngineException exception)
                    {
                        // If the engine is unable to start, a PrintEngineException will be thrown.
                        MessageBox.Show(this, exception.Message, appName);
                        return;
                    }
                    
                }
                else
                {
                    // Clear any previous image.
                    picThumbnail.ImageLocation = "";
                }
            }
        }

        void ShowMessageBox(string message)
        {
            MessageBox.Show(this, message, appName);
        }


        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewWRRHeader.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridWRRHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        private void OnMouseEnterButton1(object sender, EventArgs e)
        {
            windowsUIButtonPanel1.BackColor = SystemColors.ButtonHighlight; // or Color.Red or whatever you want       
        }


        private async void sbLoadDataForGridWRRHeaderAsync()
        {
            List<WRRDetail> wRRDetails = await wRRDetailRepository.GetUnderID(textEditWRRNumber.Text);
            gridControlWRRHeader.DataSource =wRRDetails;
        }

        private async Task printJob(int rowHandle)
        {

            

            jobStatus = "";
            
            string palletID = (string)gridViewWRRHeader.GetRowCellValue(rowHandle, "PalletID");
            int? currentIDCode = (await goodsRepository.GetUnderID(palletID))!=null? (await goodsRepository.GetUnderID(palletID))[0].CurrentIDCode:0;
            string quantityByItem = gridViewWRRHeader.GetRowCellValue(rowHandle, "QuantityByItem").ToString();
            string quantityByPack = gridViewWRRHeader.GetRowCellValue(rowHandle, "QuantityByPack").ToString();
            string idCode = $"{currentIDCode+1:D7}";
            string unit = (string)gridViewWRRHeader.GetRowCellValue(rowHandle, "Unit");
            string partName = (string)gridViewWRRHeader.GetRowCellValue(rowHandle, "PartName");
            string lotID = (string)gridViewWRRHeader.GetRowCellValue(rowHandle, "LotID");
            string mfgDate = DateTime.Parse(gridViewWRRHeader.GetRowCellValue(rowHandle, "MFGDate").ToString()).ToString("MM/dd/yyyy");
            string expDate = DateTime.Parse(gridViewWRRHeader.GetRowCellValue(rowHandle, "EXPDate").ToString()).ToString("MM/dd/yyyy");
            string partNumber = (string)gridViewWRRHeader.GetRowCellValue(rowHandle, "PartNumber");
            string productFamily = (string)gridViewWRRHeader.GetRowCellValue(rowHandle, "ProductFamily");
            

            format.SubStrings.SetSubString("10", palletID);
            format.SubStrings.SetSubString("3",((int)(decimal.Parse(quantityByItem)/decimal.Parse(quantityByPack))).ToString());
            format.SubStrings.SetSubString("2", idCode);
            format.SubStrings.SetSubString("4", unit);
            format.SubStrings.SetSubString("6", partName);
            format.SubStrings.SetSubString("7", lotID);
            format.SubStrings.SetSubString("8", mfgDate);
            format.SubStrings.SetSubString("9", expDate);
            format.SubStrings.SetSubString("11", partNumber);
            format.SubStrings.SetSubString("12", partNumber);
            format.SubStrings.SetSubString("13", ((int)(decimal.Parse(quantityByItem) / decimal.Parse(quantityByPack))).ToString());
            format.SubStrings.SetSubString("14",productFamily);




            lock (engine)
            {
                bool success = true;

                decimal? quantityByPack2 = (decimal?)gridViewWRRHeader.GetRowCellValue(rowHandle, "QuantityByPack");
                decimal? quantityByPrinted = (decimal?)gridViewWRRHeader.GetRowCellValue(rowHandle, "QuantityByPrinted");
                decimal? remainingPrintQuantity = (decimal?)gridViewWRRHeader.GetRowCellValue(rowHandle, "RemainingPrintQuantity");
                if (quantityByPrinted + remainingPrintQuantity > quantityByPack2)
                {
                    MessageBox.Show("Error!!!");
                    return;
                }
                if (remainingPrintQuantity == 0)
                {
                    return;
                }

                // Assign number of identical copies if it is not datasourced.
                //if (format.PrintSetup.SupportsIdenticalCopies)
                //{
                //    int copies = 1;
                //    success = Int32.TryParse("1", out copies) && (copies >= 1);
                //    if (!success)
                //    {
                //        MessageBox.Show(this, "Identical Copies must be an integer greater than or equal to 1.", appName);
                //        return;
                //    }
                //    else
                //    {
                //        format.PrintSetup.IdenticalCopiesOfLabel = copies;
                //    }
                //}
                format.PrintSetup.IdenticalCopiesOfLabel =1;

                // Assign number of serialized copies if it is not datasourced.
                //if (success && (format.PrintSetup.SupportsSerializedLabels))
                //{
                //    int copies = 1;
                //    success = Int32.TryParse(gridViewWRRHeader.GetRowCellValue(rowHandle, "RemainingPrintQuantity").ToString(), out copies) && (copies >= 1);
                //    if (!success)
                //    {
                //        MessageBox.Show(this, "Serialized Copies must be an integer greater than or equal to 1.", appName);
                //        return;
                //    }
                //    else
                //    {
                //        format.PrintSetup.NumberOfSerializedLabels = copies;
                //    }
                //}
                format.PrintSetup.NumberOfSerializedLabels =int.Parse(decimal.Parse(gridViewWRRHeader.GetRowCellValue(rowHandle, "RemainingPrintQuantity").ToString()).ToString("0.#####"));

                // If there are no incorrect values in the copies boxes then print.
                if (success)
                {
                    try
                    {
                        Cursor.Current = Cursors.WaitCursor;

                        // Get the printer from the dropdown and assign it to the format.
                        if (comboBoxEditPrinter.SelectedItem != null)
                            format.PrintSetup.PrinterName = comboBoxEditPrinter.SelectedItem.ToString();

                        Messages messages;

                        int waitForCompletionTimeout = 10000; // 10 seconds
                        Result result = format.Print(appName, waitForCompletionTimeout, out messages);
                        string messageString = "\n\nMessages:";

                        foreach (Seagull.BarTender.Print.Message message in messages)
                        {
                            messageString += "\n\n" + message.Text;
                        }

                        if (result == Result.Failure)
                        {
                            //MessageBox.Show(this, "Print Failed" + messageString, appName);
                            jobStatus = jobStatus + "job " + rowHandle + " failed ";
                            return;
                        }
                        else
                        {
                            //MessageBox.Show(this, "Label was successfully sent to printer." + messageString, appName);
                            jobStatus = jobStatus + "job " + rowHandle + " successed ";
                            //if (format != null)
                            //    format.Close(SaveOptions.DoNotSaveChanges);
                            //format = engine.Documents.Open(browsingFormats[0]);
                        }

                    }
                    catch (PrintEngineException exception)
                    {
                        // If the engine is unable to start, a PrintEngineException will be thrown.
                        MessageBox.Show(this, exception.Message, appName);
                        return;
                    }

                   
                }
            }

     

            List<WRRDetail> wRRDetails = await wRRDetailRepository.GetByPalletIDAndLotID(textEditWRRNumber.Text,palletID,lotID);
      
            wRRDetails[0].QuantityByPrinted = wRRDetails[0].QuantityByPrinted+ (decimal?)gridViewWRRHeader.GetRowCellValue(rowHandle, "RemainingPrintQuantity");
            wRRDetails[0].RemainingPrintQuantity = decimal.Parse(quantityByPack) - wRRDetails[0].QuantityByPrinted;
            string idCodeString = "";
            int firstIDCode = int.Parse(idCode);
            int lastIDCode = int.Parse(idCode) - 1 + int.Parse(decimal.Parse(gridViewWRRHeader.GetRowCellValue(rowHandle, "QuantityByPack").ToString()).ToString("0.#####"));
            for(int i=firstIDCode;i<=lastIDCode;i++)
            {
                idCodeString = idCodeString + $"{i:D7}" + ",";
            }
            wRRDetails[0].IDCodeRange = idCodeString.Substring(0, idCodeString.LastIndexOf(','));
            await wRRDetailRepository.Update(wRRDetails[0], textEditWRRNumber.Text, palletID, wRRDetails[0].Ordinal,wRRDetails[0].LotID);


            List<Goods> goods = await goodsRepository.GetUnderID(palletID);          
            if (goods.Count>0)
            {
                Goods goods1 = goods[0];
                goods1.CurrentIDCode = currentIDCode + int.Parse(decimal.Parse(gridViewWRRHeader.GetRowCellValue(rowHandle, "RemainingPrintQuantity").ToString()).ToString("0.#####"));
                await goodsRepository.Update(goods1,goods[0].PartNumber,goods[0].PalletID);
            }
        }


        private void grdData_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "No", false) == 0))
            {
                double mQuantityByPack = ((gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["QuantityByPack"]).ToString() == "") ? 0.0 : Convert.ToDouble(gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["QuantityByPack"])));
                double mPrintQuantity = ((gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["PrintQuantity"])==null) ? 0.0 : Convert.ToDouble(gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["PrintQuantity"])));
                double mQuantityByPrinted = ((gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["QuantityByPrinted"])==null) ? 0.0 : Convert.ToDouble(gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["QuantityByPrinted"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mPrintQuantity+mQuantityByPrinted>mQuantityByPack)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                }
                else if (mPrintQuantity + mQuantityByPrinted< mQuantityByPack)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                }
                else if (mPrintQuantity + mQuantityByPrinted== mQuantityByPack)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Green;
                }
                else if (mQuantityByPrinted==0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.White;
                }

            }
        }

        private void showPreview()
        {

        }

        private void openFile()
        {
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                // Close the previous format
                if (format != null)
                    format.Close(SaveOptions.DoNotSaveChanges);

                //If the user selected a format, open it in BarTender.
                textEditPath.Text = openFileDialog.FileName;

                try
                {
                    format = engine.Documents.Open(openFileDialog.FileName);
                }
                catch (System.Runtime.InteropServices.COMException comException)
                {
                    MessageBox.Show(this, String.Format("Unable to open format: {0}\nReason: {1}", openFileDialog.FileName, comException.Message), appName);
                    format = null;
                }


                if (format != null)
                {
                    // Select the printer in use by the format.
                    comboBoxEditPrinter.SelectedItem = format.PrintSetup.PrinterName;

                    //Generate a thumbnail for it.
                    format.ExportImageToFile(thumbnailFile, ImageType.JPEG, Seagull.BarTender.Print.ColorDepth.ColorDepth24bit, new Resolution(picThumbnail.Width, picThumbnail.Height), OverwriteOptions.Overwrite);
                    picThumbnail.ImageLocation = thumbnailFile;
                }
                else
                {
                    // Clear any previous image.
                    picThumbnail.ImageLocation = "";
                }
                
            }
        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridWRRHeaderAsync();
                    break;
                case "print":
                    
                    selectedRowHandles = gridViewWRRHeader.GetSelectedRows();
                    if (selectedRowHandles.Length > 0)
                    {
                        labelCount = 0;
                        for (int i = 0; i < selectedRowHandles.Length; i++)
                        {
                            
                            labelCount += int.Parse(decimal.Parse(gridViewWRRHeader.GetRowCellValue(selectedRowHandles[i], "RemainingPrintQuantity").ToString()).ToString("0.#####"));
                            await printJob(selectedRowHandles[i]);

                        }
                        
                        sbLoadDataForGridWRRHeaderAsync();
                    }
                    else
                    {
                        int num = gridViewWRRHeader.RowCount - 1;
                        int i = 0;
                        labelCount = 0;
                        while (true)
                        {
                            int num2 = i;
                            int num3 = num;
                            if (num2 > num3)
                            {
                                break;
                            }
                            int rowHandle = gridViewWRRHeader.GetRowHandle(i);
                            
                            labelCount += int.Parse(decimal.Parse(gridViewWRRHeader.GetRowCellValue(rowHandle, "RemainingPrintQuantity").ToString()).ToString("0.#####"));
                            await printJob(rowHandle);
                            i++;
                        }
                        
                        sbLoadDataForGridWRRHeaderAsync();
                    }
                    
                    break;
                case "preview":
                    break;
                case "open":
                    openFile();
                    
                    break;
            }

        }   


    }
}
