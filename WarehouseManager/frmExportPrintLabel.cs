﻿using DevExpress.Export;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;
using Seagull.BarTender.Print;
using System.IO;
using System.Globalization;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using LicenseContext = OfficeOpenXml.LicenseContext;
namespace WarehouseManager
{
    public partial class frmExportPrintLabel : DevExpress.XtraEditors.XtraForm
    {
        WRRDetailRepository wRRDetailRepository = null;
        GoodsRepository goodsRepository = null;
        LabelPrintRepository labelPrintRepository = null;
        Int32[] selectedRowHandles = null;
        List<LabelPrint> listWaitForPrints = null;
        public string wrrNumber
        {
            get
            {
                return textEditWRRNumber.Text;
            }
            set
            {
                textEditWRRNumber.Text = value + "";
            }
        }
        public frmExportPrintLabel()
        {
            InitializeComponent();
            this.Load += frmExportPrintLabel_Load;
            wRRDetailRepository = new WRRDetailRepository();
            goodsRepository = new GoodsRepository();
            labelPrintRepository = new LabelPrintRepository();
            listWaitForPrints = new List<LabelPrint>();
        }

        private void frmExportPrintLabel_Load(object sender, EventArgs e)
        {
            gridViewWRRHeader.OptionsSelection.MultiSelect = true;
            gridViewWRRHeader.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CheckBoxRowSelect;
            gridViewWRRHeader.OptionsSelection.UseIndicatorForSelection = true;
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);
            gridViewWRRHeader.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            gridView1.CustomColumnDisplayText += GridView1_CustomColumnDisplayText;
            gridView2.CustomColumnDisplayText += GridView2_CustomColumnDisplayText;
            gridViewWRRHeader.CustomDrawCell += grdData_CustomDrawCell;

            sbLoadDataForGridWRRHeaderAsync();
            sbLoadDataForGrid1();
            
            //customButtonEdit();
           
        }

        private void customButtonEdit()
        {
            RepositoryItemButtonEdit buttonEdit = new RepositoryItemButtonEdit();

            EditorButton copyButton = new EditorButton(ButtonPredefines.Glyph);
            copyButton.Tag = "print";
            copyButton.Caption = "Copy";
            copyButton.ImageOptions.SvgImage = global::WarehouseManager.Properties.Resources.print;
            copyButton.ImageOptions.SvgImageSize = new Size(16, 16);

            buttonEdit.Buttons.Clear();
            buttonEdit.Buttons.Add(copyButton);
            buttonEdit.TextEditStyle = TextEditStyles.HideTextEditor;
           

            
            gridView1.Columns["Print"].ColumnEdit = buttonEdit;
            gridControl1.RepositoryItems.Add(buttonEdit);

            buttonEdit.ButtonClick += Properties_ButtonClick;
            
        }

       
        private void Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (/*determine a button*/e.Button.Tag != null)
            {
                ButtonEdit editor = (ButtonEdit)sender;
                EditorButton Button = e.Button;
                string Info = "";
                string EOL = "\n";
                Info += " Kind: " + Button.Kind.ToString() + EOL;
                Info += " Caption: " + Button.Caption + EOL;
                Info += " Image assigned: " + (Button.Image != null).ToString() + EOL;
                Info += " Shortcut: " + Button.Shortcut.ToString() + EOL;
                Info += " IsLeft: " + Button.IsLeft.ToString() + EOL;
                Info += " Width: " + Button.Width.ToString() + EOL;
                Info += " Index: " + editor.Properties.Buttons.IndexOf(e.Button).ToString();

                XtraMessageBox.Show(Info, "ButtonClick event");
            }


        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewWRRHeader.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void GridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo1)
            {
                int rowHandle = gridView1.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void GridView2_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo2)
            {
                int rowHandle = gridView2.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void grdData_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "No", false) == 0))
            {
                double mQuantityByPack = ((gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["QuantityByPack"]).ToString() == "") ? 0.0 : Convert.ToDouble(gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["QuantityByPack"])));
                double mPrintQuantity = ((gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["PrintQuantity"]) == null) ? 0.0 : Convert.ToDouble(gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["PrintQuantity"])));
                double mQuantityByPrinted = ((gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["QuantityByPrinted"]) == null) ? 0.0 : Convert.ToDouble(gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["QuantityByPrinted"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mPrintQuantity + mQuantityByPrinted > mQuantityByPack)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                }
                else if (mPrintQuantity + mQuantityByPrinted < mQuantityByPack)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                }
                else if (mPrintQuantity + mQuantityByPrinted == mQuantityByPack)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Green;
                }
                else if (mQuantityByPrinted == 0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.White;
                }

            }
        }

        private void getTotalQuantityByPrinted()
        {
            Decimal totalQuantity = 0;
            int num = gridViewWRRHeader.RowCount - 1;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRRHeader.GetRowHandle(i);
                if (gridViewWRRHeader.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewWRRHeader.GetRowCellValue(rowHandle, "QuantityByPrinted");
                totalQuantity += quantity;
                i++;
            }
            textEditTotalQuantityByPrinted.Text = totalQuantity.ToString();
        }

        private void getTotalRemainingPrintQuantity()
        {
            Decimal totalQuantity = 0;
            int num = gridViewWRRHeader.RowCount - 1;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRRHeader.GetRowHandle(i);
                if (gridViewWRRHeader.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewWRRHeader.GetRowCellValue(rowHandle, "RemainingPrintQuantity");
                totalQuantity += quantity;
                i++;
            }
            textEditTotalRemainingPrintQuantity.Text = totalQuantity.ToString();
        }


        private async void sbLoadDataForGridWRRHeaderAsync()
        {
            List<WRRDetail> wRRDetails = await wRRDetailRepository.GetUnderID(textEditWRRNumber.Text);
            gridControlWRRHeader.DataSource = wRRDetails;
            getTotalQuantityByPrinted();
            getTotalRemainingPrintQuantity();
        }
        private async void sbLoadDataForGrid1()
        {
            //List<WRRDetail> wRRDetails2 = await wRRDetailRepository.GetUnderID(textEditWRRNumber.Text);
            //if (wRRDetails2.Count > 0)
            //{
            //    List<WRRDetail> wRRDetails3 = new List<WRRDetail>();
            //    foreach (WRRDetail wRRDetail in wRRDetails2)
            //    {
            //        string[] idcodes = wRRDetail.IDCodeRange.Length > 0 ? wRRDetail.IDCodeRange.Split(',') : null;
            //        if (idcodes != null)
            //        {
            //            for (int i = 0; i < idcodes.Length - 1; i++)
            //            {
            //                WRRDetail wRRDetail1 = (await wRRDetailRepository.GetByPalletIDAndLotID(textEditWRRNumber.Text, wRRDetail.PalletID, wRRDetail.LotID))[0];

            //                wRRDetail1.IDCodeRange = wRRDetail.PalletID + $"{int.Parse(idcodes[i]):D7}";
            //                wRRDetails3.Add(wRRDetail1);


            //            }
            //        }
            //    }
            //    gridControl1.DataSource = wRRDetails3;
            //}
            List<LabelPrint> labelPrints = await labelPrintRepository.GetUnderID(textEditWRRNumber.Text);
            var querys = labelPrints.Where(a =>a.Status.Equals("Đã in"))
                .Select(x => new LabelPrint
                {
                    WRRNumber = x.WRRNumber,
                    Ordinal=x.Ordinal,
                    PalletID=x.PalletID,
                    PartNumber=x.PartNumber,
                    LotID=x.LotID,
                    PartName=x.PartName,
                    ProductFamily=x.ProductFamily,
                    MFGDate=x.MFGDate,
                    EXPDate=x.EXPDate,
                    Unit=x.Unit,
                    Quantity=x.Quantity,
                    Status=x.Status,
                    IDCode=x.IDCode

                })
                .ToList();
            gridControl1.DataSource = querys;
            gridView1.ClearSorting();
            gridView1.Columns["PalletID"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            gridView1.Columns["LotID"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
        }

        private void sbLoadDataForGrid2()
        {
            gridControl2.DataSource = listWaitForPrints;
        }


        private async Task exportJob(int rowHandle)
        {

            string palletID = (string)gridViewWRRHeader.GetRowCellValue(rowHandle, "PalletID");
            int? currentIDCode = (await goodsRepository.GetUnderID(palletID)) != null ? (await goodsRepository.GetUnderID(palletID))[0].CurrentIDCode : 0;
            string quantityByItem = gridViewWRRHeader.GetRowCellValue(rowHandle, "QuantityByItem").ToString();
            string quantityByPack = gridViewWRRHeader.GetRowCellValue(rowHandle, "QuantityByPack").ToString();
            string idCode = $"{currentIDCode + 1:D7}";
            string unit = (string)gridViewWRRHeader.GetRowCellValue(rowHandle, "Unit");
            string partName = (string)gridViewWRRHeader.GetRowCellValue(rowHandle, "PartName");
            string lotID = (string)gridViewWRRHeader.GetRowCellValue(rowHandle, "LotID");
            string mfgDate = DateTime.Parse(gridViewWRRHeader.GetRowCellValue(rowHandle, "MFGDate").ToString()).ToString("MM/dd/yyyy");
            string expDate = DateTime.Parse(gridViewWRRHeader.GetRowCellValue(rowHandle, "EXPDate").ToString()).ToString("MM/dd/yyyy");
            string partNumber = (string)gridViewWRRHeader.GetRowCellValue(rowHandle, "PartNumber");
            string productFamily = (string)gridViewWRRHeader.GetRowCellValue(rowHandle, "ProductFamily");

            decimal? quantityByPack2 = (decimal?)gridViewWRRHeader.GetRowCellValue(rowHandle, "QuantityByPack");
            decimal? quantityByPrinted = (decimal?)gridViewWRRHeader.GetRowCellValue(rowHandle, "QuantityByPrinted");
            decimal? remainingPrintQuantity = (decimal?)gridViewWRRHeader.GetRowCellValue(rowHandle, "RemainingPrintQuantity");
            if (quantityByPrinted + remainingPrintQuantity > quantityByPack2)
            {
                MessageBox.Show("Error!!!");
                return;
            }
            if (remainingPrintQuantity == 0)
            {
                return;
            }


            List<WRRDetail> wRRDetails = await wRRDetailRepository.GetByPalletIDAndLotID(textEditWRRNumber.Text, palletID, lotID);

            wRRDetails[0].QuantityByPrinted = wRRDetails[0].QuantityByPrinted + (decimal?)gridViewWRRHeader.GetRowCellValue(rowHandle, "RemainingPrintQuantity");
            wRRDetails[0].RemainingPrintQuantity = decimal.Parse(quantityByPack) - wRRDetails[0].QuantityByPrinted;
            decimal printQuantity= (decimal)gridViewWRRHeader.GetRowCellValue(rowHandle, "RemainingPrintQuantity");
            string idCodeString = wRRDetails[0].IDCodeRange;
            int firstIDCode = int.Parse(idCode);
            int lastIDCode = int.Parse(idCode) - 1 + int.Parse(printQuantity.ToString("0.#####"));


            
            for (int i = firstIDCode; i <= lastIDCode; i++)
            {
                idCodeString = idCodeString +$"{i:D7}" + ",";
                //labelprint
                if ((await labelPrintRepository.GetUnderID(textEditWRRNumber.Text, palletID, partNumber, lotID, $"{i:D7}")).Count < 1)
                {
                    List<LabelPrint> labelPrints = await labelPrintRepository.GetUnderID(textEditWRRNumber.Text);
                    var querys =labelPrints.Count>0?labelPrints.Last().Ordinal:1;
                    int ordinal = int.Parse(querys.ToString()) + 1;
                   

                    LabelPrint labelPrint = new LabelPrint();
                    labelPrint.WRRNumber = textEditWRRNumber.Text;
                    labelPrint.Ordinal = ordinal;
                    labelPrint.PalletID = palletID;
                    labelPrint.PartNumber = partNumber;
                    labelPrint.LotID = lotID;
                    labelPrint.IDCode = palletID+$"{i:D7}";
                    labelPrint.PartName = partName;
                    labelPrint.ProductFamily = productFamily;
                    labelPrint.MFGDate =DateTime.ParseExact(mfgDate,"MM/dd/yyyy",new CultureInfo("vi-VI"));
                    labelPrint.EXPDate = DateTime.ParseExact(expDate, "MM/dd/yyyy", new CultureInfo("vi-VI"));
                    labelPrint.Quantity = decimal.Parse(quantityByItem) / decimal.Parse(quantityByPack);
                    labelPrint.Unit = unit;
                    labelPrint.Status="Đã in";
                    await labelPrintRepository.Create(labelPrint);
                    listWaitForPrints.Add(labelPrint);
                    ordinal++;
                }
            }
            wRRDetails[0].IDCodeRange = idCodeString.Substring(0, idCodeString.LastIndexOf(",") + 1);
            await wRRDetailRepository.Update(wRRDetails[0], textEditWRRNumber.Text, palletID, wRRDetails[0].Ordinal, wRRDetails[0].LotID);


            List<Goods> goods = await goodsRepository.GetUnderID(palletID);
            if (goods.Count > 0)
            {
                Goods goods1 = goods[0];
                goods1.CurrentIDCode = currentIDCode + int.Parse(decimal.Parse(gridViewWRRHeader.GetRowCellValue(rowHandle, "RemainingPrintQuantity").ToString()).ToString("0.#####"));
                await goodsRepository.Update(goods1, goods[0].PartNumber, goods[0].PalletID);
            }
        }

        private async Task printPrepare()
        {
            listWaitForPrints.Clear();
            selectedRowHandles = gridViewWRRHeader.GetSelectedRows();
            if (selectedRowHandles.Length > 0)
            {
                for (int i = 0; i < selectedRowHandles.Length; i++)
                {

                    await exportJob(selectedRowHandles[i]);

                }

                
            }
            else
            {
                int num = gridViewWRRHeader.RowCount - 1;
                int i = 0;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    int rowHandle = gridViewWRRHeader.GetRowHandle(i);
                    await exportJob(rowHandle);
                    i++;
                }

               
            }
        }

        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }

        public static bool IsFileOpen(string path)
        {
            FileStream stream = null;
            try
            {
                stream = File.Open(path, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            }
            catch (IOException ex)
            {
                if (ex.Message.Contains("being used by another process"))
                    return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return false;
        }

        private async void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridWRRHeaderAsync();
            }
            if (e.KeyCode == Keys.F8)
            {
                selectedRowHandles = gridViewWRRHeader.GetSelectedRows();
                if (selectedRowHandles.Length > 0)
                {
                    for (int i = 0; i < selectedRowHandles.Length; i++)
                    {

                        await exportJob(selectedRowHandles[i]);

                    }

                    
                }
                else
                {
                    int num = gridViewWRRHeader.RowCount - 1;
                    int i = 0;
                    while (true)
                    {
                        int num2 = i;
                        int num3 = num;
                        if (num2 > num3)
                        {
                            break;
                        }
                        int rowHandle = gridViewWRRHeader.GetRowHandle(i);
                        await exportJob(rowHandle);
                        i++;
                    }

                    
                }
            }
            if (e.KeyCode == Keys.F9)
            {
                String mCurrentDateTime = DateTime.Now.ToString("MMddyyyy_HHmmss");
                gridView1.BeginUpdate();
                gridView1.Columns["No"].Visible = false;
                gridView1.Columns["Status"].Visible = false;
                gridView1.Columns["Print"].Visible = false;
                string path = "in tem\\" + textEditWRRNumber.Text.Replace("-", "") + "_" + String.Format(mCurrentDateTime, "MMddyyyy_HHmmss") + ".xlsx";
                string fileName= textEditWRRNumber.Text.Replace("-", "") + "_" + String.Format(mCurrentDateTime, "MMddyyyy_HHmmss") + ".xlsx";

                if (IsFileOpen(path))
                {
                    MessageBox.Show("Excel file is opened,Please close excel file and try again!!!");
                    return;
                }

                ExportSettings.DefaultExportType = ExportType.DataAware;
                XlsxExportOptionsEx advOptions = new XlsxExportOptionsEx();
                advOptions.AllowGrouping = DevExpress.Utils.DefaultBoolean.False;
                advOptions.ShowTotalSummaries = DevExpress.Utils.DefaultBoolean.False;
                advOptions.SheetName = String.Format(mCurrentDateTime, "MMddyyyy");
                gridControl1.ExportToXlsx(path);
                // Open the created XLSX file with the default application.
                //Process.Start(path);
                gridView1.Columns["No"].Visible = true;
                gridView1.Columns["Status"].Visible = true;
                gridView1.Columns["Print"].Visible = true;
                gridView1.EndUpdate();

                if (WMPublic.sbMessageOpenBartenderRequest(this,fileName) == true)
                {
                    Process.Start(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\BarTender 2021\BarTender Designer.lnk");
                }

            }
            if (e.KeyCode == Keys.F10)

            {
                this.Close();
            }
        }
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridWRRHeaderAsync();
                    sbLoadDataForGrid1();
                    break;
                case "exportdata":

                    selectedRowHandles = gridViewWRRHeader.GetSelectedRows();
                    if (selectedRowHandles.Length > 0)
                    {
                        for (int i = 0; i < selectedRowHandles.Length; i++)
                        {

                            await exportJob(selectedRowHandles[i]);

                        }

                        sbLoadDataForGridWRRHeaderAsync();
                        sbLoadDataForGrid1();
                    }
                    else
                    {
                        int num = gridViewWRRHeader.RowCount - 1;
                        int i = 0;
                        while (true)
                        {
                            int num2 = i;
                            int num3 = num;
                            if (num2 > num3)
                            {
                                break;
                            }
                            int rowHandle = gridViewWRRHeader.GetRowHandle(i);
                            await exportJob(rowHandle);
                            i++;
                        }

                        sbLoadDataForGridWRRHeaderAsync();
                        sbLoadDataForGrid1();
                    }
                    break;
                case "print":
                    //if (decimal.Parse(textEditTotalRemainingPrintQuantity.Text) == 0)
                    //{
                    //    DialogResult dialogResult = MessageBoxEx.Show(this, "Đã in tem", "Warning!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //    return;
                    //}

                    //await printPrepare();
                    //sbLoadDataForGrid2();

                    //String mCurrentDateTime = DateTime.Now.ToString("MMddyyyy_HHmmss");
                    //gridView2.BeginUpdate();

                    //gridView2.Columns["Status"].Visible = false;
                    //gridView2.Columns["No"].Visible = false;


                    //string path = "in tem\\"+ textEditWRRNumber.Text+"\\"+ textEditWRRNumber.Text.Replace("-", "") + "_" + String.Format(mCurrentDateTime, "MMddyyyy_HHmmss") + ".xlsx";
                    //string fileName = textEditWRRNumber.Text.Replace("-", "") + "_" + String.Format(mCurrentDateTime, "MMddyyyy_HHmmss") + ".xlsx";

                    //string filePath;
                    //var saveDialog = new SaveFileDialog();
                    //saveDialog.DefaultExt = "xlsx";
                    //saveDialog.FileName = textEditWRRNumber.Text.Replace("-", "") + "_" + String.Format(mCurrentDateTime, "MMddyyyy_HHmmss") + ".xlsx";
                    //saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                    //saveDialog.ShowDialog();
                    //filePath = saveDialog.FileName;

                    //if (IsFileOpen(path))
                    //{
                    //    MessageBox.Show("Excel file is opened,Please close excel file and try again!!!");
                    //    return;
                    //}

                    //ExportSettings.DefaultExportType = ExportType.DataAware;
                    //XlsxExportOptionsEx advOptions = new XlsxExportOptionsEx();
                    //advOptions.AllowGrouping = DevExpress.Utils.DefaultBoolean.False;
                    //advOptions.ShowTotalSummaries = DevExpress.Utils.DefaultBoolean.False;
                    //advOptions.SheetName = String.Format(mCurrentDateTime, "MMddyyyy");
                    //gridControl2.ExportToXlsx(filePath);
                    //// Open the created XLSX file with the default application.
                    ////Process.Start(path);

                    //gridView2.Columns["Status"].Visible =true;
                    //gridView2.Columns["No"].Visible =true;

                    //gridView2.EndUpdate();

                    //if (WMPublic.sbMessageOpenBartenderRequest(this,fileName)==true)
                    //{
                    //    Process.Start(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\BarTender 2021\BarTender Designer.lnk");
                    //}

                    //sbLoadDataForGridWRRHeaderAsync();
                    //sbLoadDataForGrid1();

                    //// Create and Start the BarTender print engine.
                    //Engine btEngine = new Engine();
                    //btEngine.Start();

                    //// Set the BarTender main window dimensions.
                    //btEngine.Window.Width = 800;
                    //btEngine.Window.Height = 600;

                    //// Set the window location.
                    //btEngine.Window.Top = 100;
                    //btEngine.Window.Left = 100;

                    //// Make BarTender visible to the user.
                    //btEngine.Window.Visible = true;

                    //// Wait five seconds so the user can see the window.
                    //System.Threading.Thread.Sleep(5000);

                    //// Stop the BarTender print engine.
                    //btEngine.Stop();


                    String mCurrentDateTime = DateTime.Now.ToString("MMddyyyy_HHmmss");
                    gridView1.BeginUpdate();
                    gridView1.Columns["No"].Visible = false;
                    gridView1.Columns["Status"].Visible = false;
                    gridView1.Columns["Print"].Visible = false;
                    string path = "in tem\\" + textEditWRRNumber.Text.Replace("-", "") + "_" + String.Format(mCurrentDateTime, "MMddyyyy_HHmmss") + ".xlsx";
                    string fileName = textEditWRRNumber.Text.Replace("-", "") + "_" + String.Format(mCurrentDateTime, "MMddyyyy_HHmmss") + ".xlsx";

                    if (IsFileOpen(path))
                    {
                        MessageBox.Show("Excel file is opened,Please close excel file and try again!!!");
                        return;
                    }

                    ExportSettings.DefaultExportType = ExportType.DataAware;
                    XlsxExportOptionsEx advOptions = new XlsxExportOptionsEx();
                    advOptions.AllowGrouping = DevExpress.Utils.DefaultBoolean.False;
                    advOptions.ShowTotalSummaries = DevExpress.Utils.DefaultBoolean.False;
                    advOptions.SheetName = String.Format(mCurrentDateTime, "MMddyyyy");
                    gridControl1.ExportToXlsx(path);
                    // Open the created XLSX file with the default application.
                    //Process.Start(path);
                    gridView1.Columns["No"].Visible = true;
                    gridView1.Columns["Status"].Visible = true;
                    gridView1.Columns["Print"].Visible = true;
                    gridView1.EndUpdate();

                    if (WMPublic.sbMessageOpenBartenderRequest(this, fileName) == true)
                    {
                        Process.Start(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\BarTender 2021\BarTender Designer.lnk");
                    }

                    break;
                case "close":
                    this.Close();
                    break;
            }

        }

       

        private void frmExportPrintLabel_Shown(object sender, EventArgs e)
        {
            gridView1.SelectAll();
        }

        

        private void repositoryItemButtonEdit2_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            
            if (/*determine a button*/e.Button.Tag != null)
            {
                string palletID = (string)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "PalletID");
                string partNumber = (string)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "PartNumber");
                string productFamily = (string)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "ProductFamily");
                string lotID = (string)gridView1.GetRowCellValue(gridView1.FocusedRowHandle,"LotID");
                string idCode= (string)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "IDCode");
                string partName = (string)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "PartName");
                decimal? quantity = (decimal?)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Quantity");
                string unit = (string)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Unit");
                string mfgDate = DateTime.Parse(((DateTime)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MFGDate")).ToString()).ToString("MM/dd/yyyy");
                string expDate =DateTime.Parse(((DateTime)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "EXPDate")).ToString()).ToString("MM/dd/yyyy");

                DialogResult dialogResult = MessageBoxEx.Show(this, "In tem lỗi?", "Warning!!!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    // Creating an instance
                    // of ExcelPackage
                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelPackage excel = new ExcelPackage();
                    excel.Workbook.Properties.Author = "trong.hk";
                    excel.Workbook.Properties.LastModifiedBy = "trong.hk";
                    
                    

                    // name of the sheet
                    var workSheet = excel.Workbook.Worksheets.Add("Sheet");

                    // setting the properties
                    // of the work sheet 
                    workSheet.TabColor = System.Drawing.Color.Black;
                    workSheet.DefaultRowHeight = 12;

                    // Setting the properties
                    // of the first row
                    workSheet.Row(1).Height = 20;
                    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Row(1).Style.Font.Bold = true;

                    // Header of the Excel sheet
                    workSheet.Cells[1, 1].Value = "Product Family";
                    workSheet.Cells[1, 2].Value = "Pallet ID";
                    workSheet.Cells[1, 3].Value = "Part Number";
                    workSheet.Cells[1, 4].Value = "Part Name";
                    workSheet.Cells[1, 5].Value = "Lot ID";
                    workSheet.Cells[1, 6].Value = "Mã hàng";
                   
                    workSheet.Cells[1, 7].Value = "Số lượng";
                    workSheet.Cells[1, 8].Value = "Unit";
                    workSheet.Cells[1, 9].Value = "MFG Date";
                    workSheet.Column(9).Style.Numberformat.Format = "MM/dd/yyyy";
                    workSheet.Cells[1, 10].Value = "EXP Date";
                    workSheet.Column(10).Style.Numberformat.Format = "MM/dd/yyyy";

                    // Inserting the article data into excel
                    // sheet by using the for each loop
                    // As we have values to the first row 
                    // we will start with second row
                    int recordIndex = 2;

                    //foreach (var article in Articles)
                    //{
                    workSheet.Cells[recordIndex, 1].Value = productFamily;
                    workSheet.Cells[recordIndex, 2].Value = palletID;
                    workSheet.Cells[recordIndex, 3].Value = partNumber;
                    workSheet.Cells[recordIndex, 4].Value = partName;
                    workSheet.Cells[recordIndex, 5].Value = lotID;
                    workSheet.Cells[recordIndex, 6].Value = idCode;
                    workSheet.Cells[recordIndex, 7].Value = quantity;
                    workSheet.Cells[recordIndex, 8].Value = unit;
                    workSheet.Cells[recordIndex, 9].Value = mfgDate;
                    workSheet.Cells[recordIndex, 10].Value = expDate;
                    //recordIndex++;
                    //}

                    // By default, the column width is not 
                    // set to auto fit for the content
                    // of the range, so we are using
                    // AutoFit() method here. 
                    workSheet.Column(1).AutoFit();
                    workSheet.Column(2).AutoFit();
                    workSheet.Column(3).AutoFit();
                    workSheet.Column(4).AutoFit();
                    workSheet.Column(5).AutoFit();
                    workSheet.Column(6).AutoFit();
                    workSheet.Column(7).AutoFit();
                    workSheet.Column(8).AutoFit();
                    workSheet.Column(9).AutoFit();
                    workSheet.Column(10).AutoFit();

                    // file name with .xlsx extension 
                    string mCurrentDateTime = DateTime.Now.ToString("MMddyyyy_HHmmss");
                    string p_strPath = "in tem loi\\" + textEditWRRNumber.Text + "\\" + textEditWRRNumber.Text.Replace("-", "") + "_" + String.Format(mCurrentDateTime, "MMddyyyy_HHmmss") + ".xlsx";

                    if (File.Exists(p_strPath))
                        File.Delete(p_strPath);

                    bool exists = System.IO.Directory.Exists("in tem loi\\" + textEditWRRNumber.Text);
                    if (!exists)
                        System.IO.Directory.CreateDirectory("in tem loi\\" + textEditWRRNumber.Text);

                    //Save your file
                    FileInfo fi = new FileInfo(p_strPath);
                    excel.SaveAs(fi);

                    ////// Create excel file on physical disk 
                    //FileStream objFileStrm = File.Create(p_strPath);
                    //objFileStrm.Close();

                    //// Write content to excel file 
                    //File.WriteAllBytes(p_strPath, excel.GetAsByteArray());
                    ////Close Excel package
                    //excel.Dispose();
                }

                
            }
            
        }
    }
}