﻿
namespace WarehouseManager
{
    partial class OtherRightControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.rightBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet = new WarehouseManager.WARHOUSE_HPDataSet();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRightID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRightName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRightNameEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rightTableAdapter = new WarehouseManager.WARHOUSE_HPDataSetTableAdapters.RightTableAdapter();
            this.gridColumnSetRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnNo = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.rightBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(796, 310);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // rightBindingSource
            // 
            this.rightBindingSource.DataMember = "Right";
            this.rightBindingSource.DataSource = this.wARHOUSE_HPDataSet;
            // 
            // wARHOUSE_HPDataSet
            // 
            this.wARHOUSE_HPDataSet.DataSetName = "WARHOUSE_HPDataSet";
            this.wARHOUSE_HPDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnNo,
            this.colRightID,
            this.colRightName,
            this.colRightNameEN,
            this.gridColumnSetRight});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colRightID
            // 
            this.colRightID.FieldName = "RightID";
            this.colRightID.Name = "colRightID";
            this.colRightID.Visible = true;
            this.colRightID.VisibleIndex = 1;
            // 
            // colRightName
            // 
            this.colRightName.FieldName = "RightName";
            this.colRightName.Name = "colRightName";
            this.colRightName.Visible = true;
            this.colRightName.VisibleIndex = 2;
            // 
            // colRightNameEN
            // 
            this.colRightNameEN.FieldName = "RightNameEN";
            this.colRightNameEN.Name = "colRightNameEN";
            this.colRightNameEN.Visible = true;
            this.colRightNameEN.VisibleIndex = 3;
            // 
            // rightTableAdapter
            // 
            this.rightTableAdapter.ClearBeforeFill = true;
            // 
            // gridColumnSetRight
            // 
            this.gridColumnSetRight.Caption = "Cấp quyền";
            this.gridColumnSetRight.Name = "gridColumnSetRight";
            this.gridColumnSetRight.Visible = true;
            this.gridColumnSetRight.VisibleIndex = 4;
            // 
            // gridColumnNo
            // 
            this.gridColumnNo.Caption = "No";
            this.gridColumnNo.Name = "gridColumnNo";
            this.gridColumnNo.Visible = true;
            this.gridColumnNo.VisibleIndex = 0;
            // 
            // OtherRightControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "OtherRightControl";
            this.Size = new System.Drawing.Size(796, 310);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource rightBindingSource;
        private WARHOUSE_HPDataSet wARHOUSE_HPDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colRightID;
        private DevExpress.XtraGrid.Columns.GridColumn colRightName;
        private DevExpress.XtraGrid.Columns.GridColumn colRightNameEN;
        private WARHOUSE_HPDataSetTableAdapters.RightTableAdapter rightTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSetRight;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnNo;
    }
}
