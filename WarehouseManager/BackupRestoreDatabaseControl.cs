﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Helpers;

namespace WarehouseManager
{
    public partial class BackupRestoreDatabaseControl : DevExpress.XtraEditors.XtraUserControl
    {
        System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
        double pbUnit;
        int pbWIDTH, pbHEIGHT, pbComplete;
        Bitmap bmp;
        Graphics g;
        SqlCommand cmd;
        SqlConnection sqlCon;
        string conString = "Data Source=DESKTOP-VNIFULR\\APPSERVER; Initial Catalog=WARHOUSE_HP; User Id=sa; Password=admin@123;";
        BackupService backupService;
        public BackupRestoreDatabaseControl()
        {
            InitializeComponent();
            this.Load += BackupRestoreDatabaseControl_Load;
            sqlCon = new SqlConnection(conString);
            sqlCon.Open();
        }

        private void BackupRestoreDatabaseControl_Load(Object sender,EventArgs e)
        {
            curentDate();
            linkLabel1.LinkClicked+= LinkLabel1_LinkClicked;
            LoadBackinfo();
            if (linkLabel1.Text == string.Empty)
            {
                linkLabel1.Text = "Click To Set Directory Path";
            }
            simpleButtonSave.Click += btnSave_Click;
            simpleButtonBackup.Click += btnBackup_Click;
        }

        private void curentDate()
        {
            String currentDate = DateTime.Now.ToString("yyyy-MM-dd");
            dateEditCurentDate.Text = currentDate;
        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog=new FolderBrowserDialog();
            folderBrowserDialog.ShowDialog();
            linkLabel1.Text = folderBrowserDialog.SelectedPath;
        }

        private void LoadBackinfo()
        {
            if (sqlCon.State == ConnectionState.Closed)
            {
                sqlCon.Open();
            }
            DataSet dsData = new DataSet();
            cmd = new SqlCommand("DATABASE_BACKUP", sqlCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ACTIONTYPE", "BACKUP_INFO");
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dsData);
            if (dsData.Tables.Count > 0)
            {
                if (dsData.Tables[0].Rows.Count > 0)
                {
                    linkLabel1.Text = dsData.Tables[0].Rows[0]["LOCATION"].ToString();
                    textEditFileKeep.Text = dsData.Tables[0].Rows[0]["NoOfFiles"].ToString();
                    textEditAutoBackUpTimeSpan.Text = dsData.Tables[0].Rows[0]["DayInterval"].ToString();
                    textEditDatabaseName.Text = dsData.Tables[0].Rows[0]["DATABASENAME"].ToString();
                    linkLabel2.Text = dsData.Tables[0].Rows[0]["LOCATION"].ToString();
                    linkLabel3.Text=dsData.Tables[0].Rows[0]["DATABASENAME"].ToString() + "-" + DateTime.Now.ToString("ddMMyyyyHHmmssfff") + ".bak";
                }
                if (dsData.Tables[1].Rows.Count > 0)
                {
                    simpleLabelItemLastBackup.Text = string.Format("Last backup was taken {0} at {1} in location {2}.", dsData.Tables[1].Rows[0]["BackupType"].ToString(),
                        dsData.Tables[1].Rows[0]["BackupDate"].ToString(), dsData.Tables[1].Rows[0]["Location"].ToString());
                }
                else
                    simpleLabelItemLastBackup.Text = "No Backups !!!";
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (linkLabel1.Text == "Click To Set Directory Path")
            {
                MessageBox.Show("Click To Set Directory Path", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (textEditAutoBackUpTimeSpan.Text == string.Empty)
            {
                MessageBox.Show("Enter how many last backup files required ", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                int numFlag;
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                }
                cmd = new SqlCommand("DATABASE_BACKUP", sqlCon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ACTIONTYPE", "INSERT_BACKUP_INFO");
                cmd.Parameters.AddWithValue("@DatabaseName", textEditDatabaseName.Text); // Your Database Name  
                cmd.Parameters.AddWithValue("@Location", linkLabel1.Text);
                cmd.Parameters.AddWithValue("@DayInterval", textEditAutoBackUpTimeSpan.Text);
                cmd.Parameters.AddWithValue("@SoftwareDate", dateEditCurentDate.Text);
                cmd.Parameters.AddWithValue("@NoOfFiles",textEditFileKeep.Text);
                numFlag = cmd.ExecuteNonQuery();

                if (numFlag > 0)
                {
                    MessageBox.Show("Data saved successfully.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadBackinfo();
                }
                else
                {
                    MessageBox.Show("Data not saved. Plaese Try Again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            g = Graphics.FromImage(bmp);
            g.Clear(Color.LightGreen);
            g.FillRectangle(Brushes.CornflowerBlue, new Rectangle(0, 0, (int)(pbComplete * pbUnit), pbHEIGHT));
            g.DrawString(pbComplete + "%", new Font("Arial", pbHEIGHT / 2), Brushes.Black, new PointF(pbWIDTH / 2 - pbHEIGHT, pbHEIGHT / 10));
            // load bipmap
            pictureEdit1.Image = bmp;
            //update pb complete
            pbComplete++;
            if (pbComplete > 100)
            {
                //Dispose
                g.Dispose();
                t.Stop();
                panelControl1.Visible = false;
                pictureEdit1.Visible = false;
            }
        }

        private void btnBackup_Click(object sender, EventArgs e)  
        {  
            if (linkLabel2.Text == string.Empty)  
            {  
                MessageBox.Show("Please Set Backup Setting", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);  
            }  
            else if (linkLabel3.Text == string.Empty)  
            {  
                MessageBox.Show("Please Set Backup Setting", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);  
            }  
            else  
            {  
                string filaPath;  
                if (!linkLabel2.Text.EndsWith(@"\"))  
                {  
                    filaPath = linkLabel2.Text + @"\" + linkLabel3.Text;  
                }  
                else  
                {  
                    filaPath = linkLabel2.Text + linkLabel3.Text;  
                }  
                int numFlag;  
                if (sqlCon.State == ConnectionState.Closed)  
                {  
                    sqlCon.Open();  
                }  
                cmd = new SqlCommand("DATABASE_BACKUP", sqlCon);  
                cmd.CommandType = CommandType.StoredProcedure;  
                cmd.Parameters.AddWithValue("@ACTIONTYPE", "DB_BACKUP");  
                cmd.Parameters.AddWithValue("@DATABASE", textEditDatabaseName.Text); // Your Database Name  
                cmd.Parameters.AddWithValue("@FILEPATH", filaPath);  
                cmd.Parameters.AddWithValue("@BackupName", linkLabel3.Text);  
                cmd.Parameters.AddWithValue("@SoftwareDate", dateEditCurentDate.Text);  
                cmd.Parameters.AddWithValue("@Type", "Manually");  
                numFlag = cmd.ExecuteNonQuery();  
                DataTable dtLoc = new DataTable();  
                cmd = new SqlCommand("DATABASE_BACKUP", sqlCon);  
                cmd.CommandType = CommandType.StoredProcedure;  
                cmd.Parameters.AddWithValue("@ACTIONTYPE", "REMOVE_LOCATION");  
                SqlDataAdapter da = new SqlDataAdapter(cmd);  
                da.Fill(dtLoc);  
                for (int i = 0; i < dtLoc.Rows.Count; i++)  
                {  
                    string delLoc = dtLoc.Rows[i][0].ToString();  
                    string filepath = delLoc;  
                    if (File.Exists(filepath))  
                    {  
                        File.Delete(filepath);  
  
                    }  
                }  
                if (numFlag > 0)  
                {
                    panelControl1.Visible = true;
                    pictureEdit1.Visible = true;
                    pbWIDTH = pictureEdit1.Width;
                    pbHEIGHT = pictureEdit1.Height;
                    pbUnit = pbWIDTH / 100.0;
                    pbComplete = 0;
                    bmp = new Bitmap(pbWIDTH, pbHEIGHT);
                    t.Interval = 50;
                    t.Tick += new EventHandler(this.Timer1_Tick);
                    t.Start();
                }  
                else  
                {  
                    MessageBox.Show("Plaese Try Again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);  
                }  
            }  
        }  
  

    }
}
