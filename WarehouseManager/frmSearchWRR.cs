﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchWRR : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        BranchRepository branchRepository;
        WRRHeaderRepository wRRHeaderRepository;
        public frmSearchWRR()
        {
            InitializeComponent();
            this.Load += frmSearchWRR_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wRRHeaderRepository = new WRRHeaderRepository();


        }

        private void frmSearchWRR_Load(object sender, EventArgs e)
        {
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            sbLoadDataForGridWRRHeaderAsync();
            gridViewWRRHeader.DoubleClick += dataGridView_CellDoubleClick;
            simpleButtonFilter.Click += filterButton_Click;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown +=new KeyEventHandler(Form1_KeyDown);

            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridWRRHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        private void OnMouseEnterButton1(object sender, EventArgs e)
        {
            windowsUIButtonPanel1.BackColor = SystemColors.ButtonHighlight; // or Color.Red or whatever you want       
        }

        private void popupMenuSelectHandlingStatus()
        {
            List<string> list = Helpers.Constant.handlingStatuss;
            foreach (string s in list)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(s);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

        private void popupMenuSelectBranch()
        {
            List<string> list = Helpers.Constant.branchs;
            foreach (string s in list)
            {
                comboBoxEditBranch.Properties.Items.Add(s);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private async void sbLoadDataForGridWRRHeaderAsync()
        {
            List<WRRHeader> wRRHeaders = await wRRHeaderRepository.GetAll();
            gridControlWRRHeader.DataSource =wRRHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewWRRHeader.RowCount > 0)
            {
                WRRHeader wRRHeader = await wRRHeaderRepository.GetUnderID((string)(sender as GridView).GetFocusedRowCellValue("WRRNumber"));
                ReceiptRequisitionControl.selectedWRRNumber = (string)(sender as GridView).GetFocusedRowCellValue("WRRNumber");
                ReceiptRequisitionControl.selectedWRRDate = (DateTime)(sender as GridView).GetFocusedRowCellValue("WRRDate");
                ReceiptRequisitionControl.selectedNote = (string)(sender as GridView).GetFocusedRowCellValue("Note");
                ReceiptRequisitionControl.selectedHandlingID = wRRHeader.HandlingStatusID;
                ReceiptRequisitionControl.selectedHandlingName = wRRHeader.HandlingStatusName;
                ReceiptRequisitionControl.selectedBranchID =wRRHeader.BranchID;
                ReceiptRequisitionControl.selectedBranchName = wRRHeader.BranchName;
                this.Close();
            }
        }

        private async void filterButton_Click(Object sender,EventArgs e)
        {
            List<WRRHeader> wRRHeaders= await wRRHeaderRepository.GetAll();
            if (wRRHeaders.Count > 0)
            {
                if (comboBoxEditBranch.SelectedItem.ToString().Equals("<>")&& comboBoxEditHandlingStatus.SelectedItem.ToString().Equals("<>"))
                {
                    var querys = wRRHeaders.Where(a => a.WRRDate > DateTime.Parse(dateEditFromDate.Text) && a.WRRDate < DateTime.Parse(dateEditToDate.Text)).GroupBy(x => x.WRRNumber)
                         .Select(x => new WRRHeader
                         {
                             WRRNumber = x.Key,

                         })
                        .ToList();
                    gridControlWRRHeader.DataSource = querys;
                }
                else if (comboBoxEditHandlingStatus.SelectedItem.ToString().Equals("<>") && !comboBoxEditBranch.SelectedItem.ToString().Equals("<>"))
                {
                    var querys = wRRHeaders.Where(a => a.BranchID.Equals(comboBoxEditBranch.SelectedIndex.ToString())
                            && a.WRRDate > DateTime.Parse(dateEditFromDate.Text) && a.WRRDate < DateTime.Parse(dateEditToDate.Text)).GroupBy(x => x.WRRNumber)
                         .Select(x => new WRRHeader
                         {
                             WRRNumber = x.Key,

                         })
                        .ToList();
                    gridControlWRRHeader.DataSource = querys;
                }
                else if (!comboBoxEditHandlingStatus.SelectedItem.ToString().Equals("<>") && comboBoxEditBranch.SelectedItem.ToString().Equals("<>"))
                {
                    var querys = wRRHeaders.Where(a => a.BranchID.Equals(comboBoxEditHandlingStatus.SelectedIndex.ToString())
                            && a.WRRDate > DateTime.Parse(dateEditFromDate.Text) && a.WRRDate < DateTime.Parse(dateEditToDate.Text)).GroupBy(x => x.WRRNumber)
                         .Select(x => new WRRHeader
                         {
                             WRRNumber = x.Key,

                         })
                        .ToList();
                    gridControlWRRHeader.DataSource = querys;
                }
                else
                {
                    var querys = wRRHeaders.Where(a => a.BranchID.Equals(comboBoxEditBranch.SelectedIndex.ToString())
                            && a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex.ToString())
                            && a.WRRDate > DateTime.Parse(dateEditFromDate.Text) && a.WRRDate < DateTime.Parse(dateEditToDate.Text)).GroupBy(x => x.WRRNumber)
                         .Select(x => new WRRHeader
                         {
                             WRRNumber = x.Key,

                         })
                        .ToList();
                    gridControlWRRHeader.DataSource = querys;
                }
            }

        }

        public void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridWRRHeaderAsync();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }   

    }
}
