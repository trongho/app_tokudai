﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchWID : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        OrderStatusRepository orderStatusRepository;
        BranchRepository branchRepository;
        WIDataHeaderRepository wIDataHeaderRepository;
        public frmSearchWID()
        {
            InitializeComponent();
            this.Load += frmSearchWID_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wIDataHeaderRepository = new WIDataHeaderRepository();
            orderStatusRepository = new OrderStatusRepository();
        }

        private void frmSearchWID_Load(object sender, EventArgs e)
        {
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            popupMenuSelectOrderStatus();

            filter();

            gridViewWIDataHeader.DoubleClick += dataGridView_CellDoubleClick;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

           
        }

        public void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridWIDHeaderAsync();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridWIDHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        private async void popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

        private async void popupMenuSelectOrderStatus()
        {
            List<OrderStatus> orderStatuses = await orderStatusRepository.GetAll();
            foreach (OrderStatus orderStatus in orderStatuses)
            {
                comboBoxEditOrderStatus.Properties.Items.Add(orderStatus.OrderStatusName);
            }
            comboBoxEditOrderStatus.SelectedIndex = 0;
        }

        private async void popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private async void sbLoadDataForGridWIDHeaderAsync()
        {
            List<WIDataHeader> wIDataHeaders = await wIDataHeaderRepository.GetAll();
            gridControlWIDataHeader.DataSource = wIDataHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewWIDataHeader.RowCount > 0)
            {
                IssueDataControl.selectedWIDNumber = (string)(sender as GridView).GetFocusedRowCellValue("WIDNumber");
                this.Close();
            }
        }

        private async void filter()
        {
            List<WIDataHeader> wIDataHeaders = await wIDataHeaderRepository.GetAll();
            if (wIDataHeaders.Count > 0)
            {
                try
                {
                    if (comboBoxEditHandlingStatus.SelectedIndex==0)
                    {
                        if (comboBoxEditBranch.SelectedIndex==0)
                        {
                            if (comboBoxEditOrderStatus.SelectedIndex==0)
                            {
                                var querys = wIDataHeaders.Where(a => a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                                    .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 1)
                            {
                                var querys = wIDataHeaders.Where(a => a.TotalQuantity == a.TotalQuantityOrg && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 2)
                            {
                                var querys = wIDataHeaders.Where(a => a.TotalQuantity < a.TotalQuantityOrg && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 3)
                            {
                                var querys = wIDataHeaders.Where(a => a.TotalQuantity == 0 && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex ==4)
                            {
                                var querys = wIDataHeaders.Where(a => a.TotalQuantity < 0 && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                        }
                        else
                        {
                            if (comboBoxEditOrderStatus.SelectedIndex == 0)
                            {
                                var querys = wIDataHeaders.Where(a => a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                                    .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 1)
                            {
                                var querys = wIDataHeaders.Where(a => a.TotalQuantity == a.TotalQuantityOrg && a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 2)
                            {
                                var querys = wIDataHeaders.Where(a => a.TotalQuantity < a.TotalQuantityOrg && a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex ==3)
                            {
                                var querys = wIDataHeaders.Where(a => a.TotalQuantity == 0 && a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 4)
                            {
                                var querys = wIDataHeaders.Where(a => a.TotalQuantity < 0 && a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                        }

                    }
                    else
                    {
                        if (comboBoxEditBranch.SelectedIndex == 0)
                        {
                            if (comboBoxEditOrderStatus.SelectedIndex == 0)
                            {
                                var querys = wIDataHeaders.Where(a => a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex - 1 + "") && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                                    .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 1)
                            {
                                var querys = wIDataHeaders.Where(a => a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex - 1 + "") && a.TotalQuantity == a.TotalQuantityOrg && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 2)
                            {
                                var querys = wIDataHeaders.Where(a => a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex - 1 + "") && a.TotalQuantity < a.TotalQuantityOrg && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 3)
                            {
                                var querys = wIDataHeaders.Where(a => a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex - 1 + "") && a.TotalQuantity == 0 && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 4)
                            {
                                var querys = wIDataHeaders.Where(a => a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex - 1 + "") && a.TotalQuantity < 0 && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                        }
                        else
                        {
                            if (comboBoxEditOrderStatus.SelectedIndex == 0)
                            {
                                var querys = wIDataHeaders.Where(a => a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex - 1 + "") && a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                                    .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 1)
                            {
                                var querys = wIDataHeaders.Where(a => a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex - 1 + "") && a.TotalQuantity == a.TotalQuantityOrg && a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 2)
                            {
                                var querys = wIDataHeaders.Where(a => a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex - 1 + "") && a.TotalQuantity < a.TotalQuantityOrg && a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex == 3)
                            {
                                var querys = wIDataHeaders.Where(a => a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex - 1 + "") && a.TotalQuantity == 0 && a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                            else if (comboBoxEditOrderStatus.SelectedIndex ==4)
                            {
                                var querys = wIDataHeaders.Where(a => a.HandlingStatusID.Equals(comboBoxEditHandlingStatus.SelectedIndex - 1 + "") && a.TotalQuantity < 0 && a.BranchID.Equals($"{comboBoxEditBranch.SelectedIndex - 1:D3}") && a.WIDDate >= DateTime.Parse(dateEditFromDate.Text) && a.WIDDate <= DateTime.Parse(dateEditToDate.Text))
                               .ToList();
                                gridControlWIDataHeader.DataSource = querys;
                            }
                        }
                    }
                }catch(Exception ex)
                {
                    MessageBox.Show(ex.Message+"");
                }

            }
        }

        private  void comboBoxEditBranch_SelectedValueChanged(object sender, EventArgs e)
        { 
           filter();
        }

        private  void comboBoxEditHandlingStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            filter();
        }

        private  void dateEditFromDate_EditValueChanged(object sender, EventArgs e)
        {
             filter();
        }

        private void dateEditToDate_EditValueChanged(object sender, EventArgs e)
        {
            filter();
        }

        private  void comboBoxEditOrderStatus_SelectedValueChanged(object sender, EventArgs e)
        {
             filter();
        }
    }
}
