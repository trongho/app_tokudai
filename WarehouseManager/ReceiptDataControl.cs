﻿using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Accessibility;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Card;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using QRCoder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class ReceiptDataControl : DevExpress.XtraEditors.XtraUserControl
    {
        WRDataHeaderRepository wRDataHeaderRepository;
        WRDataGeneralRepository wRDataGeneralRepository;
        WRDataDetailRepository wRDataDetailRepository;
        WRRHeaderRepository wRRHeaderRepository;
        WRRDetailRepository wRRDetailRepository;
        HandlingStatusRepository handlingStatusRepository;
        WRHeaderRepository wRHeaderRepository;
        WRDetailRepository wRDetailRepository;
        public static String selectedWRDNumber="";
        Timer t = new Timer();
        int _hoveredRowHandle = GridControl.InvalidRowHandle;

        Boolean isGeneralClick = false;
        string selectedPalletID = "";
        int selectedOrdinal =0;

        internal WRDataDetailForm wRDataDetailForm = null;

       
        public ReceiptDataControl()
        {
            InitializeComponent();
            wRDataDetailForm = new WRDataDetailForm(this);

            wRDataHeaderRepository = new WRDataHeaderRepository();
            wRDataGeneralRepository = new WRDataGeneralRepository();
            wRDataDetailRepository = new WRDataDetailRepository();
            wRRHeaderRepository = new WRRHeaderRepository();
            wRRDetailRepository = new WRRDetailRepository();
            wRHeaderRepository = new WRHeaderRepository();
            wRDetailRepository = new WRDetailRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            popupMenuSelectHandlingStatus();
            sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
            this.Load += ReceiptDataControl_Load;
        }

        private void ReceiptDataControl_Load(Object sender, EventArgs e)
        {
            //gridViewDetail.MouseDown += gridView1_MouseDown;
          
            gridViewWRDataGeneral.MouseMove += GridView_MouseMove;
            //gridControlWRDataDetail.MouseLeave += gridView1_MouseLeave;
            gridViewWRDataGeneral.RowCellStyle += GridView_RowCellStyle;

            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditWRDNumber.KeyDown += new KeyEventHandler(WRDNumber_KeyDown);
            textEditWRDNumber.DoubleClick += textEditWDRNumer_CellDoubleClick;
            gridViewWRDataGeneral.CustomDrawCell += grdData_CustomDrawCell;
            gridViewWRDataGeneral.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            simpleButtonOn.Click += butOn_Click;
            simpleButtonOff.Click += butOff_Click;
            t.Tick += new EventHandler(this.Timer_Tick);
            t.Start();

            
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewWRDataGeneral.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void popupMenuSelectHandlingStatus()
        {
            List<string> list = Helpers.Constant.handlingStatuss;
            foreach (string s in list)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(s);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }


        private async void sbLoadDataForGridWRDataGeneralAsync(String wRDNumber)
        {
            List<WRDataGeneral> wRDataGenerals = await wRDataGeneralRepository.GetUnderID(wRDNumber);
            //List<WRDataDetail> wRDataDetails = await wRDataDetailRepository.GetByID(wRDNumber);
            int mMaxRow = 100;
            if (wRDataGenerals.Count < mMaxRow)
            {
                int num = mMaxRow - wRDataGenerals.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    wRDataGenerals.Add(new WRDataGeneral());
                    i++;
                }
            }

            
            gridControlWRDataDetail.DataSource = wRDataGenerals;
           

            //gridViewDetail.OptionsBehavior.Editable = false;
            //gridViewDetail.MouseDown += gridView1_MouseDown;

            //gridControlWRDataDetail.DataSource = new Binding("PalletID", wRDataDetails, "WRDataDetails");

            //gridControlWRDataDetail.DataMember = "wRDataDetails";

            getTotalQuantityOrg();
            getTotalQuantity();
            getPackingQuantity();
            textTotalQuantity_CustomDraw();

            
        }

        private void GridView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle == _hoveredRowHandle)
                e.Appearance.FontStyleDelta = FontStyle.Bold;
        }
        private void GridView_MouseMove(object sender, MouseEventArgs e)
        {
            var view = sender as GridView;
            var hitInfo = view.CalcHitInfo(e.Location);
            if (hitInfo.RowHandle != _hoveredRowHandle)
            {
                view.RefreshRow(_hoveredRowHandle);
                _hoveredRowHandle = hitInfo.RowHandle;
                view.RefreshRow(_hoveredRowHandle);
            }
        }

        private void gridView1_MouseLeave(object sender, EventArgs e)
        {
            var view = sender as GridView;
            GridHitInfo hInfo = view.CalcHitInfo(gridControlWRDataDetail.PointToClient(Cursor.Position));
            if (hInfo.HitTest == GridHitTest.None)
            {
                // ...  
            }
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if (view == null)
            {
                return;
            }
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hitInfo = view.CalcHitInfo(e.X, e.Y);
            if (hitInfo.InRowCell && hitInfo.HitTest != GridHitTest.CellButton)
            {
                int rowHandle = hitInfo.RowHandle;

                WRDataDetailForm form = new WRDataDetailForm(this);
                form.ShowDialog(this);
                view.SetMasterRowExpanded(rowHandle, !view.GetMasterRowExpanded(rowHandle));
            }
        }

        


        private void getPackingQuantity()
        {
            Decimal packingQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRDataGeneral.GetRowHandle(i);
                if (gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantityOrg = (Decimal)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "QuantityOrg");
                Decimal quantity = (Decimal)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "Quantity");
                Decimal quantityByPack = (Decimal)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "QuantityByPack");
                Decimal packingVolume = (Decimal)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PackingVolume");
                if (quantity % packingVolume != 0)
                {
                    packingQuantity = Math.Floor(quantity / packingVolume) + 1;
                }
                else
                {
                    packingQuantity = quantity / packingVolume;
                }

                gridViewWRDataGeneral.SetRowCellValue(rowHandle, "PackingQuantity", packingQuantity);
                i++;
            }
        }




        private void getTotalQuantityOrg()
        {
            Decimal totalQuantityOrg = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRDataGeneral.GetRowHandle(i);
                if (gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantityOrg = (Decimal)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "QuantityOrg");
                totalQuantityOrg += quantityOrg;
                i++;
            }
            textEditTotalQuantityOrg.Text = totalQuantityOrg.ToString("0.#####");
        }

        private void getTotalQuantity()
        {
            Decimal totalQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRDataGeneral.GetRowHandle(i);
                if (gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PalletID") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "Quantity");
                totalQuantity += quantity;
                i++;
            }
            textEditTotalQuantity.Text = totalQuantity.ToString("0.#####");
        }

        private void QRCodeGeneral(String code)
        {
            QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.Q;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, eccLevel);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeBitmap = qrCode.GetGraphic(2, System.Drawing.Color.Black, System.Drawing.Color.White, new Bitmap(10, 10));
            pictureBox1.Image = qrCodeBitmap;
            Helpers.ImageHelper.resizeImage(pictureBox1.Image, new Size(100, 100));
            pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
        }

        public async void updateWRDataHeaderAsync()
        {
            WRDataHeader wRDataHeader = await wRDataHeaderRepository.GetUnderID(textEditWRDNumber.Text);
            if (wRDataHeader.HandlingStatusID.Equals("2"))
            {
                MessageBox.Show("Dữ liệu nhập đã duyệt mức 2, không thể chỉnh sửa");
                return;
            }

            Decimal totalQuantityOrg = Decimal.Parse(textEditTotalQuantityOrg.Text);
            Decimal totalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            Decimal subQuantity = totalQuantityOrg - totalQuantity;
            if (subQuantity == totalQuantityOrg)
            {
                wRDataHeader.Status = "0";
            }
            else if (subQuantity < totalQuantityOrg)
            {
                wRDataHeader.Status = "1";
            }
            else if (subQuantity == 0)
            {
                wRDataHeader.Status = "2";
            }
            else
            {
                wRDataHeader.Status = "3";
            }


            wRDataHeader.WRDNumber = textEditWRDNumber.Text;
            wRDataHeader.WRDDate = DateTime.Parse(dateEditWRDDate.DateTime.ToString("yyyy-MM-dd"));
            wRDataHeader.WRRNumber = textEditWRDNumber.Text;
            wRDataHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            wRDataHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wRDataHeader.Note = "";
            wRDataHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            wRDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityOrg.Text);
            wRDataHeader.UpdatedUserID = WMMessage.User.UserID;
            wRDataHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
           
            if (await wRDataHeaderRepository.Update(wRDataHeader, textEditWRDNumber.Text) > 0)
            {
                WMPublic.sbMessageSaveChangeSuccess(this);
                if (comboBoxEditHandlingStatus.SelectedIndex == 3)
                {
                    await createWRHeaderAsync();
                }
            }
           
        }

        public async Task updateWRDataGeneralAsync()
        {
            int num = gridViewWRDataGeneral.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRDataGeneral.GetRowCellValue(i, gridViewWRDataGeneral.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRDataGeneral.GetRowHandle(i);
                WRDataGeneral wRDataGeneral = new WRDataGeneral();
                wRDataGeneral.WRDNumber = textEditWRDNumber.Text;
                wRDataGeneral.PartNumber = (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PartNumber");
                wRDataGeneral.ProductFamily = (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "ProductFamily");
                wRDataGeneral.LotID = (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "LotID");
                wRDataGeneral.PartName = (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PartName");
                wRDataGeneral.Ordinal = i + 1;
                wRDataGeneral.Quantity = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "Quantity");
                wRDataGeneral.TotalQuantity = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "TotalQuantity");
                wRDataGeneral.TotalGoods = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "TotalGoods");
                wRDataGeneral.QuantityOrg = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "QuantityOrg");
                wRDataGeneral.TotalQuantityOrg = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "TotalQuantityOrg");
                wRDataGeneral.TotalGoodsOrg = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "TotalGoodsOrg");
                wRDataGeneral.Status = "1";
                wRDataGeneral.PackingVolume = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PackingVolume");
                wRDataGeneral.QuantityByPack = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "QuantityByPack");
                wRDataGeneral.QuantityByItem = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "QuantityByItem");
                wRDataGeneral.PackingQuantity = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PackingQuantity");
                wRDataGeneral.PalletID= (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PalletID");
                wRDataGeneral.PONumber = (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PONumber");
                wRDataGeneral.Unit= (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "Unit");
                wRDataGeneral.IDCode = (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "IDCode");
                if (((DateTime?)gridViewWRDataGeneral.GetRowCellValue(i, "MFGDate")).ToString() != "")
                {
                    wRDataGeneral.MFGDate = (DateTime?)gridViewWRDataGeneral.GetRowCellValue(i, "MFGDate");
                }
                if (((DateTime?)gridViewWRDataGeneral.GetRowCellValue(i, "EXPDate")).ToString() != "")
                {
                    wRDataGeneral.EXPDate = (DateTime?)gridViewWRDataGeneral.GetRowCellValue(i, "EXPDate");
                }
                if (((DateTime?)gridViewWRDataGeneral.GetRowCellValue(i, "InputDate")).ToString() != "")
                {
                    wRDataGeneral.InputDate = (DateTime?)gridViewWRDataGeneral.GetRowCellValue(i, "InputDate");
                }
                if (await wRDataGeneralRepository.Update(wRDataGeneral, textEditWRDNumber.Text,wRDataGeneral.PalletID, wRDataGeneral.Ordinal,wRDataGeneral.LotID) > 0)
                {
                    i++;
                }
            }
        }

        public async void DeleteWRDataHeaderAsync()
        {
            if ((await wRDataHeaderRepository.Delete(textEditWRDNumber.Text)) > 0)
            {
                await DeleteWRDataGeneralAsync();
                await DeleteWRRHeaderAsync();
                sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                WMPublic.sbMessageDeleteSuccess(this);
            }
        }

        public async Task DeleteWRDataGeneralAsync()
        {
            await wRDataGeneralRepository.Delete(textEditWRDNumber.Text);
            clearAsync();
            gridControlWRDataDetail.DataSource = null;
        }

        public async Task DeleteWRRHeaderAsync()
        {
            if ((await wRRHeaderRepository.Delete(textEditWRDNumber.Text)) > 0)
            {
                await DeleteWRRDetail();
            }
        }

        public async Task DeleteWRRDetail()
        {
            await wRRDetailRepository.Delete(textEditWRDNumber.Text);
        }

        private void clearAsync()
        {
            textEditWRDNumber.Text = "";
            dateEditWRDDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            textEditTotalQuantityOrg.Text = "0";
            textEditTotalQuantity.Text = "0";
            List<WRDataGeneral> wRDataGenerals = new List<WRDataGeneral>();

            for (int i = 0; i < 100; i++)
            {
                wRDataGenerals.Add(new WRDataGeneral());
            }
            gridControlWRDataDetail.DataSource = wRDataGenerals;    
        }

        public async Task createWRHeaderAsync()
        {
            WRDataHeader wRDataHeader = await wRDataHeaderRepository.GetUnderID(textEditWRDNumber.Text);
            WRHeader wRHeader = new WRHeader();
            wRHeader.WRNumber = textEditWRDNumber.Text;
            wRHeader.WRDate = DateTime.Parse(dateEditWRDDate.DateTime.ToString("yyyy-MM-dd"));
            wRHeader.WRRNumber = textEditWRDNumber.Text;
            wRHeader.BranchID = wRDataHeader.BranchID;
            wRHeader.BranchName = wRDataHeader.BranchName;
            wRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            wRHeader.HandlingStatusID = "0";
            wRHeader.HandlingStatusName = "Chưa duyệt";
            wRHeader.ModalityID = "09";
            wRHeader.ModalityName = "Khác";
            wRHeader.CreatedUserID = WMMessage.User.UserID;
            wRHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            wRHeader.WarehouseID = "1";
            wRHeader.WarehouseName = "KHO CÔNG TY";

            if (await wRHeaderRepository.Create(wRHeader) > 0)
            {
                createWRDetailAsync();
                MessageBox.Show("Đã tạo phiếu nhập kho");
            }
        }

        public async void createWRDetailAsync()
        {
            int num = gridViewWRDataGeneral.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRDataGeneral.GetRowCellValue(i, gridViewWRDataGeneral.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRDataGeneral.GetRowHandle(i);
                WRDetail wRDetail = new WRDetail();
                wRDetail.WRNumber = textEditWRDNumber.Text;
                wRDetail.Ordinal = i + 1;
                wRDetail.PalletID = (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PalletID");
                wRDetail.PartNumber = (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PartNumber");
                wRDetail.LotID= (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "LotID");
                wRDetail.PartName = (string)gridViewWRDataGeneral.GetRowCellValue(i, "PartName");
                wRDetail.Unit = (string)gridViewWRDataGeneral.GetRowCellValue(i, "Unit");
                wRDetail.IDCode = (string)gridViewWRDataGeneral.GetRowCellValue(i, "IDCode");
                wRDetail.ReceiptQuantity = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(i, "Quantity");
                wRDetail.QuantityReceived = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(i, "QuantityOrg");
                wRDetail.PackingQuantity = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(i, "PackingQuantity");
                wRDetail.PackingVolume = (Decimal?)gridViewWRDataGeneral.GetRowCellValue(i, "PackingVolume");
                if (((DateTime?)gridViewWRDataGeneral.GetRowCellValue(i, "MFGDate")).ToString() != "")
                {
                    wRDetail.MFGDate = (DateTime?)gridViewWRDataGeneral.GetRowCellValue(i, "MFGDate");
                }
                if (((DateTime?)gridViewWRDataGeneral.GetRowCellValue(i, "EXPDate")).ToString() != "")
                {
                    wRDetail.EXPDate = (DateTime?)gridViewWRDataGeneral.GetRowCellValue(i, "EXPDate");
                }
                if (((DateTime?)gridViewWRDataGeneral.GetRowCellValue(i, "InputDate")).ToString() != "")
                {
                    wRDetail.InputDate = (DateTime?)gridViewWRDataGeneral.GetRowCellValue(i, "InputDate");
                }
                if (await wRDetailRepository.Create(wRDetail) > 0)
                {
                    i++;
                }
            }
        }


        private async void WRDNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                WRDataHeader wRDataHeader = new WRDataHeader();
                wRDataHeader = await wRDataHeaderRepository.GetUnderID(textEditWRDNumber.Text);
                if (wRDataHeader != null)
                {
                    dateEditWRDDate.Text = DateTime.Parse(wRDataHeader.WRDDate.ToString()).ToShortDateString();
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wRDataHeader.HandlingStatusID) + 1;
                    sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                }
                else
                {
                    MessageBox.Show("Số phiếu yêu cầu không tồn tại");
                    return;
                }

            }
        }

        private async void textEditWDRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchWRD frmSearchWRD = new frmSearchWRD();
            frmSearchWRD.ShowDialog(this);
            frmSearchWRD.Dispose();
            if (!selectedWRDNumber.Equals(""))
            {
                if (selectedWRDNumber.Contains("R"))
                {
                    gridViewWRDataGeneral.Columns["QuantityOrg"].Visible = false;
                    gridViewWRDataGeneral.Columns["QuantityByPack"].Visible = false;
                    textEditTotalQuantityOrg.Visible = false;
                }
                textEditWRDNumber.Text = selectedWRDNumber;
                WRDataHeader wRDataHeader = await wRDataHeaderRepository.GetUnderID(textEditWRDNumber.Text);
                if (wRDataHeader != null)
                {
                    dateEditWRDDate.Text = DateTime.Parse(wRDataHeader.WRDDate.ToString()).ToShortDateString();
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wRDataHeader.HandlingStatusID);
                    sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                }
                QRCodeGeneral(selectedWRDNumber);
            }

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (textEditWRDNumber.Text != "")
            {
               
                sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                loadDataGridWRDatadetail();
                
            }
        }

        private void grdData_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (selectedWRDNumber.Contains("R"))
            {
                return;
            }
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "Quantity", false) == 0))
            {
                double mQuantity = ((gridViewWRDataGeneral.GetRowCellValue(i, gridViewWRDataGeneral.Columns["Quantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataGeneral.GetRowCellValue(i, gridViewWRDataGeneral.Columns["Quantity"])));
                double mQuantityOrg = ((gridViewWRDataGeneral.GetRowCellValue(i, gridViewWRDataGeneral.Columns["QuantityOrg"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataGeneral.GetRowCellValue(i, gridViewWRDataGeneral.Columns["QuantityOrg"])));
                double mPackingQuantity = ((gridViewWRDataGeneral.GetRowCellValue(i, gridViewWRDataGeneral.Columns["PackingQuantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataGeneral.GetRowCellValue(i, gridViewWRDataGeneral.Columns["PackingQuantity"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mQuantity == mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                }
                if (mQuantity > mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                }
                if (mQuantity < mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                }
            }
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "PackingQuantity", false) == 0))
            {
                double mQuantity = ((gridViewWRDataGeneral.GetRowCellValue(i, gridViewWRDataGeneral.Columns["Quantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataGeneral.GetRowCellValue(i, gridViewWRDataGeneral.Columns["Quantity"])));
                double mQuantityOrg = ((gridViewWRDataGeneral.GetRowCellValue(i, gridViewWRDataGeneral.Columns["QuantityOrg"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataGeneral.GetRowCellValue(i, gridViewWRDataGeneral.Columns["QuantityOrg"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mQuantity == mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                }
                if (mQuantity > mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                }
                if (mQuantity < mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                }
            }
            //if ((i >= 0) && (String.Compare(e.Column.FieldName, "No", false) == 0))
            //{
            //    string partNumber = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["PartNumber"]).ToString() == "") ? "" : Convert.ToString(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["PartNumber"])));
            //    e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            //    if ((partNumber.Contains("SW") || partNumber.Contains("LW") || partNumber.Contains("WT"))&&!partNumber.Equals(""))
            //    {
            //        e.Appearance.ForeColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#000000");
            //        e.Appearance.BackColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#FFFFFF");
            //    }
            //    else
            //    {
            //        e.Appearance.ForeColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#FFFFFF");
            //        e.Appearance.BackColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#000000");
            //    }
            //}
        }

        private void textTotalQuantity_CustomDraw()
        {
            double mTotalQuantity = ((textEditTotalQuantity.Text == "") ? 0.0 : Convert.ToDouble(textEditTotalQuantity.Text));
            double mTotalQuantityOrg = ((textEditTotalQuantityOrg.Text == "") ? 0.0 : Convert.ToDouble(textEditTotalQuantityOrg.Text));
            textEditTotalQuantity.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            textEditTotalQuantityOrg.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            if (mTotalQuantity == mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.LightGreen;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            if (mTotalQuantity > mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.Yellow;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            if (mTotalQuantity < mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.Red;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
        }

        private void butOn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bắt đầu quét nhập kho");
            t.Enabled = true;
        }

        private void butOff_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Kết thúc quét nhập kho");
            t.Enabled = false;
        }

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clearAsync();
                    }
                    break;
                case "save":
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateWRDataHeaderAsync();
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteWRDataHeaderAsync();
                    }
                    break;
                case "search":

                    break;
                case "refesh":
                    sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                    break;
                case "import":
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    Demo demo = new Demo();
                    demo.Show();
                    break;
            }

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {

            switch (keyData)
            {
                case Keys.F2:
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clearAsync();
                    }
                    return true;
                case Keys.F3:
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateWRDataHeaderAsync();
                    }
                    return true;
                case Keys.F4:
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteWRDataHeaderAsync();
                    }
                    return true;
                case Keys.F5:
                    sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                    return true;
                case Keys.F10:
                    if (WMPublic.sbMessageCloseTab(this) == true)
                    {
                        XtraTabPage tabPage;
                        tabPage = (XtraTabPage)(this.Parent);
                        XtraTabControl tabControl;
                        tabControl = (XtraTabControl)tabPage.Parent;
                        tabControl.TabPages.Remove(tabPage);
                        tabPage.Dispose();
                    }
                    return true;
                case Keys.O | Keys.Control:
                    MessageBox.Show("Bắt đầu quét nhập kho");
                    t.Enabled = true;
                    return true;
                case Keys.P | Keys.Control:
                    MessageBox.Show("Kết thúc quét nhập kho");
                    t.Enabled = false;
                    return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private async void gridViewWRDataGeneral_RowCellClick(object sender, RowCellClickEventArgs e)
        {

            GridViewInfo viewInfo = gridViewWRDataGeneral.GetViewInfo() as GridViewInfo;
            GridHitInfo hitInfo = viewInfo.CalcHitInfo(e.Location);
            
           
            if (hitInfo.InRowCell)
            {
                int rowHandle = hitInfo.RowHandle;

                if (gridViewWRDataGeneral.FocusedColumn.FieldName.Equals("PalletID"))
                {
                    selectedPalletID = (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PalletID");
                    selectedOrdinal = (int)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "Ordinal");
                    string palletID = (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "PalletID");
                    int ordinal = (int)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "Ordinal");
                    string lotID= (string)gridViewWRDataGeneral.GetRowCellValue(rowHandle, "LotID");
                    simpleLabelItemPalletID.Text = "PalletID: " + palletID;
                    simpleLabelItemLotID.Text = "Lot ID: " + lotID;

                    List<WRDataGeneral> wRDataGenerals = await wRDataGeneralRepository.GetUnderParams(textEditWRDNumber.Text, palletID, ordinal);
                    if (wRDataGenerals.Count > 0)
                    {
                        string[] idcodes = wRDataGenerals[0].IDCode.Split(',');
                        List<WRDataDetail> wRDataDetails = new List<WRDataDetail>();


                        for (int i=0;i<idcodes.Length-1;i++)
                        {
                            WRDataDetail wRDataDetail = new WRDataDetail();
                            wRDataDetail.IDCode =idcodes[i];
                            wRDataDetails.Add(wRDataDetail);
                        }
                        gridControl1.DataSource = wRDataDetails;
                    }

                    
                }
            }
        }

        private async void loadDataGridWRDatadetail()
        {
            List<WRDataGeneral> wRDataGenerals = await wRDataGeneralRepository.GetUnderParams(textEditWRDNumber.Text, selectedPalletID,selectedOrdinal);
            if (wRDataGenerals.Count > 0)
            {
                string[] idcodes = wRDataGenerals[0].IDCode.Split(',');
                List<WRDataDetail> wRDataDetails = new List<WRDataDetail>();


                for (int i = 0; i < idcodes.Length - 1; i++)
                {
                    WRDataDetail wRDataDetail = new WRDataDetail();
                    wRDataDetail.IDCode =idcodes[i];
                    wRDataDetails.Add(wRDataDetail);
                }
                gridControl1.DataSource = wRDataDetails;
            }
        }
    }
}
