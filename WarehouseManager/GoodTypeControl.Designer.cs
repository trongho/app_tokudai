﻿
namespace WarehouseManager
{
    partial class GoodTypeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.searchControl11 = new DevExpress.XtraEditors.SearchControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.comboBoxEditmodality = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.Root1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemFromDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemToDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemModality = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlData = new DevExpress.XtraGrid.GridControl();
            this.gridViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWRNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModalityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModalityName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWRDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWRRNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWRRReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaymentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaymentTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHandlingStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHandlingStatusName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBranchID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBranchName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalOrderType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalDiscountAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalVATAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountExcludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountIncludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckerID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckerName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckerID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckerName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditmodality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl1.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl1.Controls.Add(this.searchControl11);
            this.layoutControl1.Controls.Add(this.dateEditFromDate);
            this.layoutControl1.Controls.Add(this.dateEditToDate);
            this.layoutControl1.Controls.Add(this.comboBoxEditmodality);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(903, 77);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(100, 12);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(263, 20);
            this.comboBoxEditBranch.StyleController = this.layoutControl1;
            this.comboBoxEditBranch.TabIndex = 4;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(792, 12);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(90, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl1;
            this.comboBoxEditHandlingStatus.TabIndex = 7;
            // 
            // searchControl11
            // 
            this.searchControl11.Location = new System.Drawing.Point(367, 40);
            this.searchControl11.Name = "searchControl11";
            this.searchControl11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl11.Size = new System.Drawing.Size(515, 20);
            this.searchControl11.StyleController = this.layoutControl1;
            this.searchControl11.TabIndex = 8;
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(446, 12);
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Size = new System.Drawing.Size(90, 20);
            this.dateEditFromDate.StyleController = this.layoutControl1;
            this.dateEditFromDate.TabIndex = 9;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(619, 12);
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Size = new System.Drawing.Size(90, 20);
            this.dateEditToDate.StyleController = this.layoutControl1;
            this.dateEditToDate.TabIndex = 10;
            // 
            // comboBoxEditmodality
            // 
            this.comboBoxEditmodality.Location = new System.Drawing.Point(100, 40);
            this.comboBoxEditmodality.Name = "comboBoxEditmodality";
            this.comboBoxEditmodality.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditmodality.Size = new System.Drawing.Size(263, 20);
            this.comboBoxEditmodality.StyleController = this.layoutControl1;
            this.comboBoxEditmodality.TabIndex = 11;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Root1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(903, 77);
            this.Root.TextVisible = false;
            // 
            // Root1
            // 
            this.Root1.CustomizationFormText = "Root";
            this.Root1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root1.GroupBordersVisible = false;
            this.Root1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemBranch,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItem6,
            this.layoutControlItemFromDate,
            this.layoutControlItemToDate,
            this.layoutControlItemModality});
            this.Root1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.Root1.Location = new System.Drawing.Point(0, 0);
            this.Root1.Name = "Root1";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 40D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 20D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 20D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 20D;
            this.Root1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4});
            rowDefinition1.Height = 50D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 50D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            this.Root1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2});
            this.Root1.Padding = new DevExpress.XtraLayout.Utils.Padding(9, 9, 0, 0);
            this.Root1.Size = new System.Drawing.Size(883, 57);
            this.Root1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root1.Text = "Root";
            this.Root1.TextVisible = false;
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItemBranch.CustomizationFormText = "Chi nhánh";
            this.layoutControlItemBranch.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.Size = new System.Drawing.Size(346, 28);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItemHandlingStatus.CustomizationFormText = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(692, 0);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(173, 28);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.searchControl11;
            this.layoutControlItem6.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(346, 28);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItem6.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem6.Size = new System.Drawing.Size(519, 29);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItemFromDate
            // 
            this.layoutControlItemFromDate.Control = this.dateEditFromDate;
            this.layoutControlItemFromDate.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItemFromDate.CustomizationFormText = "Từ ngày";
            this.layoutControlItemFromDate.Location = new System.Drawing.Point(346, 0);
            this.layoutControlItemFromDate.Name = "layoutControlItemFromDate";
            this.layoutControlItemFromDate.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItemFromDate.Size = new System.Drawing.Size(173, 28);
            this.layoutControlItemFromDate.Text = "Từ ngày";
            this.layoutControlItemFromDate.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemToDate
            // 
            this.layoutControlItemToDate.Control = this.dateEditToDate;
            this.layoutControlItemToDate.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItemToDate.CustomizationFormText = "Đến ngày";
            this.layoutControlItemToDate.Location = new System.Drawing.Point(519, 0);
            this.layoutControlItemToDate.Name = "layoutControlItemToDate";
            this.layoutControlItemToDate.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemToDate.Size = new System.Drawing.Size(173, 28);
            this.layoutControlItemToDate.Text = "Đến ngày";
            this.layoutControlItemToDate.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemModality
            // 
            this.layoutControlItemModality.Control = this.comboBoxEditmodality;
            this.layoutControlItemModality.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItemModality.CustomizationFormText = "Phương thức";
            this.layoutControlItemModality.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItemModality.Name = "layoutControlItemModality";
            this.layoutControlItemModality.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemModality.Size = new System.Drawing.Size(346, 29);
            this.layoutControlItemModality.Text = "Phương thức";
            this.layoutControlItemModality.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.gridControlData);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 77);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(903, 407);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // gridControlData
            // 
            this.gridControlData.Location = new System.Drawing.Point(12, 12);
            this.gridControlData.MainView = this.gridViewData;
            this.gridControlData.Name = "gridControlData";
            this.gridControlData.Size = new System.Drawing.Size(879, 383);
            this.gridControlData.TabIndex = 6;
            this.gridControlData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewData});
            // 
            // gridViewData
            // 
            this.gridViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWRNumber,
            this.colModalityID,
            this.colModalityName,
            this.colWRDate,
            this.colReferenceNumber,
            this.colWRRNumber,
            this.colWRRReference,
            this.colWarehouseID,
            this.colWarehouseName,
            this.colSupplierID,
            this.colSupplierName,
            this.colCustomerID,
            this.colCustomerName,
            this.colOrderNumber,
            this.colOrderDate,
            this.colContractNumber,
            this.colContractDate,
            this.colIONumber,
            this.colPriceTypeID,
            this.colPriceTypeName,
            this.colPaymentTypeID,
            this.colPaymentTypeName,
            this.colHandlingStatusID,
            this.colHandlingStatusName,
            this.colBranchID,
            this.colBranchName,
            this.colLocalOrderType,
            this.colTotalDiscountAmount,
            this.colTotalVATAmount,
            this.colTotalAmountExcludedVAT,
            this.colTotalAmountIncludedVAT,
            this.colTotalAmount,
            this.colTotalQuantity,
            this.colNote,
            this.colCheckerID1,
            this.colCheckerName1,
            this.colCheckerID2,
            this.colCheckerName2,
            this.colStatus,
            this.colCreatedUserID,
            this.colCreatedDate,
            this.colUpdatedUserID,
            this.colUpdatedDate});
            this.gridViewData.GridControl = this.gridControlData;
            this.gridViewData.Name = "gridViewData";
            this.gridViewData.OptionsFind.AllowFindPanel = false;
            // 
            // colWRNumber
            // 
            this.colWRNumber.FieldName = "WRNumber";
            this.colWRNumber.Name = "colWRNumber";
            this.colWRNumber.Visible = true;
            this.colWRNumber.VisibleIndex = 0;
            // 
            // colModalityID
            // 
            this.colModalityID.FieldName = "ModalityID";
            this.colModalityID.Name = "colModalityID";
            // 
            // colModalityName
            // 
            this.colModalityName.FieldName = "ModalityName";
            this.colModalityName.Name = "colModalityName";
            // 
            // colWRDate
            // 
            this.colWRDate.FieldName = "WRDate";
            this.colWRDate.Name = "colWRDate";
            this.colWRDate.Visible = true;
            this.colWRDate.VisibleIndex = 1;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 2;
            // 
            // colWRRNumber
            // 
            this.colWRRNumber.FieldName = "WRRNumber";
            this.colWRRNumber.Name = "colWRRNumber";
            // 
            // colWRRReference
            // 
            this.colWRRReference.FieldName = "WRRReference";
            this.colWRRReference.Name = "colWRRReference";
            // 
            // colWarehouseID
            // 
            this.colWarehouseID.FieldName = "WarehouseID";
            this.colWarehouseID.Name = "colWarehouseID";
            // 
            // colWarehouseName
            // 
            this.colWarehouseName.FieldName = "WarehouseName";
            this.colWarehouseName.Name = "colWarehouseName";
            this.colWarehouseName.Visible = true;
            this.colWarehouseName.VisibleIndex = 3;
            // 
            // colSupplierID
            // 
            this.colSupplierID.FieldName = "SupplierID";
            this.colSupplierID.Name = "colSupplierID";
            // 
            // colSupplierName
            // 
            this.colSupplierName.FieldName = "SupplierName";
            this.colSupplierName.Name = "colSupplierName";
            // 
            // colCustomerID
            // 
            this.colCustomerID.FieldName = "CustomerID";
            this.colCustomerID.Name = "colCustomerID";
            // 
            // colCustomerName
            // 
            this.colCustomerName.FieldName = "CustomerName";
            this.colCustomerName.Name = "colCustomerName";
            // 
            // colOrderNumber
            // 
            this.colOrderNumber.FieldName = "OrderNumber";
            this.colOrderNumber.Name = "colOrderNumber";
            // 
            // colOrderDate
            // 
            this.colOrderDate.FieldName = "OrderDate";
            this.colOrderDate.Name = "colOrderDate";
            // 
            // colContractNumber
            // 
            this.colContractNumber.FieldName = "ContractNumber";
            this.colContractNumber.Name = "colContractNumber";
            // 
            // colContractDate
            // 
            this.colContractDate.FieldName = "ContractDate";
            this.colContractDate.Name = "colContractDate";
            // 
            // colIONumber
            // 
            this.colIONumber.FieldName = "IONumber";
            this.colIONumber.Name = "colIONumber";
            // 
            // colPriceTypeID
            // 
            this.colPriceTypeID.FieldName = "PriceTypeID";
            this.colPriceTypeID.Name = "colPriceTypeID";
            // 
            // colPriceTypeName
            // 
            this.colPriceTypeName.FieldName = "PriceTypeName";
            this.colPriceTypeName.Name = "colPriceTypeName";
            // 
            // colPaymentTypeID
            // 
            this.colPaymentTypeID.FieldName = "PaymentTypeID";
            this.colPaymentTypeID.Name = "colPaymentTypeID";
            // 
            // colPaymentTypeName
            // 
            this.colPaymentTypeName.FieldName = "PaymentTypeName";
            this.colPaymentTypeName.Name = "colPaymentTypeName";
            // 
            // colHandlingStatusID
            // 
            this.colHandlingStatusID.FieldName = "HandlingStatusID";
            this.colHandlingStatusID.Name = "colHandlingStatusID";
            // 
            // colHandlingStatusName
            // 
            this.colHandlingStatusName.FieldName = "HandlingStatusName";
            this.colHandlingStatusName.Name = "colHandlingStatusName";
            this.colHandlingStatusName.Visible = true;
            this.colHandlingStatusName.VisibleIndex = 4;
            // 
            // colBranchID
            // 
            this.colBranchID.FieldName = "BranchID";
            this.colBranchID.Name = "colBranchID";
            // 
            // colBranchName
            // 
            this.colBranchName.FieldName = "BranchName";
            this.colBranchName.Name = "colBranchName";
            // 
            // colLocalOrderType
            // 
            this.colLocalOrderType.FieldName = "LocalOrderType";
            this.colLocalOrderType.Name = "colLocalOrderType";
            // 
            // colTotalDiscountAmount
            // 
            this.colTotalDiscountAmount.FieldName = "TotalDiscountAmount";
            this.colTotalDiscountAmount.Name = "colTotalDiscountAmount";
            // 
            // colTotalVATAmount
            // 
            this.colTotalVATAmount.FieldName = "TotalVATAmount";
            this.colTotalVATAmount.Name = "colTotalVATAmount";
            // 
            // colTotalAmountExcludedVAT
            // 
            this.colTotalAmountExcludedVAT.FieldName = "TotalAmountExcludedVAT";
            this.colTotalAmountExcludedVAT.Name = "colTotalAmountExcludedVAT";
            // 
            // colTotalAmountIncludedVAT
            // 
            this.colTotalAmountIncludedVAT.FieldName = "TotalAmountIncludedVAT";
            this.colTotalAmountIncludedVAT.Name = "colTotalAmountIncludedVAT";
            // 
            // colTotalAmount
            // 
            this.colTotalAmount.FieldName = "TotalAmount";
            this.colTotalAmount.Name = "colTotalAmount";
            this.colTotalAmount.Visible = true;
            this.colTotalAmount.VisibleIndex = 5;
            // 
            // colTotalQuantity
            // 
            this.colTotalQuantity.FieldName = "TotalQuantity";
            this.colTotalQuantity.Name = "colTotalQuantity";
            this.colTotalQuantity.Visible = true;
            this.colTotalQuantity.VisibleIndex = 6;
            // 
            // colNote
            // 
            this.colNote.FieldName = "Note";
            this.colNote.Name = "colNote";
            this.colNote.Visible = true;
            this.colNote.VisibleIndex = 7;
            // 
            // colCheckerID1
            // 
            this.colCheckerID1.FieldName = "CheckerID1";
            this.colCheckerID1.Name = "colCheckerID1";
            // 
            // colCheckerName1
            // 
            this.colCheckerName1.FieldName = "CheckerName1";
            this.colCheckerName1.Name = "colCheckerName1";
            // 
            // colCheckerID2
            // 
            this.colCheckerID2.FieldName = "CheckerID2";
            this.colCheckerID2.Name = "colCheckerID2";
            // 
            // colCheckerName2
            // 
            this.colCheckerName2.FieldName = "CheckerName2";
            this.colCheckerName2.Name = "colCheckerName2";
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            // 
            // colCreatedUserID
            // 
            this.colCreatedUserID.FieldName = "CreatedUserID";
            this.colCreatedUserID.Name = "colCreatedUserID";
            // 
            // colCreatedDate
            // 
            this.colCreatedDate.FieldName = "CreatedDate";
            this.colCreatedDate.Name = "colCreatedDate";
            // 
            // colUpdatedUserID
            // 
            this.colUpdatedUserID.FieldName = "UpdatedUserID";
            this.colUpdatedUserID.Name = "colUpdatedUserID";
            // 
            // colUpdatedDate
            // 
            this.colUpdatedDate.FieldName = "UpdatedDate";
            this.colUpdatedDate.Name = "colUpdatedDate";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(903, 407);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlData;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(883, 387);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // GoodTypeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Name = "GoodTypeControl";
            this.Size = new System.Drawing.Size(903, 484);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditmodality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl gridControlData;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewData;
        private DevExpress.XtraGrid.Columns.GridColumn colWRNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colModalityID;
        private DevExpress.XtraGrid.Columns.GridColumn colModalityName;
        private DevExpress.XtraGrid.Columns.GridColumn colWRDate;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colWRRNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colWRRReference;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseID;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierName;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomerID;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomerName;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaymentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPaymentTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn colHandlingStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colHandlingStatusName;
        private DevExpress.XtraGrid.Columns.GridColumn colBranchID;
        private DevExpress.XtraGrid.Columns.GridColumn colBranchName;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalOrderType;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalDiscountAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalVATAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountExcludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountIncludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckerID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckerName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckerID2;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckerName2;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdatedUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdatedDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraEditors.SearchControl searchControl11;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditmodality;
        private DevExpress.XtraLayout.LayoutControlGroup Root1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFromDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemToDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemModality;
    }
}
