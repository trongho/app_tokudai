﻿
namespace WarehouseManager
{
    partial class GoodManagerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GoodManagerControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition6 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.xtraTabControlGoods = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageGoodsGeneral = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditPartNumber = new DevExpress.XtraEditors.TextEdit();
            this.textEditPartName = new DevExpress.XtraEditors.TextEdit();
            this.textEditProductFamily = new DevExpress.XtraEditors.TextEdit();
            this.textEditCartonName = new DevExpress.XtraEditors.TextEdit();
            this.textEditMaterial = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditCartonSize = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditPads = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditNilon = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditLabelSize = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditMold = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditUnit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditPalletID = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalGoods = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDelete = new DevExpress.XtraEditors.SimpleButton();
            this.textEditCurrentIDCode = new DevExpress.XtraEditors.TextEdit();
            this.textEditStatus = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemGoodsID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGoodsName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStockUnitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPacking = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.goodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet = new WarehouseManager.WARHOUSE_HPDataSet();
            this.goodsTableAdapter = new WarehouseManager.WARHOUSE_HPDataSetTableAdapters.GoodsTableAdapter();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlGoods = new DevExpress.XtraGrid.GridControl();
            this.gridViewGoods = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductFamily = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPalletID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCartonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCartonSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPads = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNilon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabelSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentIDCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlGoods)).BeginInit();
            this.xtraTabControlGoods.SuspendLayout();
            this.xtraTabPageGoodsGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPartNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPartName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProductFamily.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCartonName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMaterial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditCartonSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPads.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditNilon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditLabelSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMold.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPalletID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGoods.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCurrentIDCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStockUnitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPacking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.goodsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlGoods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewGoods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(800, 204, 812, 500);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1451, 41);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1447, 32);
            this.windowsUIButtonPanel1.TabIndex = 4;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1451, 41);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1451, 41);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.xtraTabControlGoods);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 41);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(972, 257, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1451, 237);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // xtraTabControlGoods
            // 
            this.xtraTabControlGoods.Location = new System.Drawing.Point(12, 12);
            this.xtraTabControlGoods.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xtraTabControlGoods.Name = "xtraTabControlGoods";
            this.xtraTabControlGoods.SelectedTabPage = this.xtraTabPageGoodsGeneral;
            this.xtraTabControlGoods.Size = new System.Drawing.Size(1427, 213);
            this.xtraTabControlGoods.TabIndex = 4;
            this.xtraTabControlGoods.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageGoodsGeneral});
            // 
            // xtraTabPageGoodsGeneral
            // 
            this.xtraTabPageGoodsGeneral.Controls.Add(this.layoutControl4);
            this.xtraTabPageGoodsGeneral.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xtraTabPageGoodsGeneral.Name = "xtraTabPageGoodsGeneral";
            this.xtraTabPageGoodsGeneral.Size = new System.Drawing.Size(1422, 187);
            this.xtraTabPageGoodsGeneral.Text = "Thông tin chung";
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.label2);
            this.layoutControl4.Controls.Add(this.label1);
            this.layoutControl4.Controls.Add(this.textEditPartNumber);
            this.layoutControl4.Controls.Add(this.textEditPartName);
            this.layoutControl4.Controls.Add(this.textEditProductFamily);
            this.layoutControl4.Controls.Add(this.textEditCartonName);
            this.layoutControl4.Controls.Add(this.textEditMaterial);
            this.layoutControl4.Controls.Add(this.comboBoxEditCartonSize);
            this.layoutControl4.Controls.Add(this.comboBoxEditPads);
            this.layoutControl4.Controls.Add(this.comboBoxEditNilon);
            this.layoutControl4.Controls.Add(this.comboBoxEditLabelSize);
            this.layoutControl4.Controls.Add(this.comboBoxEditMold);
            this.layoutControl4.Controls.Add(this.comboBoxEditUnit);
            this.layoutControl4.Controls.Add(this.textEditQuantity);
            this.layoutControl4.Controls.Add(this.textEditPalletID);
            this.layoutControl4.Controls.Add(this.textEditTotalGoods);
            this.layoutControl4.Controls.Add(this.simpleButtonSave);
            this.layoutControl4.Controls.Add(this.simpleButtonDelete);
            this.layoutControl4.Controls.Add(this.textEditCurrentIDCode);
            this.layoutControl4.Controls.Add(this.textEditStatus);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(611, 257, 812, 500);
            this.layoutControl4.Root = this.layoutControlGroup3;
            this.layoutControl4.Size = new System.Drawing.Size(1422, 187);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(476, 151);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 24);
            this.label2.TabIndex = 41;
            this.label2.Text = "(*)";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(476, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 23);
            this.label1.TabIndex = 5;
            this.label1.Text = "(*)";
            // 
            // textEditPartNumber
            // 
            this.textEditPartNumber.Location = new System.Drawing.Point(117, 12);
            this.textEditPartNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditPartNumber.Name = "textEditPartNumber";
            this.textEditPartNumber.Size = new System.Drawing.Size(355, 20);
            this.textEditPartNumber.StyleController = this.layoutControl4;
            this.textEditPartNumber.TabIndex = 4;
            // 
            // textEditPartName
            // 
            this.textEditPartName.Location = new System.Drawing.Point(117, 39);
            this.textEditPartName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditPartName.Name = "textEditPartName";
            this.textEditPartName.Size = new System.Drawing.Size(355, 20);
            this.textEditPartName.StyleController = this.layoutControl4;
            this.textEditPartName.TabIndex = 6;
            // 
            // textEditProductFamily
            // 
            this.textEditProductFamily.Location = new System.Drawing.Point(117, 95);
            this.textEditProductFamily.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditProductFamily.Name = "textEditProductFamily";
            this.textEditProductFamily.Size = new System.Drawing.Size(355, 20);
            this.textEditProductFamily.StyleController = this.layoutControl4;
            this.textEditProductFamily.TabIndex = 8;
            // 
            // textEditCartonName
            // 
            this.textEditCartonName.Location = new System.Drawing.Point(615, 39);
            this.textEditCartonName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditCartonName.Name = "textEditCartonName";
            this.textEditCartonName.Size = new System.Drawing.Size(338, 20);
            this.textEditCartonName.StyleController = this.layoutControl4;
            this.textEditCartonName.TabIndex = 14;
            // 
            // textEditMaterial
            // 
            this.textEditMaterial.Location = new System.Drawing.Point(1095, 12);
            this.textEditMaterial.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditMaterial.Name = "textEditMaterial";
            this.textEditMaterial.Size = new System.Drawing.Size(315, 20);
            this.textEditMaterial.StyleController = this.layoutControl4;
            this.textEditMaterial.TabIndex = 19;
            // 
            // comboBoxEditCartonSize
            // 
            this.comboBoxEditCartonSize.Location = new System.Drawing.Point(615, 67);
            this.comboBoxEditCartonSize.Name = "comboBoxEditCartonSize";
            this.comboBoxEditCartonSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditCartonSize.Size = new System.Drawing.Size(338, 20);
            this.comboBoxEditCartonSize.StyleController = this.layoutControl4;
            this.comboBoxEditCartonSize.TabIndex = 30;
            // 
            // comboBoxEditPads
            // 
            this.comboBoxEditPads.Location = new System.Drawing.Point(615, 95);
            this.comboBoxEditPads.Name = "comboBoxEditPads";
            this.comboBoxEditPads.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditPads.Size = new System.Drawing.Size(338, 20);
            this.comboBoxEditPads.StyleController = this.layoutControl4;
            this.comboBoxEditPads.TabIndex = 31;
            // 
            // comboBoxEditNilon
            // 
            this.comboBoxEditNilon.Location = new System.Drawing.Point(615, 123);
            this.comboBoxEditNilon.Name = "comboBoxEditNilon";
            this.comboBoxEditNilon.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditNilon.Size = new System.Drawing.Size(338, 20);
            this.comboBoxEditNilon.StyleController = this.layoutControl4;
            this.comboBoxEditNilon.TabIndex = 32;
            // 
            // comboBoxEditLabelSize
            // 
            this.comboBoxEditLabelSize.Location = new System.Drawing.Point(615, 151);
            this.comboBoxEditLabelSize.Name = "comboBoxEditLabelSize";
            this.comboBoxEditLabelSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditLabelSize.Size = new System.Drawing.Size(338, 20);
            this.comboBoxEditLabelSize.StyleController = this.layoutControl4;
            this.comboBoxEditLabelSize.TabIndex = 33;
            // 
            // comboBoxEditMold
            // 
            this.comboBoxEditMold.Location = new System.Drawing.Point(615, 12);
            this.comboBoxEditMold.Name = "comboBoxEditMold";
            this.comboBoxEditMold.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditMold.Size = new System.Drawing.Size(338, 20);
            this.comboBoxEditMold.StyleController = this.layoutControl4;
            this.comboBoxEditMold.TabIndex = 34;
            // 
            // comboBoxEditUnit
            // 
            this.comboBoxEditUnit.Location = new System.Drawing.Point(117, 67);
            this.comboBoxEditUnit.Name = "comboBoxEditUnit";
            this.comboBoxEditUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditUnit.Size = new System.Drawing.Size(355, 20);
            this.comboBoxEditUnit.StyleController = this.layoutControl4;
            this.comboBoxEditUnit.TabIndex = 35;
            // 
            // textEditQuantity
            // 
            this.textEditQuantity.Location = new System.Drawing.Point(117, 123);
            this.textEditQuantity.Name = "textEditQuantity";
            this.textEditQuantity.Size = new System.Drawing.Size(355, 20);
            this.textEditQuantity.StyleController = this.layoutControl4;
            this.textEditQuantity.TabIndex = 36;
            // 
            // textEditPalletID
            // 
            this.textEditPalletID.Location = new System.Drawing.Point(117, 151);
            this.textEditPalletID.Name = "textEditPalletID";
            this.textEditPalletID.Size = new System.Drawing.Size(355, 20);
            this.textEditPalletID.StyleController = this.layoutControl4;
            this.textEditPalletID.TabIndex = 37;
            // 
            // textEditTotalGoods
            // 
            this.textEditTotalGoods.Location = new System.Drawing.Point(1095, 39);
            this.textEditTotalGoods.Name = "textEditTotalGoods";
            this.textEditTotalGoods.Size = new System.Drawing.Size(315, 20);
            this.textEditTotalGoods.StyleController = this.layoutControl4;
            this.textEditTotalGoods.TabIndex = 38;
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Location = new System.Drawing.Point(990, 151);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(207, 22);
            this.simpleButtonSave.StyleController = this.layoutControl4;
            this.simpleButtonSave.TabIndex = 39;
            this.simpleButtonSave.Text = "Lưu";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // simpleButtonDelete
            // 
            this.simpleButtonDelete.Location = new System.Drawing.Point(1201, 151);
            this.simpleButtonDelete.Name = "simpleButtonDelete";
            this.simpleButtonDelete.Size = new System.Drawing.Size(209, 22);
            this.simpleButtonDelete.StyleController = this.layoutControl4;
            this.simpleButtonDelete.TabIndex = 40;
            this.simpleButtonDelete.Text = "Xóa";
            this.simpleButtonDelete.Click += new System.EventHandler(this.simpleButtonDelete_ClickAsync);
            // 
            // textEditCurrentIDCode
            // 
            this.textEditCurrentIDCode.Location = new System.Drawing.Point(1095, 67);
            this.textEditCurrentIDCode.Name = "textEditCurrentIDCode";
            this.textEditCurrentIDCode.Size = new System.Drawing.Size(102, 20);
            this.textEditCurrentIDCode.StyleController = this.layoutControl4;
            this.textEditCurrentIDCode.TabIndex = 43;
            // 
            // textEditStatus
            // 
            this.textEditStatus.Location = new System.Drawing.Point(1095, 95);
            this.textEditStatus.Name = "textEditStatus";
            this.textEditStatus.Size = new System.Drawing.Size(102, 20);
            this.textEditStatus.StyleController = this.layoutControl4;
            this.textEditStatus.TabIndex = 44;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemGoodsID,
            this.layoutControlItemGoodsName,
            this.layoutControlItemStockUnitID,
            this.layoutControlItemDescription,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItemPacking,
            this.layoutControlItem3,
            this.layoutControlItem8,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.layoutControlItem18,
            this.layoutControlItem17});
            this.layoutControlGroup3.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.BackColor = System.Drawing.Color.Red;
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.BackColor2 = System.Drawing.Color.Red;
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.BorderColor = System.Drawing.Color.Red;
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.ForeColor = System.Drawing.Color.Red;
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.Options.UseBackColor = true;
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.Options.UseBorderColor = true;
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.Options.UseForeColor = true;
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 33.0983757278578D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 2.43227334860801D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 31.916290880434307D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 2.3836278816358494D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 15.084716080732019D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 15.084716080732019D;
            this.layoutControlGroup3.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6});
            rowDefinition1.Height = 16.666666666666668D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 16.666666666666668D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 16.666666666666668D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 16.666666666666668D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition5.Height = 16.666666666666668D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition6.Height = 16.666666666666668D;
            rowDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup3.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5,
            rowDefinition6});
            this.layoutControlGroup3.Size = new System.Drawing.Size(1422, 187);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItemGoodsID
            // 
            this.layoutControlItemGoodsID.Control = this.textEditPartNumber;
            this.layoutControlItemGoodsID.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemGoodsID.Name = "layoutControlItemGoodsID";
            this.layoutControlItemGoodsID.Size = new System.Drawing.Size(464, 27);
            this.layoutControlItemGoodsID.Text = "Part Number";
            this.layoutControlItemGoodsID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItemGoodsName
            // 
            this.layoutControlItemGoodsName.Control = this.textEditPartName;
            this.layoutControlItemGoodsName.Location = new System.Drawing.Point(0, 27);
            this.layoutControlItemGoodsName.Name = "layoutControlItemGoodsName";
            this.layoutControlItemGoodsName.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemGoodsName.Size = new System.Drawing.Size(464, 28);
            this.layoutControlItemGoodsName.Text = "Part Name";
            this.layoutControlItemGoodsName.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItemStockUnitID
            // 
            this.layoutControlItemStockUnitID.Control = this.textEditProductFamily;
            this.layoutControlItemStockUnitID.Location = new System.Drawing.Point(0, 83);
            this.layoutControlItemStockUnitID.Name = "layoutControlItemStockUnitID";
            this.layoutControlItemStockUnitID.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItemStockUnitID.Size = new System.Drawing.Size(464, 28);
            this.layoutControlItemStockUnitID.Text = "Product Family";
            this.layoutControlItemStockUnitID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItemDescription
            // 
            this.layoutControlItemDescription.Control = this.textEditCartonName;
            this.layoutControlItemDescription.Location = new System.Drawing.Point(498, 27);
            this.layoutControlItemDescription.Name = "layoutControlItemDescription";
            this.layoutControlItemDescription.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemDescription.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemDescription.Size = new System.Drawing.Size(447, 28);
            this.layoutControlItemDescription.Text = "Carton Name";
            this.layoutControlItemDescription.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.comboBoxEditCartonSize;
            this.layoutControlItem9.Location = new System.Drawing.Point(498, 55);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem9.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem9.Size = new System.Drawing.Size(447, 28);
            this.layoutControlItem9.Text = "Carton Size";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.comboBoxEditPads;
            this.layoutControlItem10.Location = new System.Drawing.Point(498, 83);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem10.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem10.Size = new System.Drawing.Size(447, 28);
            this.layoutControlItem10.Text = "Pads";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.comboBoxEditNilon;
            this.layoutControlItem11.Location = new System.Drawing.Point(498, 111);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem11.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem11.Size = new System.Drawing.Size(447, 28);
            this.layoutControlItem11.Text = "Nilon";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.comboBoxEditLabelSize;
            this.layoutControlItem12.Location = new System.Drawing.Point(498, 139);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem12.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItem12.Size = new System.Drawing.Size(447, 28);
            this.layoutControlItem12.Text = "Label Size";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.comboBoxEditMold;
            this.layoutControlItem13.Location = new System.Drawing.Point(498, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem13.Size = new System.Drawing.Size(447, 27);
            this.layoutControlItem13.Text = "Mold";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.comboBoxEditUnit;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 55);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem14.Size = new System.Drawing.Size(464, 28);
            this.layoutControlItem14.Text = "Unit";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.textEditQuantity;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 111);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem15.Size = new System.Drawing.Size(464, 28);
            this.layoutControlItem15.Text = "Quy cách";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.textEditPalletID;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 139);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItem16.Size = new System.Drawing.Size(464, 28);
            this.layoutControlItem16.Text = "Pallet ID";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItemPacking
            // 
            this.layoutControlItemPacking.Control = this.textEditMaterial;
            this.layoutControlItemPacking.Location = new System.Drawing.Point(978, 0);
            this.layoutControlItemPacking.Name = "layoutControlItemPacking";
            this.layoutControlItemPacking.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemPacking.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemPacking.Size = new System.Drawing.Size(424, 27);
            this.layoutControlItemPacking.Text = "Material";
            this.layoutControlItemPacking.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(464, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(34, 27);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.label2;
            this.layoutControlItem8.Location = new System.Drawing.Point(464, 139);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItem8.Size = new System.Drawing.Size(34, 28);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButtonSave;
            this.layoutControlItem6.Location = new System.Drawing.Point(978, 139);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem6.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItem6.Size = new System.Drawing.Size(211, 28);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButtonDelete;
            this.layoutControlItem7.Location = new System.Drawing.Point(1189, 139);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItem7.Size = new System.Drawing.Size(213, 28);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEditTotalGoods;
            this.layoutControlItem4.Location = new System.Drawing.Point(978, 27);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem4.Size = new System.Drawing.Size(424, 28);
            this.layoutControlItem4.Text = "Σ Pallet- Part Number";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.textEditCurrentIDCode;
            this.layoutControlItem18.Location = new System.Drawing.Point(978, 55);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem18.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem18.Size = new System.Drawing.Size(211, 28);
            this.layoutControlItem18.Text = "STT in tem";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.textEditStatus;
            this.layoutControlItem17.Location = new System.Drawing.Point(978, 83);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem17.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem17.Size = new System.Drawing.Size(211, 28);
            this.layoutControlItem17.Text = "Trạng thái";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1451, 237);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.xtraTabControlGoods;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(89, 19);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1431, 217);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // goodsBindingSource
            // 
            this.goodsBindingSource.DataMember = "Goods";
            this.goodsBindingSource.DataSource = this.wARHOUSE_HPDataSet;
            // 
            // wARHOUSE_HPDataSet
            // 
            this.wARHOUSE_HPDataSet.DataSetName = "WARHOUSE_HPDataSet";
            this.wARHOUSE_HPDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // goodsTableAdapter
            // 
            this.goodsTableAdapter.ClearBeforeFill = true;
            // 
            // gridView1
            // 
            this.gridView1.Name = "gridView1";
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.gridControlGoods);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(0, 278);
            this.layoutControl5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup4;
            this.layoutControl5.Size = new System.Drawing.Size(1451, 364);
            this.layoutControl5.TabIndex = 2;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // gridControlGoods
            // 
            this.gridControlGoods.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlGoods.Location = new System.Drawing.Point(12, 12);
            this.gridControlGoods.MainView = this.gridViewGoods;
            this.gridControlGoods.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlGoods.Name = "gridControlGoods";
            this.gridControlGoods.Size = new System.Drawing.Size(1427, 340);
            this.gridControlGoods.TabIndex = 6;
            this.gridControlGoods.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewGoods});
            // 
            // gridViewGoods
            // 
            this.gridViewGoods.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNo,
            this.colStatus,
            this.colCreatedUserID,
            this.colCreatedDate,
            this.colUpdatedUserID,
            this.colUpdatedDate,
            this.colPartNumber,
            this.colPartName,
            this.colUnit,
            this.colProductFamily,
            this.colQuantity,
            this.colPalletID,
            this.colMold,
            this.colCartonName,
            this.colCartonSize,
            this.colPads,
            this.colNilon,
            this.colLabelSize,
            this.colMaterial,
            this.colOrdinal,
            this.colCurrentIDCode});
            this.gridViewGoods.DetailHeight = 284;
            this.gridViewGoods.GridControl = this.gridControlGoods;
            this.gridViewGoods.Name = "gridViewGoods";
            this.gridViewGoods.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrdinal, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewGoods.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridViewGoods_RowCellClick);
            // 
            // colNo
            // 
            this.colNo.Caption = "No";
            this.colNo.FieldName = "No";
            this.colNo.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colNo.MinWidth = 34;
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 34;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 86;
            this.colStatus.Name = "colStatus";
            this.colStatus.Width = 86;
            // 
            // colCreatedUserID
            // 
            this.colCreatedUserID.FieldName = "CreatedUserID";
            this.colCreatedUserID.MinWidth = 86;
            this.colCreatedUserID.Name = "colCreatedUserID";
            this.colCreatedUserID.Width = 86;
            // 
            // colCreatedDate
            // 
            this.colCreatedDate.FieldName = "CreatedDate";
            this.colCreatedDate.MinWidth = 86;
            this.colCreatedDate.Name = "colCreatedDate";
            this.colCreatedDate.Width = 86;
            // 
            // colUpdatedUserID
            // 
            this.colUpdatedUserID.FieldName = "UpdatedUserID";
            this.colUpdatedUserID.MinWidth = 86;
            this.colUpdatedUserID.Name = "colUpdatedUserID";
            this.colUpdatedUserID.Width = 86;
            // 
            // colUpdatedDate
            // 
            this.colUpdatedDate.FieldName = "UpdatedDate";
            this.colUpdatedDate.MinWidth = 86;
            this.colUpdatedDate.Name = "colUpdatedDate";
            this.colUpdatedDate.Width = 86;
            // 
            // colPartNumber
            // 
            this.colPartNumber.Caption = "Part Number";
            this.colPartNumber.FieldName = "PartNumber";
            this.colPartNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPartNumber.Name = "colPartNumber";
            this.colPartNumber.Visible = true;
            this.colPartNumber.VisibleIndex = 1;
            this.colPartNumber.Width = 64;
            // 
            // colPartName
            // 
            this.colPartName.Caption = "Part Name";
            this.colPartName.FieldName = "PartName";
            this.colPartName.Name = "colPartName";
            this.colPartName.Visible = true;
            this.colPartName.VisibleIndex = 2;
            this.colPartName.Width = 64;
            // 
            // colUnit
            // 
            this.colUnit.Caption = "Unit";
            this.colUnit.FieldName = "Unit";
            this.colUnit.Name = "colUnit";
            this.colUnit.Visible = true;
            this.colUnit.VisibleIndex = 3;
            this.colUnit.Width = 64;
            // 
            // colProductFamily
            // 
            this.colProductFamily.Caption = "Product Family";
            this.colProductFamily.FieldName = "ProductFamily";
            this.colProductFamily.Name = "colProductFamily";
            this.colProductFamily.Visible = true;
            this.colProductFamily.VisibleIndex = 4;
            this.colProductFamily.Width = 64;
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "Quy cách";
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 5;
            this.colQuantity.Width = 64;
            // 
            // colPalletID
            // 
            this.colPalletID.Caption = "Pallet ID";
            this.colPalletID.FieldName = "PalletID";
            this.colPalletID.Name = "colPalletID";
            this.colPalletID.Visible = true;
            this.colPalletID.VisibleIndex = 6;
            this.colPalletID.Width = 64;
            // 
            // colMold
            // 
            this.colMold.Caption = "Mold";
            this.colMold.FieldName = "Mold";
            this.colMold.Name = "colMold";
            this.colMold.Visible = true;
            this.colMold.VisibleIndex = 7;
            this.colMold.Width = 64;
            // 
            // colCartonName
            // 
            this.colCartonName.Caption = "Carton Name";
            this.colCartonName.FieldName = "CartonName";
            this.colCartonName.Name = "colCartonName";
            this.colCartonName.Visible = true;
            this.colCartonName.VisibleIndex = 8;
            this.colCartonName.Width = 64;
            // 
            // colCartonSize
            // 
            this.colCartonSize.Caption = "Carton Size";
            this.colCartonSize.FieldName = "CartonSize";
            this.colCartonSize.Name = "colCartonSize";
            this.colCartonSize.Visible = true;
            this.colCartonSize.VisibleIndex = 9;
            this.colCartonSize.Width = 64;
            // 
            // colPads
            // 
            this.colPads.Caption = "Pads";
            this.colPads.FieldName = "Pads";
            this.colPads.Name = "colPads";
            this.colPads.Visible = true;
            this.colPads.VisibleIndex = 10;
            this.colPads.Width = 64;
            // 
            // colNilon
            // 
            this.colNilon.Caption = "Nilon";
            this.colNilon.FieldName = "Nilon";
            this.colNilon.Name = "colNilon";
            this.colNilon.Visible = true;
            this.colNilon.VisibleIndex = 11;
            this.colNilon.Width = 64;
            // 
            // colLabelSize
            // 
            this.colLabelSize.Caption = "Label Size";
            this.colLabelSize.FieldName = "LabelSize";
            this.colLabelSize.Name = "colLabelSize";
            this.colLabelSize.Visible = true;
            this.colLabelSize.VisibleIndex = 12;
            this.colLabelSize.Width = 64;
            // 
            // colMaterial
            // 
            this.colMaterial.Caption = "Material";
            this.colMaterial.FieldName = "Material";
            this.colMaterial.Name = "colMaterial";
            this.colMaterial.Visible = true;
            this.colMaterial.VisibleIndex = 13;
            this.colMaterial.Width = 64;
            // 
            // colOrdinal
            // 
            this.colOrdinal.Caption = "Ordinal";
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.Name = "colOrdinal";
            // 
            // colCurrentIDCode
            // 
            this.colCurrentIDCode.Caption = "STT in tem";
            this.colCurrentIDCode.FieldName = "CurrentIDCode";
            this.colCurrentIDCode.Name = "colCurrentIDCode";
            this.colCurrentIDCode.Visible = true;
            this.colCurrentIDCode.VisibleIndex = 14;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1451, 364);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControlGoods;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(1431, 344);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // GoodManagerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl5);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Name = "GoodManagerControl";
            this.Size = new System.Drawing.Size(1451, 642);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlGoods)).EndInit();
            this.xtraTabControlGoods.ResumeLayout(false);
            this.xtraTabPageGoodsGeneral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditPartNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPartName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProductFamily.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCartonName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMaterial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditCartonSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPads.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditNilon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditLabelSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMold.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPalletID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGoods.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCurrentIDCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStockUnitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPacking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.goodsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlGoods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewGoods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlGoods;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageGoodsGeneral;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.BindingSource goodsBindingSource;
        private WARHOUSE_HPDataSet wARHOUSE_HPDataSet;
        private WARHOUSE_HPDataSetTableAdapters.GoodsTableAdapter goodsTableAdapter;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEditPartNumber;
        private DevExpress.XtraEditors.TextEdit textEditPartName;
        private DevExpress.XtraEditors.TextEdit textEditProductFamily;
        private DevExpress.XtraEditors.TextEdit textEditCartonName;
        private DevExpress.XtraEditors.TextEdit textEditMaterial;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGoodsID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGoodsName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStockUnitID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDescription;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraGrid.GridControl gridControlGoods;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewGoods;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdatedUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdatedDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditCartonSize;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditPads;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditNilon;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditLabelSize;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditMold;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditUnit;
        private DevExpress.XtraEditors.TextEdit textEditQuantity;
        private DevExpress.XtraEditors.TextEdit textEditPalletID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPacking;
        private DevExpress.XtraGrid.Columns.GridColumn colPartNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPartName;
        private DevExpress.XtraGrid.Columns.GridColumn colUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colProductFamily;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colPalletID;
        private DevExpress.XtraGrid.Columns.GridColumn colMold;
        private DevExpress.XtraGrid.Columns.GridColumn colCartonName;
        private DevExpress.XtraGrid.Columns.GridColumn colCartonSize;
        private DevExpress.XtraGrid.Columns.GridColumn colPads;
        private DevExpress.XtraGrid.Columns.GridColumn colNilon;
        private DevExpress.XtraGrid.Columns.GridColumn colLabelSize;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterial;
        private DevExpress.XtraEditors.TextEdit textEditTotalGoods;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDelete;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentIDCode;
        private DevExpress.XtraEditors.TextEdit textEditCurrentIDCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.TextEdit textEditStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
    }
}
