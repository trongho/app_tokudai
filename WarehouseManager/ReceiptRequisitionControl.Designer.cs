﻿
namespace WarehouseManager
{
    partial class ReceiptRequisitionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReceiptRequisitionControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions10 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditWRRNumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditWRRDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditTotalQuantityByItem = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalQuantityByPack = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditPalletID = new DevExpress.XtraEditors.TextEdit();
            this.textEditPartNumber = new DevExpress.XtraEditors.TextEdit();
            this.textEditPartName = new DevExpress.XtraEditors.TextEdit();
            this.textEditProductFamily = new DevExpress.XtraEditors.TextEdit();
            this.textEditLotID = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditUnit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditQuantity = new DevExpress.XtraEditors.TextEdit();
            this.dateEditMFGDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditPONumber = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDelete = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonClear = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditEXPDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditRemark = new DevExpress.XtraEditors.MemoEdit();
            this.textEditQuantityByPack = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemWRRNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityByItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityByPack = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRRDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRRDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.wRRDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlWRRDetail = new DevExpress.XtraGrid.GridControl();
            this.gridViewWRRDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWRRNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPalletID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductFamily = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLotID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByItem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByPack = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMFGDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXPDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.collNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDCodeRange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByPrinted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemainingPrintQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRRNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRRDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRRDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByPack.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPalletID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPartNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPartName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProductFamily.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLotID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditMFGDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditMFGDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPONumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEXPDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEXPDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditQuantityByPack.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByPack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(400, 144, 812, 500);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1380, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions10.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions10.Image")));
            windowsUIButtonImageOptions10.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Làm mới", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Tìm kiếm", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn( Pro BT)", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("In ấn(Automation BT)", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print_automation", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions10, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1376, 32);
            this.windowsUIButtonPanel1.TabIndex = 4;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1380, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1380, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.label2);
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEditWRRNumber);
            this.layoutControl2.Controls.Add(this.dateEditWRRDate);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityByItem);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityByPack);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl2.Controls.Add(this.textEditPalletID);
            this.layoutControl2.Controls.Add(this.textEditPartNumber);
            this.layoutControl2.Controls.Add(this.textEditPartName);
            this.layoutControl2.Controls.Add(this.textEditProductFamily);
            this.layoutControl2.Controls.Add(this.textEditLotID);
            this.layoutControl2.Controls.Add(this.comboBoxEditUnit);
            this.layoutControl2.Controls.Add(this.textEditQuantity);
            this.layoutControl2.Controls.Add(this.dateEditMFGDate);
            this.layoutControl2.Controls.Add(this.textEditPONumber);
            this.layoutControl2.Controls.Add(this.simpleButtonSave);
            this.layoutControl2.Controls.Add(this.simpleButtonDelete);
            this.layoutControl2.Controls.Add(this.simpleButtonClear);
            this.layoutControl2.Controls.Add(this.dateEditEXPDate);
            this.layoutControl2.Controls.Add(this.textEditRemark);
            this.layoutControl2.Controls.Add(this.textEditQuantityByPack);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(711, 260, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1380, 165);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(276, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 25);
            this.label2.TabIndex = 31;
            this.label2.Text = "(*)";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(276, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "(*)";
            // 
            // textEditWRRNumber
            // 
            this.textEditWRRNumber.Location = new System.Drawing.Point(99, 12);
            this.textEditWRRNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditWRRNumber.Name = "textEditWRRNumber";
            this.textEditWRRNumber.Size = new System.Drawing.Size(173, 20);
            this.textEditWRRNumber.StyleController = this.layoutControl2;
            this.textEditWRRNumber.TabIndex = 4;
            this.textEditWRRNumber.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.textEditWRRNumber_PreviewKeyDown);
            // 
            // dateEditWRRDate
            // 
            this.dateEditWRRDate.EditValue = null;
            this.dateEditWRRDate.Location = new System.Drawing.Point(395, 12);
            this.dateEditWRRDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateEditWRRDate.Name = "dateEditWRRDate";
            this.dateEditWRRDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRRDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRRDate.Properties.CalendarTimeProperties.DisplayFormat.FormatString = "d";
            this.dateEditWRRDate.Properties.CalendarTimeProperties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRRDate.Properties.CalendarTimeProperties.EditFormat.FormatString = "d";
            this.dateEditWRRDate.Properties.CalendarTimeProperties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRRDate.Size = new System.Drawing.Size(181, 20);
            this.dateEditWRRDate.StyleController = this.layoutControl2;
            this.dateEditWRRDate.TabIndex = 7;
            // 
            // textEditTotalQuantityByItem
            // 
            this.textEditTotalQuantityByItem.Location = new System.Drawing.Point(1187, 12);
            this.textEditTotalQuantityByItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityByItem.Name = "textEditTotalQuantityByItem";
            this.textEditTotalQuantityByItem.Properties.DisplayFormat.FormatString = "G29";
            this.textEditTotalQuantityByItem.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.textEditTotalQuantityByItem.Size = new System.Drawing.Size(181, 20);
            this.textEditTotalQuantityByItem.StyleController = this.layoutControl2;
            this.textEditTotalQuantityByItem.TabIndex = 10;
            // 
            // textEditTotalQuantityByPack
            // 
            this.textEditTotalQuantityByPack.Location = new System.Drawing.Point(1187, 41);
            this.textEditTotalQuantityByPack.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityByPack.Name = "textEditTotalQuantityByPack";
            this.textEditTotalQuantityByPack.Properties.DisplayFormat.FormatString = "G29";
            this.textEditTotalQuantityByPack.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.textEditTotalQuantityByPack.Size = new System.Drawing.Size(181, 20);
            this.textEditTotalQuantityByPack.StyleController = this.layoutControl2;
            this.textEditTotalQuantityByPack.TabIndex = 11;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(923, 12);
            this.comboBoxEditHandlingStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(181, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 13;
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(659, 12);
            this.comboBoxEditBranch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(181, 20);
            this.comboBoxEditBranch.StyleController = this.layoutControl2;
            this.comboBoxEditBranch.TabIndex = 14;
            // 
            // textEditPalletID
            // 
            this.textEditPalletID.Location = new System.Drawing.Point(91, 70);
            this.textEditPalletID.Name = "textEditPalletID";
            this.textEditPalletID.Size = new System.Drawing.Size(181, 20);
            this.textEditPalletID.StyleController = this.layoutControl2;
            this.textEditPalletID.TabIndex = 15;
            // 
            // textEditPartNumber
            // 
            this.textEditPartNumber.Location = new System.Drawing.Point(91, 41);
            this.textEditPartNumber.Name = "textEditPartNumber";
            this.textEditPartNumber.Size = new System.Drawing.Size(181, 20);
            this.textEditPartNumber.StyleController = this.layoutControl2;
            this.textEditPartNumber.TabIndex = 16;
            // 
            // textEditPartName
            // 
            this.textEditPartName.Location = new System.Drawing.Point(91, 99);
            this.textEditPartName.Name = "textEditPartName";
            this.textEditPartName.Size = new System.Drawing.Size(181, 20);
            this.textEditPartName.StyleController = this.layoutControl2;
            this.textEditPartName.TabIndex = 17;
            // 
            // textEditProductFamily
            // 
            this.textEditProductFamily.Location = new System.Drawing.Point(91, 128);
            this.textEditProductFamily.Name = "textEditProductFamily";
            this.textEditProductFamily.Size = new System.Drawing.Size(181, 20);
            this.textEditProductFamily.StyleController = this.layoutControl2;
            this.textEditProductFamily.TabIndex = 18;
            // 
            // textEditLotID
            // 
            this.textEditLotID.Location = new System.Drawing.Point(395, 41);
            this.textEditLotID.Name = "textEditLotID";
            this.textEditLotID.Size = new System.Drawing.Size(181, 20);
            this.textEditLotID.StyleController = this.layoutControl2;
            this.textEditLotID.TabIndex = 19;
            this.textEditLotID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textEditLotID_KeyDown);
            // 
            // comboBoxEditUnit
            // 
            this.comboBoxEditUnit.Location = new System.Drawing.Point(395, 70);
            this.comboBoxEditUnit.Name = "comboBoxEditUnit";
            this.comboBoxEditUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditUnit.Size = new System.Drawing.Size(181, 20);
            this.comboBoxEditUnit.StyleController = this.layoutControl2;
            this.comboBoxEditUnit.TabIndex = 20;
            // 
            // textEditQuantity
            // 
            this.textEditQuantity.Location = new System.Drawing.Point(659, 41);
            this.textEditQuantity.Name = "textEditQuantity";
            this.textEditQuantity.Properties.DisplayFormat.FormatString = "G29";
            this.textEditQuantity.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.textEditQuantity.Properties.Mask.BeepOnError = true;
            this.textEditQuantity.Properties.Mask.EditMask = "d";
            this.textEditQuantity.Properties.Mask.IgnoreMaskBlank = false;
            this.textEditQuantity.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditQuantity.Size = new System.Drawing.Size(181, 20);
            this.textEditQuantity.StyleController = this.layoutControl2;
            this.textEditQuantity.TabIndex = 21;
            // 
            // dateEditMFGDate
            // 
            this.dateEditMFGDate.EditValue = null;
            this.dateEditMFGDate.Location = new System.Drawing.Point(923, 70);
            this.dateEditMFGDate.Name = "dateEditMFGDate";
            this.dateEditMFGDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditMFGDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditMFGDate.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.dateEditMFGDate.Properties.DisplayFormat.FormatString = "dd-MM-yyyy";
            this.dateEditMFGDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditMFGDate.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditMFGDate.Properties.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.dateEditMFGDate_Properties_Closed);
            this.dateEditMFGDate.Size = new System.Drawing.Size(181, 20);
            this.dateEditMFGDate.StyleController = this.layoutControl2;
            this.dateEditMFGDate.TabIndex = 22;
            // 
            // textEditPONumber
            // 
            this.textEditPONumber.Location = new System.Drawing.Point(659, 70);
            this.textEditPONumber.Name = "textEditPONumber";
            this.textEditPONumber.Size = new System.Drawing.Size(181, 20);
            this.textEditPONumber.StyleController = this.layoutControl2;
            this.textEditPONumber.TabIndex = 24;
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Location = new System.Drawing.Point(1108, 70);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(260, 22);
            this.simpleButtonSave.StyleController = this.layoutControl2;
            this.simpleButtonSave.TabIndex = 26;
            this.simpleButtonSave.Text = "Lưu( Ctrl+S)";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // simpleButtonDelete
            // 
            this.simpleButtonDelete.Location = new System.Drawing.Point(1108, 99);
            this.simpleButtonDelete.Name = "simpleButtonDelete";
            this.simpleButtonDelete.Size = new System.Drawing.Size(260, 22);
            this.simpleButtonDelete.StyleController = this.layoutControl2;
            this.simpleButtonDelete.TabIndex = 27;
            this.simpleButtonDelete.Text = "Xóa( Ctrl+D)";
            this.simpleButtonDelete.Click += new System.EventHandler(this.simpleButtonDelete_Click);
            // 
            // simpleButtonClear
            // 
            this.simpleButtonClear.Location = new System.Drawing.Point(1108, 128);
            this.simpleButtonClear.Name = "simpleButtonClear";
            this.simpleButtonClear.Size = new System.Drawing.Size(260, 22);
            this.simpleButtonClear.StyleController = this.layoutControl2;
            this.simpleButtonClear.TabIndex = 28;
            this.simpleButtonClear.Text = "Nhập lại( Ctrl+R)";
            this.simpleButtonClear.Click += new System.EventHandler(this.simpleButtonClear_Click);
            // 
            // dateEditEXPDate
            // 
            this.dateEditEXPDate.EditValue = null;
            this.dateEditEXPDate.Location = new System.Drawing.Point(923, 99);
            this.dateEditEXPDate.Name = "dateEditEXPDate";
            this.dateEditEXPDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEXPDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEXPDate.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.dateEditEXPDate.Properties.DisplayFormat.FormatString = "dd-MM-yyyy";
            this.dateEditEXPDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditEXPDate.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditEXPDate.Size = new System.Drawing.Size(181, 20);
            this.dateEditEXPDate.StyleController = this.layoutControl2;
            this.dateEditEXPDate.TabIndex = 29;
            // 
            // textEditRemark
            // 
            this.textEditRemark.Location = new System.Drawing.Point(395, 99);
            this.textEditRemark.Name = "textEditRemark";
            this.textEditRemark.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textEditRemark.Size = new System.Drawing.Size(445, 54);
            this.textEditRemark.StyleController = this.layoutControl2;
            this.textEditRemark.TabIndex = 25;
            // 
            // textEditQuantityByPack
            // 
            this.textEditQuantityByPack.Location = new System.Drawing.Point(923, 41);
            this.textEditQuantityByPack.Name = "textEditQuantityByPack";
            this.textEditQuantityByPack.Properties.Mask.EditMask = "d";
            this.textEditQuantityByPack.Properties.Mask.IgnoreMaskBlank = false;
            this.textEditQuantityByPack.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditQuantityByPack.Size = new System.Drawing.Size(181, 20);
            this.textEditQuantityByPack.StyleController = this.layoutControl2;
            this.textEditQuantityByPack.TabIndex = 30;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemWRRNumber,
            this.layoutControlItem3,
            this.layoutControlItemTotalQuantityByItem,
            this.layoutControlItemTotalQuantityByPack,
            this.layoutControlItemWRRDate,
            this.layoutControlItemBranch,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem10,
            this.layoutControlItem13,
            this.layoutControlItem5,
            this.layoutControlItem4,
            this.layoutControlItem14,
            this.layoutControlItem18,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem19});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 19.405592722871681D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 2.9720363856415997D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 19.405592722871681D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 19.405592722871681D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 19.405592722871681D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 19.405592722871681D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6});
            rowDefinition1.Height = 20D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 20D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 20D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 20D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition5.Height = 20D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1380, 165);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemWRRNumber
            // 
            this.layoutControlItemWRRNumber.Control = this.textEditWRRNumber;
            this.layoutControlItemWRRNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemWRRNumber.Name = "layoutControlItemWRRNumber";
            this.layoutControlItemWRRNumber.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItemWRRNumber.Text = "Số phiếu yêu cầu";
            this.layoutControlItemWRRNumber.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemWRRNumber.TextSize = new System.Drawing.Size(82, 13);
            this.layoutControlItemWRRNumber.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(264, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(40, 29);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemTotalQuantityByItem
            // 
            this.layoutControlItemTotalQuantityByItem.Control = this.textEditTotalQuantityByItem;
            this.layoutControlItemTotalQuantityByItem.Location = new System.Drawing.Point(1096, 0);
            this.layoutControlItemTotalQuantityByItem.Name = "layoutControlItemTotalQuantityByItem";
            this.layoutControlItemTotalQuantityByItem.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalQuantityByItem.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItemTotalQuantityByItem.Text = "∑ SL yêu cầu";
            this.layoutControlItemTotalQuantityByItem.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemTotalQuantityByPack
            // 
            this.layoutControlItemTotalQuantityByPack.Control = this.textEditTotalQuantityByPack;
            this.layoutControlItemTotalQuantityByPack.CustomizationFormText = "Σ Mã yêu cầu";
            this.layoutControlItemTotalQuantityByPack.Location = new System.Drawing.Point(1096, 29);
            this.layoutControlItemTotalQuantityByPack.Name = "layoutControlItemTotalQuantityByPack";
            this.layoutControlItemTotalQuantityByPack.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalQuantityByPack.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalQuantityByPack.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItemTotalQuantityByPack.Text = "Σ Mã yêu cầu";
            this.layoutControlItemTotalQuantityByPack.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemWRRDate
            // 
            this.layoutControlItemWRRDate.Control = this.dateEditWRRDate;
            this.layoutControlItemWRRDate.Location = new System.Drawing.Point(304, 0);
            this.layoutControlItemWRRDate.Name = "layoutControlItemWRRDate";
            this.layoutControlItemWRRDate.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemWRRDate.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItemWRRDate.Text = "Ngày yêu cầu";
            this.layoutControlItemWRRDate.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.Location = new System.Drawing.Point(568, 0);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItemBranch.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(832, 0);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEditPartName;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 87);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem6.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem6.Text = "PartName";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEditProductFamily;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 116);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem7.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem7.Text = "Product Family";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEditLotID;
            this.layoutControlItem8.Location = new System.Drawing.Point(304, 29);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem8.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem8.Text = "Lot ID";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.comboBoxEditUnit;
            this.layoutControlItem9.Location = new System.Drawing.Point(304, 58);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem9.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem9.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem9.Text = "Unit";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.simpleButtonSave;
            this.layoutControlItem15.Location = new System.Drawing.Point(1096, 58);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem15.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem15.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.simpleButtonDelete;
            this.layoutControlItem16.Location = new System.Drawing.Point(1096, 87);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem16.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem16.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.simpleButtonClear;
            this.layoutControlItem17.Location = new System.Drawing.Point(1096, 116);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem17.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem17.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.textEditQuantity;
            this.layoutControlItem10.Location = new System.Drawing.Point(568, 29);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem10.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem10.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem10.Text = "Quantity";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.textEditPONumber;
            this.layoutControlItem13.Location = new System.Drawing.Point(568, 58);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem13.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem13.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem13.Text = "PO Number";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEditPartNumber;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem5.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem5.Text = "Part Number";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEditPalletID;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem4.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem4.Text = "Pallet ID";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.textEditRemark;
            this.layoutControlItem14.Location = new System.Drawing.Point(304, 87);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem14.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem14.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem14.OptionsTableLayoutItem.RowSpan = 2;
            this.layoutControlItem14.Size = new System.Drawing.Size(528, 58);
            this.layoutControlItem14.Text = "Remark";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.dateEditEXPDate;
            this.layoutControlItem18.Location = new System.Drawing.Point(832, 87);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem18.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem18.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem18.Text = "EXP Date";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.dateEditMFGDate;
            this.layoutControlItem11.Location = new System.Drawing.Point(832, 58);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem11.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem11.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem11.Text = "MFG Date";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEditQuantityByPack;
            this.layoutControlItem12.Location = new System.Drawing.Point(832, 29);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem12.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem12.Size = new System.Drawing.Size(264, 29);
            this.layoutControlItem12.Text = "SL Cartons";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.label2;
            this.layoutControlItem19.Location = new System.Drawing.Point(264, 29);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem19.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem19.Size = new System.Drawing.Size(40, 29);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // wRRDetailBindingSource
            // 
            this.wRRDetailBindingSource.DataMember = "WRRDetail";
            this.wRRDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wRRDetailTableAdapter
            // 
            this.wRRDetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlWRRDetail);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 202);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1380, 444);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlWRRDetail
            // 
            this.gridControlWRRDetail.DataSource = this.wRRDetailBindingSource;
            this.gridControlWRRDetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRRDetail.Location = new System.Drawing.Point(12, 12);
            this.gridControlWRRDetail.MainView = this.gridViewWRRDetail;
            this.gridControlWRRDetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRRDetail.Name = "gridControlWRRDetail";
            this.gridControlWRRDetail.Size = new System.Drawing.Size(1356, 420);
            this.gridControlWRRDetail.TabIndex = 5;
            this.gridControlWRRDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWRRDetail});
            // 
            // gridViewWRRDetail
            // 
            this.gridViewWRRDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWRRNumber,
            this.colOrdinal,
            this.colPalletID,
            this.colPartNumber,
            this.colPartName,
            this.colProductFamily,
            this.colLotID,
            this.colQuantityByItem,
            this.colQuantityByPack,
            this.colPackingVolume,
            this.colTotalQuantity,
            this.colUnit,
            this.colMFGDate,
            this.colEXPDate,
            this.colPONumber,
            this.colStatus,
            this.colRemark,
            this.collNo,
            this.colIDCodeRange,
            this.colQuantityByPrinted,
            this.colRemainingPrintQuantity});
            this.gridViewWRRDetail.DetailHeight = 284;
            this.gridViewWRRDetail.GridControl = this.gridControlWRRDetail;
            this.gridViewWRRDetail.Name = "gridViewWRRDetail";
            this.gridViewWRRDetail.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewWRRDetail.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridViewWRRDetail.OptionsCustomization.AllowSort = false;
            this.gridViewWRRDetail.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewWRRDetail.OptionsView.RowAutoHeight = true;
            this.gridViewWRRDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPackingVolume, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewWRRDetail.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewWRRDetail_RowClick);
            // 
            // colWRRNumber
            // 
            this.colWRRNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colWRRNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colWRRNumber.FieldName = "WRRNumber";
            this.colWRRNumber.MinWidth = 21;
            this.colWRRNumber.Name = "colWRRNumber";
            this.colWRRNumber.Width = 81;
            // 
            // colOrdinal
            // 
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.MinWidth = 21;
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Width = 81;
            // 
            // colPalletID
            // 
            this.colPalletID.Caption = "Pallet ID";
            this.colPalletID.FieldName = "PalletID";
            this.colPalletID.MinWidth = 86;
            this.colPalletID.Name = "colPalletID";
            this.colPalletID.Visible = true;
            this.colPalletID.VisibleIndex = 4;
            this.colPalletID.Width = 86;
            // 
            // colPartNumber
            // 
            this.colPartNumber.Caption = "Part Number";
            this.colPartNumber.FieldName = "PartNumber";
            this.colPartNumber.MinWidth = 86;
            this.colPartNumber.Name = "colPartNumber";
            this.colPartNumber.Visible = true;
            this.colPartNumber.VisibleIndex = 2;
            this.colPartNumber.Width = 86;
            // 
            // colPartName
            // 
            this.colPartName.Caption = "Part Name";
            this.colPartName.FieldName = "PartName";
            this.colPartName.MinWidth = 129;
            this.colPartName.Name = "colPartName";
            this.colPartName.Visible = true;
            this.colPartName.VisibleIndex = 3;
            this.colPartName.Width = 129;
            // 
            // colProductFamily
            // 
            this.colProductFamily.Caption = "Product Family";
            this.colProductFamily.FieldName = "ProductFamily";
            this.colProductFamily.MinWidth = 86;
            this.colProductFamily.Name = "colProductFamily";
            this.colProductFamily.Visible = true;
            this.colProductFamily.VisibleIndex = 1;
            this.colProductFamily.Width = 86;
            // 
            // colLotID
            // 
            this.colLotID.Caption = "Lot ID";
            this.colLotID.CustomizationCaption = "Lot ID";
            this.colLotID.FieldName = "LotID";
            this.colLotID.MinWidth = 86;
            this.colLotID.Name = "colLotID";
            this.colLotID.Visible = true;
            this.colLotID.VisibleIndex = 5;
            this.colLotID.Width = 86;
            // 
            // colQuantityByItem
            // 
            this.colQuantityByItem.Caption = "Qty";
            this.colQuantityByItem.DisplayFormat.FormatString = "G29";
            this.colQuantityByItem.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityByItem.FieldName = "QuantityByItem";
            this.colQuantityByItem.MinWidth = 86;
            this.colQuantityByItem.Name = "colQuantityByItem";
            this.colQuantityByItem.Visible = true;
            this.colQuantityByItem.VisibleIndex = 7;
            this.colQuantityByItem.Width = 86;
            // 
            // colQuantityByPack
            // 
            this.colQuantityByPack.Caption = "Cartons";
            this.colQuantityByPack.DisplayFormat.FormatString = "G29";
            this.colQuantityByPack.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityByPack.FieldName = "QuantityByPack";
            this.colQuantityByPack.MinWidth = 86;
            this.colQuantityByPack.Name = "colQuantityByPack";
            this.colQuantityByPack.Visible = true;
            this.colQuantityByPack.VisibleIndex = 6;
            this.colQuantityByPack.Width = 86;
            // 
            // colPackingVolume
            // 
            this.colPackingVolume.Caption = "Qty per Carton";
            this.colPackingVolume.DisplayFormat.FormatString = "G29";
            this.colPackingVolume.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPackingVolume.FieldName = "PackingVolume";
            this.colPackingVolume.MinWidth = 86;
            this.colPackingVolume.Name = "colPackingVolume";
            this.colPackingVolume.Visible = true;
            this.colPackingVolume.VisibleIndex = 8;
            this.colPackingVolume.Width = 86;
            // 
            // colTotalQuantity
            // 
            this.colTotalQuantity.Caption = "Tổng lượng";
            this.colTotalQuantity.FieldName = "TotalQuantity";
            this.colTotalQuantity.MinWidth = 86;
            this.colTotalQuantity.Name = "colTotalQuantity";
            this.colTotalQuantity.Width = 86;
            // 
            // colUnit
            // 
            this.colUnit.Caption = "Unit";
            this.colUnit.FieldName = "Unit";
            this.colUnit.MinWidth = 50;
            this.colUnit.Name = "colUnit";
            this.colUnit.Visible = true;
            this.colUnit.VisibleIndex = 9;
            this.colUnit.Width = 50;
            // 
            // colMFGDate
            // 
            this.colMFGDate.Caption = "MFG Date";
            this.colMFGDate.DisplayFormat.FormatString = "MM/dd/yyyy";
            this.colMFGDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colMFGDate.FieldName = "MFGDate";
            this.colMFGDate.MinWidth = 86;
            this.colMFGDate.Name = "colMFGDate";
            this.colMFGDate.Visible = true;
            this.colMFGDate.VisibleIndex = 10;
            this.colMFGDate.Width = 86;
            // 
            // colEXPDate
            // 
            this.colEXPDate.Caption = "EXP Date";
            this.colEXPDate.DisplayFormat.FormatString = "MM/dd/yyyy";
            this.colEXPDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colEXPDate.FieldName = "EXPDate";
            this.colEXPDate.MinWidth = 86;
            this.colEXPDate.Name = "colEXPDate";
            this.colEXPDate.Visible = true;
            this.colEXPDate.VisibleIndex = 11;
            this.colEXPDate.Width = 86;
            // 
            // colPONumber
            // 
            this.colPONumber.Caption = "PO Number";
            this.colPONumber.FieldName = "PONumber";
            this.colPONumber.MinWidth = 80;
            this.colPONumber.Name = "colPONumber";
            this.colPONumber.Visible = true;
            this.colPONumber.VisibleIndex = 13;
            this.colPONumber.Width = 80;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 21;
            this.colStatus.Name = "colStatus";
            this.colStatus.Width = 100;
            // 
            // colRemark
            // 
            this.colRemark.Caption = "Remark";
            this.colRemark.FieldName = "Remark";
            this.colRemark.MinWidth = 86;
            this.colRemark.Name = "colRemark";
            this.colRemark.Visible = true;
            this.colRemark.VisibleIndex = 12;
            this.colRemark.Width = 86;
            // 
            // collNo
            // 
            this.collNo.Caption = "No.";
            this.collNo.FieldName = "No";
            this.collNo.MinWidth = 43;
            this.collNo.Name = "collNo";
            this.collNo.Visible = true;
            this.collNo.VisibleIndex = 0;
            this.collNo.Width = 43;
            // 
            // colIDCodeRange
            // 
            this.colIDCodeRange.Caption = "Tem da in";
            this.colIDCodeRange.FieldName = "IDCodeRange";
            this.colIDCodeRange.MinWidth = 50;
            this.colIDCodeRange.Name = "colIDCodeRange";
            this.colIDCodeRange.Visible = true;
            this.colIDCodeRange.VisibleIndex = 16;
            this.colIDCodeRange.Width = 150;
            // 
            // colQuantityByPrinted
            // 
            this.colQuantityByPrinted.Caption = "SL tem da in";
            this.colQuantityByPrinted.FieldName = "QuantityByPrinted";
            this.colQuantityByPrinted.Name = "colQuantityByPrinted";
            this.colQuantityByPrinted.Visible = true;
            this.colQuantityByPrinted.VisibleIndex = 14;
            // 
            // colRemainingPrintQuantity
            // 
            this.colRemainingPrintQuantity.Caption = "SL tem chua in";
            this.colRemainingPrintQuantity.FieldName = "RemainingPrintQuantity";
            this.colRemainingPrintQuantity.MinWidth = 50;
            this.colRemainingPrintQuantity.Name = "colRemainingPrintQuantity";
            this.colRemainingPrintQuantity.Visible = true;
            this.colRemainingPrintQuantity.VisibleIndex = 15;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1380, 444);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlWRRDetail;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1360, 424);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // ReceiptRequisitionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ReceiptRequisitionControl";
            this.Size = new System.Drawing.Size(1380, 646);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRRNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRRDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRRDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByPack.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPalletID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPartNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPartName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProductFamily.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLotID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditMFGDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditMFGDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPONumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEXPDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEXPDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditQuantityByPack.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByPack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit textEditWRRNumber;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRRNumber;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.DateEdit dateEditWRRDate;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityByItem;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityByPack;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityByItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityByPack;
        private System.Windows.Forms.BindingSource wRRDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter wRRDetailTableAdapter;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.GridControl gridControlWRRDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWRRDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colWRRNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colPartNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPartName;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByItem;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByPack;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn collNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRRDate;
        private DevExpress.XtraGrid.Columns.GridColumn colProductFamily;
        private DevExpress.XtraGrid.Columns.GridColumn colLotID;
        private DevExpress.XtraGrid.Columns.GridColumn colPalletID;
        private DevExpress.XtraGrid.Columns.GridColumn colMFGDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEXPDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemark;
        private DevExpress.XtraGrid.Columns.GridColumn colUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colPONumber;
        private DevExpress.XtraEditors.TextEdit textEditPalletID;
        private DevExpress.XtraEditors.TextEdit textEditPartNumber;
        private DevExpress.XtraEditors.TextEdit textEditPartName;
        private DevExpress.XtraEditors.TextEdit textEditProductFamily;
        private DevExpress.XtraEditors.TextEdit textEditLotID;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditUnit;
        private DevExpress.XtraEditors.TextEdit textEditQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.DateEdit dateEditMFGDate;
        private DevExpress.XtraEditors.TextEdit textEditPONumber;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDelete;
        private DevExpress.XtraEditors.SimpleButton simpleButtonClear;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.DateEdit dateEditEXPDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Columns.GridColumn colIDCodeRange;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByPrinted;
        private DevExpress.XtraGrid.Columns.GridColumn colRemainingPrintQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.MemoEdit textEditRemark;
        private DevExpress.XtraEditors.TextEdit textEditQuantityByPack;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
    }
}
