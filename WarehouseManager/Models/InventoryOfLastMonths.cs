﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class InventoryOfLastMonths
    {
        public Int16 Year { get; set; }
        public Int16 Month { get; set; }
        public string WarehouseID { get; set; }
        public string PartNumber { get; set; }
        public Decimal? OpeningStockQuantity { get; set; }
        public Decimal? ReceiptQuantity { get; set; }
        public Decimal? IssueQuantity { get; set; }
        public Decimal? ClosingStockQuantity { get; set; }
        public string Status { get; set; }
        public Decimal? PackingVolume { get; set; }
        public string Material { get; set; }
        public string Mold { get; set; }
        public string Unit { get; set; }
        public string IDCode { get; set; }
        public string PalletID { get; set; }
    }
}
