﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    class LotInfoIO
    {
        public string PartNumber { get; set; }
        public DateTime? MFGDate { get; set; }
        public decimal? Qty { get; set; }
        public string Lot { get; set; }
        public decimal? QuantityPerlot { get; set; }
        public decimal? Cartons { get; set; }
        public int LotCount { get; set; }
    }
}
