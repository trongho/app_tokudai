﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class HandlingStatus
    {
      public String HandlingStatusID{get;set;}
      public String HandlingStatusName{get;set;}
      public String HandlingStatusNameEN{get;set;}
    }
}
