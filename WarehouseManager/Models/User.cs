﻿using Microsoft.Build.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Sdk;
using RequiredAttribute = System.ComponentModel.DataAnnotations.RequiredAttribute;

namespace WarehouseManager.Models
{
    public class User
    {
        public String UserID { get; set; }

        public String UserName { get; set; }
        public String? Adress { get; set; }
        public String? Position { get; set; }
        public String? BranchID { get; set; }
        public String? BranchName { get; set; }
        public String? Email { get; set; }
        public String LoginName { get; set; }
        public String? Password { get; set; }
        public String? PasswordSalt { get; set; }
        public String? Description { get; set; }
        public String? Status { get; set; }
        public String? CreatedUserID { get; set; }
        public DateTime? CreateDate { get; set; }
        public String? UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Active { get; set; }
        public bool Blocked { get; set; }
        public String Role { get; set; }
        public Int16? ControlAllBranches { get; set; }
        public Int16? ControlAllSystems { get; set; }
        public Int16? ApproveWRLevel1 { get; set; }
        public Int16? ApproveWRLevel2 { get; set; }
        public Int16? CancelWR { get; set; }
        public Int16? AdjustWR { get; set; }
        public Int16? ApproveIOLevel1 { get; set; }
        public Int16? ApproveIOLevel2 { get; set; }
        public Int16? CancelIO { get; set; }
        public Int16? AdjustIO { get; set; }
        public Int16? ProcessInventory { get; set; }
        public Int16? ApproveWRRLevel1 { get; set; }
        public Int16? ApproveWRRLevel2 { get; set; }
        public Int16? CancelWRR { get; set; }
        public Int16? AdjustWRR { get; set; }
        public Int16? ApproveWIRLevel1 { get; set; }
        public Int16? ApproveWIRLevel2 { get; set; }
        public Int16? CancelWIR { get; set; }
        public Int16? AdjustWIR { get; set; }
        public Int16? ApproveWRDLevel1 { get; set; }
        public Int16? ApproveWRDLevel2 { get; set; }
        public Int16? CancelWRD { get; set; }
        public Int16? AdjustWRD { get; set; }
        public Int16? ApproveWIDLevel1 { get; set; }
        public Int16? ApproveWIDLevel2 { get; set; }
        public Int16? CancelWID { get; set; }
        public Int16? AdjustWID { get; set; }
        public Int16? AdjustReceiptQuantity { get; set; }
        public Int16? AdjustIssueQuantity { get; set; }
        public Int16? ApprovePalletDataLevel1 { get; set; }
        public Int16? ApprovePalletDataLevel2 { get; set; }
        public Int16? CancelPalletData { get; set; }
        public Int16? AdjustPalletData { get; set; }

        public User(string userID, string userName, string adress, string position, string branchID, string branchName, string email, string loginName, string password, string passwordSalt, string description, string status, string createdUserID, DateTime? createDate, string updatedUserID, DateTime? updatedDate, bool active, bool blocked)
        {
            UserID = userID;
            UserName = userName;
            Adress = adress;
            Position = position;
            BranchID = branchID;
            BranchName = branchName;
            Email = email;
            LoginName = loginName;
            Password = password;
            PasswordSalt = passwordSalt;
            Description = description;
            Status = status;
            CreatedUserID = createdUserID;
            CreateDate = createDate;
            UpdatedUserID = updatedUserID;
            UpdatedDate = updatedDate;
            Active = active;
            Blocked = blocked;
        }

        public User()
        {
        }
    }
}
