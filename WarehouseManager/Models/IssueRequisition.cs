﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    class IssueRequisition
    {
        public string LotID { get; set; }
        public string PalletID { get; set; }
        public DateTime? MFGDate { get; set; }
        public DateTime? EXPDate { get; set; }
        public DateTime? InputDate { get;set; }
        public decimal? Quantity { get; set; }
        public decimal? IRQuantity { get; set; }
    }
}
