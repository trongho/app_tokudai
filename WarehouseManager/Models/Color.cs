﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class Color
    {
      public String ColorID{get;set;}
      public String? ColorName{get;set;}
      public String? Description{get;set;}
      public String? Status{get;set;}
      public String? CreatedUserID{get;set;}
      public DateTime? CreatedDate{get;set;}
      public String? UpdatedUserID{get;set;}
      public DateTime? UpdatedDate{get;set;}
    }
}
