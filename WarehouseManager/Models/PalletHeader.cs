﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    class PalletHeader
    {
        public string PalletID { get; set; }
        public int Ordinal { get; set; }
        public decimal? TotalIDCode { get; set; }
        public string Status { get; set; }
        public String HandlingStatusID { get; set; }
        public String HandlingStatusName { get; set; }
        public String CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public String UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
