﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class WRHeader
    {
        public string WRNumber { get; set; }
        public string ModalityID { get; set; }
        public string ModalityName { get; set; }
        public DateTime? WRDate { get; set; }
        public string WRRNumber { get; set; }
        public string WarehouseID { get; set; }
        public string WarehouseName { get; set; }
        public string HandlingStatusID { get; set; }
        public string HandlingStatusName { get; set; }
        public string BranchID { get; set; }
        public string BranchName { get; set; }
        public decimal? TotalQuantity { get; set; }
        public string Note { get; set; }
        public string Status { get; set; }
        public string CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
