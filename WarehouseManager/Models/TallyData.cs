﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class TallyData
    {
        public String TSNumber { get; set; }
        public String? TSDate { get; set; }
        public String PartNumber { get; set; }
        public String? PartName { get; set; }
        public string IDCode { get; set; }
        public Decimal? Quantity { get; set; }
        public Decimal? TotalQuantity { get; set; }
        public Decimal? TotalRow { get; set; }
        public String? CreatorID { get; set; }
        public String? CreatedDateTime { get; set; }
        public String? EditerID { get; set; }
        public String? EditedDateTime { get; set; }
        public String? Status { get; set; }
        public int No { get; set; }
    }
}
