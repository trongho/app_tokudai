﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    class Label
    {
        public string PalletID { get; set; }
        public int? CurrentIDCode { get; set; }
    }
}
