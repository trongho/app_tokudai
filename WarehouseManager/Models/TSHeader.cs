﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class TSHeader
    {
        public string TSNumber { get; set; }
        public DateTime? TSDate { get; set; }
        public string WarehouseID { get; set; }
        public string WarehouseName { get; set; }
        public string HandlingStatusID { get; set; }
        public string HandlingStatusName { get; set; }
        public string Note { get; set; }
        public string BranchID { get; set; }
        public string BranchName { get; set; }
        public decimal? TotalQuantity { get; set; }
        public decimal? TotalQuantityByPack { get; set; }
        public string CheckerID1 { get; set; }
        public string CheckerName1 { get; set; }
        public string CheckerID2 { get; set; }
        public string CheckerName2 { get; set; }
        public string Status { get; set; }
        public string CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
