﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class WRDataGeneral
    {
        public string WRDNumber { get; set; }
        public int Ordinal { get; set; }
        public string PalletID { get; set; }
        public string IDCode { get; set; }
        public string PartNumber { get; set; }
        public string ProductFamily { get; set; }
        public string LotID { get; set; }
        public string PartName { get; set; }
        public string Unit { get; set; }
        public decimal? QuantityOrg { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? TotalGoodsOrg { get; set; }
        public decimal? TotalGoods { get; set; }
        public decimal? TotalQuantityOrg { get; set; }
        public decimal? TotalQuantity { get; set; }
        public decimal? PackingVolume { get; set; }
        public decimal? QuantityByPack { get; set; }
        public decimal? QuantityByItem { get; set; }
        public decimal? PackingQuantity { get; set; }
        public DateTime? MFGDate { get; set; }
        public DateTime? EXPDate { get; set; }
        public DateTime? InputDate { get; set; }
        public string PONumber { get; set; }
        public string Remark { get; set; }
        public string Status { get; set; }

        //public BindingList<WRDataDetail> _wRDataDetails = new BindingList<WRDataDetail>();
        //public BindingList<WRDataDetail> WRDataDetails { get; set; }
    }

    public class WRDataDetail
    {
        public string WRDNumber { get; set; }
        public int Ordinal { get; set; }
        public string PalletID { get; set; }
        public string IDCode { get; set; }
        public decimal? QuantityOrg { get; set; }
        public decimal? Quantity { get; set; }
    }
}
