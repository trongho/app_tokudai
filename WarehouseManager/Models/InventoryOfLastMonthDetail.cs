﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    class InventoryOfLastMonthDetail
    {
        public Int16 Year { get; set; }
        public Int16 Month { get; set; }
        public string WarehouseID { get; set; }
        public string PartNumber { get; set; }
        public string LotID { get; set; }
        public string PalletID { get; set; }
        public string IDCode { get; set; }
        public decimal? Quantity { get; set; }
        public DateTime? MFGDate { get; set; }
        public DateTime? EXPDate { get; set; }
        public DateTime? InputDate { get; set; }
        public string Status { get; set; }
    }
}
