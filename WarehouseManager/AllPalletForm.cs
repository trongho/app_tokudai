﻿using DevExpress.Export;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid;

namespace WarehouseManager
{

    public partial class AllPalletForm : DevExpress.XtraEditors.XtraForm
    {
        PalletHeaderRepository palletHeaderRepository = null;
        PalletDetailRepository palletDetailRepository = null;
        InventoryRepository inventoryRepository = null;
        InventoryDetailRepository inventoryDetailRepository = null;
        InventoryOfLastMonthsRepository inventoryOfLastMonthsRepository = null;
        InventoryOfLastMonthDetailRepository inventoryOfLastMonthDetailRepository = null;
        List<PalletHeader> palletHeaders1 = null;



        public AllPalletForm()
        {
            InitializeComponent();
            palletHeaderRepository = new PalletHeaderRepository();
            palletDetailRepository = new PalletDetailRepository();
            inventoryRepository = new InventoryRepository();
            inventoryDetailRepository = new InventoryDetailRepository();
            inventoryOfLastMonthsRepository = new InventoryOfLastMonthsRepository();
            inventoryOfLastMonthDetailRepository = new InventoryOfLastMonthDetailRepository();


        }

        private async void AllPalletForm_Load(object sender, EventArgs e)
        {
            palletHeaders1 = new List<PalletHeader>();
            palletHeaders1 = (await palletHeaderRepository.GetAll()).Where(w => w.HandlingStatusID.Equals("1")).ToList();
            gridControl1.DataSource = palletHeaders1;

        }



        private void gridView_CustomColumnGroup(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnSortEventArgs e)
        {
            if (e.Column.FieldName == "Quantity")
            {
                e.Handled = true;
                double val1 = Math.Floor(Convert.ToDouble(e.Value1) / 100);
                double val2 = Math.Floor(Convert.ToDouble(e.Value2) / 100);
                if (val1 >= 10 && val2 >= 10)
                    e.Result = 0;
                else e.Result = System.Collections.Comparer.Default.Compare(val1, val2);
            }
        }

        private async void getAllPallet()
        {
            DataSet dataSet = new DataSet();

            DataTable palletHeaderTable = new DataTable();
            palletHeaderTable.Columns.Add("Ordinal", typeof(Int16)).Caption = "No.";
            palletHeaderTable.Columns.Add("PalletID", typeof(string)).Caption = "Pallet ID";
            palletHeaderTable.Columns.Add("TotalIDCode", typeof(decimal)).Caption = "Σ Mã hàng";
            palletHeaderTable.Columns.Add("Status", typeof(string)).Caption = "Status";


            List<PalletHeader> palletHeaders = await palletHeaderRepository.GetAll();
            foreach (PalletHeader palletHeader in palletHeaders)
            {
                if (palletHeader.TotalIDCode > 0)
                {
                    palletHeaderTable.Rows.Add(new object[]
                    {
                    palletHeader.Ordinal,
                    palletHeader.PalletID,
                    palletHeader.TotalIDCode,
                    palletHeader.HandlingStatusName,
                    });
                }
            }
            palletHeaderTable.TableName = "PalletHeader";

            DataTable palletDetailTable = new DataTable();
            palletDetailTable.Columns.Add("Ordinal", typeof(Int16)).Caption = "No.";
            palletDetailTable.Columns.Add("PalletID", typeof(string)).Caption = "Pallet ID";
            palletDetailTable.Columns.Add("IDCode", typeof(string)).Caption = "Mã hàng";
            palletDetailTable.Columns.Add("PartNumber", typeof(string)).Caption = "Part Number";
            palletDetailTable.Columns.Add("PartName", typeof(string)).Caption = "PartName";
            palletDetailTable.Columns.Add("ProductFamily", typeof(string)).Caption = "Product Family";
            palletDetailTable.Columns.Add("LotID", typeof(string)).Caption = "Lot ID";
            palletDetailTable.Columns.Add("Unit", typeof(string)).Caption = "Unit";
            palletDetailTable.Columns.Add("Quantity", typeof(decimal)).Caption = "Qty";
            palletDetailTable.Columns.Add("MFGDate", typeof(DateTime)).Caption = "MFG Date";
            palletDetailTable.Columns.Add("EXPDate", typeof(DateTime)).Caption = "EXP Date";

            List<PalletDetail> palletDetails = await palletDetailRepository.GetAll();
            foreach (PalletDetail palletDetail in palletDetails)
            {
                palletDetailTable.Rows.Add(new object[]
                {
                   palletDetail.Ordinal,
                   palletDetail.PalletID,
                   palletDetail.IDCode,
                   palletDetail.PartNumber,
                   palletDetail.PartName,
                   palletDetail.ProductFamily,
                   palletDetail.LotID,
                   palletDetail.Unit,
                   palletDetail.Quantity,
                   palletDetail.MFGDate,
                   palletDetail.EXPDate,
                });
            }
            palletDetailTable.TableName = "PalletDetail";

            dataSet.Tables.Add(palletHeaderTable);
            dataSet.Tables.Add(palletDetailTable);
            dataSet.Relations.Add("Chi tiết", palletHeaderTable.Columns["PalletID"], palletDetailTable.Columns["PalletID"]);
            gridControl1.DataSource = dataSet;

            gridControl1.DataMember = "PalletHeader";

        }

        private async void windowsUIButtonPanel1_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            String tag = e.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    //getAllPallet();
                    palletHeaders1 = (await palletHeaderRepository.GetAll()).Where(w => !w.HandlingStatusID.Equals("3") || !w.HandlingStatusID.Equals("4")).ToList();
                    gridControl1.DataSource = palletHeaders1;
                    break;
                case "export":
                    int[] selectedRowHandles = gridView1.GetSelectedRows();
                    if (selectedRowHandles.Length == 0)
                    {
                        MessageBox.Show("Không có pallet nào được chọn", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        DialogResult dialog = new DialogResult();
                        dialog = MessageBox.Show("Bạn chắc chắn duyệt mức 2 " + selectedRowHandles.Length + " pallet đã chọn", "Cảnh báo!", MessageBoxButtons.YesNo);

                        if (dialog == DialogResult.Yes)
                        {
                            for (int i = 0; i < selectedRowHandles.Length; i++)
                            {
                                string palletID = (string)gridView1.GetRowCellValue(selectedRowHandles[i], "PalletID");
                                PalletHeader palletHeader = await palletHeaderRepository.GetUnderID(palletID);
                                List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(palletID);
                                if (palletHeader != null)
                                {
                                    palletHeader.HandlingStatusID = "2";
                                    palletHeader.HandlingStatusName = "Duyệt mức 2";
                                    palletHeader.UpdatedUserID = WMMessage.User.UserID;
                                    palletHeader.UpdatedDate = DateTime.Now;
                                    if (await palletHeaderRepository.Update(palletHeader, palletHeader.PalletID, palletHeader.Ordinal) > 0)
                                    {
                                        if (palletDetails.Count > 0)
                                        {
                                            updateInventory(palletDetails);
                                            updateInventoryDetail(palletDetails);
                                        }
                                    }
                                }
                            }

                            palletHeaders1 = (await palletHeaderRepository.GetAll()).Where(w => w.HandlingStatusID.Equals("1")).ToList();
                            gridControl1.DataSource = palletHeaders1;

                            MessageBox.Show("Duyệt mức 2 thành cộng " + selectedRowHandles.Length + " Pallet");
                        }

                    }
                    //using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Excel Workbook|*.xlsx", FileName = "AllPalletData" })
                    //{
                    //    if (sfd.ShowDialog() == DialogResult.OK)
                    //    {
                    //        string path = sfd.FileName;
                    //        // Create a new object defining how a document is exported to the XLSX format.
                    //        var options = new XlsxExportOptionsEx();
                    //        // Specify a name of the sheet in the created XLSX file.
                    //        options.SheetName = "Pallet Data";
                    //        options.ExportType = ExportType.DataAware;
                    //        options.AllowGrouping = DevExpress.Utils.DefaultBoolean.True;
                    //        gridView1.BeginUpdate();

                    //        gridView1.Columns["Status"].Visible = false;
                    //        gridControl1.ExportToXlsx(path, options);
                    //        gridView1.Columns["Status"].Visible = true;
                    //        gridView1.EndDataUpdate();


                    //        // Open the created XLSX file with the default application.
                    //        Process.Start(path);
                    //    }
                    //}


                    break;
            }
        }

        private async void repositoryItemButtonEdit1_Click(object sender, EventArgs e)
        {
            string palletNumber = (string)gridView1.GetFocusedRowCellValue("PalletID");
            DialogResult dialog = MessageBoxEx.Show(this, "Duyệt mức 2 pallet number " + palletNumber, "Alert", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                PalletHeader palletHeader = await palletHeaderRepository.GetUnderID(palletNumber);
                List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(palletNumber);
                if (palletHeader != null)
                {
                    palletHeader.HandlingStatusID = "2";
                    palletHeader.HandlingStatusName = "Duyệt mức 2";
                    palletHeader.UpdatedUserID = WMMessage.User.UserID;
                    palletHeader.UpdatedDate = DateTime.Now;
                    if (await palletHeaderRepository.Update(palletHeader, palletHeader.PalletID, palletHeader.Ordinal) > 0)
                    {
                        if (palletDetails.Count > 0)
                        {
                            updateInventory(palletDetails);
                            updateInventoryDetail(palletDetails);
                        }

                        MessageBox.Show("Duyệt mức 2 thành công. Đã cập nhật tồn kho");
                    }
                }
            }
        }

        private async void updateInventory(List<PalletDetail> palletDetails)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));

            foreach (PalletDetail palletDetail in palletDetails)
            {
                List<Inventory> inventories = await inventoryRepository.GetUnderMultiPartNumber(mYear, mMonth, "1", palletDetail.PartNumber);
                if (inventories.Count > 0)
                {
                    inventories[0].ReceiptQuantity = inventories[0].ReceiptQuantity + palletDetail.Quantity;
                    inventories[0].ClosingStockQuantity = inventories[0].ClosingStockQuantity + palletDetail.Quantity;
                    await inventoryRepository.Update(inventories[0], mYear, mMonth, "1", palletDetail.PartNumber);
                }
                else
                {
                    Inventory inventory = new Inventory();
                    inventory.Year = mYear;
                    inventory.Month = mMonth;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = palletDetail.PartNumber;
                    inventory.PackingVolume = palletDetail.Quantity;
                    inventory.OpeningStockQuantity = 0;
                    inventory.ReceiptQuantity = palletDetail.Quantity;
                    inventory.IssueQuantity = 0;
                    inventory.ClosingStockQuantity = inventory.OpeningStockQuantity + inventory.ReceiptQuantity;
                    inventory.Unit = palletDetail.Unit;
                    await inventoryRepository.Create(inventory);
                }
            }
        }

        private async void updateInventoryDetail(List<PalletDetail> palletDetails)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            foreach (PalletDetail palletDetail in palletDetails)
            {
                List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams6(mYear, mMonth, "1", palletDetail.PartNumber, palletDetail.PalletID, palletDetail.IDCode);
                if (inventoryDetails.Count == 0)
                {
                    InventoryDetail inventoryDetail = new InventoryDetail();
                    inventoryDetail.Year = mYear;
                    inventoryDetail.Month = mMonth;
                    inventoryDetail.WarehouseID = "1";
                    inventoryDetail.PartNumber = palletDetail.PartNumber;
                    inventoryDetail.PalletID = palletDetail.PalletID;
                    inventoryDetail.IDCode = palletDetail.IDCode;
                    inventoryDetail.LotID = palletDetail.LotID;
                    inventoryDetail.MFGDate = palletDetail.MFGDate;
                    inventoryDetail.EXPDate = palletDetail.EXPDate;
                    inventoryDetail.Quantity = palletDetail.Quantity;
                    inventoryDetail.InputDate = DateTime.Now;

                    await inventoryDetailRepository.Create(inventoryDetail);

                    palletHeaders1 = (await palletHeaderRepository.GetAll()).Where(w => w.HandlingStatusID.Equals("1")).ToList();
                    gridControl1.DataSource = palletHeaders1;
                }

            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            textEditSelectedPallet.Text = gridView1.GetSelectedRows().Length + "";
        }
    }
}