﻿using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Accessibility;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Card;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraTab;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using QRCoder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using WarehouseManager.Helpers;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class IssueDataControl : DevExpress.XtraEditors.XtraUserControl
    {
        WIDataHeaderRepository WIDataHeaderRepository;
        WIDataGeneralRepository WIDataGeneralRepository;
        WIDataDetailRepository WIDataDetailRepository;
        WIRHeaderRepository WIRHeaderRepository;
        WIRDetailRepository WIRDetailRepository;
        HandlingStatusRepository handlingStatusRepository;
        IOHeaderRepository IOHeaderRepository;
        IODetailRepository IODetailRepository;
        public static String selectedWIDNumber="";
        Timer t = new Timer();
        int _hoveredRowHandle = GridControl.InvalidRowHandle;

        string selectedPalletID = "";
        int selectedOrdinal =0;

        internal MainForm m_MainForm;
        public IssueDataControl(MainForm mainForm)
        {
            InitializeComponent();
            m_MainForm = mainForm;
            BottomBarHelper.normalMessage(m_MainForm.barStaticItemMessage, "!!!");
            WIDataHeaderRepository = new WIDataHeaderRepository();
            WIDataGeneralRepository = new WIDataGeneralRepository();
            WIDataDetailRepository = new WIDataDetailRepository();
            WIRHeaderRepository = new WIRHeaderRepository();
            WIRDetailRepository = new WIRDetailRepository();
            IOHeaderRepository = new IOHeaderRepository();
            IODetailRepository = new IODetailRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            popupMenuSelectHandlingStatus();
            sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
            this.Load += IssueDataControl_Load;
        }

        private void IssueDataControl_Load(Object sender, EventArgs e)
        {
            //gridViewDetail.MouseDown += gridView1_MouseDown;
            gridViewWIDataGeneral.DoubleClick += dataGridViewWIR_CellDoubleClick;
            gridViewWIDataGeneral.MouseMove += GridView_MouseMove;
            //gridControlWIDataDetail.MouseLeave += gridView1_MouseLeave;
            gridViewWIDataGeneral.RowCellStyle += GridView_RowCellStyle;

            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditWIDNumber.KeyDown += new KeyEventHandler(WIDNumber_KeyDown);
            textEditWIDNumber.DoubleClick += textEditWDRNumer_CellDoubleClick;
            gridViewWIDataGeneral.CustomDrawCell += grdData_CustomDrawCell;
            gridViewWIDataGeneral.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            simpleButtonOn.Click += butOn_Click;
            simpleButtonOff.Click += butOff_Click;
            t.Tick += new EventHandler(this.Timer_Tick);
            t.Start();

            gridViewWIDataGeneral.SelectRow(0);
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewWIDataGeneral.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void popupMenuSelectHandlingStatus()
        {
            List<string> list = Helpers.Constant.handlingStatuss;
            foreach (string s in list)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(s);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }


        private async void sbLoadDataForGridWIDataGeneralAsync(String wRDNumber)
        {
            List<WIDataGeneral> WIDataGenerals = await WIDataGeneralRepository.GetUnderID(wRDNumber);
            //List<WIDataDetail> WIDataDetails = await WIDataDetailRepository.GetByID(wRDNumber);
            int mMaxRow = 100;
            if (WIDataGenerals.Count < mMaxRow)
            {
                int num = mMaxRow - WIDataGenerals.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    WIDataGenerals.Add(new WIDataGeneral());
                    i++;
                }
            }

            
            gridControlWIDataDetail.DataSource = WIDataGenerals;

            getTotalQuantityOrg();
            getTotalQuantity();
            getPackingQuantity();
            textTotalQuantity_CustomDraw();

            
        }

        private void GridView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle == _hoveredRowHandle)
                e.Appearance.FontStyleDelta = FontStyle.Bold;
        }
        private void GridView_MouseMove(object sender, MouseEventArgs e)
        {
            var view = sender as GridView;
            var hitInfo = view.CalcHitInfo(e.Location);
            if (hitInfo.RowHandle != _hoveredRowHandle)
            {
                view.RefreshRow(_hoveredRowHandle);
                _hoveredRowHandle = hitInfo.RowHandle;
                view.RefreshRow(_hoveredRowHandle);
            }
        }

        private void gridView1_MouseLeave(object sender, EventArgs e)
        {
            var view = sender as GridView;
            GridHitInfo hInfo = view.CalcHitInfo(gridControlWIDataDetail.PointToClient(Cursor.Position));
            if (hInfo.HitTest == GridHitTest.None)
            {
                // ...  
            }
        }


        private async void dataGridViewWIR_CellDoubleClick(object sender, EventArgs e)
        {
            gridViewDetail.OptionsBehavior.Editable = false;
            DevExpress.XtraGrid.Views.Grid.GridView sndr =
                sender as DevExpress.XtraGrid.Views.Grid.GridView;

            DevExpress.Utils.DXMouseEventArgs dxMouseEventArgs =
                e as DevExpress.Utils.DXMouseEventArgs;


            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hitInfo =
                   sndr.CalcHitInfo(dxMouseEventArgs.Location);
            if (hitInfo.InRowCell)
            {
                int rowHandle = hitInfo.RowHandle;
                


                if (gridViewWIDataGeneral.FocusedColumn.FieldName.Equals("PartNumber"))
                {
                    //WIDataDetailForm.wrdNumber = textEditWRDNumber.Text;
                    //WIDataDetailForm.palletID = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PalletID");
                    //WIDataDetailForm.ordinal = (int)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "Ordinal");
                    //WIDataDetailForm.quantityByPack = (decimal)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "QuantityByPack");

                    string widNumber = textEditWIDNumber.Text;
                    string partNumber = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PartNumber");
                    string palletID = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PalletID");
                    int ordinal = (int)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "Ordinal");

                    List<WIDataGeneral> WIDataGenerals = await WIDataGeneralRepository.GetByParams3(widNumber,palletID,partNumber);
                    if (WIDataGenerals.Count > 0)
                    {
                        string[] idcodes = WIDataGenerals[0].IDCode.Split(',');
                        List<WIDataDetail> WIDataDetails = new List<WIDataDetail>();

                        foreach (string s in idcodes)
                        {
                            WIDataDetail WIDataDetail = new WIDataDetail();
                            WIDataDetail.IDCode = s;
                            WIDataDetails.Add(WIDataDetail);
                        }
                        WIDataDetails.Remove(WIDataDetails[WIDataDetails.Count - 1]);
                        gridControl1.DataSource = WIDataDetails;
                    }

                    //WIDataDetailForm.ShowDialog(this);
                }
            }
        }


        private void getPackingQuantity()
        {
            Decimal packingQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWIDataGeneral.GetRowHandle(i);
                if (gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantityOrg = (Decimal)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "QuantityOrg");
                Decimal quantity = (Decimal)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "Quantity");
                Decimal quantityByPack = (Decimal)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "QuantityByPack");
                Decimal packingVolume = quantityOrg / quantityByPack;
                if (quantity % packingVolume != 0)
                {
                    packingQuantity = Math.Floor(quantity / packingVolume) + 1;
                }
                else
                {
                    packingQuantity = quantity / packingVolume;
                }

                gridViewWIDataGeneral.SetRowCellValue(rowHandle, "PackingQuantity", packingQuantity);
                i++;
            }
        }




        private void getTotalQuantityOrg()
        {
            Decimal totalQuantityOrg = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWIDataGeneral.GetRowHandle(i);
                if (gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantityOrg = (Decimal)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "QuantityOrg");
                totalQuantityOrg += quantityOrg;
                i++;
            }
            textEditTotalQuantityOrg.Text = totalQuantityOrg.ToString("0.#####");
        }

        private void getTotalQuantity()
        {
            Decimal totalQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWIDataGeneral.GetRowHandle(i);
                if (gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PalletID") == null)
                {
                    break;
                }
                Decimal quantityOrg = (Decimal)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "QuantityOrg");
                Decimal quantity = (Decimal)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "Quantity");
                totalQuantity += quantityOrg - quantity;
                i++;
            }
            textEditTotalQuantity.Text = totalQuantity.ToString("0.#####");
        }

        private void QRCodeGeneral(String code)
        {
            QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.Q;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, eccLevel);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeBitmap = qrCode.GetGraphic(2, System.Drawing.Color.Black, System.Drawing.Color.White, new Bitmap(10, 10));
            pictureBox1.Image = qrCodeBitmap;
            Helpers.ImageHelper.resizeImage(pictureBox1.Image, new Size(100, 100));
            pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
        }

        public async void updateWIDataHeaderAsync()
        {
            WIDataHeader WIDataHeader = await WIDataHeaderRepository.GetUnderID(textEditWIDNumber.Text);
            if (WIDataHeader.HandlingStatusID.Equals("2"))
            {
                MessageBox.Show("Dữ liệu nhập đã duyệt mức 2, không thể chỉnh sửa","Alert",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }

            Decimal totalQuantityOrg = Decimal.Parse(textEditTotalQuantityOrg.Text);
            Decimal totalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            Decimal subQuantity = totalQuantityOrg - totalQuantity;
            if (subQuantity == totalQuantityOrg)
            {
                WIDataHeader.Status = "0";
            }
            else if (subQuantity < totalQuantityOrg)
            {
                WIDataHeader.Status = "1";
            }
            else if (subQuantity == 0)
            {
                WIDataHeader.Status = "2";
            }
            else
            {
                WIDataHeader.Status = "3";
            }


            WIDataHeader.WIDNumber = textEditWIDNumber.Text;
            WIDataHeader.WIDDate = DateTime.Parse(dateEditWIDDate.DateTime.ToString("yyyy-MM-dd"));
            WIDataHeader.WIRNumber = textEditWIDNumber.Text;
            WIDataHeader.HandlingStatusID =comboBoxEditHandlingStatus.SelectedIndex==0?"<>":(comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            WIDataHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            WIDataHeader.Note ="";
            WIDataHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            WIDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityOrg.Text);
            WIDataHeader.UpdatedUserID = WMMessage.User.UserID;
            WIDataHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
           
            if (await WIDataHeaderRepository.Update(WIDataHeader, textEditWIDNumber.Text) > 0)
            {
                MessageBox.Show(WMMessage.msgSaveChangeSuccess);
                if (comboBoxEditHandlingStatus.SelectedIndex == 3)
                {
                    List<IOHeader> iOHeaders = await IOHeaderRepository.GetUnderID(textEditWIDNumber.Text);
                    if (iOHeaders.Count>0)
                    {
                        if (iOHeaders[0].HandlingStatusID.Equals("2"))
                        {
                            MessageBox.Show("Phiếu xuất kho đã duyệt mức 2, không thể thay đổi", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        await updateIOHeaderAsync();
                    }
                    else
                    {
                        await createIOHeaderAsync();
                    }
                }
            }
           
        }

        public async Task updateWIDataGeneralAsync()
        {
            int num = gridViewWIDataGeneral.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWIDataGeneral.GetRowHandle(i);
                WIDataGeneral WIDataGeneral = new WIDataGeneral();
                WIDataGeneral.WIDNumber = textEditWIDNumber.Text;
                WIDataGeneral.PartNumber = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PartNumber");
                WIDataGeneral.ProductFamily = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "ProductFamily");
                WIDataGeneral.LotID = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "LotID");
                WIDataGeneral.IDCode = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "IDCode");
                WIDataGeneral.PartName = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PartName");
                WIDataGeneral.Ordinal = i + 1;
                WIDataGeneral.Quantity = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "Quantity");
                WIDataGeneral.TotalQuantity = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "TotalQuantity");
                WIDataGeneral.TotalGoods = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "TotalGoods");
                WIDataGeneral.QuantityOrg = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "QuantityOrg");
                WIDataGeneral.TotalQuantityOrg = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "TotalQuantityOrg");
                WIDataGeneral.TotalGoodsOrg = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "TotalGoodsOrg");
                WIDataGeneral.Status = "1";
                WIDataGeneral.PackingVolume = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PackingVolume");
                WIDataGeneral.QuantityByPack = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "QuantityByPack");
                WIDataGeneral.QuantityByItem = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "QuantityByItem");
                WIDataGeneral.PackingQuantity = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PackingQuantity");
                WIDataGeneral.PalletID= (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PalletID");
                WIDataGeneral.PONumber = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PONumber");
                WIDataGeneral.Unit= (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "Unit");
                if (((DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "MFGDate")).ToString() != "")
                {
                    WIDataGeneral.MFGDate = (DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "MFGDate");
                }
                if (((DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "EXPDate")).ToString() != "")
                {
                    WIDataGeneral.EXPDate = (DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "EXPDate");
                }
                if (((DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "InputDate")).ToString() != "")
                {
                    WIDataGeneral.InputDate = (DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "InputDate");
                }
                if (((DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "OutputDate")).ToString() != "")
                {
                    WIDataGeneral.OutputDate = (DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "OutputDate");
                }
                if (await WIDataGeneralRepository.Update(WIDataGeneral, textEditWIDNumber.Text,WIDataGeneral.PalletID, WIDataGeneral.PartNumber, WIDataGeneral.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        public async void DeleteWIDataHeaderAsync()
        {
            if ((await WIDataHeaderRepository.Delete(textEditWIDNumber.Text)) > 0)
            {
                await DeleteWIDataGeneralAsync();
                await DeleteWIRHeaderAsync();
                sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
                BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage,WMMessage.msgDeleteSuccess);
            }
        }

        public async Task DeleteWIDataGeneralAsync()
        {
            await WIDataGeneralRepository.Delete(textEditWIDNumber.Text);
            clearAsync();
            gridControlWIDataDetail.DataSource = null;
        }

        public async Task DeleteWIRHeaderAsync()
        {
            if ((await WIRHeaderRepository.Delete(textEditWIDNumber.Text)) > 0)
            {
                await DeleteWIRDetail();
            }
        }

        public async Task DeleteWIRDetail()
        {
            await WIRDetailRepository.Delete(textEditWIDNumber.Text);
        }

        private void clearAsync()
        {
            textEditWIDNumber.Text = "";
            dateEditWIDDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            textEditTotalQuantityOrg.Text = "0";
            textEditTotalQuantity.Text = "0";
            List<WIDataGeneral> WIDataGenerals = new List<WIDataGeneral>();

            for (int i = 0; i < 100; i++)
            {
                WIDataGenerals.Add(new WIDataGeneral());
            }
            gridControlWIDataDetail.DataSource = WIDataGenerals;    
        }

        public async Task createIOHeaderAsync()
        {
            WIDataHeader WIDataHeader = await WIDataHeaderRepository.GetUnderID(textEditWIDNumber.Text);
            IOHeader IOHeader = new IOHeader();
            IOHeader.IONumber = textEditWIDNumber.Text;
            IOHeader.IODate = DateTime.Parse(dateEditWIDDate.DateTime.ToString("yyyy-MM-dd"));
            IOHeader.WIRNumber = textEditWIDNumber.Text;
            IOHeader.BranchID = WIDataHeader.BranchID;
            IOHeader.BranchName = WIDataHeader.BranchName;
            IOHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            IOHeader.HandlingStatusID = "0";
            IOHeader.HandlingStatusName = "Chưa duyệt";
            IOHeader.ModalityID = "39";
            IOHeader.ModalityName = "Bán khác";
            IOHeader.CreatedUserID = WMMessage.User.UserID;
            IOHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            IOHeader.WarehouseID = "1";
            IOHeader.WarehouseName = "KHO CÔNG TY";

            if (await IOHeaderRepository.Create(IOHeader) > 0)
            {
                createIODetailAsync();
                MessageBox.Show("-> Đã tạo phiếu xuất kho");
            }
        }

        public async void createIODetailAsync()
        {
            int num = gridViewWIDataGeneral.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWIDataGeneral.GetRowHandle(i);
                IODetail IODetail= new IODetail();
                IODetail.IONumber = textEditWIDNumber.Text;
                IODetail.Ordinal = i + 1;
                IODetail.PalletID = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PalletID");
                IODetail.LotID = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "LotID");
                IODetail.PartNumber = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PartNumber");
                IODetail.PartName = (string)gridViewWIDataGeneral.GetRowCellValue(i, "PartName");
                IODetail.Unit = (string)gridViewWIDataGeneral.GetRowCellValue(i, "Unit");
                IODetail.IDCode = (string)gridViewWIDataGeneral.GetRowCellValue(i, "IDCode");
                IODetail.IssueQuantity = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(i, "Quantity");
                IODetail.QuantityOrdered = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(i, "QuantityOrg");
                IODetail.PackingQuantity = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(i, "PackingQuantity");
                IODetail.PackingVolume = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(i, "PackingVolume");
                //if (((DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "MFGDate")).ToString() != "")
                //{
                //    IODetail.MFGDate = (DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "MFGDate");
                //}
                //if (((DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "EXPDate")).ToString() != "")
                //{
                //    IODetail.EXPDate = (DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "EXPDate");
                //}
                //if (((DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "InputDate")).ToString() != "")
                //{
                //    IODetail.InputDate = (DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "InputDate");
                //}
                if (((DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "OutputDate")).ToString() != "")
                {
                    IODetail.OutputDate = (DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "OutputDate");
                }
                if (await IODetailRepository.Create(IODetail) > 0)
                {
                    i++;
                }
            }
        }

        public async Task updateIOHeaderAsync()
        {
            WIDataHeader WIDataHeader = await WIDataHeaderRepository.GetUnderID(textEditWIDNumber.Text);
            IOHeader IOHeader = new IOHeader();
            IOHeader.IONumber = textEditWIDNumber.Text;
            IOHeader.IODate = DateTime.Parse(dateEditWIDDate.DateTime.ToString("yyyy-MM-dd"));
            IOHeader.WIRNumber = textEditWIDNumber.Text;
            IOHeader.BranchID = WIDataHeader.BranchID;
            IOHeader.BranchName = WIDataHeader.BranchName;
            IOHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityOrg.Text)-Decimal.Parse(textEditTotalQuantity.Text);
            IOHeader.HandlingStatusID = "0";
            IOHeader.HandlingStatusName = "Chưa duyệt";
            IOHeader.ModalityID = "39";
            IOHeader.ModalityName = "Bán khác";
            IOHeader.UpdatedUserID = WMMessage.User.UserID;
            IOHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            IOHeader.WarehouseID = "1";

            if (await IOHeaderRepository.Update(IOHeader,textEditWIDNumber.Text) > 0)
            {
                updateIODetailAsync();
                BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage,m_MainForm.barStaticItemMessage.Caption+ "-> Đã cập nhật phiếu xuất");
            }
        }

        public async void updateIODetailAsync()
        {
            int num = gridViewWIDataGeneral.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWIDataGeneral.GetRowHandle(i);
                IODetail IODetail = new IODetail();
                IODetail.IONumber = textEditWIDNumber.Text;
                IODetail.Ordinal = i + 1;
                IODetail.PalletID = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PalletID");
                IODetail.LotID = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "LotID");
                IODetail.PartNumber = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PartNumber");
                IODetail.PartName = (string)gridViewWIDataGeneral.GetRowCellValue(i, "PartName");
                IODetail.Unit = (string)gridViewWIDataGeneral.GetRowCellValue(i, "Unit");
                IODetail.IDCode = (string)gridViewWIDataGeneral.GetRowCellValue(i, "IDCode");
                IODetail.IssueQuantity = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(i, "Quantity");
                IODetail.QuantityOrdered = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(i, "QuantityOrg");
                IODetail.PackingQuantity = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(i, "PackingQuantity");
                IODetail.PackingVolume = (Decimal?)gridViewWIDataGeneral.GetRowCellValue(i, "PackingVolume");
                //if (((DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "MFGDate")).ToString() != "")
                //{
                //    IODetail.MFGDate = (DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "MFGDate");
                //}
                //if (((DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "EXPDate")).ToString() != "")
                //{
                //    IODetail.EXPDate = (DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "EXPDate");
                //}
                //if (((DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "InputDate")).ToString() != "")
                //{
                //    IODetail.InputDate = (DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "InputDate");
                //}
                if (((DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "OutputDate")).ToString() != "")
                {
                    IODetail.OutputDate = (DateTime?)gridViewWIDataGeneral.GetRowCellValue(i, "OutputDate");
                }
                if (await IODetailRepository.Update(IODetail,textEditWIDNumber.Text,IODetail.PalletID, IODetail.PartNumber,IODetail.Ordinal) > 0)
                {
                    i++;
                }
            }
        }


        private async void WIDNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                WIDataHeader WIDataHeader = new WIDataHeader();
                WIDataHeader = await WIDataHeaderRepository.GetUnderID(textEditWIDNumber.Text);
                if (WIDataHeader != null)
                {
                    dateEditWIDDate.Text = DateTime.Parse(WIDataHeader.WIDDate.ToString()).ToShortDateString();
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(WIDataHeader.HandlingStatusID) + 1;
                    sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
                }
                else
                {
                    BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Số phiếu yêu cầu không tồn tại");
                    return;
                }

            }
        }

        private async void textEditWDRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchWID frmSearchWID = new frmSearchWID();
            frmSearchWID.ShowDialog(this);
            frmSearchWID.Dispose();
            if (!selectedWIDNumber.Equals(""))
            {
                textEditWIDNumber.Text = selectedWIDNumber;
                WIDataHeader WIDataHeader = await WIDataHeaderRepository.GetUnderID(textEditWIDNumber.Text);
                if (WIDataHeader != null)
                {
                    dateEditWIDDate.Text = DateTime.Parse(WIDataHeader.WIDDate.ToString()).ToShortDateString();
                    comboBoxEditHandlingStatus.SelectedIndex =WIDataHeader.HandlingStatusID.Equals("<>")?0: int.Parse(WIDataHeader.HandlingStatusID)+1;
                    sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
                }
                QRCodeGeneral(selectedWIDNumber);
            }

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (textEditWIDNumber.Text != "")
            {
                
                sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
                //loadDataGridWIDatadetail();
            }
        }

        private void grdData_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "Quantity", false) == 0))
            {
                double mQuantity = ((gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["Quantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["Quantity"])));
                double mQuantityOrg = ((gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["QuantityOrg"]) == "") ? 0.0 : Convert.ToDouble(gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["QuantityOrg"])));
                double mPackingQuantity = ((gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["PackingQuantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["PackingQuantity"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mQuantity == mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                }
                if (mQuantity > mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                }
                if (mQuantity < mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                }
            }
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "PackingQuantity", false) == 0))
            {
                double mQuantity = ((gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["Quantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["Quantity"])));
                double mQuantityOrg = ((gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["QuantityOrg"]) == "") ? 0.0 : Convert.ToDouble(gridViewWIDataGeneral.GetRowCellValue(i, gridViewWIDataGeneral.Columns["QuantityOrg"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mQuantity == mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                }
                if (mQuantity > mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                }
                if (mQuantity < mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                }
            }
            //if ((i >= 0) && (String.Compare(e.Column.FieldName, "No", false) == 0))
            //{
            //    string partNumber = ((gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["PartNumber"]).ToString() == "") ? "" : Convert.ToString(gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["PartNumber"])));
            //    e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            //    if ((partNumber.Contains("SW") || partNumber.Contains("LW") || partNumber.Contains("WT"))&&!partNumber.Equals(""))
            //    {
            //        e.Appearance.ForeColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#000000");
            //        e.Appearance.BackColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#FFFFFF");
            //    }
            //    else
            //    {
            //        e.Appearance.ForeColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#FFFFFF");
            //        e.Appearance.BackColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#000000");
            //    }
            //}
        }

        private void textTotalQuantity_CustomDraw()
        {
            double mTotalQuantity = ((textEditTotalQuantity.Text == "") ? 0.0 : Convert.ToDouble(textEditTotalQuantity.Text));
            double mTotalQuantityOrg = ((textEditTotalQuantityOrg.Text == "") ? 0.0 : Convert.ToDouble(textEditTotalQuantityOrg.Text));
            textEditTotalQuantity.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            textEditTotalQuantityOrg.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            if (mTotalQuantity == mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.LightGreen;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            if (mTotalQuantity > mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.Yellow;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            if (mTotalQuantity < mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.Red;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
        }

        private void butOn_Click(object sender, EventArgs e)
        {
            BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Bắt đầu quét nhập kho");
            t.Enabled = true;
        }

        private void butOff_Click(object sender, EventArgs e)
        {
            BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Kết thúc quét nhập kho");
            t.Enabled = false;
        }

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clearAsync();
                    }
                    break;
                case "save":
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateWIDataHeaderAsync();
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteWIDataHeaderAsync();
                    }
                    break;
                case "search":

                    break;
                case "refesh":
                    WIDataHeader WIDataHeader = await WIDataHeaderRepository.GetUnderID(textEditWIDNumber.Text);
                    if (WIDataHeader != null)
                    {
                        comboBoxEditHandlingStatus.SelectedIndex = WIDataHeader.HandlingStatusID.Equals("<>") ? 0 : int.Parse(WIDataHeader.HandlingStatusID) + 1;
                        sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
                    }
                    break;
                case "import":
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    Demo demo = new Demo();
                    demo.Show();
                    break;
            }

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {

            switch (keyData)
            {
                case Keys.F2:
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clearAsync();
                    }
                    return true;
                case Keys.F3:
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateWIDataHeaderAsync();
                    }
                    return true;
                case Keys.F4:
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteWIDataHeaderAsync();
                    }
                    return true;
                case Keys.F5:
                    sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
                    return true;
                case Keys.F10:
                    if (WMPublic.sbMessageCloseTab(this) == true)
                    {
                        XtraTabPage tabPage;
                        tabPage = (XtraTabPage)(this.Parent);
                        XtraTabControl tabControl;
                        tabControl = (XtraTabControl)tabPage.Parent;
                        tabControl.TabPages.Remove(tabPage);
                        tabPage.Dispose();
                    }
                    return true;
                case Keys.O | Keys.Control:
                    BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Bắt đầu quét xuất kho");
                    t.Enabled = true;
                    return true;
                case Keys.P | Keys.Control:
                    BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Kết thúc quét xuất kho");
                    t.Enabled = false;
                    return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        //private async void gridViewWIDataGeneral_RowCellClickAsync(object sender, RowCellClickEventArgs e)
        //{
        //    GridViewInfo viewInfo = gridViewWIDataGeneral.GetViewInfo() as GridViewInfo;
        //    GridHitInfo hitInfo = viewInfo.CalcHitInfo(e.Location);

        //    if (e.Button == System.Windows.Forms.MouseButtons.Left)
        //    {
        //        if (hitInfo.InRowCell)
        //        {
        //            int rowHandle = hitInfo.RowHandle;

        //            if (gridViewWIDataGeneral.FocusedColumn.FieldName.Equals("PalletID"))
        //            {
        //                selectedPalletID = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PalletID");
        //                selectedOrdinal = (int)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "Ordinal");
        //                string palletID = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "PalletID");
        //                int ordinal = (int)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "Ordinal");
        //                string lotID = (string)gridViewWIDataGeneral.GetRowCellValue(rowHandle, "LotID");


        //                List<WIDataGeneral> wRDataGenerals = await WIDataGeneralRepository.GetUnderParams(textEditWIDNumber.Text, palletID, ordinal);
        //                if (wRDataGenerals.Count > 0)
        //                {
        //                    string[] idcodes = wRDataGenerals[0].IDCode.Split(',');
        //                    List<WRDataDetail> wRDataDetails = new List<WRDataDetail>();


        //                    for (int i = 0; i < idcodes.Length - 1; i++)
        //                    {
        //                        WRDataDetail wRDataDetail = new WRDataDetail();
        //                        wRDataDetail.IDCode = idcodes[i];
        //                        wRDataDetails.Add(wRDataDetail);
        //                    }
        //                    gridControl1.DataSource = wRDataDetails;
        //                }


        //            }
        //        }
        //    }
        //}

        //private async void loadDataGridWIDatadetail()
        //{
        //    List<WIDataGeneral> wIDataGenerals = await WIDataGeneralRepository.GetUnderParams(textEditWIDNumber.Text, selectedPalletID);
        //    if (wIDataGenerals.Count > 0)
        //    {
        //        string[] idcodes = wIDataGenerals[0].IDCode.Split(',');
        //        List<WRDataDetail> wRDataDetails = new List<WRDataDetail>();


        //        for (int i = 0; i < idcodes.Length - 1; i++)
        //        {
        //            WRDataDetail wRDataDetail = new WRDataDetail();
        //            wRDataDetail.IDCode =idcodes[i];
        //            wRDataDetails.Add(wRDataDetail);
        //        }
        //        gridControl1.DataSource = wRDataDetails;
        //    }
        //}

        private async void gridViewWIDataGeneral_RowClick(object sender, RowClickEventArgs e)
        {
            string widNumber = textEditWIDNumber.Text;
            string palletID = (string)gridViewWIDataGeneral.GetRowCellValue(e.RowHandle, "PartNumber");
            string partNumber = (string)gridViewWIDataGeneral.GetRowCellValue(e.RowHandle, "PalletID");
            int ordinal = (int)gridViewWIDataGeneral.GetRowCellValue(e.RowHandle, "Ordinal");


            List<WIDataGeneral> wRDataGenerals = await WIDataGeneralRepository.GetByParams3(widNumber,palletID, partNumber);
            if (wRDataGenerals.Count > 0)
            {
                string[] idcodes = wRDataGenerals[0].IDCode.Split(',');
                List<WRDataDetail> wRDataDetails = new List<WRDataDetail>();


                for (int i = 0; i < idcodes.Length - 1; i++)
                {
                    WRDataDetail wRDataDetail = new WRDataDetail();
                    wRDataDetail.IDCode = idcodes[i];
                    wRDataDetails.Add(wRDataDetail);
                }
                gridControl1.DataSource = wRDataDetails;
            }
        }
    }
}
