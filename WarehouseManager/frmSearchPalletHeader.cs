﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchPalletHeader : Form
    {

        PalletHeaderRepository palletHeaderRepository = null;
        PalletDetailRepository palletDetailRepository = null;
        PalletManagerControl m_PalletManagerControl = null;
        public frmSearchPalletHeader(PalletManagerControl palletManagerControl)
        {
            InitializeComponent();
            m_PalletManagerControl = palletManagerControl;




            this.Load += frmSearchPalletHeader_Load;
            palletHeaderRepository = new PalletHeaderRepository();
            palletDetailRepository = new PalletDetailRepository();

        }

        private void frmSearchPalletHeader_Load(object sender, EventArgs e)
        {
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            sbLoadDataForGridWRRHeaderAsync();
            gridViewWRRHeader.DoubleClick += dataGridView_CellDoubleClick;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            popupMenuSelectHandlingStatus();
            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

            gridViewWRRHeader.Columns["PalletID"].Summary.Add(DevExpress.Data.SummaryItemType.Count, "PalletID", "Tổng Pallet={0}");
            gridViewWRRHeader.Columns["TotalIDCode"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "TotalIDCode", "Tổng thùng hàng={0}");
        }



        private void popupMenuSelectHandlingStatus()
        {
            List<string> list = Helpers.Constant.handlingStatuss;
            foreach (string s in list)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(s);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridWRRHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        private async void sbLoadDataForGridWRRHeaderAsync()
        {
            List<PalletHeader> palletHeaders = await palletHeaderRepository.GetAll();
            var querys = palletHeaders.Where(a => a.CreatedDate >= DateTime.Parse(dateEditFromDate.Text) && a.CreatedDate <= DateTime.Parse(dateEditToDate.Text)&&(a.HandlingStatusID.Equals("2")||a.HandlingStatusID.Equals("3")||a.HandlingStatusID.Equals("4")))
                       .ToList();
            gridControlWRRHeader.DataSource = querys;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewWRRHeader.RowCount > 0)
            {
                PalletHeader palletHeader = await palletHeaderRepository.GetUnderID((string)(sender as GridView).GetFocusedRowCellValue("PalletID"));
                PalletManagerControl.selecttedID = (string)(sender as GridView).GetFocusedRowCellValue("PalletID");
                this.Close();
            }
        }

        private async void filter()
        {
            List<PalletHeader> palletHeaders = await palletHeaderRepository.GetAll();
            if (palletHeaders.Count > 0)
            {
                try
                {
                    if (comboBoxEditHandlingStatus.SelectedItem.ToString().Equals("<>"))
                    {
                        var querys = palletHeaders.Where(a => a.CreatedDate >= DateTime.Parse(dateEditFromDate.Text) && a.CreatedDate <= DateTime.Parse(dateEditToDate.Text))
                            .ToList();
                        gridControlWRRHeader.DataSource = querys;
                    }
                    else
                    {
                        var querys = palletHeaders.Where(a => a.HandlingStatusID.Equals((comboBoxEditHandlingStatus.SelectedIndex - 1).ToString())
                                 && a.CreatedDate >= DateTime.Parse(dateEditFromDate.Text) && a.CreatedDate <= DateTime.Parse(dateEditToDate.Text))
                            .ToList();
                        gridControlWRRHeader.DataSource = querys;
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridWRRHeaderAsync();
                    break;
                case "deletepallet":
                    int[] selectedRowHandles = gridViewWRRHeader.GetSelectedRows();
                    if (selectedRowHandles.Length == 0)
                    {
                        MessageBox.Show("Không có pallet nào được chọn");
                    }
                    else
                    {
                        DialogResult dialog = new DialogResult();

                        dialog = MessageBox.Show("Bạn chắc chắn xóa " + selectedRowHandles.Length + " pallet đã chọn", "Cảnh báo!", MessageBoxButtons.YesNo);

                        if (dialog == DialogResult.Yes)
                        {
                            StringBuilder palletsDeleted = new StringBuilder();
                            for (int i = 0; i < selectedRowHandles.Length; i++)
                            {
                                string palletID = (string)gridViewWRRHeader.GetRowCellValue(selectedRowHandles[i], "PalletID");
                                palletsDeleted.Append(palletID + ", ");
                                deletePalletHeader(palletID);
                                deletePalletDetail(palletID);
                            }
                            sbLoadDataForGridWRRHeaderAsync();
                            MessageBox.Show("Xóa thành công " + selectedRowHandles.Length + " pallet: " + palletsDeleted);
                        }

                    }
                    break;
                case "get":
                    List<PalletHeader> palletHeaders = await palletHeaderRepository.GetAll();
                    var querys = palletHeaders.Where(a => a.CreatedDate >= DateTime.Parse(dateEditFromDate.Text) && a.CreatedDate <= DateTime.Parse(dateEditToDate.Text) && a.PalletID.Contains(gridViewWRRHeader.FindFilterText))
                               .ToList();


                    m_PalletManagerControl.gridControl1.DataSource = querys;
                    this.Close();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }

        private async void deletePalletHeader(string palletID)
        {
            await palletHeaderRepository.Delete(palletID);
        }

        private async void deletePalletDetail(string palletID)
        {
            await palletDetailRepository.Delete(palletID);
        }

        private void comboBoxEditHandlingStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            filter();
        }

        private void dateEditFromDate_EditValueChanged(object sender, EventArgs e)
        {
            filter();
        }

        private void dateEditToDate_EditValueChanged(object sender, EventArgs e)
        {
            filter();
        }

        private void gridViewWRRHeader_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "HandlingStatusName", false) == 0))
            {
                string handlingStatusName= ((gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["HandlingStatusName"]).Equals("")) ?"": (string)gridViewWRRHeader.GetRowCellValue(i, gridViewWRRHeader.Columns["HandlingStatusName"]));
              
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (handlingStatusName.Equals("Duyệt mức 2"))
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                }
            }
        }
    }
}
