﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraLayout.Filtering.Templates;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;
using LicenseContext = OfficeOpenXml.LicenseContext;
using DevExpress.XtraEditors.Repository;
using DevExpress.Utils.Extensions;
using DevExpress.XtraReports.UI;
using DevExpress.Utils.Drawing;
using WarehouseManager.Helpers;

namespace WarehouseManager
{

    public partial class InventoryProcessControl : DevExpress.XtraEditors.XtraUserControl
    {
        BranchRepository branchRepository;
        WRHeaderRepository WRHeaderRepository;
        WRDetailRepository wRDetailRepository;
        IOHeaderRepository iOHeaderRepository;
        IODetailRepository iODetailRepository;
        WarehouseRepository warehouseRepository;
        InventoryRepository inventoryRepository;
        InventoryDetailRepository inventoryDetailRepository = null;
        InventoryOfLastMonthsRepository inventoryOfLastMonthsRepository;
        InventoryOfLastMonthDetailRepository inventoryOfLastMonthDetailRepository = null;
        String monnthYearNow = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
        String mCurrentDateTime = DateTime.Now.ToString("MM/yyyy");
        string mCurrentPeriod;
        string mLastPeriod;

        internal MainForm m_MainForm;
        public InventoryProcessControl(MainForm mainForm)
        {
            InitializeComponent();
            m_MainForm = mainForm;
            BottomBarHelper.normalMessage(m_MainForm.barStaticItemMessage, "!!!");
            branchRepository = new BranchRepository();
            WRHeaderRepository = new WRHeaderRepository();
            wRDetailRepository = new WRDetailRepository();
            iOHeaderRepository = new IOHeaderRepository();
            iODetailRepository = new IODetailRepository();
            warehouseRepository = new WarehouseRepository();
            inventoryRepository = new InventoryRepository();
            inventoryDetailRepository = new InventoryDetailRepository();
            inventoryOfLastMonthsRepository = new InventoryOfLastMonthsRepository();
            inventoryOfLastMonthDetailRepository = new InventoryOfLastMonthDetailRepository();
            this.Load += InventoryProcess_Load;

        }

        private void InventoryProcess_Load(Object sender, EventArgs e)
        {
            getMonthYear();
            popupMenuSelectBranch();
            popupMenuSelectPeriod();
            popupMenuSelectWarehouse();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            gridViewInventory.CustomDrawCell += grdData_CustomDrawCell;
            gridViewInventory.RowCellClick += partNumber_CellDoubleClick;

            //sbLoadDataForGridAsync(2022, 6, "1");
            loadGridCurrent();

        }

        private async void loadGridCurrent()
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));

            DataSet dataSet = new DataSet();

            DataTable inventoryTable = new DataTable();
            inventoryTable.Columns.Add("Year", typeof(Int16));
            inventoryTable.Columns.Add("Month", typeof(Int16));
            inventoryTable.Columns.Add("WarehouseID", typeof(string));
            inventoryTable.Columns.Add("PartNumber", typeof(string));
            inventoryTable.Columns.Add("Material", typeof(string));
            inventoryTable.Columns.Add("Mold", typeof(string));
            inventoryTable.Columns.Add("OpeningStockQuantity", typeof(decimal));
            inventoryTable.Columns.Add("ReceiptQuantity", typeof(decimal));
            inventoryTable.Columns.Add("IssueQuantity", typeof(decimal));
            inventoryTable.Columns.Add("ClosingStockQuantity", typeof(decimal));
            inventoryTable.Columns.Add("PackingVolume", typeof(decimal));

            List<Inventory> inventories = await inventoryRepository.GetUnderMultiID(mYear, mMonth, "1");
            List<InventoryDetail> inventoryDetails1 = await inventoryDetailRepository.GetByParams3(mYear, mMonth, "1");


            //if (inventories.Count == 0)
            //{
            //    BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Chưa xử lý tồn kho tháng " + mMonth + " ,năm " + mYear);
            //    return;
            //}

            foreach (Inventory inventory in inventories)
            {
                inventory.Mold = "";
                foreach (InventoryDetail inventoryDetail in inventoryDetails1)
                {
                    if (inventory.PartNumber.Equals(inventoryDetail.PartNumber))
                    {
                        inventory.Mold +=inventory.Mold.Contains(inventoryDetail.LotID.Substring(inventoryDetail.LotID.Length - 1))?"":inventoryDetail.LotID.Substring(inventoryDetail.LotID.Length - 1)+",";
                       
                    }

                }
                inventoryTable.Rows.Add(new object[]
                {
                    inventory.Year,
                    inventory.Month,
                    inventory.WarehouseID,
                    inventory.PartNumber,
                    inventory.Material,
                    inventory.Mold.Length>1?inventory.Mold.Substring(0,inventory.Mold.Length-1):inventory.Mold,
                    inventory.OpeningStockQuantity,
                    inventory.ReceiptQuantity,
                    inventory.IssueQuantity,
                    inventory.ClosingStockQuantity,
                    inventory.PackingVolume,
                });

            }
            inventoryTable.TableName = "Inventory";

            DataTable inventoryDetailTable = new DataTable();
            inventoryDetailTable.Columns.Add("PartNumber", typeof(string));
            inventoryDetailTable.Columns.Add("PalletID", typeof(string)).Caption = "Pallet Number";
            inventoryDetailTable.Columns.Add("LotID", typeof(string));
            inventoryDetailTable.Columns.Add("IDCode", typeof(string)).Caption = "Mã hàng";
            inventoryDetailTable.Columns.Add("Quantity", typeof(decimal));

            List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetAll();
            foreach (InventoryDetail inventoryDetail in inventoryDetails)
            {
                inventoryDetailTable.Rows.Add(new object[]
                {
                    inventoryDetail.PartNumber,
                    inventoryDetail.PalletID,
                    inventoryDetail.LotID,
                    inventoryDetail.IDCode,
                    inventoryDetail.Quantity,
                });
            }
            inventoryDetailTable.TableName = "InventoryDetail";

            dataSet.Tables.Add(inventoryTable);
            dataSet.Tables.Add(inventoryDetailTable);
            dataSet.Relations.Add("Chi tiết", inventoryTable.Columns["PartNumber"], inventoryDetailTable.Columns["PartNumber"]);
            gridControlInventory.DataSource = dataSet;

            gridControlInventory.DataMember = "Inventory";

            getTotalOpenStockQuantity();
            getTotalOpenStockCarton();
            getTotalCloseStockQuantity();
            getTotalCloseStockCarton();
            getTotalReceiptQuantity();
            getTotalReceiptCarton();
            getTotalIssueQuantity();
            getTotalIssueCarton();
        }

        private async void loadGridLast()
        {

            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;
            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }

            DataSet dataSet = new DataSet();

            DataTable inventoryTable = new DataTable();
            inventoryTable.Columns.Add("Year", typeof(Int16));
            inventoryTable.Columns.Add("Month", typeof(Int16));
            inventoryTable.Columns.Add("WarehouseID", typeof(string));
            inventoryTable.Columns.Add("PartNumber", typeof(string));
            inventoryTable.Columns.Add("Material", typeof(string));
            inventoryTable.Columns.Add("Mold", typeof(string));
            inventoryTable.Columns.Add("OpeningStockQuantity", typeof(decimal));
            inventoryTable.Columns.Add("ReceiptQuantity", typeof(decimal));
            inventoryTable.Columns.Add("IssueQuantity", typeof(decimal));
            inventoryTable.Columns.Add("ClosingStockQuantity", typeof(decimal));
            inventoryTable.Columns.Add("PackingVolume", typeof(decimal));

            List<InventoryOfLastMonths> inventories = await inventoryOfLastMonthsRepository.GetAll();
            List<InventoryOfLastMonthDetail> inventoryDetails1 = await inventoryOfLastMonthDetailRepository.GetByParams3(mLastYear,mLastMonth, "1");


            foreach (InventoryOfLastMonths inventory in inventories)
            {
                inventory.Mold = "";
                foreach (InventoryOfLastMonthDetail inventoryOfLastMonthDetail in inventoryDetails1)
                {
                    if (inventory.PartNumber.Equals(inventoryOfLastMonthDetail.PartNumber))
                    {
                        inventory.Mold += inventory.Mold.Contains(inventoryOfLastMonthDetail.LotID.Substring(inventoryOfLastMonthDetail.LotID.Length - 1)) ? "" : inventoryOfLastMonthDetail.LotID.Substring(inventoryOfLastMonthDetail.LotID.Length - 1) + ",";

                    }

                }
                

                inventoryTable.Rows.Add(new object[]
                {
                    inventory.Year,
                    inventory.Month,
                    inventory.WarehouseID,
                    inventory.PartNumber,
                    inventory.Material,
                    inventory.Mold.Length>1?inventory.Mold.Substring(0,inventory.Mold.Length-1):inventory.Mold,
                    inventory.OpeningStockQuantity,
                    inventory.ReceiptQuantity,
                    inventory.IssueQuantity,
                    inventory.ClosingStockQuantity,
                    inventory.PackingVolume,
                });
            }
            inventoryTable.TableName = "Inventory";

            DataTable inventoryDetailTable = new DataTable();
            inventoryDetailTable.Columns.Add("PartNumber", typeof(string));
            inventoryDetailTable.Columns.Add("PalletID", typeof(string)).Caption="Pallet Number";
            inventoryDetailTable.Columns.Add("LotID", typeof(string));
            inventoryDetailTable.Columns.Add("IDCode", typeof(string)).Caption = "Mã hàng";
            inventoryDetailTable.Columns.Add("Quantity", typeof(decimal));

            List<InventoryOfLastMonthDetail> inventoryDetails = await inventoryOfLastMonthDetailRepository.GetAll();
            foreach (InventoryOfLastMonthDetail inventoryDetail in inventoryDetails)
            {
                inventoryDetailTable.Rows.Add(new object[]
                {
                    inventoryDetail.PartNumber,
                    inventoryDetail.PalletID,
                    inventoryDetail.LotID,
                    inventoryDetail.IDCode,
                    inventoryDetail.Quantity,
                });
            }
            inventoryDetailTable.TableName = "InventoryDetail";

            dataSet.Tables.Add(inventoryTable);
            dataSet.Tables.Add(inventoryDetailTable);
            dataSet.Relations.Add("Chi tiết", inventoryTable.Columns["PartNumber"], inventoryDetailTable.Columns["PartNumber"]);
            gridControlInventory.DataSource = dataSet;

            gridControlInventory.DataMember = "Inventory";

            getTotalOpenStockQuantity();
            getTotalOpenStockCarton();
            getTotalCloseStockQuantity();
            getTotalCloseStockCarton();
            getTotalReceiptQuantity();
            getTotalReceiptCarton();
            getTotalIssueQuantity();
            getTotalIssueCarton();
        }


        private async void partNumber_CellDoubleClick(object sender, EventArgs e)
        {
            gridViewInventory.OptionsBehavior.Editable = false;
            DevExpress.XtraGrid.Views.Grid.GridView sndr =
                sender as DevExpress.XtraGrid.Views.Grid.GridView;

            DevExpress.Utils.DXMouseEventArgs dxMouseEventArgs =
                e as DevExpress.Utils.DXMouseEventArgs;


            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hitInfo =
                   sndr.CalcHitInfo(dxMouseEventArgs.Location);
            if (hitInfo.InRowCell)
            {
                int rowHandle = hitInfo.RowHandle;



                if (gridViewInventory.FocusedColumn.FieldName.Equals("PartNumber"))
                {
                    frmInventoryDetail frmInventoryDetail = new frmInventoryDetail();
                    frmInventoryDetail.partNumber = (string)gridViewInventory.GetRowCellValue(rowHandle, "PartNumber");
                    frmInventoryDetail.warehouseID = (string)gridViewInventory.GetRowCellValue(rowHandle, "WarehouseID");
                    frmInventoryDetail.year = (Int16)gridViewInventory.GetRowCellValue(rowHandle, "Year");
                    frmInventoryDetail.month = (Int16)gridViewInventory.GetRowCellValue(rowHandle, "Month");


                    frmInventoryDetail.ShowDialog(this);
                }
            }
        }
        private void grdData_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "No", false) == 0))
            {
                string partNumber = ((gridViewInventory.GetRowCellValue(i, gridViewInventory.Columns["PartNumber"]).ToString() == "") ? "" : Convert.ToString(gridViewInventory.GetRowCellValue(i, gridViewInventory.Columns["PartNumber"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (partNumber.Contains("SW") || partNumber.Contains("LW") || partNumber.Contains("WT"))
                {
                    e.Appearance.ForeColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#000000");
                    e.Appearance.BackColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#FFFFFF");
                }
                else
                {
                    e.Appearance.ForeColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#FFFFFF");
                    e.Appearance.BackColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#000000");
                }
            }
        }

        private DateTime GetDateTime(int year, int month)
        {
            DateTime dateTime = DateTime.Parse($"{month:00}" + "/" + String.Format(year.ToString(), "####"));
            return dateTime;
        }

        private void getMonthYear()
        {
            mCurrentPeriod = String.Format(mCurrentDateTime, "MM/yyyy");
            int mYear = int.Parse(mCurrentPeriod.Substring(3, 4));
            int mMonth = int.Parse(mCurrentPeriod.Substring(0, 2));
            int mLastMonth;
            int mLastYear;
            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = mYear - 1;
            }
            else
            {
                mLastMonth = mMonth - 1;
                mLastYear = mYear;
            }
            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = mYear - 1;
            }
            else
            {
                mLastMonth = mMonth - 1;
                mLastYear = mYear;
            }
            mLastPeriod = $"{mLastMonth:00}" + "/" + String.Format(mLastYear.ToString(), "####");
        }



        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 1;
        }

        private void popupMenuSelectPeriod()
        {

            comboBoxEditProcessingPeriod.Properties.Items.Add(mLastPeriod);
            comboBoxEditProcessingPeriod.Properties.Items.Add(mCurrentPeriod);
            comboBoxEditProcessingPeriod.SelectedIndex = 1;
        }

        private async void popupMenuSelectWarehouse()
        {
            List<Warehouse> warehouses = await warehouseRepository.getAll();
            foreach (Warehouse warehouse in warehouses)
            {
                comboBoxEditWarehouse.Properties.Items.Add(warehouse.WarehouseName);
            }
            comboBoxEditWarehouse.SelectedIndex = 1;
        }

        private void getTotalOpenStockQuantity()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "OpeningStockQuantity");
                total += quantity;
                i++;
            }
            textEditTotalOpenStockQuantity.Text = total.ToString("0.#####");
        }
        private void getTotalOpenStockCarton()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "OpeningStockQuantity");
                Decimal packingVolume = (decimal)gridViewInventory.GetRowCellValue(rowHandle, "PackingVolume");
                total += quantity / packingVolume;
                i++;
            }
            textEditTotalOpenStockCarton.Text = total.ToString("0.#####");
        }
        private void getTotalOpenStockAmount()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal amount = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "OpeningStockAmount");
                total += amount;
                i++;
            }
            textEditTotalOpenStockCarton.Text = total.ToString("0.#####");
        }

        private void getTotalReceiptQuantity()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "ReceiptQuantity");
                total += quantity;
                i++;
            }
            textEditTotalReceiptQuantity.Text = total.ToString("0.#####");
        }
        private void getTotalReceiptCarton()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "ReceiptQuantity");
                Decimal packingVolume = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "PackingVolume");
                total += quantity / packingVolume;
                i++;
            }
            textEditTotalReceiptCarton.Text = total.ToString("0.#####");
        }
        private void getTotalReceiptAmount()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal amount = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "ReceiptAmount");
                total += amount;
                i++;
            }
            textEditTotalReceiptCarton.Text = total.ToString("0.#####");
        }
        private void getTotalIssueQuantity()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "IssueQuantity");
                total += quantity;
                i++;
            }
            textEditTotalIssueQuantity.Text = total.ToString("0.#####");
        }
        private void getTotalIssueCarton()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "IssueQuantity");
                Decimal packingVolume = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "PackingVolume");
                total += quantity / packingVolume;
                i++;
            }
            textEditTotalIssueCarton.Text = total.ToString("0.#####");
        }
        private void getTotalIssueAmount()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal amount = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "IssueAmount");
                total += amount;
                i++;
            }
            textEditTotalIssueCarton.Text = total.ToString("0.#####");
        }
        private void getTotalCloseStockQuantity()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "ClosingStockQuantity");
                total += quantity;
                i++;
            }
            textEditCloseStockQuantity.Text = total.ToString("0.#####");
        }
        private void getTotalCloseStockCarton()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "ClosingStockQuantity");
                Decimal packingVolume = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "PackingVolume");
                total += quantity / packingVolume;
                i++;
            }
            textEditCloseStockCarton.Text = total.ToString("0.#####");
        }
        private void getTotalCloseStockAmount()
        {

            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal amount = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "ClosingStockAmount");
                total += amount;
                i++;
            }
            textEditCloseStockCarton.Text = total.ToString("0.#####");
        }

        //private async Task DeleteInventory(String monthYear)
        //{
        //    Int16 month = Int16.Parse(monthYear.Substring(0, 2));
        //    Int16 year = Int16.Parse(monthYear.Substring(3, 4));
        //    await inventoryRepository.Delete(year, month, $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}");
        //}

        //private async Task DeleteInventoryOfLastMonths(String monthYear)
        //{
        //    Int16 month = Int16.Parse(monthYear.Substring(0, 2));
        //    Int16 year = Int16.Parse(monthYear.Substring(3, 4));
        //    await inventoryOfLastMonthsRepository.Delete(year, month, $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}");
        //}



        //private async Task<List<WRHeader>> fetchWRHeaderUnderPeriodDate()
        //{
        //    List<WRHeader> wRHeaders = new List<WRHeader>();
        //    wRHeaders = await WRHeaderRepository.GetUnderPriodDate(comboBoxEditProcessingPeriod.SelectedItem.ToString());
        //    return wRHeaders;
        //}

        //private async Task<List<IOHeader>> fetchIOHeaderUnderPeriodDate()
        //{
        //    List<IOHeader> iOHeaders = new List<IOHeader>();
        //    iOHeaders = await iOHeaderRepository.GetUnderPriodDate(comboBoxEditProcessingPeriod.SelectedItem.ToString());
        //    return iOHeaders;
        //}

        //private async Task<List<WRHeader>> fetchWRHeaderUnderLastPeriodDate()
        //{
        //    List<WRHeader> wRHeaders = new List<WRHeader>();
        //    wRHeaders = await WRHeaderRepository.GetUnderLastPriodDate(comboBoxEditProcessingPeriod.SelectedItem.ToString());
        //    return wRHeaders;
        //}

        //private async Task<List<IOHeader>> fetchIOHeaderUnderLastPeriodDate()
        //{
        //    List<IOHeader> iOHeaders = new List<IOHeader>();
        //    iOHeaders = await iOHeaderRepository.GetUnderLastPriodDate(comboBoxEditProcessingPeriod.SelectedItem.ToString());
        //    return iOHeaders;
        //}

        //private async Task<List<WRDetail>> fetchWRDetailUnderWRHeader()
        //{
        //    List<WRDetail> wRDetails = new List<WRDetail>();
        //    List<WRHeader> wRHeaders = await fetchWRHeaderUnderPeriodDate();
        //    foreach (WRHeader wRHeader in wRHeaders)
        //    {
        //        String wrNumber = wRHeader.WRNumber;
        //        List<WRDetail> wRDetails1 = new List<WRDetail>();
        //        wRDetails1 = await wRDetailRepository.GetUnderID(wrNumber);
        //        foreach (WRDetail wRDetail in wRDetails1)
        //        {
        //            wRDetails.Add(wRDetail);
        //        }
        //    }

        //    var querys = wRDetails.GroupBy(x => x.PartNumber)
        //         .Select(x => new WRDetail
        //         {
        //             PartNumber = x.Key,
        //             PalletID = x.First().PalletID,
        //             IDCode = string.Join("", x.Select(ss => ss.IDCode)),
        //             PartName = x.First().PartName,
        //             Unit = x.First().Unit,
        //             QuantityReceived = x.Sum(y => y.QuantityReceived),
        //             ReceiptQuantity = x.Sum(y => y.ReceiptQuantity),
        //             MFGDate = x.First().MFGDate,
        //             EXPDate = x.First().EXPDate,
        //         })
        //        .ToList();
        //    return querys;
        //}

        //private async Task<List<WRDetail>> fetchWRDetailUnderWRHeader2()
        //{
        //    List<WRDetail> wRDetails = new List<WRDetail>();
        //    List<WRHeader> wRHeaders = await fetchWRHeaderUnderPeriodDate();
        //    foreach (WRHeader wRHeader in wRHeaders)
        //    {
        //        String wrNumber = wRHeader.WRNumber;
        //        List<WRDetail> wRDetails1 = new List<WRDetail>();
        //        wRDetails1 = await wRDetailRepository.GetUnderID(wrNumber);
        //        foreach (WRDetail wRDetail in wRDetails1)
        //        {
        //            wRDetails.Add(wRDetail);
        //        }
        //    }

        //    var querys = wRDetails.GroupBy(x => new { x.PartNumber, x.PalletID })
        //         .Select(x => new WRDetail
        //         {
        //             PartNumber = x.Key.PartNumber,
        //             PalletID = x.Key.PalletID,
        //             IDCode = string.Join("", x.Select(ss => ss.IDCode)),
        //             PartName = x.First().PartName,
        //             Unit = x.First().Unit,
        //             QuantityReceived = x.Sum(y => y.QuantityReceived),
        //             ReceiptQuantity = x.Sum(y => y.ReceiptQuantity),
        //             MFGDate = x.First().MFGDate,
        //             EXPDate = x.First().EXPDate,
        //         })
        //        .ToList();
        //    return querys;
        //}

        //private async Task<List<IODetail>> fetchIODetailUnderIOHeader()
        //{
        //    List<IODetail> iODetails = new List<IODetail>();
        //    List<IOHeader> iOHeaders = await fetchIOHeaderUnderPeriodDate();
        //    foreach (IOHeader entry in iOHeaders)
        //    {
        //        String ioNumber = entry.IONumber;
        //        List<IODetail> iODetails1 = new List<IODetail>();
        //        iODetails1 = await iODetailRepository.GetUnderID(ioNumber);
        //        foreach (IODetail iODetail in iODetails1)
        //        {
        //            iODetails.Add(iODetail);
        //        }
        //    }

        //    var querys = iODetails.GroupBy(x => x.PartNumber)
        //         .Select(x => new IODetail
        //         {
        //             PartNumber = x.Key,
        //             PalletID = x.First().PalletID,
        //             IDCode = string.Join("", x.Select(ss => ss.IDCode)),
        //             PartName = x.First().PartName,
        //             Unit = x.First().Unit,
        //             QuantityOrdered = x.Sum(y => y.QuantityOrdered),
        //             IssueQuantity = x.Sum(y => y.IssueQuantity),
        //             MFGDate = x.First().MFGDate,
        //             EXPDate = x.First().EXPDate,

        //         })
        //        .ToList();
        //    return querys;
        //}

        //private async Task<List<IODetail>> fetchIODetailUnderIOHeader2()
        //{
        //    List<IODetail> iODetails = new List<IODetail>();
        //    List<IOHeader> iOHeaders = await fetchIOHeaderUnderPeriodDate();
        //    foreach (IOHeader entry in iOHeaders)
        //    {
        //        String ioNumber = entry.IONumber;
        //        List<IODetail> iODetails1 = new List<IODetail>();
        //        iODetails1 = await iODetailRepository.GetUnderID(ioNumber);
        //        foreach (IODetail iODetail in iODetails1)
        //        {
        //            iODetails.Add(iODetail);
        //        }
        //    }

        //    var querys = iODetails.GroupBy(x => new { x.PartNumber, x.PalletID })
        //         .Select(x => new IODetail
        //         {
        //             PartNumber = x.Key.PartNumber,
        //             PalletID = x.Key.PalletID,
        //             IDCode = string.Join("", x.Select(ss => ss.IDCode)),
        //             PartName = x.First().PartName,
        //             Unit = x.First().Unit,
        //             QuantityOrdered = x.Sum(y => y.QuantityOrdered),
        //             IssueQuantity = x.Sum(y => y.IssueQuantity),
        //             MFGDate = x.First().MFGDate,
        //             EXPDate = x.First().EXPDate,

        //         })
        //        .ToList();
        //    return querys;
        //}

        //private async Task<List<Inventory>> processData()
        //{
        //    List<WRDetail> wRDetails = await fetchWRDetailUnderWRHeader();
        //    List<IODetail> iODetails = await fetchIODetailUnderIOHeader();
        //    List<Inventory> inventories = new List<Inventory>();

        //    List<Inventory> inventories1 = new List<Inventory>();

        //    foreach (WRDetail wRDetail in wRDetails)
        //    {
        //        inventories1 = await inventoryRepository.GetByPartNumber(comboBoxEditWarehouse.SelectedIndex.ToString(), wRDetail.PartNumber);
        //        Inventory inventory = new Inventory();
        //        inventory.Month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
        //        inventory.Year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
        //        inventory.WarehouseID = comboBoxEditWarehouse.SelectedIndex.ToString();
        //        inventory.PartNumber = wRDetail.PartNumber;
        //        inventory.OpeningStockQuantity = inventories1.Count > 0 ? inventories1[0].OpeningStockQuantity : 0;
        //        inventory.ReceiptQuantity = wRDetail.ReceiptQuantity;
        //        inventory.IssueQuantity = 0;
        //        inventory.ClosingStockQuantity = inventory.ReceiptQuantity - inventory.IssueQuantity;
        //        inventory.Status = "1";
        //        inventory.PackingVolume = 50;
        //        inventory.IDCode = wRDetail.IDCode;
        //        inventories.Add(inventory);
        //    }

        //    foreach (IODetail iODetail in iODetails)
        //    {
        //        inventories1 = await inventoryRepository.GetByPartNumber(comboBoxEditWarehouse.SelectedIndex.ToString(), iODetail.PartNumber);
        //        Inventory inventory = new Inventory();
        //        inventory.Month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
        //        inventory.Year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
        //        inventory.WarehouseID = comboBoxEditWarehouse.SelectedIndex.ToString();
        //        inventory.PartNumber = iODetail.PartNumber;
        //        inventory.OpeningStockQuantity = inventories1.Count > 0 ? inventories1[0].OpeningStockQuantity : 0;
        //        inventory.ReceiptQuantity = 0;
        //        inventory.IssueQuantity = iODetail.IssueQuantity;
        //        inventory.ClosingStockQuantity = inventory.ReceiptQuantity - inventory.IssueQuantity;
        //        inventory.Status = "1";
        //        inventory.PackingVolume = 50;
        //        inventory.IDCode = iODetail.IDCode;
        //        inventories.Add(inventory);
        //    }

        //    var querys = inventories.GroupBy(x => x.PartNumber)
        //         .Select(x => new Inventory
        //         {
        //             PartNumber = x.Key,
        //             Year = x.First().Year,
        //             Month = x.First().Month,
        //             WarehouseID = x.First().WarehouseID,
        //             OpeningStockQuantity = x.Sum(y => y.OpeningStockQuantity),
        //             Status = x.First().Status,
        //             ReceiptQuantity = x.Sum(y => y.ReceiptQuantity),
        //             IssueQuantity = x.Sum(y => y.IssueQuantity),
        //             ClosingStockQuantity = x.Sum(y => y.ClosingStockQuantity),
        //             PackingVolume = 50,
        //             Material = string.Join(",", x.Select(ss => ss.Material)),
        //             IDCode = string.Join("", x.Select(ss => ss.IDCode)),
        //         })
        //        .ToList();
        //    return querys;
        //}

        //private async Task<List<InventoryDetail>> processDataDetail()
        //{
        //    List<WRDetail> wRDetails = await fetchWRDetailUnderWRHeader2();
        //    List<IODetail> iODetails = await fetchIODetailUnderIOHeader2();
        //    List<InventoryDetail> inventories = new List<InventoryDetail>();

        //    List<InventoryDetail> inventories1 = new List<InventoryDetail>();

        //    foreach (WRDetail wRDetail in wRDetails)
        //    {
        //        inventories1 = await inventoryDetailRepository.GetByParams5(Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2)),
        //            Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4)),
        //            comboBoxEditWarehouse.SelectedIndex.ToString(),
        //            wRDetail.PartNumber, wRDetail.PalletID);
        //        InventoryDetail inventory = new InventoryDetail();
        //        inventory.Month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
        //        inventory.Year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
        //        inventory.WarehouseID = comboBoxEditWarehouse.SelectedIndex.ToString();
        //        inventory.PartNumber = wRDetail.PartNumber;
        //        inventory.PalletID = wRDetail.PalletID;
        //        inventory.IDCode = wRDetail.IDCode;
        //        inventory.Quantity = inventories1.Count > 0 ? inventories1[0].Quantity : 0;
        //        inventory.Status = "1";
        //        inventories.Add(inventory);
        //    }

        //    foreach (IODetail iODetail in iODetails)
        //    {
        //        inventories1 = await inventoryDetailRepository.GetByParams5(Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2)),
        //            Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4)),
        //            comboBoxEditWarehouse.SelectedIndex.ToString(),
        //            iODetail.PartNumber, iODetail.PalletID);
        //        InventoryDetail inventory = new InventoryDetail();
        //        inventory.Month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
        //        inventory.Year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
        //        inventory.WarehouseID = comboBoxEditWarehouse.SelectedIndex.ToString();
        //        inventory.PartNumber = iODetail.PartNumber;
        //        inventory.PalletID = iODetail.PalletID;
        //        inventory.IDCode = iODetail.IDCode;
        //        inventory.Quantity = inventories1.Count > 0 ? inventories1[0].Quantity : 0;
        //        inventory.Status = "1";
        //        inventories.Add(inventory);
        //    }

        //    var querys = inventories.GroupBy(x => new { x.PartNumber, x.PalletID })
        //         .Select(x => new InventoryDetail
        //         {
        //             PartNumber = x.Key.PartNumber,
        //             PalletID = x.Key.PalletID,
        //             Year = x.First().Year,
        //             Month = x.First().Month,
        //             WarehouseID = x.First().WarehouseID,
        //             Status = x.First().Status,
        //             Quantity = x.Sum(y => y.Quantity),
        //             IDCode = string.Join("", x.Select(ss => ss.IDCode)),
        //         })
        //        .ToList();
        //    return querys;
        //}

        //private async Task<List<InventoryOfLastMonths>> processDataLastMonth()
        //{
        //    List<WRDetail> wRDetails = await fetchWRDetailUnderWRHeader();
        //    List<IODetail> iODetails = await fetchIODetailUnderIOHeader();
        //    List<InventoryOfLastMonths> inventoryOfLastMonths = new List<InventoryOfLastMonths>();
        //    List<InventoryOfLastMonths> inventoryLastMonths1 = new List<InventoryOfLastMonths>();

        //    foreach (WRDetail wRDetail in wRDetails)
        //    {
        //        inventoryLastMonths1 = await inventoryOfLastMonthsRepository.GetByPartNumber(comboBoxEditWarehouse.SelectedIndex.ToString(), wRDetail.PartNumber);
        //        InventoryOfLastMonths inventoryOfLastMonth = new InventoryOfLastMonths();
        //        inventoryOfLastMonth.Month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
        //        inventoryOfLastMonth.Year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
        //        inventoryOfLastMonth.WarehouseID = comboBoxEditWarehouse.SelectedIndex.ToString();
        //        inventoryOfLastMonth.PartNumber = wRDetail.PartNumber;
        //        inventoryOfLastMonth.OpeningStockQuantity = inventoryLastMonths1.Count > 0 ? inventoryLastMonths1[0].OpeningStockQuantity : 0;
        //        inventoryOfLastMonth.ReceiptQuantity = wRDetail.ReceiptQuantity;
        //        inventoryOfLastMonth.IssueQuantity = 0;
        //        inventoryOfLastMonth.ClosingStockQuantity = inventoryOfLastMonth.ReceiptQuantity - inventoryOfLastMonth.IssueQuantity;
        //        inventoryOfLastMonth.Status = "1";
        //        inventoryOfLastMonth.PackingVolume = 50;
        //        inventoryOfLastMonths.Add(inventoryOfLastMonth);
        //    }

        //    foreach (IODetail iODetail in iODetails)
        //    {
        //        inventoryLastMonths1 = await inventoryOfLastMonthsRepository.GetByPartNumber(comboBoxEditWarehouse.SelectedIndex.ToString(), iODetail.PartNumber);
        //        InventoryOfLastMonths inventoryOfLastMonth = new InventoryOfLastMonths();
        //        inventoryOfLastMonth.Month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
        //        inventoryOfLastMonth.Year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
        //        inventoryOfLastMonth.WarehouseID = comboBoxEditWarehouse.SelectedIndex.ToString();
        //        inventoryOfLastMonth.PartNumber = iODetail.PartNumber;
        //        inventoryOfLastMonth.OpeningStockQuantity = inventoryLastMonths1.Count > 0 ? inventoryLastMonths1[0].OpeningStockQuantity : 0;
        //        inventoryOfLastMonth.ReceiptQuantity = 0;
        //        inventoryOfLastMonth.IssueQuantity = iODetail.IssueQuantity;
        //        inventoryOfLastMonth.ClosingStockQuantity = inventoryOfLastMonth.ReceiptQuantity - inventoryOfLastMonth.IssueQuantity;
        //        inventoryOfLastMonth.Status = "1";
        //        inventoryOfLastMonth.PackingVolume = 50;
        //        inventoryOfLastMonths.Add(inventoryOfLastMonth);
        //    }


        //    var querys = inventoryOfLastMonths.GroupBy(x => x.PartNumber)
        //         .Select(x => new InventoryOfLastMonths
        //         {
        //             PartNumber = x.Key,
        //             Year = x.First().Year,
        //             Month = x.First().Month,
        //             WarehouseID = x.First().WarehouseID,
        //             OpeningStockQuantity = x.Sum(y => y.OpeningStockQuantity),
        //             Status = x.First().Status,
        //             ReceiptQuantity = x.Sum(y => y.ReceiptQuantity),
        //             IssueQuantity = x.Sum(y => y.IssueQuantity),
        //             ClosingStockQuantity = x.Sum(y => y.ClosingStockQuantity),
        //             PackingVolume = 50,
        //         })
        //        .ToList();
        //    return querys;
        //}

        private async Task sbLoadDataForGridAsync(Int16 year, Int16 month, String warehouseID)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            if ((year == mYear && month == mMonth))
            {
                List<Inventory> inventories = await inventoryRepository.GetAll();
                int mMaxRow = 100;
                if (inventories.Count < mMaxRow)
                {
                    int num = mMaxRow - inventories.Count;
                    int i = 1;
                    while (true)
                    {
                        int num2 = i;
                        int num3 = num;
                        if (num2 > num3)
                        {
                            break;
                        }
                        inventories.Add(new Inventory());
                        i++;
                    }
                }
                gridControlInventory.DataSource = inventories;
            }
            else
            {
                List<InventoryOfLastMonths> inventories = await inventoryOfLastMonthsRepository.GetAll();
                int mMaxRow = 100;
                if (inventories.Count < mMaxRow)
                {
                    int num = mMaxRow - inventories.Count;
                    int i = 1;
                    while (true)
                    {
                        int num2 = i;
                        int num3 = num;
                        if (num2 > num3)
                        {
                            break;
                        }
                        inventories.Add(new InventoryOfLastMonths());
                        i++;
                    }
                }
                gridControlInventory.DataSource = inventories;
            }

            getTotalOpenStockQuantity();
            getTotalOpenStockCarton();
            //getTotalOpenStockAmount();
            getTotalCloseStockQuantity();
            getTotalCloseStockCarton();
            //();
            //getTotalCloseStockAmount();
            getTotalReceiptQuantity();
            getTotalReceiptCarton();
            //getTotalReceiptAmount();
            getTotalIssueQuantity();
            getTotalIssueCarton();
            //getTotalIssueAmount();
        }

        //private async Task<List<InventoryOfLastMonths>> getInventoryOfLastMonths(String monthYear)
        //{
        //    int mYear = int.Parse(monthYear.Substring(3, 4));
        //    int mMonth = int.Parse(monthYear.Substring(0, 2));
        //    Int16 mLastMonth;
        //    Int16 mLastYear;
        //    if (mMonth == 1)
        //    {
        //        mLastMonth = 12;
        //        mLastYear = (short)(mYear - 1);
        //    }
        //    else
        //    {
        //        mLastMonth = (short)(mMonth - 1);
        //        mLastYear = (short)mYear;
        //    }
        //    if (mMonth == 1)
        //    {
        //        mLastMonth = 12;
        //        mLastYear = (short)(mYear - 1);
        //    }
        //    else
        //    {
        //        mLastMonth = (short)(mMonth - 1);
        //        mLastYear = (short)mYear;
        //    }
        //    string mLastMonthYear = $"{mLastMonth:00}" + "/" + String.Format(mLastYear.ToString(), "####");
        //    List<InventoryOfLastMonths> inventoryOfLastMonths = await inventoryOfLastMonthsRepository.GetUnderMultiID(
        //       mLastYear, mLastMonth, $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}");
        //    return inventoryOfLastMonths;
        //}

        //public async void createInventory(String monthYear)
        //{
        //    List<Inventory> inventories = await processData();
        //    foreach (Inventory inventory in inventories)
        //    {
        //        Inventory inventorys = new Inventory();
        //        inventorys.Year = inventory.Year;
        //        inventorys.Month = inventory.Month;
        //        inventorys.WarehouseID = inventory.WarehouseID;
        //        inventorys.PartNumber = inventory.PartNumber;
        //        inventorys.OpeningStockQuantity = inventory.OpeningStockQuantity;
        //        inventorys.ReceiptQuantity = inventory.ReceiptQuantity;
        //        inventorys.IssueQuantity = inventory.IssueQuantity;
        //        inventorys.ClosingStockQuantity = inventory.ClosingStockQuantity;
        //        inventorys.IDCode = string.Join(",", inventory.IDCode.Trim().Replace(" ", "").Split(',').Distinct().ToArray());
        //        inventorys.Status = "1";

        //        List<Inventory> inventories1 = await inventoryRepository.GetUnderMultiPartNumber(inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber);

        //        if (inventories1.Count > 0)
        //        {
        //            await inventoryRepository.Update(inventorys, inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber);
        //            textEditMessage.Text = "Đã cập nhật tồn kho tháng " + mLastPeriod + " Bạn có thể xử lý tồn kho tháng " + mCurrentPeriod;
        //        }
        //        else
        //        {
        //            await inventoryRepository.Create(inventorys);
        //            textEditMessage.Text = "Đã xử lý tồn kho tháng " + mLastPeriod + " Bạn có thể xử lý tồn kho tháng " + mCurrentPeriod;
        //        }

        //    }
        //}

        //public async void createInventoryDetail(String monthYear)
        //{
        //    List<InventoryDetail> inventories = await processDataDetail();

        //    foreach (InventoryDetail inventory in inventories)
        //    {
        //        InventoryDetail inventorys = new InventoryDetail();
        //        inventorys.Year = inventory.Year;
        //        inventorys.Month = inventory.Month;
        //        inventorys.WarehouseID = inventory.WarehouseID;
        //        inventorys.PartNumber = inventory.PartNumber;
        //        inventorys.PalletID = inventory.PalletID;
        //        inventorys.IDCode = string.Join(",", inventory.IDCode.Trim().Replace(" ", "").Split(',').Distinct().ToArray());
        //        inventorys.Quantity = inventory.Quantity;
        //        inventorys.LotID = "KC";
        //        inventorys.Status = "1";

        //        List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams5(inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber, inventorys.PalletID);

        //        if (inventoryDetails.Count > 0)
        //        {
        //            await inventoryDetailRepository.Update(inventorys, inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber, inventorys.PalletID);
        //            textEditMessage.Text = "Đã cập nhật tồn kho tháng " + mLastPeriod + " Bạn có thể xử lý tồn kho tháng " + mCurrentPeriod;
        //        }
        //        else
        //        {
        //            await inventoryDetailRepository.Create(inventorys);
        //            textEditMessage.Text = "Đã xử lý tồn kho tháng " + mLastPeriod + " Bạn có thể xử lý tồn kho tháng " + mCurrentPeriod;
        //        }

        //    }
        //}

        //public async void createInventoryOfLastMonth(String monthYear)
        //{
        //    List<InventoryOfLastMonths> inventoryOfLastMonths = await processDataLastMonth();
        //    foreach (InventoryOfLastMonths inventory in inventoryOfLastMonths)
        //    {
        //        InventoryOfLastMonths inventorys = new InventoryOfLastMonths();
        //        inventorys.Year = inventory.Year;
        //        inventorys.Month = inventory.Month;
        //        inventorys.WarehouseID = inventory.WarehouseID;
        //        inventorys.PartNumber = inventory.PartNumber;
        //        inventorys.OpeningStockQuantity = inventory.OpeningStockQuantity;
        //        inventorys.ReceiptQuantity = inventory.ReceiptQuantity;
        //        inventorys.IssueQuantity = inventory.IssueQuantity;
        //        inventorys.ClosingStockQuantity = inventory.ClosingStockQuantity;
        //        inventorys.Status = inventory.Status;

        //        if (await inventoryOfLastMonthsRepository.GetUnderMultiPartNumber(inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber) != null)
        //        {
        //            await inventoryOfLastMonthsRepository.Update(inventorys, inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber);
        //            textEditMessage.Text = "Đã cập nhật tồn kho tháng " + mLastPeriod + " Bạn có thể xử lý tồn kho tháng " + mCurrentPeriod;
        //        }
        //        else
        //        {
        //            await inventoryOfLastMonthsRepository.Create(inventorys);
        //            textEditMessage.Text = "Đã xử lý tồn kho tháng " + mLastPeriod + " Bạn có thể xử lý tồn kho tháng " + mCurrentPeriod;
        //        }

        //    }
        //}

        private void unlockVoucher()
        {

        }


        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "unlock":
                    String monthYear = comboBoxEditProcessingPeriod.SelectedItem.ToString();
                    //createInventory(monthYear);
                    //createInventoryDetail(monthYear);

                    //List<Inventory> inventories = await inventoryRepository.GetAll();
                    //if (inventories.Count > 0)
                    //{
                    //    if (DateTime.Now > GetDateTime(inventories[0].Year, inventories[0].Month))
                    //    {
                    //        createInventoryOfLastMonth(monthYear);
                    //        MessageBox.Show("xử lý tồn kho tháng " + DateTime.Now.Month);

                    //    }
                    //    else
                    //    {
                    //        MessageBox.Show("xử lý tồn kho tháng " + inventories[0].Month);

                    //    }
                    //}
                    //else
                    //{

                    //}


                    break;
                case "process":
                    inventoryProcess();
                    break;
                case "refesh":
                    Int16 month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
                    Int16 year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
                    String warehouseID = comboBoxEditWarehouse.SelectedIndex.ToString();


                    if (comboBoxEditProcessingPeriod.SelectedIndex == 0)
                    {
                        loadGridLast();
                    }
                    else
                    {
                        loadGridCurrent();
                    }

                    //sbLoadDataForGridAsync(year, month, warehouseID);
                    break;
                case "print":
                    report();
                    //ShowGridPreview(gridControlInventory);
                    //PrintGrid(gridControlInventory);
                    break;
                case "close":

                    break;

            }

        }

        private async void report()
        {
            ReportPrintTool reportPrintTool = new ReportPrintTool(new InventoryReportControl());
            reportPrintTool.ShowRibbonPreviewDialog();
            reportPrintTool.PreviewRibbonForm.Text = "Báo cáo tồn kho";
            //ReportDesignTool designTool = new ReportDesignTool(new InventoryReportControl());
            //designTool.ShowRibbonDesignerDialog();
        }

        async Task<XtraReport> CreateReport()
        {
            // Create a dataset.           
            //DataSet ds = FillDataset();
            // Define a report
            XtraReport report = new XtraReport()
            {
                DataSource = await inventoryRepository.GetAll(),

                Bands = {
                new DetailBand()
                {
                    Controls = {
                        CreateLabel()
                        }
                }
            }
            };


            return report;
        }

        XRLabel CreateLabel()
        {
            ExpressionBinding eb = new ExpressionBinding("BeforePrint", "Text", "[PartNumber] + ' | ' + [ClosingStockQuantity]");
            XRLabel lb = new XRLabel { Left = 30, WidthF = 300 };
            lb.ExpressionBindings.Add(eb);
            return lb;
        }


        public DataSet FillDataset()
        {
            DataSet dataSet1 = new DataSet();
            dataSet1.DataSetName = "nwindDataSet1";
            DataTable dataTable1 = new DataTable();




            dataSet1.Tables.Add(dataTable1);

            dataTable1.TableName = "Products";
            dataTable1.Columns.Add("ProductId", typeof(int));
            dataTable1.Columns.Add("ProductName", typeof(string));
            dataTable1.Columns.Add("SupplierId", typeof(int));
            dataTable1.Columns.Add("Category", typeof(int));


            dataSet1.Tables["Products"].Rows.Add(new Object[] { 1, "Chai", 1, 1 });
            dataSet1.Tables["Products"].Rows.Add(new Object[] { 2, "Chang", 1, 1 });
            dataSet1.Tables["Products"].Rows.Add(new Object[] { 3, "Aniseed Syrup", 1, 2 });
            dataSet1.Tables["Products"].Rows.Add(new Object[] { 4, "Chef Anton's Cajun Seasoning", 2, 2 });
            dataSet1.Tables["Products"].Rows.Add(new Object[] { 5, "Chef Anton's Gumbo Mix", 2, 2 });
            return dataSet1;

        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //e.Graphics.DrawImage(bitmap, 0, 0);
        }

        private void ShowGridPreview(GridControl grid)
        {
            // Check whether the GridControl can be previewed.
            if (!grid.IsPrintingAvailable)
            {
                MessageBox.Show("The 'DevExpress.XtraPrinting' library is not found", "Error");
                return;
            }

            // Open the Preview window.
            grid.ShowPrintPreview();
        }

        private void PrintGrid(GridControl grid)
        {
            // Check whether the GridControl can be printed.
            if (!grid.IsPrintingAvailable)
            {
                MessageBox.Show("The 'DevExpress.XtraPrinting' library is not found", "Error");
                return;
            }

            // Print.
            grid.Print();
        }


        private async void inventoryProcess()
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));


            await inventoryOfLastMonthsRepository.DeleteOldDate("1");

            //update inventory and create inventory of last month
            List<Inventory> inventories = await inventoryRepository.GetAll();
            if (inventories.Count > 0)
            {
                foreach (Inventory inventory in inventories)
                {
                    short oldYear = inventory.Year;
                    short oldMonth = inventory.Month;
                    inventory.Year = mYear;
                    inventory.Month = mMonth;
                    if ((oldYear == mYear && oldMonth != mMonth) || (oldYear != mYear && oldMonth == mMonth) || (oldYear != mYear && oldMonth != mMonth))
                    {
                        await inventoryRepository.Create(inventory);
                        await inventoryRepository.Delete(oldYear, oldMonth, "1", inventory.PartNumber);
                    }

                    if ((await inventoryOfLastMonthsRepository.GetUnderMultiPartNumber(oldYear, oldMonth, "1", inventory.PartNumber)).Count == 0)
                    {
                        InventoryOfLastMonths inventorys = new InventoryOfLastMonths();
                        inventorys.Year = oldYear;
                        inventorys.Month = oldMonth;
                        inventorys.WarehouseID = inventory.WarehouseID;
                        inventorys.PartNumber = inventory.PartNumber;
                        inventorys.Status = "0";
                        inventorys.PackingVolume = inventory.PackingVolume;
                        inventorys.Material = inventory.Material;
                        inventorys.Mold = inventory.Mold;
                        inventorys.OpeningStockQuantity = inventory.OpeningStockQuantity;
                        inventorys.ReceiptQuantity = inventory.ReceiptQuantity;
                        inventorys.IssueQuantity = inventory.IssueQuantity;
                        inventorys.ClosingStockQuantity = inventory.ClosingStockQuantity;
                        await inventoryOfLastMonthsRepository.Create(inventorys);
                    }

                }

            }

            await inventoryOfLastMonthDetailRepository.DeleteOldDate("1");

            //update inventoryDetail and create inventory of last month detail
            List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetAll();
            if (inventoryDetails.Count > 0)
            {
                foreach (InventoryDetail inventoryDetail in inventoryDetails)
                {
                    short oldYear = inventoryDetail.Year;
                    short oldMonth = inventoryDetail.Month;
                    inventoryDetail.Year = mYear;
                    inventoryDetail.Month = mMonth;
                    if ((oldYear == mYear && oldMonth != mMonth) || (oldYear != mYear && oldMonth == mMonth) || (oldYear != mYear && oldMonth != mMonth))
                    {
                        await inventoryDetailRepository.Create(inventoryDetail);
                        await inventoryDetailRepository.Delete5(oldYear, oldMonth, "1", inventoryDetail.PartNumber, inventoryDetail.PalletID);
                    }

                    if ((await inventoryOfLastMonthDetailRepository.GetByParams6(oldYear, oldMonth, "1", inventoryDetail.PartNumber, inventoryDetail.PalletID, inventoryDetail.IDCode)).Count == 0)
                    {
                        InventoryOfLastMonthDetail inventory = new InventoryOfLastMonthDetail();
                        inventory.Month = oldMonth;
                        inventory.Year = oldYear;
                        inventory.WarehouseID = "1";
                        inventory.PartNumber = inventoryDetail.PartNumber;
                        inventory.PalletID = inventoryDetail.PalletID;
                        inventory.LotID = inventoryDetail.LotID;
                        inventory.IDCode = inventoryDetail.IDCode;
                        inventory.Quantity = inventoryDetail.Quantity;
                        inventory.Status = "1";
                        inventory.MFGDate = inventoryDetail.MFGDate;
                        inventory.EXPDate = inventoryDetail.EXPDate;
                        inventory.InputDate = inventoryDetail.InputDate;
                        await inventoryOfLastMonthDetailRepository.Create(inventory);
                    }

                }

            }

            BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Đã xử lý tồn kho tháng " + mMonth + " ,năm " + mYear);

        }




        private void gridViewInventory_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column ==colNo)
            {
                int rowHandle = gridViewInventory.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void comboBoxEditBranch_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBoxEditProcessingPeriod.SelectedIndex == 0)
            {
                loadGridLast();
            }
            else
            {
                loadGridCurrent();
            }
        }

        private void comboBoxEditWarehouse_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBoxEditProcessingPeriod.SelectedIndex == 0)
            {
                loadGridLast();
            }
            else
            {
                loadGridCurrent();
            }
        }

        private void comboBoxEditProcessingPeriod_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBoxEditProcessingPeriod.SelectedIndex == 0)
            {
                loadGridLast();
            }
            else
            {
                loadGridCurrent();
            }
        }
    }
}
