﻿using DevExpress.XtraBars.Docking2010;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace WarehouseManager
{
    public partial class TallySheetControl : DevExpress.XtraEditors.XtraUserControl
    {
        WarehouseRepository warehouseRepository;
        HandlingStatusRepository handlingStatusRepository;
        TSHeaderRepository tSHeaderRepository;
        TSDetailRepository tSDetailRepository;
        GoodsRepository goodsRepository = null;
        InventoryRepository inventoryRepository = null;
        InventoryDetailRepository inventoryDetailRepository = null;
        String lastID = "";
        public static String selectedID="";
        bool isTSNumberExist = false;
        public TallySheetControl()
        {
            InitializeComponent();
            this.Load += TallySheetControl_Load;
            warehouseRepository = new WarehouseRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            tSHeaderRepository = new TSHeaderRepository();
            tSDetailRepository = new TSDetailRepository();
            goodsRepository = new GoodsRepository();
            inventoryRepository = new InventoryRepository();
            inventoryDetailRepository = new InventoryDetailRepository();
        }

        private void TallySheetControl_Load(Object sender,EventArgs e)
        {
            popupMenuSelectWarehouse();
            popupMenuSelectHandlingStatus();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditTSNumber.DoubleClick += textEdit_CellDoubleClick;
            autoCompletePartNumberAsync();
        }


        private async void popupMenuSelectWarehouse()
        {
            List<Warehouse> warehouses = await warehouseRepository.getAll();
            foreach (Warehouse warehouse in warehouses)
            {
                comboBoxEditWarehouse.Properties.Items.Add(warehouse.WarehouseName);
            }
            comboBoxEditWarehouse.SelectedIndex = 0;
        }

        private async void popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private async void sbLoadDataForGridDataAsync(String tsNumber)
        {
            List<TSDetail> tSDetails = await tSDetailRepository.GetUnderID(tsNumber);
            int mMaxRow = 100;
            if (tSDetails.Count < mMaxRow)
            {
                int num = mMaxRow - tSDetails.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    tSDetails.Add(new TSDetail());
                    i++;
                }
            }
            gridControlData.DataSource = tSDetails;
            getTotalQuantity();
            getTotalQuantityByPack();
        }

        public async void getlastIDAsync()
        {
            lastID= await tSHeaderRepository.GetLastID();
            string date = DateTime.Now.Year.ToString("ddMMyy");
            if (lastID == "")
            {
                textEditTSNumber.Text ="KK"+date+ "-" + $"{1:D5}";
            }
            else
            {
                string[] arrListStr = lastID.Split('-');
                String partOne = arrListStr[0];
                String partTwo = arrListStr[1];
                partTwo = (int.Parse(partTwo) + 1).ToString();
                textEditTSNumber.Text ="KK"+date + "-" + $"{int.Parse(partTwo):D5}";
            }
        }

        private void getTotalQuantity()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewData.GetRowHandle(i);
                if (gridViewData.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewData.GetRowCellValue(rowHandle, "Quantity");
                total += quantity;
                i++;
            }
            textEditTotalQuantity.Text = total.ToString("0.#####");
        }

        private void getTotalQuantityByPack()
        {
            Decimal totalQuantityByPack = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewData.GetRowHandle(i);
                if (gridViewData.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantityByPack = (Decimal)gridViewData.GetRowCellValue(rowHandle, "QuantityByPack");
                totalQuantityByPack += quantityByPack;
                i++;
            }
            textEditTotalQuantityByPack.Text = totalQuantityByPack.ToString();
        }

        public async void createTSHeaderAsync()
        {
            TSHeader tSHeader = new TSHeader();
            tSHeader.TSNumber = textEditTSNumber.Text;
            tSHeader.TSDate = DateTime.Parse(dateEditTallyDate.DateTime.ToString("yyyy-MM-dd"));
           
            tSHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            tSHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            tSHeader.WarehouseID = $"{int.Parse(comboBoxEditWarehouse.ToString()) - 1:D3}";
            tSHeader.WarehouseName = comboBoxEditWarehouse.SelectedItem.ToString();
            tSHeader.BranchID = WMMessage.User.BranchID;
            tSHeader.BranchName = WMMessage.User.BranchName;
            tSHeader.Note = textEditNote.Text;
            tSHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            tSHeader.TotalQuantityByPack= Decimal.Parse(textEditTotalQuantityByPack.Text);
            tSHeader.CreatedUserID = WMMessage.User.UserID;
            tSHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await tSHeaderRepository.Create(tSHeader) > 0)
            {
                await createTSDetailAsync();
                WMPublic.sbMessageSaveNewSuccess(this);
            }
        }

        public async Task createTSDetailAsync()
        {
            int num = gridViewData.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 ||gridViewData.GetRowCellValue(i,gridViewData.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewData.GetRowHandle(i);
                TSDetail tSDetail = new TSDetail();
                tSDetail.TSNumber=textEditTSNumber.Text;
                tSDetail.PartNumber = (string?)gridViewData.GetRowCellValue(rowHandle, "PartNumber");
                tSDetail.PartName = (string?)gridViewData.GetRowCellValue(rowHandle, "PartName");
                tSDetail.Ordinal = i + 1;
                tSDetail.TallyUnitID = (string?)gridViewData.GetRowCellValue(rowHandle, "TallyUnitID");
                tSDetail.UnitRate = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "UnitRate");
                tSDetail.StockUnitID = (string?)gridViewData.GetRowCellValue(rowHandle, "StockUnitID");
                tSDetail.PackingVolume = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "PackingVolume");
                tSDetail.Quantity = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "Quantity");
                tSDetail.QuantityByPack = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "QuantityByPack");
                tSDetail.Note = textEditNote.Text;

                if (await tSDetailRepository.Create(tSDetail) > 0)
                {
                    i++;
                }
            }
        }

        public async void updateTSHeaderAsync()
        {
            TSHeader tSHeader = new TSHeader();
            tSHeader.TSNumber = textEditTSNumber.Text;
            tSHeader.TSDate = DateTime.Parse(dateEditTallyDate.DateTime.ToString("yyyy-MM-dd"));
            tSHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            tSHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            tSHeader.WarehouseID = $"{int.Parse(comboBoxEditWarehouse.ToString()) - 1:D3}";
            tSHeader.WarehouseName = comboBoxEditWarehouse.SelectedItem.ToString();
            tSHeader.BranchID = WMMessage.User.BranchID;
            tSHeader.BranchName = WMMessage.User.BranchName;
            tSHeader.Note = textEditNote.Text;
            tSHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            tSHeader.TotalQuantityByPack= Decimal.Parse(textEditTotalQuantityByPack.Text);
            tSHeader.CreatedUserID = WMMessage.User.UserID;
            tSHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await tSHeaderRepository.Update(tSHeader,textEditTSNumber.Text) > 0)
            {
                updateTSDetailAsync();
                WMPublic.sbMessageSaveChangeSuccess(this);
            }
        }

        public async Task updateTSDetailAsync()
        {
            int num = gridViewData.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewData.GetRowCellValue(i, gridViewData.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewData.GetRowHandle(i);
                TSDetail tSDetail = new TSDetail();
                tSDetail.TSNumber = textEditTSNumber.Text;
                tSDetail.PartNumber= (string?)gridViewData.GetRowCellValue(rowHandle, "PartNumber");
                tSDetail.PartName = (string?)gridViewData.GetRowCellValue(rowHandle, "PartName");
                tSDetail.Ordinal = i + 1;
                tSDetail.TallyUnitID = (string?)gridViewData.GetRowCellValue(rowHandle, "TallyUnitID");
                tSDetail.UnitRate = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "UnitRate");
                tSDetail.StockUnitID = (string?)gridViewData.GetRowCellValue(rowHandle, "StockUnitID");
                tSDetail.PackingVolume = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "PackingVolume");
                tSDetail.Quantity = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "Quantity");
                tSDetail.QuantityByPack = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "QuantityByPack");
                tSDetail.Note = textEditNote.Text;

                if (await tSDetailRepository.Update(tSDetail,tSDetail.TSNumber,tSDetail.PartNumber,tSDetail.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        public async void DeleteTSHeaderAsync()
        {
            if ((await tSHeaderRepository.Delete(textEditTSNumber.Text)) > 0)
            {
                await DeleteTSDetailAsync();
                sbLoadDataForGridDataAsync(textEditTSNumber.Text);
                WMPublic.sbMessageDeleteSuccess(this);
            }
        }

        public async Task DeleteTSDetailAsync()
        {
            await tSHeaderRepository.Delete(textEditTSNumber.Text);
            clearAsync();
            gridControlData.DataSource = null;
        }

        private void clearAsync()
        {
            textEditTSNumber.Text = "";
            dateEditTallyDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            textEditNote.Text = "";
            textEditTotalQuantity.Text = "0";
            List<TSDetail> tSDetails = new List<TSDetail>();

            for (int i = 0; i < 100; i++)
            {
                tSDetails.Add(new TSDetail());
            }
            gridControlData.DataSource = tSDetails;
        }


        private async void textEdit_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchTSH frmSearch = new frmSearchTSH();
            frmSearch.ShowDialog(this);
            frmSearch.Dispose();
            if ( selectedID!= null)
            {
                textEditTSNumber.Text = selectedID;
                TSHeader tSHeader = await tSHeaderRepository.GetUnderID(textEditTSNumber.Text);
                dateEditTallyDate.Text = tSHeader.TSDate.ToString();
                textEditNote.Text = tSHeader.Note;
                comboBoxEditWarehouse.SelectedIndex=int.Parse($"{int.Parse(tSHeader.WarehouseID)+1:D3}");
                comboBoxEditHandlingStatus.SelectedIndex = int.Parse(tSHeader.HandlingStatusID);
                comboBoxEditWarehouse.SelectedIndex = int.Parse(tSHeader.BranchID);
                textEditNote.Text = tSHeader.Note;
                sbLoadDataForGridDataAsync(textEditTSNumber.Text);
            }
        }

        private async void selectFileExcelAsync()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"d:\";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                List<TSDetail> tSDetails = await ExcelToListAsync(fdlg.FileName);
                int mMaxRow = 100;
                if (tSDetails.Count < mMaxRow)
                {
                    int num = mMaxRow - tSDetails.Count;
                    int i = 1;
                    while (true)
                    {
                        int num2 = i;
                        int num3 = num;
                        if (num2 > num3)
                        {
                            break;
                        }
                        tSDetails.Add(new TSDetail());
                        i++;
                    }
                }
                gridControlData.DataSource = tSDetails;
                getTotalQuantity();
                getTotalQuantityByPack();
                Application.DoEvents();
            }
        }


        private async Task<List<TSDetail>> ExcelToListAsync(String fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                dateEditTallyDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

                int totalRows = workSheet.Dimension.Rows;
                List<TSDetail> tSDetails = new List<TSDetail>();
                for (int i =4; i <= totalRows; i++)
                {

                    tSDetails.Add(new TSDetail
                    {
                        PartNumber = workSheet?.Cells[i, 3].Text.Trim(),
                    });

                }
                List<Goods> goodss = await goodsRepository.getAll();
                for (int i = 0; i < tSDetails.Count; i++)
                {
                    foreach (Goods goods in goodss)
                    {
                        if (goods.PartNumber.Equals(tSDetails[i].PartNumber))
                        {
                            tSDetails[i].PackingVolume = goods.Quantity;
                            tSDetails[i].PartName = goods.PartName;
                        }
                    }
                }
                return tSDetails;
            }
        }

       

        public void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":

                    break;
                case "save":
                    if (textEditTSNumber.Text.Equals(""))
                    {
                        textEditTSNumber.Focus();
                        textEditTSNumber.BackColor = System.Drawing.Color.Red;
                        return;                    
                    }
                    if (isTSNumberExist == true)
                    {
                        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                        {
                            updateTSHeaderAsync();
                        }
                    }
                    else
                    {
                        if (WMPublic.sbMessageSaveNewRequest(this) == true)
                        {

                            createTSHeaderAsync();
                        }
                    }
                    break;
                case "delete":
                    DeleteTSHeaderAsync();
                    break;
                case "search":

                    break;
                case "refesh":
                    sbLoadDataForGridDataAsync(textEditTSNumber.Text);
                    break;
                case "import":
                    selectFileExcelAsync();
                    break;
                case "export":

                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }

        private void simpleButtonSave_Click(object sender, EventArgs e)
        {

        }

        private void simpleButtonDelete_Click(object sender, EventArgs e)
        {

        }

        private void simpleButtonClear_Click(object sender, EventArgs e)
        {

        }

        private async Task checkExistTSNumber(string tsNumber)
        {
            if (!tsNumber.Equals("") && (await tSHeaderRepository.GetUnderID(tsNumber)) != null)
            {
                isTSNumberExist = true;
            }
        }

        private async void autoCompletePartNumberAsync()
        {
            AutoCompleteStringCollection data = new AutoCompleteStringCollection();
            List<Goods> goodss = await goodsRepository.getAll();
            foreach (Goods goods in goodss)
            {
                data.Add(goods.PartNumber);
            }
            textEditPartNumber.Text = goodss[0].PartNumber;
            textEditPartNumber.MaskBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            textEditPartNumber.MaskBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            textEditPartNumber.MaskBox.AutoCompleteCustomSource = data;
        }

        public async void createTSHeaderFromForm()
        {
            TSHeader TSHeader = new TSHeader();
            TSHeader.TSNumber = textEditTSNumber.Text;
            TSHeader.TSDate = DateTime.Parse(dateEditTallyDate.DateTime.ToString("yyyy-MM-dd"));
            TSHeader.WarehouseID = comboBoxEditWarehouse.SelectedIndex+"";
            TSHeader.WarehouseName = comboBoxEditWarehouse.SelectedText.ToString();
            TSHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex).ToString();
            TSHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            TSHeader.BranchID = (comboBoxEditBranch.SelectedIndex).ToString();
            TSHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            TSHeader.Note = "";
            TSHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            TSHeader.TotalQuantityByPack = Decimal.Parse(textEditTotalQuantityByPack.Text);
            TSHeader.CreatedUserID = WMMessage.User.UserID;
            TSHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            TSHeader.Status = "0";

            TSHeader TSHeader1 = await tSHeaderRepository.GetUnderID(textEditTSNumber.Text);
            if (TSHeader1 != null)
            {


                if (await tSHeaderRepository.Update(TSHeader, textEditTSNumber.Text) > 0)
                {
                    createTSDetailFromForm();

                }
            }
            else
            {
                if (await tSHeaderRepository.Create(TSHeader) > 0)
                {
                    createTSDetailFromForm();

                }
            }

            await checkExistTSNumber(textEditTSNumber.Text);
        }

        private async void createTSDetailFromForm()
        {
            if (textEditTSNumber.Text.Equals("") || dateEditTallyDate.Text.Equals(""))
            {
                MessageBox.Show("Nhập thiếu dữ liệu");
                return;
            }


            List<TSDetail> TSDetails = await tSDetailRepository.GetByTSNumberAndPartNumber(textEditTSNumber.Text, textEditPartNumber.Text);
            List<TSDetail> TSDetails1 = await tSDetailRepository.GetUnderID(textEditTSNumber.Text);

            if (TSDetails.Count > 0)
            {
                TSDetail TSDetail = TSDetails[0];
                TSDetail.TSNumber = textEditTSNumber.Text;
                TSDetail.PartNumber = textEditPartNumber.Text;


                TSDetail.TSNumber = textEditTSNumber.Text;
                TSDetail.Quantity =decimal.Parse( textEditQuantity.Text);

                if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                {
                    if (await tSDetailRepository.Update(TSDetail, textEditTSNumber.Text,textEditPartNumber.Text, TSDetails[0].Ordinal) > 0)
                    {

                        sbLoadDataForGridDataAsync(textEditTSNumber.Text);
                        WMPublic.sbMessageSaveChangeSuccess(this);
                    }
                }
            }
            else
            {

                TSDetail TSDetail = new TSDetail();
                TSDetail.Ordinal = TSDetails1.Count + 1;
                TSDetail.TSNumber = textEditTSNumber.Text;
                TSDetail.PartNumber = textEditPartNumber.Text;
       
                List<Goods> goodss = await goodsRepository.getAll();
                foreach (Goods goods in goodss)
                {
                    if (goods.PartNumber.Equals(textEditPartNumber.Text))
                    {
                        TSDetail.PackingVolume = goods.Quantity;
                        TSDetail.QuantityByPack = decimal.Parse(textEditQuantity.Text) / goods.Quantity;
                        TSDetail.PartName = goods.PartName;
                        break;
                    }
                }


                if (WMPublic.sbMessageSaveNewRequest(this) == true)
                {
                    if (await tSDetailRepository.Create(TSDetail) > 0)
                    {

                        sbLoadDataForGridDataAsync(textEditTSNumber.Text);
                        WMPublic.sbMessageSaveNewSuccess(this);
                    }
                }
            }

        }

        private async void textEditPartNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                List<Inventory> inventories =await inventoryRepository.GetByPartNumber("1", textEditPartNumber.Text);
                if (inventories.Count > 0)
                {
                    textEditQuantity.Text = inventories[0].ClosingStockQuantity + "";
                }
                else
                {
                    textEditQuantity.Text =0+"";
                }
            }
        }
    }
}
