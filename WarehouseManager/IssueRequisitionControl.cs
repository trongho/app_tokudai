﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraLayout.Filtering.Templates;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;
using LicenseContext = OfficeOpenXml.LicenseContext;
using DevExpress.XtraEditors.Repository;
using System.Globalization;
using DevExpress.XtraTab;
using System.Threading.Tasks;
using WarehouseManager.Helpers;

namespace WarehouseManager
{
    public partial class IssueRequisitionControl : DevExpress.XtraEditors.XtraUserControl
    {
        WIRDetailRepository wIRDetailRepository;
        WIRHeaderRepository wIRHeaderRepository;
        HandlingStatusRepository handlingStatusRepository;
        BranchRepository branchRepository;
        WIDataHeaderRepository wIDataHeaderRepository;
        WIDataDetailRepository wIDataDetailRepository;
        WIDataGeneralRepository wIDataGeneralRepository;
        GoodsRepository goodsRepository;
        InventoryRepository inventoryRepository = null;
        InventoryDetailRepository inventoryDetailRepository = null;
        public static string SelectedTable = string.Empty;
        public static String selectedWIRNumber;

        String lastWIRNumber = "";
        Boolean isWIRNumberExist = false;

        AutoCompleteStringCollection dataPalletID = new AutoCompleteStringCollection();
        AutoCompleteStringCollection dataLotID = new AutoCompleteStringCollection();

        internal MainForm m_MainForm;
        public IssueRequisitionControl(MainForm mainForm)
        {
            InitializeComponent();
            m_MainForm = mainForm;
            BottomBarHelper.normalMessage(m_MainForm.barStaticItemMessage,"!!!");
            this.Load += IssueRequisitionControl_Load;
            wIRDetailRepository = new WIRDetailRepository();
            wIRHeaderRepository = new WIRHeaderRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wIDataHeaderRepository = new WIDataHeaderRepository();
            wIDataDetailRepository = new WIDataDetailRepository();
            wIDataGeneralRepository = new WIDataGeneralRepository();
            goodsRepository = new GoodsRepository();
            inventoryRepository = new InventoryRepository();
            inventoryDetailRepository = new InventoryDetailRepository();
        }

        private async void IssueRequisitionControl_Load(Object sender, EventArgs e)
        {
            textEditWIRNumber.Focus();
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            popupMenuSelectHandlingStatus();
            popupMenuSelectBranch();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditWIRNumber.KeyDown += new KeyEventHandler(WIRNumber_KeyDown);
            gridViewWIRDetail.CustomColumnDisplayText += WIRHeaderGridView_CustomColumnDisplayText;
            gridViewWIRDetail.CellValueChanged += YourDGV_CellValueChanged;

            textEditWIRNumber.DoubleClick += textEditWIRNumer_CellDoubleClick;

            gridViewWIRDetail.ClearSorting();
            sbLoadDataForGridWIRDetailAsync(textEditWIRNumber.Text);
            textEditQuantity.KeyDown += textEditQuantity_KeyDown;

        }

        private async void popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> list = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in list)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private async void popupMenuSelectBranch()
        {
            List<Branch> list = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in list)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 1;
        }



        private async void loadDataGridView2()
        {
            List<Inventory> inventories = await inventoryRepository.GetAll();
            var query = inventories.Where(x => x.ClosingStockQuantity > 0).ToList();
            gridControl2.DataSource =query;
        }



        private async void gridView2_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            //if (gridView2.RowCount > 0)
            //{
            //    string partNumber = (string)(sender as GridView).GetFocusedRowCellValue("PartNumber");
            //    if (textEditWIRNumber.Text.Length > 0)
            //    {
            //        if (textEditWIRNumber.Text[0].ToString().Equals("-"))
            //        {
            //            textEditWIRNumber.Text = "-" + partNumber;
            //        }
            //        else if (textEditWIRNumber.Text[textEditWIRNumber.Text.Length - 1].ToString().Equals("-"))
            //        {
            //            textEditWIRNumber.Text = textEditWIRNumber.Text + partNumber;
            //        }
            //        else
            //        {
            //            textEditWIRNumber.Text = textEditWIRNumber.Text.Substring(0, textEditWIRNumber.Text.LastIndexOf("-") + 1) + partNumber;
            //        }
            //    }
            //    else
            //    {
            //        textEditWIRNumber.Text = "-" + partNumber;
            //    }

            //    //gridControl1.DataSource = null;
            //    textEditPalletID.Text = "";

            //    List<Inventory> inventorys = await inventoryRepository.GetByPartNumber("1", partNumber);
            //    if (inventorys.Count > 0)
            //    {
            //        if (inventorys[0].ClosingStockQuantity == 0)
            //        {
            //            BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Part Number: " + partNumber + " không còn tồn kho");
            //            return;
            //        }

            //        List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByPartNumber(partNumber);
            //        var querys = inventoryDetails.OrderBy(x => x.MFGDate).GroupBy(x => x.LotID)
            //           .Select(x => new InventoryDetail
            //           {
            //               LotID = x.Key,
            //               Quantity = x.Sum(y => y.Quantity),
            //               MFGDate = x.First().MFGDate,
            //               EXPDate = x.First().EXPDate,
            //               InputDate = x.First().InputDate,
            //               PalletID = x.First().PalletID,

            //           })
            //           .ToList();
            //        gridControl1.DataSource = querys;
            //    }
            //    else
            //    {
            //        BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Part Number: " + partNumber + " không có trong kho");
            //    }
            //    textEditPONumber.Focus();
            //    getTotalClosingStockQuantity();
            //}

            if (gridView2.RowCount > 0)
            {
                string partNumber = (string)(sender as GridView).GetFocusedRowCellValue("PartNumber");
                if (textEditWIRNumber.Text.Length > 0)
                {
                    if (textEditWIRNumber.Text[0].ToString().Equals("-"))
                    {
                        textEditWIRNumber.Text = "-" + partNumber;
                    }
                    else if (textEditWIRNumber.Text[textEditWIRNumber.Text.Length - 1].ToString().Equals("-"))
                    {
                        textEditWIRNumber.Text = textEditWIRNumber.Text + partNumber;
                    }
                    else
                    {
                        textEditWIRNumber.Text = textEditWIRNumber.Text.Substring(0, textEditWIRNumber.Text.LastIndexOf("-") + 1) + partNumber;
                    }
                }
                else
                {
                    textEditWIRNumber.Text = "-" + partNumber;
                }

                //gridControl1.DataSource = null;
                textEditPalletID.Text = "";

                List<Inventory> inventorys = await inventoryRepository.GetByPartNumber("1", partNumber);
                if (inventorys.Count > 0)
                {
                    if (inventorys[0].ClosingStockQuantity == 0)
                    {
                        MessageBox.Show("Part Number: " + partNumber + " không còn tồn kho","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                        return;
                    }

                    List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByPartNumber(partNumber);
                    var querys = inventoryDetails.OrderBy(x => x.MFGDate).GroupBy(x => x.PalletID)
                       .Select(x => new InventoryDetail
                       {
                           Quantity = x.Sum(y => y.Quantity),
                           PalletID = x.Key,

                       })
                       .ToList();
                    gridControl1.DataSource = querys;
                }
                else
                {
                    MessageBox.Show("Part Number: " + partNumber + " không có trong kho", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                textEditPONumber.Focus();
                getTotalClosingStockQuantity();
            }
        }

        private void gridViewWIRDetail_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (gridView2.RowCount > 0)
            {
                string partNumber = (string)(sender as GridView).GetFocusedRowCellValue("PartNumber");

                gridView2.SetFocusedRowCellValue("PartNumber", partNumber);

            }
        }


        private async void textEditQuantity_KeyDown(object sender, KeyEventArgs e)
        {
            //String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            //Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            //Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            //if (e.KeyCode == Keys.Enter)
            //{
            //    if (textEditQuantity.Text.Equals(""))
            //    {
            //        return;
            //    }

            //    List<Inventory> inventorys = await inventoryRepository.GetByPartNumber("1", (string)gridView2.GetFocusedRowCellValue("PartNumber"));
            //    if (inventorys[0].ClosingStockQuantity < decimal.Parse(textEditQuantity.Text))
            //    {
            //        BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Số lượng xuất vượt quá số lượng tồn kho");
            //        textEditQuantity.Text = "";
            //        return;
            //    }

            //    List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByPartNumber((string)gridView2.GetFocusedRowCellValue("PartNumber"));
            //    var querys = inventoryDetails.OrderBy(x => x.MFGDate).GroupBy(x => x.LotID)
            //        .Select(x => new InventoryDetail
            //        {
            //            LotID = x.Key,
            //            Quantity = x.Sum(y => y.Quantity),
            //            MFGDate = x.First().MFGDate,
            //            EXPDate = x.First().EXPDate,
            //            InputDate = x.First().InputDate,
            //            PalletID = x.First().PalletID
            //        })
            //        .ToList();
            //    List<IssueRequisition> issueRequisitions = new List<IssueRequisition>();
            //    decimal? sumQuantity = decimal.Parse(textEditQuantity.Text);
            //    decimal? sumIRQuantity = 0m;
            //    foreach (InventoryDetail inventoryDetail in querys)
            //    {
            //        IssueRequisition issueRequisition = new IssueRequisition();
            //        issueRequisition.LotID = inventoryDetail.LotID;
            //        issueRequisition.Quantity = inventoryDetail.Quantity;
            //        issueRequisition.MFGDate = inventoryDetail.MFGDate;
            //        issueRequisition.EXPDate = inventoryDetail.EXPDate;
            //        issueRequisition.InputDate = inventoryDetail.InputDate;
            //        issueRequisition.PalletID = inventoryDetail.PalletID;
            //        if (sumQuantity == sumIRQuantity)
            //        {
            //            issueRequisition.IRQuantity = 0;
            //        }
            //        else
            //        {
            //            issueRequisition.IRQuantity = inventoryDetail.Quantity > sumQuantity - sumIRQuantity ? sumQuantity - sumIRQuantity : inventoryDetail.Quantity;
            //        }
            //        issueRequisitions.Add(issueRequisition);
            //        sumIRQuantity += issueRequisition.IRQuantity;

            //    }
            //    gridControl1.DataSource = issueRequisitions;
            //    getTotalIRQuantity();

            //    string palletStr = "";
            //    if (inventoryDetails.Count > 0)
            //    {
            //        foreach (InventoryDetail inventoryDetail1 in inventoryDetails)
            //        {
            //            palletStr += palletStr.Contains(inventoryDetail1.IDCode.Substring(0, 5)) ? "" : inventoryDetail1.IDCode.Substring(0, 5) + ",";
            //        }
            //        if (palletStr.Length >= 5)
            //        {
            //            textEditPalletID.Text = palletStr.Substring(0, palletStr.Length - 1);
            //        }
            //    }
            //}

            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            if (e.KeyCode == Keys.Enter)
            {
                if (textEditQuantity.Text.Equals(""))
                {
                    return;
                }

                List<Inventory> inventorys = await inventoryRepository.GetByPartNumber("1", (string)gridView2.GetFocusedRowCellValue("PartNumber"));
                if (inventorys[0].ClosingStockQuantity < decimal.Parse(textEditQuantity.Text))
                {
                    MessageBox.Show("Số lượng xuất vượt quá số lượng tồn kho","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    textEditQuantity.Text = "";
                    return;
                }

                List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByPartNumber((string)gridView2.GetFocusedRowCellValue("PartNumber"));
                var querys = inventoryDetails.OrderBy(x => x.MFGDate).GroupBy(x => x.PalletID)
                    .Select(x => new InventoryDetail
                    {                      
                        Quantity = x.Sum(y => y.Quantity),
                        PalletID = x.Key
                    })
                    .ToList();
                List<IssueRequisition> issueRequisitions = new List<IssueRequisition>();
                decimal? sumQuantity = decimal.Parse(textEditQuantity.Text);
                decimal? sumIRQuantity = 0m;
                foreach (InventoryDetail inventoryDetail in querys)
                {
                    IssueRequisition issueRequisition = new IssueRequisition();
                    issueRequisition.Quantity = inventoryDetail.Quantity;
                    issueRequisition.PalletID = inventoryDetail.PalletID;
                    if (sumQuantity == sumIRQuantity)
                    {
                        issueRequisition.IRQuantity = 0;
                    }
                    else
                    {
                        issueRequisition.IRQuantity = inventoryDetail.Quantity > sumQuantity - sumIRQuantity ? sumQuantity - sumIRQuantity : inventoryDetail.Quantity;
                    }
                    issueRequisitions.Add(issueRequisition);
                    sumIRQuantity += issueRequisition.IRQuantity;

                }
                gridControl1.DataSource = issueRequisitions;
                getTotalIRQuantity();

                //string palletStr = "";
                //if (inventoryDetails.Count > 0)
                //{
                //    foreach (InventoryDetail inventoryDetail1 in inventoryDetails)
                //    {
                //        palletStr += palletStr.Contains(inventoryDetail1.IDCode.Substring(0, 5)) ? "" : inventoryDetail1.IDCode.Substring(0, 5) + ",";
                //    }
                //    if (palletStr.Length >= 5)
                //    {
                //        textEditPalletID.Text = palletStr.Substring(0, palletStr.Length - 1);
                //    }
                //}
            }

        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "PartNumber", false) == 0))
            {
                double mQuantity = ((gridView1.GetRowCellValue(i, gridView1.Columns["IRQuantity"]) == "") ? 0.0 : Convert.ToDouble(gridView1.GetRowCellValue(i, gridView1.Columns["IRQuantity"])));
                if (mQuantity > 0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightSkyBlue;
                }
            }
        }


        private void gridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            GridView view = sender as GridView;
            if (view == null) return;
            if (e.Column.FieldName != "IRQuantity") return;
            if ((decimal?)gridView1.GetRowCellValue(e.RowHandle, "Quantity") < (decimal?)gridView1.GetRowCellValue(e.RowHandle, "IRQuantity"))
            {
                MessageBox.Show("SL xuất lớn hơn SL tồn kho");
                gridView1.SetRowCellValue(e.RowHandle, "IRQuantity",0);
            }

            if (decimal.Parse(textEditQuantity.Text) < decimal.Parse(textEditTotalIRQuantity.Text))
            {
                BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "SL đã chọn lớn hơn SL xuất");
                gridView1.SetRowCellValue(e.RowHandle, "IRQuantity", 0);
            }

            else if (decimal.Parse(textEditQuantity.Text)>decimal.Parse(textEditTotalIRQuantity.Text))
            {
                BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "SL đã chọn nhỏ hơn SL xuất");
                gridView1.SetRowCellValue(e.RowHandle, "IRQuantity", 0);
            }

            getTotalIRQuantity();
           
            
        }

        private void getTotalIRQuantity()
        {
            //Decimal totalQuantity = 0;
            //int num = gridView1.RowCount - 1;
            //int i = 0;
            //while (true)
            //{
            //    int rowHandle = gridView1.GetRowHandle(i);
            //    if (gridView1.GetRowCellValue(rowHandle, "LotID") == null)
            //    {
            //        break;
            //    }
            //    Decimal quantityByItem = (Decimal)gridView1.GetRowCellValue(rowHandle, "IRQuantity");
            //    totalQuantity += quantityByItem;
            //    i++;
            //}
            //textEditTotalIRQuantity.Text = totalQuantity.ToString();

            Decimal totalQuantity = 0;
            int num = gridView1.RowCount - 1;
            int i = 0;
            while (true)
            {
                int rowHandle = gridView1.GetRowHandle(i);
                if (gridView1.GetRowCellValue(rowHandle, "PalletID") == null)
                {
                    break;
                }
                Decimal quantityByItem = (Decimal)gridView1.GetRowCellValue(rowHandle, "IRQuantity");
                totalQuantity += quantityByItem;
                i++;
            }
            textEditTotalIRQuantity.Text = totalQuantity.ToString();
        }

        private void getTotalClosingStockQuantity()
        {
            //Decimal totalQuantity = 0;
            //int num = gridView1.RowCount - 1;
            //int i = 0;
            //while (true)
            //{
            //    int rowHandle = gridView1.GetRowHandle(i);
            //    if (gridView1.GetRowCellValue(rowHandle, "LotID") == null)
            //    {
            //        break;
            //    }
            //    Decimal quantityByItem = (Decimal)gridView1.GetRowCellValue(rowHandle, "Quantity");
            //    totalQuantity += quantityByItem;
            //    i++;
            //}
            //textEditTotalClosingStockQuantity.Text = totalQuantity.ToString();

            Decimal totalQuantity = 0;
            int num = gridView1.RowCount - 1;
            int i = 0;
            while (true)
            {
                int rowHandle = gridView1.GetRowHandle(i);
                if (gridView1.GetRowCellValue(rowHandle, "PalletID") == null)
                {
                    break;
                }
                Decimal quantityByItem = (Decimal)gridView1.GetRowCellValue(rowHandle, "Quantity");
                totalQuantity += quantityByItem;
                i++;
            }
            textEditTotalClosingStockQuantity.Text = totalQuantity.ToString();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

            if (textEditWIRNumber.Text == "" || textEditWIRNumber.Text[0].ToString().Equals("-") || textEditWIRNumber.Text[textEditWIRNumber.Text.Length - 1].ToString().Equals("-"))
            {
                BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Số phiếu xuất không đúng");
                return;
            }
            if (textEditQuantity.Text == "")
            {
                BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Chưa nhập số lượng");
                return;
            }
            if (textEditTotalClosingStockQuantity.Text == "" || textEditTotalIRQuantity.Text == "")
            {
                MessageBox.Show("error","Alert",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }
            if (decimal.Parse(textEditTotalClosingStockQuantity.Text) < decimal.Parse(textEditTotalIRQuantity.Text))
            {
                BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "SL xuất lớn hơn SL tồn kho!!!");
                return;
            }
            if (decimal.Parse(textEditQuantity.Text) < decimal.Parse(textEditTotalIRQuantity.Text))
            {
                BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "SL đã chọn lớn hơn SL Cần xuất!!!");
                return;
            }
            createOrUpdateWIRHeader(textEditWIRNumber.Text);
        }

        private async void createOrUpdateWIRHeader(string wiRNumber)
        {
            List<WIRHeader> wIRHeaders = await wIRHeaderRepository.GetUnderID(wiRNumber);
            if (wIRHeaders.Count > 0)
            {
                wIRHeaders[0].WIRDate = DateTime.ParseExact(dateEditWIRDate.Text, Constant.smallDateTimeFormat, CultureInfo.InvariantCulture);          
                wIRHeaders[0].BranchID = comboBoxEditBranch.SelectedIndex == 0 ? "<>" : $"{comboBoxEditBranch.SelectedIndex - 1:D3}";
                wIRHeaders[0].BranchName = comboBoxEditBranch.SelectedItem.ToString();
                wIRHeaders[0].Note = textEditNote.Text;
                wIRHeaders[0].TotalQuantity = Decimal.Parse(textEditQuantity.Text);
                wIRHeaders[0].UpdatedUserID = WMMessage.User.UserID;
                wIRHeaders[0].UpdatedDate = DateTime.Now;
                wIRHeaders[0].Status = "";
                if (await wIRHeaderRepository.Update(wIRHeaders[0], wiRNumber) > 0)
                {
                    createOrUpdateWIRDetail(wiRNumber);
                    MessageBox.Show(WMMessage.msgSaveChangeSuccess);
                }
            }
            else {
                WIRHeader WIRHeader = new WIRHeader();
                WIRHeader.WIRNumber = wiRNumber;
                WIRHeader.WIRDate =DateTime.ParseExact(dateEditWIRDate.Text,Constant.smallDateTimeFormat,CultureInfo.InvariantCulture);
                WIRHeader.HandlingStatusID ="0";
                WIRHeader.HandlingStatusName = "Chưa duyệt";
                WIRHeader.BranchID = comboBoxEditBranch.SelectedIndex == 0 ? "<>" : $"{comboBoxEditBranch.SelectedIndex - 1:D3}";
                WIRHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
                WIRHeader.Note = textEditNote.Text;
                WIRHeader.TotalQuantity = Decimal.Parse(textEditQuantity.Text);
                WIRHeader.CreatedUserID = WMMessage.User.UserID;
                WIRHeader.CreatedDate = DateTime.Now;
                WIRHeader.Status = "";

                if (await wIRHeaderRepository.Create(WIRHeader) > 0)
                {
                    createWIRDetail(wiRNumber);
                    MessageBox.Show(WMMessage.msgSaveNewSuccess);
                }
            }
            //await checkExistWIRNumber(textEditWIRNumber.Text);
        }

        private async void createWIRDetail(string wirNumber)
        {
            int num = gridView1.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridView1.GetRowCellValue(i, gridView1.Columns["colLotID1"]) == "" || (decimal?)gridView1.GetRowCellValue(i, gridView1.Columns["IRQuantity"]) == 0)
                {
                    break;
                }
                int rowHandle = gridViewWIRDetail.GetRowHandle(i);

                WIRDetail wIRDetail = new WIRDetail();
                wIRDetail.WIRNumber = wirNumber;
                wIRDetail.Ordinal = i + 1;
                wIRDetail.PartNumber = (string)gridView2.GetFocusedRowCellValue("PartNumber");
                wIRDetail.PalletID = (string)gridView1.GetRowCellValue(rowHandle, "PalletID");
                wIRDetail.LotID = (string)gridView1.GetRowCellValue(rowHandle, "LotID");
                wIRDetail.MFGDate = (DateTime?)gridView1.GetRowCellValue(rowHandle, "MFGDate");
                wIRDetail.EXPDate = (DateTime?)gridView1.GetRowCellValue(rowHandle, "EXPDate");
                wIRDetail.InputDate = (DateTime?)gridView1.GetRowCellValue(rowHandle, "InputDate");
                wIRDetail.QuantityByItem = (decimal?)gridView1.GetRowCellValue(rowHandle, "IRQuantity");
                wIRDetail.TotalQuantity = (decimal?)gridView1.GetRowCellValue(rowHandle, "IRQuantity");
                wIRDetail.Remark = "";
                wIRDetail.PONumber = textEditPONumber.Text;
                wIRDetail.Status = "";

                List<Goods> goodss = await goodsRepository.getAll();
                foreach (Goods goods in goodss)
                {
                    if (goods.PartNumber.Equals((string)gridView2.GetFocusedRowCellValue("PartNumber")))
                    {
                        wIRDetail.PartName = goods.PartName;
                        wIRDetail.Unit = goods.Unit;
                        wIRDetail.ProductFamily = goods.ProductFamily;
                        wIRDetail.PackingVolume = goods.Quantity;
                        wIRDetail.QuantityByPack = wIRDetail.QuantityByItem / wIRDetail.PackingVolume;
                        break;
                    }
                }
                if (await wIRDetailRepository.Create(wIRDetail) > 0)
                {
                    i++;
                }
            }

            sbLoadDataForGridWIRDetailAsync(wirNumber);
        }

        private async void createOrUpdateWIRDetail(string wirNumber)
        {

            //int num = gridView1.RowCount - 1;
            //int i = 0;
            //while (true)
            //{
            //    int num2 = i;
            //    int num3 = num;
            //    if (num2 > num3 || gridView1.GetRowCellValue(i, gridView1.Columns["colLotID1"]) == "" || (decimal?)gridView1.GetRowCellValue(i, gridView1.Columns["IRQuantity"]) == 0)
            //    {
            //        break;
            //    }
            //    int rowHandle = gridViewWIRDetail.GetRowHandle(i);

            //    string palletID = (string)gridView1.GetRowCellValue(rowHandle, "PalletID");
            //    string lotID = (string)gridView1.GetRowCellValue(rowHandle, "LotID");
            //    List<WIRDetail> wIRDetails = await wIRDetailRepository.GetByPalletIDAndLotID(textEditWIRNumber.Text, palletID, lotID);
            //    List<WIRDetail> wIRDetails1 = await wIRDetailRepository.GetUnderID(textEditWIRNumber.Text);


            //    if (wIRDetails.Count > 0)
            //    {
            //        wIRDetails[0].PalletID = (string)gridView1.GetRowCellValue(rowHandle, "PalletID");
            //        wIRDetails[0].LotID = (string)gridView1.GetRowCellValue(rowHandle, "LotID");
            //        wIRDetails[0].MFGDate = (DateTime?)gridView1.GetRowCellValue(rowHandle, "MFGDate");
            //        wIRDetails[0].EXPDate = (DateTime?)gridView1.GetRowCellValue(rowHandle, "EXPDate");
            //        wIRDetails[0].InputDate = (DateTime?)gridView1.GetRowCellValue(rowHandle, "InputDate");
            //        wIRDetails[0].QuantityByItem = (decimal?)gridView1.GetRowCellValue(rowHandle, "IRQuantity");
            //        wIRDetails[0].TotalQuantity = (decimal?)gridView1.GetRowCellValue(rowHandle, "IRQuantity");
            //        wIRDetails[0].Remark = "";
            //        wIRDetails[0].PONumber = textEditPONumber.Text;
            //        wIRDetails[0].Status = "";
            //        if (await wIRDetailRepository.Update(wIRDetails[0], textEditWIRNumber.Text, wIRDetails[0].PalletID, wIRDetails[0].Ordinal, wIRDetails[0].LotID) > 0)
            //        {
            //            i++;
            //        }
            //    }
            //    else
            //    {
            //        WIRDetail wIRDetail = new WIRDetail();
            //        wIRDetail.WIRNumber = wirNumber;
            //        wIRDetail.Ordinal = wIRDetails1.Count + 1;
            //        wIRDetail.PartNumber = (string)gridView2.GetFocusedRowCellValue("PartNumber");
            //        wIRDetail.PalletID = (string)gridView1.GetRowCellValue(rowHandle, "PalletID");
            //        wIRDetail.LotID = (string)gridView1.GetRowCellValue(rowHandle, "LotID");
            //        wIRDetail.MFGDate = (DateTime?)gridView1.GetRowCellValue(rowHandle, "MFGDate");
            //        wIRDetail.EXPDate = (DateTime?)gridView1.GetRowCellValue(rowHandle, "EXPDate");
            //        wIRDetail.InputDate = (DateTime?)gridView1.GetRowCellValue(rowHandle, "InputDate");
            //        wIRDetail.QuantityByItem = (decimal?)gridView1.GetRowCellValue(rowHandle, "IRQuantity");
            //        wIRDetail.TotalQuantity = (decimal?)gridView1.GetRowCellValue(rowHandle, "IRQuantity");
            //        wIRDetail.Remark = "";
            //        wIRDetail.PONumber = textEditPONumber.Text;
            //        wIRDetail.Status = "";

            //        List<Goods> goodss = await goodsRepository.getAll();
            //        foreach (Goods goods in goodss)
            //        {
            //            if (goods.PalletID.Equals(textEditPalletID.Text))
            //            {
            //                wIRDetail.PartName = goods.PartName;
            //                wIRDetail.Unit = goods.Unit;
            //                wIRDetail.ProductFamily = goods.ProductFamily;
            //                wIRDetail.PackingVolume = goods.Quantity;
            //                wIRDetail.QuantityByPack = wIRDetail.QuantityByItem / wIRDetail.PackingVolume;
            //                break;
            //            }
            //        }
            //        if (await wIRDetailRepository.Create(wIRDetail) > 0)
            //        {
            //            i++;
            //        }

            //    }

            //}

            //List<WIRDetail> wIRDetails2 = await wIRDetailRepository.GetUnderID(textEditWIRNumber.Text);
            //for (int j = 0; j < gridView1.RowCount; j++)
            //{
            //    for (int k = 0; k < wIRDetails2.Count; k++)
            //    {
            //        if (((string)gridView2.GetFocusedRowCellValue("PartNumber")).Equals(wIRDetails2[k].PartNumber)
            //            && ((string)gridView1.GetRowCellValue(j, "LotID")).Equals(wIRDetails2[k].LotID)
            //            && ((decimal?)gridView1.GetRowCellValue(j, "IRQuantity")) == 0)
            //        {
            //            await wIRDetailRepository.Delete3(wIRDetails2[k].WIRNumber, wIRDetails2[k].PalletID, wIRDetails2[k].Ordinal);
            //        }
            //    }
            //}

            //sbLoadDataForGridWIRDetailAsync(wirNumber);

            int num = gridView1.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridView1.GetRowCellValue(i, gridView1.Columns["colPalletID1"]) == "" || (decimal?)gridView1.GetRowCellValue(i, gridView1.Columns["IRQuantity"]) == 0)
                {
                    break;
                }
                int rowHandle = gridViewWIRDetail.GetRowHandle(i);

                string palletID = (string)gridView1.GetRowCellValue(rowHandle, "PalletID");
                List<WIRDetail> wIRDetails = await wIRDetailRepository.GetByWIRNumberAndPalletID(textEditWIRNumber.Text, palletID);
                List<WIRDetail> wIRDetails1 = await wIRDetailRepository.GetUnderID(textEditWIRNumber.Text);


                if (wIRDetails.Count > 0)
                {
                    wIRDetails[0].QuantityByItem = (decimal?)gridView1.GetRowCellValue(rowHandle, "IRQuantity");
                    wIRDetails[0].TotalQuantity = (decimal?)gridView1.GetRowCellValue(rowHandle, "IRQuantity");
                    wIRDetails[0].Remark = "";
                    wIRDetails[0].PONumber = textEditPONumber.Text;
                    wIRDetails[0].Status = "";
                    if (await wIRDetailRepository.Update(wIRDetails[0], textEditWIRNumber.Text, wIRDetails[0].PalletID, wIRDetails[0].Ordinal) > 0)
                    {
                        i++;
                    }
                }
                else
                {
                    WIRDetail wIRDetail = new WIRDetail();
                    wIRDetail.WIRNumber = wirNumber;
                    wIRDetail.Ordinal = wIRDetails1.Count + 1;
                    wIRDetail.PartNumber = (string)gridView2.GetFocusedRowCellValue("PartNumber");
                    wIRDetail.PalletID = (string)gridView1.GetRowCellValue(rowHandle, "PalletID");
                    wIRDetail.QuantityByItem = (decimal?)gridView1.GetRowCellValue(rowHandle, "IRQuantity");
                    wIRDetail.TotalQuantity = (decimal?)gridView1.GetRowCellValue(rowHandle, "IRQuantity");
                    wIRDetail.Remark = "";
                    wIRDetail.PONumber = textEditPONumber.Text;
                    wIRDetail.Status = "";

                    List<Goods> goodss = await goodsRepository.getAll();
                    foreach (Goods goods in goodss)
                    {
                        if (goods.PartNumber.Equals((string)gridView2.GetFocusedRowCellValue("PartNumber")))
                        {
                            wIRDetail.PartName = goods.PartName;
                            wIRDetail.Unit = goods.Unit;
                            wIRDetail.ProductFamily = goods.ProductFamily;
                            wIRDetail.PackingVolume = goods.Quantity;
                            wIRDetail.QuantityByPack = wIRDetail.QuantityByItem / wIRDetail.PackingVolume;
                            break;
                        }
                    }
                    if (await wIRDetailRepository.Create(wIRDetail) > 0)
                    {
                        i++;
                    }

                }

            }

            //List<WIRDetail> wIRDetails2 = await wIRDetailRepository.GetUnderID(textEditWIRNumber.Text);
            //for (int j = 0; j < gridView1.RowCount; j++)
            //{
            //    for (int k = 0; k < wIRDetails2.Count; k++)
            //    {
            //        if (((string)gridView2.GetFocusedRowCellValue("PartNumber")).Equals(wIRDetails2[k].PartNumber)
            //            && ((string)gridView1.GetRowCellValue(j, "LotID")).Equals(wIRDetails2[k].LotID)
            //            && ((decimal?)gridView1.GetRowCellValue(j, "IRQuantity")) == 0)
            //        {
            //            await wIRDetailRepository.Delete3(wIRDetails2[k].WIRNumber, wIRDetails2[k].PalletID, wIRDetails2[k].Ordinal);
            //        }
            //    }
            //}

            sbLoadDataForGridWIRDetailAsync(wirNumber);
        }

        private void textEditPONumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textEditWIRNumber.Text.Length > 0)
                {
                    if (textEditWIRNumber.Text[0].ToString().Equals("-"))
                    {
                        textEditWIRNumber.Text = textEditPONumber.Text + textEditWIRNumber.Text;
                    }
                    else if (textEditWIRNumber.Text[textEditWIRNumber.Text.Length - 1].ToString().Equals("-"))
                    {

                        textEditWIRNumber.Text = textEditPONumber.Text + "-";
                    }
                    else
                    {
                        textEditWIRNumber.Text = textEditPONumber.Text + textEditWIRNumber.Text.Substring(textEditWIRNumber.Text.LastIndexOf("-"));
                    }
                }
                else
                {
                    textEditWIRNumber.Text = textEditPONumber.Text + "-";
                }
                textEditQuantity.Focus();

            }
        }



        private void YourDGV_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName.Equals("QuantityByItem"))
            {
                getTotalQuantityByItem();
            }
            if (e.Column.FieldName.Equals("QuantityByPack"))
            {
                getTotalQuantityByPack();
            }
        }


        private Boolean checkExistPartNumber(String PartNumber)
        {
            int num = gridViewWIRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIRDetail.GetRowCellValue(i, gridViewWIRDetail.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWIRDetail.GetRowHandle(i);
                String PartNumber1 = (String)gridViewWIRDetail.GetRowCellValue(rowHandle, "PartNumber");
                if (PartNumber.Equals(PartNumber1))
                {
                    return true;
                }
                i++;
            }
            return false;
        }


        private void getTotalQuantityByItem()
        {
            Decimal totalQuantityByItem = 0;
            int num = gridViewWIRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWIRDetail.GetRowHandle(i);
                if (gridViewWIRDetail.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantityByItem = (Decimal)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                totalQuantityByItem += quantityByItem;
                i++;
            }
            textEditTotalQuantityByItem.Text = totalQuantityByItem.ToString();
        }

        private void getTotalQuantityByPack()
        {
            Decimal totalQuantityByPack = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWIRDetail.GetRowHandle(i);
                if (gridViewWIRDetail.GetRowCellValue(rowHandle, "PartNumber") == null)
                {
                    break;
                }
                Decimal quantityByPack = (Decimal)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                totalQuantityByPack += quantityByPack;
                i++;
            }
            textEditTotalQuantityByPack.Text = totalQuantityByPack.ToString();
        }

        private async void WIRHeaderGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == collNo)
            {
                int rowHandle = gridViewWIRDetail.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }

            }

        }

        private async void sbLoadDataForGridWIRDetailAsync(String WIRNumber)
        {
            List<WIRDetail> WIRDetails = await wIRDetailRepository.GetUnderID(WIRNumber);
            //int mMaxRow = 100;
            //if (WIRDetails.Count < mMaxRow)
            //{
            //    int num = mMaxRow - WIRDetails.Count;
            //    int i = 1;
            //    while (true)
            //    {
            //        int num2 = i;
            //        int num3 = num;
            //        if (num2 > num3)
            //        {
            //            break;
            //        }
            //        WIRDetails.Add(new WIRDetail());
            //        i++;
            //    }
            //}
            gridControlWIRDetail.DataSource = WIRDetails;
            getTotalQuantityByItem();
            getTotalQuantityByPack();
        }

        public async void getlastWIRNumberAsync()
        {
            lastWIRNumber = await wIRHeaderRepository.GetLastWRRNumber();
            String WIRNumberYear = DateTime.Now.Year.ToString().Substring(2, 2);
            if (lastWIRNumber == "")
            {
                textEditWIRNumber.Text = WIRNumberYear + "000" + "-" + $"{1:D5}";
            }
            else
            {
                string[] arrListStr = lastWIRNumber.Split('-');
                String WIRNumberPartOne = arrListStr[0];
                String WIRNumberPartTwo = arrListStr[1];
                WIRNumberPartTwo = (int.Parse(WIRNumberPartTwo) + 1).ToString();
                textEditWIRNumber.Text = WIRNumberYear + "000" + "-" + $"{int.Parse(WIRNumberPartTwo):D5}";
            }


        }

        public async void createWIRHeaderAsync()
        {
            WIRHeader WIRHeader = new WIRHeader();
            WIRHeader.WIRNumber = textEditWIRNumber.Text;
            WIRHeader.WIRDate = DateTime.ParseExact(dateEditWIRDate.Text, Constant.smallDateTimeFormat, CultureInfo.InvariantCulture);
            WIRHeader.HandlingStatusID = comboBoxEditBranch.SelectedIndex == 0 ? "<>" : (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            WIRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            WIRHeader.BranchID = (comboBoxEditBranch.SelectedIndex).ToString();
            WIRHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            WIRHeader.Note = textEditNote.Text;
            WIRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            WIRHeader.CreatedUserID = WMMessage.User.UserID;
            WIRHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            WIRHeader.Status = "1";

            if (await wIRHeaderRepository.Create(WIRHeader) > 0)
            {
                createWIRDetailAsync();
                BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, WMMessage.msgSaveNewSuccess);
            }
        }

        public async void createWIRDetailAsync()
        {
            int num = gridViewWIRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIRDetail.GetRowCellValue(i, gridViewWIRDetail.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWIRDetail.GetRowHandle(i);
                WIRDetail WIRDetail = new WIRDetail();
                WIRDetail.WIRNumber = textEditWIRNumber.Text;
                WIRDetail.PartNumber = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "PartNumber");
                WIRDetail.PartName = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "PartName");
                WIRDetail.Ordinal = i + 1;
                WIRDetail.TotalQuantity = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "TotalQuantity");
                WIRDetail.QuantityByItem = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                WIRDetail.QuantityByPack = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                WIRDetail.PackingVolume = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "PackingVolume");
                WIRDetail.LotID = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "LotID");
                WIRDetail.PalletID = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "PalletID");
                WIRDetail.MFGDate = (DateTime?)gridViewWIRDetail.GetRowCellValue(rowHandle, "MFGDate");
                WIRDetail.EXPDate = (DateTime?)gridViewWIRDetail.GetRowCellValue(rowHandle, "EXPDate");
                WIRDetail.InputDate = (DateTime?)gridViewWIRDetail.GetRowCellValue(rowHandle, "InputDate");
                WIRDetail.Remark = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "Remark");
                WIRDetail.Unit = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "Unit");
                WIRDetail.PONumber = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "PONumber");
                WIRDetail.Status = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "Status");
                WIRDetail.ProductFamily = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "ProductFamily");
                if (await wIRDetailRepository.Create(WIRDetail) > 0)
                {
                    i++;
                }
            }
        }

        public async void createwIDataHeaderAsync()
        {
            WIDataHeader wIDataHeader = new WIDataHeader();
            wIDataHeader.WIDNumber = textEditWIRNumber.Text;
            wIDataHeader.WIDDate = DateTime.ParseExact(dateEditWIRDate.Text, Constant.smallDateTimeFormat, CultureInfo.InvariantCulture);
            wIDataHeader.WIRNumber = textEditWIRNumber.Text;
            wIDataHeader.HandlingStatusID = "0";
            wIDataHeader.HandlingStatusName = "Chưa duyệt";
            wIDataHeader.BranchID = comboBoxEditBranch.SelectedIndex == 0 ? "<>" : $"{comboBoxEditBranch.SelectedIndex - 1:D3}";
            wIDataHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            wIDataHeader.Note = textEditNote.Text;
            wIDataHeader.Status = "";
            wIDataHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wIDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wIDataHeader.CreatedUserID = WMMessage.User.UserID;
            wIDataHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wIDataHeaderRepository.Create(wIDataHeader) > 0)
            {
                createWIDataGeneralAsync();
                MessageBox.Show("-> Đã tạo dữ liệu xuất!!!");
            }
        }

        public async void createWIDataGeneralAsync()
        {
            int num = gridViewWIRDetail.RowCount - 1;
            for (int i = 0; i < gridViewWIRDetail.RowCount - 1; i++)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIRDetail.GetRowCellValue(i, gridViewWIRDetail.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                WIDataGeneral WIDataGeneral = new WIDataGeneral();
                WIDataGeneral.WIDNumber = textEditWIRNumber.Text;
                WIDataGeneral.PartNumber = (string)gridViewWIRDetail.GetRowCellValue(i, "PartNumber");
                WIDataGeneral.PartName = (string)gridViewWIRDetail.GetRowCellValue(i, "PartName");
                WIDataGeneral.IDCode = "";
                WIDataGeneral.Ordinal = i + 1;
                WIDataGeneral.Quantity = 0m;
                WIDataGeneral.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
                WIDataGeneral.PackingQuantity = 0m;
                WIDataGeneral.QuantityOrg = (Decimal?)gridViewWIRDetail.GetRowCellValue(i, "QuantityByItem");
                WIDataGeneral.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);

                List<WIRDetail> WIRDetails = await wIRDetailRepository.GetUnderID(textEditWIRNumber.Text);
                WIDataGeneral.TotalGoodsOrg = Convert.ToDecimal(WIRDetails.Count);
                WIDataGeneral.TotalGoods = Convert.ToDecimal(WIRDetails.Count);

                WIDataGeneral.Status = "0";
                WIDataGeneral.PackingVolume = (Decimal?)gridViewWIRDetail.GetRowCellValue(i, "PackingVolume");
                WIDataGeneral.QuantityByPack = (Decimal?)gridViewWIRDetail.GetRowCellValue(i, "QuantityByPack");
                WIDataGeneral.QuantityByItem = (Decimal?)gridViewWIRDetail.GetRowCellValue(i, "QuantityByItem");
                WIDataGeneral.LotID = (string)gridViewWIRDetail.GetRowCellValue(i, "LotID");
                WIDataGeneral.PalletID = (string)gridViewWIRDetail.GetRowCellValue(i, "PalletID");
                WIDataGeneral.PONumber = (string)gridViewWIRDetail.GetRowCellValue(i, "PONumber");
                WIDataGeneral.ProductFamily = (string)gridViewWIRDetail.GetRowCellValue(i, "ProductFamily");
                WIDataGeneral.Unit = (string)gridViewWIRDetail.GetRowCellValue(i, "Unit");

                if (((DateTime?)gridViewWIRDetail.GetRowCellValue(i, "MFGDate")).ToString() != "")
                {
                    WIDataGeneral.MFGDate = (DateTime?)gridViewWIRDetail.GetRowCellValue(i, "MFGDate");
                }
                if (((DateTime?)gridViewWIRDetail.GetRowCellValue(i, "EXPDate")).ToString() != "")
                {
                    WIDataGeneral.EXPDate = (DateTime?)gridViewWIRDetail.GetRowCellValue(i, "EXPDate");
                }
                if (((DateTime?)gridViewWIRDetail.GetRowCellValue(i, "InputDate")).ToString() != "")
                {
                    WIDataGeneral.InputDate = (DateTime?)gridViewWIRDetail.GetRowCellValue(i, "InputDate");
                }
                WIDataGeneral.Remark = (string)gridViewWIRDetail.GetRowCellValue(i, "Remark");
                await wIDataGeneralRepository.Create(WIDataGeneral);
            }
        }


        public async void updateWIRHeaderAsync()
        {
            
            List<WIRHeader> WIRHeaders = await wIRHeaderRepository.GetUnderID(textEditWIRNumber.Text);
            if (WIRHeaders[0].HandlingStatusID.Equals("2"))
            {
                MessageBox.Show("Yêu cầu xuất đã duyệt mức 2, không thể chỉnh sửa");
                return;
            }
            WIRHeaders[0].WIRDate = DateTime.ParseExact(dateEditWIRDate.Text, Constant.smallDateTimeFormat, CultureInfo.InvariantCulture);
            WIRHeaders[0].HandlingStatusID = comboBoxEditBranch.SelectedIndex == 0 ? "<>" : (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            WIRHeaders[0].HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            WIRHeaders[0].BranchID = comboBoxEditBranch.SelectedIndex == 0 ? "<>" : $"{comboBoxEditBranch.SelectedIndex - 1:D3}";
            WIRHeaders[0].BranchName = comboBoxEditBranch.SelectedItem.ToString();
            WIRHeaders[0].Note = textEditNote.Text;
            WIRHeaders[0].Status = "";
            WIRHeaders[0].TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            WIRHeaders[0].UpdatedUserID = WMMessage.User.UserID;
            WIRHeaders[0].UpdatedDate = DateTime.Now;

            if (await wIRHeaderRepository.Update(WIRHeaders[0], WIRHeaders[0].WIRNumber) > 0)
            {
                updateWIRDetailAsync();
                MessageBox.Show(WMMessage.msgSaveChangeSuccess);
                if (comboBoxEditHandlingStatus.SelectedIndex == 3)
                {
                    createwIDataHeaderAsync();
                }
            }

        }

        public async void updateWIRDetailAsync()
        {
            //int num = gridViewWIRDetail.RowCount - 1;
            //int i = 0;
            //while (true)
            //{
            //    int num2 = i;
            //    int num3 = num;
            //    if (num2 > num3 || gridViewWIRDetail.GetRowCellValue(i, gridViewWIRDetail.Columns["colPartNumber"]) == "")
            //    {
            //        break;
            //    }
            //    int rowHandle = gridViewWIRDetail.GetRowHandle(i);
            //    WIRDetail WIRDetail = new WIRDetail();
            //    WIRDetail.WIRNumber = textEditWIRNumber.Text;
            //    WIRDetail.PartNumber = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "PartNumber");
            //    WIRDetail.PartName = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "PartName");
            //    WIRDetail.PackingVolume = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "PackingVolume");
            //    WIRDetail.TotalQuantity = decimal.Parse(textEditTotalQuantityByItem.Text);
            //    WIRDetail.QuantityByItem = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
            //    WIRDetail.QuantityByPack = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
            //    WIRDetail.LotID = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "LotID");
            //    WIRDetail.PalletID = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "PalletID");
            //    WIRDetail.MFGDate = (DateTime?)gridViewWIRDetail.GetRowCellValue(rowHandle, "MFGDate");
            //    WIRDetail.EXPDate = (DateTime?)gridViewWIRDetail.GetRowCellValue(rowHandle, "EXPDate");
            //    WIRDetail.InputDate = (DateTime?)gridViewWIRDetail.GetRowCellValue(rowHandle, "InputDate");
            //    WIRDetail.Remark = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "Remark");
            //    WIRDetail.PONumber = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "PONumber");
            //    WIRDetail.Unit = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "Unit");
            //    WIRDetail.ProductFamily = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "ProductFamily");
            //    WIRDetail.Status = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "Status");

            //    List<WIRDetail> list = await wIRDetailRepository.GetByWIRNumberAndPalletID(textEditWIRNumber.Text, WIRDetail.PalletID);

            //    if (list.Count > 0)
            //    {
            //        WIRDetail.Ordinal = i + 1;
            //        if (await wIRDetailRepository.Update(WIRDetail, textEditWIRNumber.Text, WIRDetail.PalletID, WIRDetail.Ordinal, WIRDetail.LotID) > 0)
            //        {
            //            i++;
            //        }

            //    }
            //    else
            //    {
            //        WIRDetail.Ordinal = i + 1;
            //        if (await wIRDetailRepository.Create(WIRDetail) > 0)
            //        {
            //            i++;
            //        }
            //    }

            //}

            int num = gridViewWIRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIRDetail.GetRowCellValue(i, gridViewWIRDetail.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWIRDetail.GetRowHandle(i);
                WIRDetail WIRDetail = new WIRDetail();
                WIRDetail.WIRNumber = textEditWIRNumber.Text;
                WIRDetail.PartNumber = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "PartNumber");
                WIRDetail.PartName = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "PartName");
                WIRDetail.PackingVolume = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "PackingVolume");
                WIRDetail.TotalQuantity = decimal.Parse(textEditTotalQuantityByItem.Text);
                WIRDetail.QuantityByItem = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                WIRDetail.QuantityByPack = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                WIRDetail.PalletID = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "PalletID");
                WIRDetail.InputDate = (DateTime?)gridViewWIRDetail.GetRowCellValue(rowHandle, "InputDate");
                WIRDetail.Remark = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "Remark");
                WIRDetail.PONumber = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "PONumber");
                WIRDetail.Unit = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "Unit");
                WIRDetail.ProductFamily = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "ProductFamily");
                WIRDetail.Status = (string)gridViewWIRDetail.GetRowCellValue(rowHandle, "Status");

                List<WIRDetail> list = await wIRDetailRepository.GetByWIRNumberAndPalletID(textEditWIRNumber.Text, WIRDetail.PalletID);

                if (list.Count > 0)
                {
                    WIRDetail.Ordinal = i + 1;
                    if (await wIRDetailRepository.Update(WIRDetail, textEditWIRNumber.Text, WIRDetail.PalletID, WIRDetail.Ordinal) > 0)
                    {
                        i++;
                    }

                }
                else
                {
                    WIRDetail.Ordinal = i + 1;
                    if (await wIRDetailRepository.Create(WIRDetail) > 0)
                    {
                        i++;
                    }
                }

            }
        }

        public async void DeleteWIRHeaderAsync()
        {
            if ((await wIRHeaderRepository.Delete(textEditWIRNumber.Text)) > 0)
            {
                await DeleteWIRDetailAsync();
                await DeletewIDataHeaderAsync();
                sbLoadDataForGridWIRDetailAsync(textEditWIRNumber.Text);
                BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, WMMessage.msgDeleteSuccess);
                textEditWIRNumber.Text = "";
                comboBoxEditHandlingStatus.SelectedIndex = 0;
                comboBoxEditBranch.SelectedIndex = 0;
                dateEditWIRDate.Text = DateTime.Parse(DateTime.Now.ToString()).ToString(Constant.smallDateTimeFormat);
            }
        }

        public async Task DeleteWIRDetailAsync()
        {
            await wIRDetailRepository.Delete(textEditWIRNumber.Text);
            clearAsync();
            gridControlWIRDetail.DataSource = null;
        }

        public async Task DeletewIDataHeaderAsync()
        {
            if (await wIDataHeaderRepository.GetUnderID(textEditWIRNumber.Text) != null)
            {
                if ((await wIDataHeaderRepository.Delete(textEditWIRNumber.Text)) > 0)
                {
                    DeleteWIDataGeneralAsync();
                }
            }
        }

        public async Task DeleteWIDataGeneralAsync()
        {
            await wIDataGeneralRepository.Delete(textEditWIRNumber.Text);
        }


        private async void clearAsync()
        {
            //textEditWIRNumber.Text = "";
            dateEditWIRDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            comboBoxEditBranch.SelectedIndex = 1;
            textEditTotalQuantityByItem.Text = "0";
            textEditTotalQuantityByPack.Text = "0";
            textEditQuantity.Text = "";
            textEditTotalIRQuantity.Text = "";
            textEditTotalClosingStockQuantity.Text = "";
            List<WIRDetail> WIRDetails = new List<WIRDetail>();

            for (int i = 0; i < 100; i++)
            {
                WIRDetails.Add(new WIRDetail());
            }
            gridControlWIRDetail.DataSource = WIRDetails;

        }

        private async void checkExistWIRNumber(String WIRNumber)
        {
           
            List<WIRHeader> wIRHeaders = await wIRHeaderRepository.GetUnderID(WIRNumber);
            if (!string.IsNullOrEmpty(WIRNumber)&&wIRHeaders.Count>0)
            {
                isWIRNumberExist = true;
            }
           
        }


        private async void WIRNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                List<WIRHeader> wIRHeaders = await wIRHeaderRepository.GetUnderID(textEditWIRNumber.Text);

                if (wIRHeaders.Count > 0)
                {
                    dateEditWIRDate.Text = DateTime.Parse(wIRHeaders[0].WIRDate.ToString()).ToShortDateString();
                    comboBoxEditBranch.SelectedIndex = wIRHeaders[0].BranchID.Equals("<>") ? 0 : int.Parse(wIRHeaders[0].BranchID) + 1;
                    comboBoxEditHandlingStatus.SelectedIndex = wIRHeaders[0].HandlingStatusID.Equals("<>") ? 0 : int.Parse(wIRHeaders[0].HandlingStatusID) + 1;
                    sbLoadDataForGridWIRDetailAsync(textEditWIRNumber.Text);
                }
                else
                {
                    BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Số phiếu yêu cầu không tồn tại");
                    return;
                }

            }
        }

        private void loadDataGrid2(string wirNumber)
        {
            List<Inventory> inventories = new List<Inventory>();
            inventories.Add(new Inventory
            {
                PartNumber = wirNumber.Substring(wirNumber.LastIndexOf("-") + 1)
            });
            gridControl2.DataSource = inventories;
            gridView2.FocusedRowHandle = 0;

        }
        private async void loadDataGrid1(string wirNumber)
        {
            string partNumber = wirNumber.Substring(wirNumber.LastIndexOf("-") + 1);
            List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByPartNumber(partNumber);


            var querys = inventoryDetails.OrderBy(x => x.MFGDate).GroupBy(x => x.LotID)
                   .Select(x => new InventoryDetail
                   {
                       PartNumber = x.First().PartNumber,
                       LotID = x.Key,
                       Quantity = x.Sum(y => y.Quantity),
                       MFGDate = x.First().MFGDate,
                       EXPDate = x.First().EXPDate,
                       InputDate = x.First().InputDate,
                       PalletID = x.First().PalletID
                   })
                   .ToList();

            if (comboBoxEditHandlingStatus.SelectedIndex == 3|| comboBoxEditHandlingStatus.SelectedIndex == 4)
            {
                textEditTotalClosingStockQuantity.Text = "";
                gridView1.Columns["Quantity"].Visible = false;

                List<WIRDetail> wIRDetails = await wIRDetailRepository.GetUnderID(wirNumber);
                List<IssueRequisition> issueRequisitions = new List<IssueRequisition>();

                foreach (WIRDetail wIRDetail in wIRDetails)
                {
                    IssueRequisition issueRequisition = new IssueRequisition();
                    issueRequisition.LotID = wIRDetail.LotID;
                    issueRequisition.MFGDate = wIRDetail.MFGDate;
                    issueRequisition.EXPDate = wIRDetail.EXPDate;
                    issueRequisition.InputDate = wIRDetail.InputDate;
                    issueRequisition.PalletID = wIRDetail.PalletID;
                    issueRequisition.IRQuantity = wIRDetail.QuantityByItem;

                    issueRequisitions.Add(issueRequisition);

                }
                gridControl1.DataSource = issueRequisitions;
                getTotalIRQuantity();
                
            }
            else
            {
                gridView1.Columns["Quantity"].Visible =true;
                List<IssueRequisition> issueRequisitions = new List<IssueRequisition>();
                foreach (InventoryDetail inventoryDetail in querys)
                {
                    IssueRequisition issueRequisition = new IssueRequisition();
                    issueRequisition.LotID = inventoryDetail.LotID;
                    issueRequisition.Quantity = inventoryDetail.Quantity;
                    issueRequisition.MFGDate = inventoryDetail.MFGDate;
                    issueRequisition.EXPDate = inventoryDetail.EXPDate;
                    issueRequisition.InputDate = inventoryDetail.InputDate;
                    issueRequisition.PalletID = inventoryDetail.PalletID;
                    issueRequisition.IRQuantity = 0;
                    List<WIRDetail> wIRDetails = await wIRDetailRepository.GetUnderID(wirNumber);
                    foreach (WIRDetail wIRDetail in wIRDetails)
                    {

                        if (wIRDetail.PartNumber.Equals(inventoryDetail.PartNumber) && wIRDetail.LotID.Equals(inventoryDetail.LotID))
                        {

                            issueRequisition.IRQuantity = wIRDetail.QuantityByItem;
                            break;
                        }

                    }
                    issueRequisitions.Add(issueRequisition);


                }

                gridControl1.DataSource = issueRequisitions;
                getTotalIRQuantity();
                getTotalClosingStockQuantity();
            }

        }



        private async void textEditWIRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchWIR frmSearchWIR = new frmSearchWIR();
            frmSearchWIR.ShowDialog(this);
            frmSearchWIR.Dispose();
            if (selectedWIRNumber != null)
            {
                textEditWIRNumber.Text = selectedWIRNumber;
                List<WIRHeader> wIRHeaders = await wIRHeaderRepository.GetUnderID(textEditWIRNumber.Text);
                if (wIRHeaders.Count > 0)
                {
                    textEditWIRNumber.Text = wIRHeaders[0].WIRNumber;
                    dateEditWIRDate.Text = DateTime.Parse(wIRHeaders[0].WIRDate.ToString()).ToString(Constant.smallDateTimeFormat);
                    comboBoxEditBranch.Text = wIRHeaders[0].BranchName;
                    comboBoxEditHandlingStatus.Text = wIRHeaders[0].HandlingStatusName;
                    textEditPONumber.Text = wIRHeaders[0].WIRNumber.Substring(0, wIRHeaders[0].WIRNumber.LastIndexOf("-"));
                    textEditQuantity.Text = wIRHeaders[0].TotalQuantity + "";
                    textEditNote.Text = wIRHeaders[0].Note;
                    sbLoadDataForGridWIRDetailAsync(selectedWIRNumber);
                    loadDataGrid2(selectedWIRNumber);
                    loadDataGrid1(selectedWIRNumber);

                }
                checkExistWIRNumber(textEditWIRNumber.Text);
            }
           
        }

        private async void selectFileExcelAsync()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"d:\";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                List<WIRDetail> WIRDetails = await ExcelToListAsync(fdlg.FileName);
                int mMaxRow = 100;
                if (WIRDetails.Count < mMaxRow)
                {
                    int num = mMaxRow - WIRDetails.Count;
                    int i = 1;
                    while (true)
                    {
                        int num2 = i;
                        int num3 = num;
                        if (num2 > num3)
                        {
                            break;
                        }
                        WIRDetails.Add(new WIRDetail());
                        i++;
                    }
                }
                gridControlWIRDetail.DataSource = WIRDetails;
                getTotalQuantityByItem();
                getTotalQuantityByPack();
                Application.DoEvents();
            }
        }


        private async Task<List<WIRDetail>> ExcelToListAsync(String fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                dateEditWIRDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

                int totalRows = workSheet.Dimension.Rows;
                List<WIRDetail> WIRDetails = new List<WIRDetail>();
                for (int i = 6; i <= totalRows + 4; i++)
                {
                    int firstDotMFGDate = workSheet.Cells[i, 9].Text.Trim().IndexOf("/");
                    int secondDotMFGDate = workSheet.Cells[i, 9].Text.Trim().LastIndexOf("/");
                    String dateMFGDate = workSheet.Cells[i, 9].Text.Trim().Substring((firstDotMFGDate + 1), 2);
                    String monthMFGDate = workSheet.Cells[i, 9].Text.Trim().Substring(0, 2);
                    String yearMFGDate = workSheet.Cells[i, 9].Text.Trim().Substring((secondDotMFGDate + 1), 2);


                    int firstDotInputDate = workSheet.Cells[i, 11].Text.Trim().IndexOf("/");
                    int secondDotInputDate = workSheet.Cells[i, 11].Text.Trim().LastIndexOf("/");
                    String dateInputDate = workSheet.Cells[i, 11].Text.Trim().Substring((firstDotInputDate + 1), 2);
                    String monthInputDate = workSheet.Cells[i, 11].Text.Trim().Substring(0, 2);
                    String yearInputDate = workSheet.Cells[i, 11].Text.Trim().Substring((secondDotInputDate + 1), 2);

                    string partNumber = workSheet?.Cells[i, 3].Text.Trim();
                    double mQuantityInStock = 1;
                    double mQuantityByItem = 1;
                    List<Inventory> inventories = await inventoryRepository.GetUnderMultiPartNumber(2022, 6, "1", partNumber);
                    if (inventories.Count > 0)
                    {
                        mQuantityInStock = (double)inventories[0].ClosingStockQuantity;
                        mQuantityByItem = double.Parse(workSheet?.Cells[i, 7].Text.Trim());
                    }



                    WIRDetails.Add(new WIRDetail
                    {
                        PalletID = workSheet?.Cells[i, 2].Text.Trim(),
                        PartNumber = workSheet?.Cells[i, 3].Text.Trim(),
                        PartName = workSheet?.Cells[i, 4].Text.Trim(),
                        ProductFamily = workSheet?.Cells[i, 5].Text.Trim(),
                        LotID = workSheet?.Cells[i, 6].Text.Trim(),
                        QuantityByItem = Decimal.Parse(workSheet?.Cells[i, 7].Text.Trim()),
                        TotalQuantity = Decimal.Parse(workSheet?.Cells[i, 7].Text.Trim()),
                        Unit = workSheet?.Cells[i, 8].Text.Trim(),
                        MFGDate = DateTime.Parse(dateMFGDate + "/" + monthMFGDate + "/" + yearMFGDate),
                        EXPDate = (DateTime.Parse(dateMFGDate + "/" + monthMFGDate + "/" + yearMFGDate)).AddYears(2).AddDays(1),
                        InputDate = DateTime.Parse(dateInputDate + "/" + monthInputDate + "/" + yearInputDate),
                        PONumber = workSheet?.Cells[i, 13].Text.Trim(),
                        Remark = workSheet?.Cells[i, 14].Text.Trim(),
                        Status = (inventories.Count == 0) ? "Không còn tồn kho" : (mQuantityByItem > mQuantityInStock ? "Tồn kho không đủ" : "OK")
                    });
                }
                List<Goods> goodss = await goodsRepository.getAll();
                for (int i = 0; i < WIRDetails.Count; i++)
                {
                    foreach (Goods goods in goodss)
                    {
                        if (goods.PartNumber.Equals(WIRDetails[i].PartNumber))
                        {
                            WIRDetails[i].PackingVolume = goods.Quantity;
                            WIRDetails[i].QuantityByPack = WIRDetails[i].QuantityByItem / WIRDetails[i].PackingVolume;
                        }
                    }
                }
                return WIRDetails;
            }
        }

        //barbutton click
        public void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        //getlastWIRNumberAsync();
                        loadDataGridView2();
                        clearAsync();
                        textEditPONumber.Focus();
                        textEditTotalClosingStockQuantity.Visible = true;
                    }
                    break;
                case "save":
                    
                    checkExistWIRNumber(textEditWIRNumber.Text);
                    if (textEditWIRNumber.Text == "" || textEditWIRNumber.Text[0].Equals("-") || textEditWIRNumber.Text[textEditWIRNumber.Text.Length - 1].Equals("-"))
                    {
                        BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Số phiếu xuất không đúng");
                        return;
                    }
                    if (textEditQuantity.Text == "")
                    {
                        return;
                    }
                    if (isWIRNumberExist == true)
                    {
                        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                        {
                            updateWIRHeaderAsync();
                        }
                    }
                    else
                    {
                        if (WMPublic.sbMessageSaveNewRequest(this) == true)
                        {

                            createWIRHeaderAsync();
                        }
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {

                        DeleteWIRHeaderAsync();
                    }
                    break;
                case "search":
                    break;
                case "refesh":
                    sbLoadDataForGridWIRDetailAsync(textEditWIRNumber.Text);
                    break;
                case "import":
                    try
                    {
                        Application.UseWaitCursor = true;
                        selectFileExcelAsync();
                    }
                    finally
                    {
                        BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Tải thành công file excel");
                        Application.UseWaitCursor = false;
                    }
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {

            switch (keyData)
            {
                case Keys.F2:
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        //getlastWIRNumberAsync();
                        loadDataGridView2();
                        clearAsync();
                        textEditPONumber.Focus();
                    }
                    return true;
                case Keys.F3:
                    checkExistWIRNumber(textEditWIRNumber.Text);
                    if (textEditWIRNumber.Text == "" || textEditWIRNumber.Text[0].Equals("-") || textEditWIRNumber.Text[textEditWIRNumber.Text.Length - 1].Equals("-"))
                    {
                        BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Số phiếu xuất không đúng");
                    }
                    else
                    {
                        if (isWIRNumberExist == true)
                        {
                            if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                            {
                                updateWIRHeaderAsync();
                            }
                        }
                        else
                        {
                            if (WMPublic.sbMessageSaveNewRequest(this) == true)
                            {

                                createWIRHeaderAsync();
                            }
                        }
                    }
                    return true;
                case Keys.F4:
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {

                        DeleteWIRHeaderAsync();
                    }
                    return true;
                case Keys.F5:
                    sbLoadDataForGridWIRDetailAsync(textEditWIRNumber.Text);
                    return true;
                case Keys.F7:
                    try
                    {
                        Application.UseWaitCursor = true;
                        selectFileExcelAsync();
                    }
                    finally
                    {
                        if (gridViewWIRDetail.GetRowCellValue(0, "PartNumber") != "")
                        {
                            BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Tải thành công file excel");
                        }
                        Application.UseWaitCursor = false;
                    }
                    return true;
                case Keys.F10:
                    if (WMPublic.sbMessageCloseTab(this) == true)
                    {
                        XtraTabPage tabPage;
                        tabPage = (XtraTabPage)(this.Parent);
                        XtraTabControl tabControl;
                        tabControl = (XtraTabControl)tabPage.Parent;
                        tabControl.TabPages.Remove(tabPage);
                        tabPage.Dispose();
                    }
                    return true;
                case Keys.S | Keys.Control:
                    //createWIRHeaderFromForm();
                    return true;
                case Keys.D | Keys.Control:
                    //deleteWIRDetailFromForm();
                    return true;
                case Keys.R | Keys.Control:
                    //clearForm();
                    return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        //public async void createWIRHeaderFromForm()
        //{
        //    if (textEditWIRNumber.Text.Equals(""))
        //    {
        //        MessageBox.Show("Số phiếu yêu cầu không đc để trống");
        //        return;
        //    }

        //    WIRHeader wIRHeader = new WIRHeader();
        //    wIRHeader.WIRNumber = textEditWIRNumber.Text;
        //    wIRHeader.WIRDate = DateTime.Parse(dateEditWIRDate.DateTime.ToString("yyyy-MM-dd"));
        //    wIRHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex).ToString();
        //    wIRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
        //    wIRHeader.BranchID = (comboBoxEditBranch.SelectedIndex).ToString();
        //    wIRHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
        //    wIRHeader.Note = "";
        //    wIRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
        //    wIRHeader.CreatedUserID = WMMessage.User.UserID;
        //    wIRHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
        //    wIRHeader.Status = "1";

        //    WIRHeader wIRHeader1 = await wIRHeaderRepository.GetUnderID(textEditWIRNumber.Text);
        //    if (wIRHeader1 != null)
        //    {
        //        if (await wIRHeaderRepository.Update(wIRHeader, textEditWIRNumber.Text) > 0)
        //        {
        //            createWIRDetailFromForm();

        //        }
        //    }
        //    else
        //    {
        //        if (await wIRHeaderRepository.Create(wIRHeader) > 0)
        //        {
        //            createWIRDetailFromForm();

        //        }
        //    }
        //}


        //private async void createWIRDetailFromForm()
        //{
        //    List<WIRDetail> wIRDetails = await wIRDetailRepository.GetByPalletIDAndLotID(textEditWIRNumber.Text, textEditPalletID.Text, comboBoxEditLotID.Text);
        //    List<WIRDetail> wIRDetails1 = await wIRDetailRepository.GetUnderID(textEditWIRNumber.Text);

        //    if (wIRDetails.Count > 0)
        //    {
        //        WIRDetail wIRDetail = wIRDetails[0];
        //        wIRDetail.WIRNumber = textEditWIRNumber.Text;
        //        wIRDetail.PartNumber = textEditPartNumber.Text;
        //        wIRDetail.PartName = textEditPartName.Text;


        //        wIRDetail.TotalQuantity = Decimal.Parse(textEditQuantity.Text);
        //        wIRDetail.QuantityByItem = Decimal.Parse(textEditQuantity.Text);

        //        List<Goods> goodss = await goodsRepository.getAll();
        //        foreach (Goods goods in goodss)
        //        {
        //            if (goods.PalletID.Equals(textEditPalletID.Text))
        //            {
        //                wIRDetail.PackingVolume = goods.Quantity;
        //                wIRDetail.QuantityByPack = wIRDetail.QuantityByItem / wIRDetail.PackingVolume;
        //                break;
        //            }
        //        }


        //        wIRDetail.LotID = comboBoxEditLotID.Text;
        //        wIRDetail.PalletID = textEditPalletID.Text;
        //        wIRDetail.MFGDate = DateTime.Parse(dateEditMFGDate.DateTime.ToString("yyyy-MM-dd"));
        //        wIRDetail.EXPDate = DateTime.Parse(dateEditEXPDate.DateTime.ToString("yyyy-MM-dd"));
        //        wIRDetail.InputDate = DateTime.Parse(dateEditInputDate.DateTime.ToString("yyyy-MM-dd"));
        //        wIRDetail.Remark = textEditRemark.Text;
        //        wIRDetail.Unit = comboBoxEditUnit.Text;
        //        wIRDetail.PONumber = textEditPONumber.Text;
        //        wIRDetail.Status = "0";
        //        wIRDetail.ProductFamily = textEditProductFamily.Text;
        //        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
        //        {
        //            if (await wIRDetailRepository.Update(wIRDetail, textEditWIRNumber.Text, textEditPalletID.Text, wIRDetail.Ordinal, wIRDetail.LotID) > 0)
        //            {

        //                sbLoadDataForGridWIRDetailAsync(textEditWIRNumber.Text);
        //                WMPublic.sbMessageSaveChangeSuccess(this);
        //            }
        //        }
        //    }
        //    else
        //    {

        //        WIRDetail wIRDetail = new WIRDetail();
        //        wIRDetail.Ordinal = wIRDetails1.Count + 1;
        //        wIRDetail.WIRNumber = textEditWIRNumber.Text;
        //        wIRDetail.PartNumber = textEditPartNumber.Text;
        //        wIRDetail.PartName = textEditPartName.Text;


        //        wIRDetail.TotalQuantity = Decimal.Parse(textEditQuantity.Text);
        //        wIRDetail.QuantityByItem = Decimal.Parse(textEditQuantity.Text);

        //        List<Goods> goodss = await goodsRepository.getAll();
        //        foreach (Goods goods in goodss)
        //        {
        //            if (goods.PalletID.Equals(textEditPalletID.Text))
        //            {
        //                wIRDetail.PackingVolume = goods.Quantity;
        //                wIRDetail.QuantityByPack = wIRDetail.QuantityByItem / wIRDetail.PackingVolume;
        //                break;
        //            }
        //        }


        //        wIRDetail.LotID = comboBoxEditLotID.Text;
        //        wIRDetail.PalletID = textEditPalletID.Text;
        //        wIRDetail.MFGDate = DateTime.Parse(dateEditMFGDate.DateTime.ToString("yyyy-MM-dd"));
        //        wIRDetail.EXPDate = DateTime.Parse(dateEditEXPDate.DateTime.ToString("yyyy-MM-dd"));
        //        wIRDetail.InputDate = DateTime.Parse(dateEditInputDate.DateTime.ToString("yyyy-MM-dd"));
        //        wIRDetail.Remark = textEditRemark.Text;
        //        wIRDetail.Unit = comboBoxEditUnit.Text;
        //        wIRDetail.PONumber = textEditPONumber.Text;
        //        wIRDetail.Status = "0";
        //        wIRDetail.ProductFamily = textEditProductFamily.Text;
        //        if (WMPublic.sbMessageSaveNewRequest(this) == true)
        //        {
        //            if (await wIRDetailRepository.Create(wIRDetail) > 0)
        //            {

        //                sbLoadDataForGridWIRDetailAsync(textEditWIRNumber.Text);
        //                WMPublic.sbMessageSaveNewSuccess(this);
        //            }
        //        }
        //    }
        //    await checkExistWIRNumber(textEditWIRNumber.Text);

        //}

        private async void deleteWIRDetailFromForm()
        {
            List<WIRDetail> wIRDetails = await wIRDetailRepository.GetByWIRNumberAndPalletID(textEditWIRNumber.Text, textEditPalletID.Text);
            if (wIRDetails.Count > 0)
            {
                if (WMPublic.sbMessageDeleteRequest(this) == true)
                {
                    if (await wIRDetailRepository.Delete3(textEditWIRNumber.Text, textEditPalletID.Text, wIRDetails[0].Ordinal) > 0)
                    {
                        BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, WMMessage.msgDeleteSuccess);
                        sbLoadDataForGridWIRDetailAsync(textEditWIRNumber.Text);
                    }
                }
            }
        }

        //private void clearForm()
        //{
        //    textEditPalletID.Text = "";
        //    textEditPartNumber.Text = "";
        //    textEditPartName.Text = "";
        //    textEditProductFamily.Text = "";
        //    comboBoxEditLotID.Controls.Clear();
        //    comboBoxEditUnit.Controls.Clear();
        //    textEditQuantity.Text = "";
        //    textEditPONumber.Text = "";
        //    dateEditMFGDate.Text = "";
        //    dateEditEXPDate.Text = "";
        //    dateEditInputDate.Text = "";
        //    textEditPartNumber.Focus();
        //}

        private void simpleButtonSave_Click(object sender, EventArgs e)
        {
            //createWIRHeaderFromForm();
        }

        private void simpleButtonClear_Click(object sender, EventArgs e)
        {
            //clearForm();
        }

        private void simpleButtonDelete_Click(object sender, EventArgs e)
        {
            //deleteWIRDetailFromForm();
        }

        private void gridViewWIRDetail_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if ((string)(sender as GridView).GetFocusedRowCellValue("PartNumber") != null)
            {

                //textEditPartName.Text = (string)(sender as GridView).GetFocusedRowCellValue("PartName");
                //comboBoxEditUnit.Text = (string)(sender as GridView).GetFocusedRowCellValue("Unit");
                //textEditProductFamily.Text = (string)(sender as GridView).GetFocusedRowCellValue("ProductFamily");
                //textEditQuantity.Text = (string)(sender as GridView).GetFocusedRowCellValue("QuantityByItem").ToString();
                //textEditPalletID.Text = (string)(sender as GridView).GetFocusedRowCellValue("PalletID");
                //comboBoxEditLotID.Text = (string)(sender as GridView).GetFocusedRowCellValue("LotID");
                //textEditPONumber.Text = (string)(sender as GridView).GetFocusedRowCellValue("PONumber");
                //textEditRemark.Text = (string)(sender as GridView).GetFocusedRowCellValue("Remark");
                //dateEditMFGDate.Text = DateTime.Parse((sender as GridView).GetFocusedRowCellValue("MFGDate").ToString()).ToString("yyyy-MM-dd");
                //dateEditEXPDate.Text = DateTime.Parse((sender as GridView).GetFocusedRowCellValue("EXPDate").ToString()).ToString("yyyy-MM-dd");
                //dateEditInputDate.Text = DateTime.Parse((sender as GridView).GetFocusedRowCellValue("InputDate").ToString()).ToString("yyyy-MM-dd");
            }
        }





        //private async void textEditLotID_EditValueChanged(object sender, EventArgs e)
        //{
        //    TextEdit textEditor = (TextEdit)sender;

        //    String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
        //    Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
        //    Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
        //    Int16 mLastMonth;
        //    Int16 mLastYear;

        //    if (mMonth == 1)
        //    {
        //        mLastMonth = 12;
        //        mLastYear = (short)(mYear - 1);
        //    }
        //    else
        //    {
        //        mLastMonth = (short)(mMonth - 1);
        //        mLastYear = mYear;
        //    }

        //    List<InventoryDetail> inventoryDetails2 = await inventoryDetailRepository.GetByParams3(mYear, mMonth, "1");
        //    List<InventoryDetail> inventoryDetailsLastMonth = await inventoryDetailRepository.GetByParams62(mLastYear, mLastMonth, "1", textEditPartNumber.Text,textEditPalletID.Text,comboBoxEditLotID.Text);
        //    List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams62(mYear, mMonth, "1", textEditPartNumber.Text, textEditPalletID.Text, comboBoxEditLotID.Text);

        //    if (inventoryDetails2.Count>0)
        //    {
        //        if (inventoryDetailsLastMonth.Count > 0 && inventoryDetails.Count == 0)
        //        {
        //            textEditor.BackColor = System.Drawing.Color.Red;
        //            return;
        //        }
        //    }


        //    List<InventoryDetail> inventoryDetails1 = await inventoryDetailRepository.GetAll();
        //    var querys = inventoryDetails1.Where(a =>a.PartNumber.Equals(textEditPartNumber.Text)&&a.PalletID.Equals(textEditPalletID.Text)&& a.LotID.Equals(comboBoxEditLotID.Text)).GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID })
        //                 .Select(x => new InventoryDetail
        //                 {
        //                     PartNumber = x.Key.PartNumber,
        //                     PalletID = x.Key.PalletID,
        //                     LotID = x.Key.LotID,
        //                     MFGDate = x.First().MFGDate,
        //                     EXPDate = x.First().EXPDate,
        //                     InputDate = x.First().InputDate,
        //                     Quantity = x.Sum(y => y.Quantity)

        //                 }).LastOrDefault();
        //    if (Equals(textEditor.EditValue, "") ||querys == null)
        //    {
        //        textEditor.BackColor = System.Drawing.Color.Red;
        //    }
        //    else
        //    {
        //        textEditor.BackColor = System.Drawing.Color.White;
        //    }
        //}

        //private async void comboBoxEditLotID_SelectedValueChanged(object sender, EventArgs e)
        //{
        //    EnumConverter ec = TypeDescriptor.GetConverter(typeof(FontStyle)) as EnumConverter;
        //    var cBox = sender as ComboBoxEdit;
        //    if (cBox.SelectedIndex != -1)
        //    {
        //        //textEditPartName.Text = "";
        //        //comboBoxEditUnit.SelectedIndex = 0; ;
        //        //textEditProductFamily.Text = "";
        //        //Goods goods = await goodsRepository.GetUnderID(textEditPartNumber.Text.ToString(), textEditPalletID.Text.ToString());
        //        //if (goods != null)
        //        //{
        //        //    textEditPartName.Text = goods.PartName;
        //        //    comboBoxEditUnit.Text = goods.Unit;
        //        //    textEditProductFamily.Text = goods.ProductFamily;
        //        //}
        //        String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
        //        Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
        //        Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));

        //        List<InventoryDetail> inventorys = await inventoryDetailRepository.GetByParams4(mYear, mMonth, "1", textEditPartNumber.Text);
        //        if (inventorys.Count > 0)
        //        {
        //            var querys = inventorys.Where(a => a.LotID.Equals(comboBoxEditLotID.Text)).GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID })
        //                 .Select(x => new InventoryDetail
        //                 {
        //                     PartNumber = x.Key.PartNumber,
        //                     PalletID = x.Key.PalletID,
        //                     LotID = x.Key.LotID,
        //                     MFGDate = x.First().MFGDate,
        //                     EXPDate = x.First().EXPDate,
        //                     InputDate = x.First().InputDate,
        //                     Quantity = x.Sum(y => y.Quantity)

        //                 })
        //                .ToList();
        //            if (querys.Count > 0)
        //            {
        //                textEditQuantity.Text = querys[0].Quantity + "";
        //            }
        //            else
        //            {
        //                MessageBox.Show("Lot ID is incorrect!!!");
        //            }

        //            try
        //            {
        //                dateEditMFGDate.Text = DateTime.Parse(querys[0].MFGDate + "").ToString("yyyy-MM-dd");
        //                dateEditEXPDate.Text = DateTime.Parse(querys[0].EXPDate + "").ToString("yyyy-MM-dd");
        //                dateEditInputDate.Text = DateTime.Parse(querys[0].InputDate + "").ToString("yyyy-MM-dd");
        //            }
        //            catch (Exception ex)
        //            {
        //                MessageBox.Show(ex.Message);
        //            }
        //        }
        //        textEditQuantity.Focus();
        //    }
        //}
    }
}
