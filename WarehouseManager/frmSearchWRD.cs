﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchWRD : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        OrderStatusRepository orderStatusRepository;
        BranchRepository branchRepository;
        WRDataHeaderRepository wRDataHeaderRepository;
        public frmSearchWRD()
        {
            InitializeComponent();
            this.Load += frmSearchWRR_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wRDataHeaderRepository = new WRDataHeaderRepository();
            orderStatusRepository = new OrderStatusRepository();
        }

        private void frmSearchWRR_Load(object sender, EventArgs e)
        {
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            popupMenuSelectOrderStatus();
            sbLoadDataForGridWRDHeaderAsync();
            gridViewWRDataHeader.DoubleClick += dataGridView_CellDoubleClick;
            simpleButtonFilter.Click += filterButton_Click;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            gridViewWRDataHeader.CustomDrawCell += grdData_CustomDrawCell;

            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        public void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridWRDHeaderAsync();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridWRDHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
                this.Dispose();
            }
            if (e.KeyCode == Keys.F)
            {
                filter();
            }
        }

        private void popupMenuSelectHandlingStatus()
        {
            List<string> list = Helpers.Constant.handlingStatuss;
            foreach (string s in list)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(s);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

        private void popupMenuSelectBranch()
        {
            List<string> list = Helpers.Constant.branchs;
            foreach (string s in list)
            {
                comboBoxEditBranch.Properties.Items.Add(s);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private async void popupMenuSelectOrderStatus()
        {
            List<OrderStatus> orderStatuses = await orderStatusRepository.GetAll();
            foreach (OrderStatus orderStatus in orderStatuses)
            {
                comboBoxEditOrderStatus.Properties.Items.Add(orderStatus.OrderStatusName);
            }
            comboBoxEditOrderStatus.SelectedIndex = 0;
        }

        private async void sbLoadDataForGridWRDHeaderAsync()
        {
            List<WRDataHeader> wRDataHeaders = await wRDataHeaderRepository.GetAll();
            gridControlWRDataHeader.DataSource =wRDataHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewWRDataHeader.RowCount > 0)
            {
                WRDataHeader wRDataHeader = await wRDataHeaderRepository.GetUnderID((string)(sender as GridView).GetFocusedRowCellValue("WRDNumber"));
                ReceiptDataControl.selectedWRDNumber = (string)(sender as GridView).GetFocusedRowCellValue("WRDNumber");
                this.Close();
            }
        }

        private async void filter()
        {
            String branchID = "<>";
            String handlingStatusID = "<>";
            if (!comboBoxEditBranch.SelectedItem.ToString().Equals("<>"))
            {
                branchID = comboBoxEditBranch.SelectedIndex.ToString();
            }
            if (!comboBoxEditHandlingStatus.SelectedItem.ToString().Equals("<>"))
            {
                handlingStatusID = comboBoxEditHandlingStatus.SelectedIndex.ToString();
            }
            String orderStatusID = (comboBoxEditOrderStatus.SelectedIndex - 1).ToString();
            DateTime fromDate = DateTime.Parse(dateEditFromDate.Text);
            DateTime toDate = DateTime.Parse(dateEditToDate.Text);

            List<WRDataHeader> wRDataHeaders = new List<WRDataHeader>();

            wRDataHeaders = await wRDataHeaderRepository.FilterFinal(branchID, fromDate, toDate, handlingStatusID);
            gridControlWRDataHeader.DataSource = wRDataHeaders;
        }

        private void filterButton_Click(Object sender,EventArgs e)
        {
            filter();
        }

        private void grdData_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "TotalQuantity", false) == 0))
            {
                double mTotalQuantity = ((gridViewWRDataHeader.GetRowCellValue(i, gridViewWRDataHeader.Columns["TotalQuantity"]).ToString() == "")? 0.0 : Convert.ToDouble(gridViewWRDataHeader.GetRowCellValue(i, gridViewWRDataHeader.Columns["TotalQuantity"])));
                double mTotalTotalQuantityOrg = ((gridViewWRDataHeader.GetRowCellValue(i, gridViewWRDataHeader.Columns["TotalQuantityOrg"]).ToString() == "") ? 0.0 : Convert.ToDouble(gridViewWRDataHeader.GetRowCellValue(i, gridViewWRDataHeader.Columns["TotalQuantityOrg"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mTotalQuantity == mTotalTotalQuantityOrg && mTotalQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                }
                if (mTotalQuantity > mTotalTotalQuantityOrg && mTotalQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                }
                if (mTotalQuantity < mTotalTotalQuantityOrg && mTotalQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                }
            }
        }

    }
}
