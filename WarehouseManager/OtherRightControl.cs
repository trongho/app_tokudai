﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;

namespace WarehouseManager
{
    public partial class OtherRightControl : DevExpress.XtraEditors.XtraUserControl
    {
        public OtherRightControl()
        {
            InitializeComponent();
            this.Load += OtherRightControl_Load;

        }

        private void OtherRightControl_Load(Object sender,EventArgs e)
        {
            sbLoadDataForGridRightAsync();
        }

        private async Task sbLoadDataForGridRightAsync()
        {
            string URI = "https://localhost:5001/api/right";
            List<Right> rights = new List<Right>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            gridControl1.DataSource = JsonConvert.DeserializeObject<Right[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }


            int i = 0;
            gridView1.SetRowCellValue(i, gridView1.Columns["No"], i + 1);
        }
    }
}
