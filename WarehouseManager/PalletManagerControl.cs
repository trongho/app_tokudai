﻿using DevExpress.Export;
using DevExpress.Export.Xl;
using DevExpress.Printing.ExportHelpers;
using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Accessibility;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Card;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPrinting;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using OfficeOpenXml.Style;
using QRCoder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using WarehouseManager.Helpers;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class PalletManagerControl : DevExpress.XtraEditors.XtraUserControl
    {
        public static string selecttedID = "";
        PalletHeaderRepository palletHeaderRepository = null;
        PalletDetailRepository palletDetailRepository = null;
        InventoryRepository inventoryRepository = null;
        InventoryDetailRepository inventoryDetailRepository = null;
        InventoryOfLastMonthsRepository inventoryOfLastMonthsRepository = null;
        InventoryOfLastMonthDetailRepository inventoryOfLastMonthDetailRepository = null;
        GoodsRepository goodsRepository = null;

        Int16 SaveNewRight = 1;
        Int16 SaveChangeRight = 1;

        internal MainForm m_MainForm;

        public PalletManagerControl(MainForm mainForm)
        {
            InitializeComponent();
            m_MainForm = mainForm;
            BottomBarHelper.normalMessage(m_MainForm.barStaticItemMessage, "!!!");
            updateLanguage();
            palletHeaderRepository = new PalletHeaderRepository();
            palletDetailRepository = new PalletDetailRepository();
            inventoryRepository = new InventoryRepository();
            inventoryDetailRepository = new InventoryDetailRepository();
            inventoryOfLastMonthsRepository = new InventoryOfLastMonthsRepository();
            inventoryOfLastMonthDetailRepository = new InventoryOfLastMonthDetailRepository();
            goodsRepository = new GoodsRepository();
            this.Load += PalletManagerControl_Load;
            windowsUIButtonPanel2.ButtonClick += button_Click;


        }

        private void PalletManagerControl_Load(Object sender, EventArgs e)
        {

            popupMenuSelectHandlingStatus();
            popupMenuSelectReceiptDate();
            gridViewWRDataGeneral.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            checkRightOnScreen();

        }

        private void updateLanguage()
        {
            ResourceManager rm;
            String cultureName = WMMessage.msgLanguage;
            if (cultureName.Equals("vi-VN"))
            {
                rm = new
                   ResourceManager("WarehouseManager.Resource.vi_local", typeof(UserManagerControl).Assembly);
            }
            else if (cultureName.Equals("en-US"))
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.en_local", typeof(UserManagerControl).Assembly);
            }
            else
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.cn_local", typeof(UserManagerControl).Assembly);
            }

            //WindowsUIButton buttonAdd = windowsUIButtonPanel1.Buttons[0] as WindowsUIButton;
            ((WindowsUIButton)windowsUIButtonPanel2.Buttons[0]).Caption = rm.GetString("addnew");
            ((WindowsUIButton)windowsUIButtonPanel2.Buttons[2]).Caption = rm.GetString("save");
            ((WindowsUIButton)windowsUIButtonPanel2.Buttons[4]).Caption = rm.GetString("delete");
            ((WindowsUIButton)windowsUIButtonPanel2.Buttons[6]).Caption = rm.GetString("refesh");
            ((WindowsUIButton)windowsUIButtonPanel2.Buttons[8]).Caption = rm.GetString("import_data");
            ((WindowsUIButton)windowsUIButtonPanel2.Buttons[10]).Caption = rm.GetString("export_data");
            ((WindowsUIButton)windowsUIButtonPanel2.Buttons[12]).Caption = rm.GetString("export_po");
            ((WindowsUIButton)windowsUIButtonPanel2.Buttons[14]).Caption = rm.GetString("exit");



            gridViewWRDataGeneral.Columns["IDCode"].Caption = rm.GetString("idcode");
            gridViewWRDataGeneral.Columns["Quantity"].Caption = rm.GetString("quantity");

        }


        private void checkRightOnScreen()
        {
            int findIndex = WMMessage.UserRightOnScreens.FindIndex(x => x.ScreenID.Equals("frmPalletData"));
            if (WMMessage.UserRightOnScreens[findIndex].SaveNewRight == 0 && WMMessage.UserRightOnScreens[findIndex].SaveChangeRight == 0)
            {
                ((WindowsUIButton)windowsUIButtonPanel2.Buttons[2]).Enabled = false;
            }
            else if (WMMessage.UserRightOnScreens[findIndex].SaveNewRight == 0 && WMMessage.UserRightOnScreens[findIndex].SaveChangeRight == 1)
            {
                SaveNewRight = 0;
            }
            else if (WMMessage.UserRightOnScreens[findIndex].SaveNewRight == 1 && WMMessage.UserRightOnScreens[findIndex].SaveChangeRight == 0)
            {
                SaveChangeRight = 0;
            }

            if (WMMessage.UserRightOnScreens[findIndex].DeleteRight == 0)
            {
                ((WindowsUIButton)windowsUIButtonPanel2.Buttons[4]).Enabled = false;
            }
            if (WMMessage.UserRightOnScreens[findIndex].ImportRight == 0)
            {
                ((WindowsUIButton)windowsUIButtonPanel2.Buttons[10]).Enabled = false;
            }
            if (WMMessage.UserRightOnScreens[findIndex].PrintRight == 0)
            {
                ((WindowsUIButton)windowsUIButtonPanel2.Buttons[14]).Enabled = false;
            }
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewWRDataGeneral.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void popupMenuSelectHandlingStatus()
        {
            List<string> list = Helpers.Constant.handlingStatuss;
            foreach (string s in list)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(s);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

        private void popupMenuSelectReceiptDate()
        {

            comboBoxEditReceiptDate.Properties.Items.Add("<>");

            comboBoxEditReceiptDate.SelectedIndex = 0;
        }



        private async void sbLoadDataForGrid(String id)
        {
            List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(id);
            gridControlWRDataDetail.DataSource = palletDetails;
            getPackingQuantity();

            //comboBoxEditReceiptDate.Properties.Items.Add("<>");

            //if (palletDetails.Count > 0)
            //{
            //    List<PalletDetail> groups = palletDetails.GroupBy(g => g.ReceiptDate)
            //        .Select(s => new PalletDetail
            //        {
            //            ReceiptDate = s.Key,
            //        }).ToList();
            //    foreach (PalletDetail palletDetail in groups)
            //    {
            //        comboBoxEditReceiptDate.Properties.Items.Add(palletDetail.ReceiptDate);

            //    }
            //}

            //comboBoxEditReceiptDate.SelectedIndex = 0;
        }


        private async void getPackingQuantity()
        {
            Decimal total = 0;
            List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(textEditPalletID.Text);
            total += Convert.ToDecimal(palletDetails.Count);
            textEditTotalIDCode.Text = total.ToString("0.#####");
        }

        private async void deletePalletHeader(string palletID)
        {
            if (await palletHeaderRepository.Delete(palletID) > 0)
            {
                BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, "Xóa thành công pallet " + palletID);
                gridControlWRDataDetail.DataSource = null;
                textEditPalletID.Text = "";
                comboBoxEditHandlingStatus.SelectedIndex = 0;
                textEditTotalIDCode.Text = "";
                dateEditCreatedDate.Text = "";
            }
        }

        private async void deletePalletDetail(string palletID)
        {
            await palletDetailRepository.Delete(palletID);

        }


        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {

                    }
                    break;
                case "save":

                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updatePalletHeader();
                    }
                    break;
                case "delete":
                    if (textEditPalletID.Text.Equals(""))
                    {
                        BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Mã pallet trống");
                        return;
                    }
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        deletePalletDetail(textEditPalletID.Text);
                        deletePalletHeader(textEditPalletID.Text);
                    }
                    break;
                case "search":

                    break;
                case "refesh":
                    sbLoadDataForGrid(textEditPalletID.Text);
                    break;
                case "import":
                    break;
                case "export":
                    using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Excel Workbook|*.xlsx", FileName = textEditPalletID.Text })
                    {
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            string path = sfd.FileName;
                            ExportSettings.DefaultExportType = ExportType.DataAware;
                            // Create a new object defining how a document is exported to the XLSX format.
                            var options = new XlsxExportOptionsEx();
                            // Specify a name of the sheet in the created XLSX file.
                            options.SheetName = textEditPalletID.Text;

                            // Subscribe to export customization events. 
                            //options.CustomizeSheetSettings += options_CustomizeSheetSettings;
                            //options.CustomizeSheetHeader += options_CustomizeSheetHeader;
                            options.CustomizeCell += options_CustomizeCell;
                            options.CustomizeSheetFooter += options_CustomizeSheetFooter;
                            //options.AfterAddRow += options_AfterAddRow;
                            options.TextExportMode = TextExportMode.Text;

                            gridControl1.BeginUpdate();
                            gridViewWRDataGeneral.Columns["ReceiptDate"].Visible = false;

                            gridControlWRDataDetail.ExportToXlsx(path, options);

                            // Open the created XLSX file with the default application.

                            gridViewWRDataGeneral.Columns["ReceiptDate"].Visible = true;
                            gridControl1.EndUpdate();

                            Process.Start(path);
                        }
                    }
                    break;
                case "export_po":
                    exportExcelAsync();
                    break;
                case "print":

                    break;
                case "close":

                    break;
            }

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {

            switch (keyData)
            {
                case Keys.F2:
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {

                    }
                    return true;
                case Keys.F3:
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updatePalletHeader();
                    }
                    return true;
                case Keys.F4:
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {

                    }
                    return true;
                case Keys.F5:
                    sbLoadDataForGrid(textEditPalletID.Text);
                    return true;
                case Keys.F8:
                    using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Excel Workbook|*.xlsx", FileName = textEditPalletID.Text })
                    {
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            string path = sfd.FileName;
                            ExportSettings.DefaultExportType = ExportType.DataAware;
                            // Create a new object defining how a document is exported to the XLSX format.
                            var options = new XlsxExportOptionsEx();
                            // Specify a name of the sheet in the created XLSX file.
                            options.SheetName = textEditPalletID.Text;

                            // Subscribe to export customization events. 
                            //options.CustomizeSheetSettings += options_CustomizeSheetSettings;
                            //options.CustomizeSheetHeader += options_CustomizeSheetHeader;
                            options.CustomizeCell += options_CustomizeCell;
                            options.CustomizeSheetFooter += options_CustomizeSheetFooter;
                            //options.AfterAddRow += options_AfterAddRow;
                            gridControlWRDataDetail.ExportToXlsx(path, options);

                            // Open the created XLSX file with the default application.
                            Process.Start(path);
                        }
                    }
                    return true;
                case Keys.F9:
                    ShowGridPreview(gridControlWRDataDetail);
                    return true;
                case Keys.F10:
                    if (WMPublic.sbMessageCloseTab(this) == true)
                    {
                        XtraTabPage tabPage;
                        tabPage = (XtraTabPage)(this.Parent);
                        XtraTabControl tabControl;
                        tabControl = (XtraTabControl)tabPage.Parent;
                        tabControl.TabPages.Remove(tabPage);
                        tabPage.Dispose();
                    }
                    return true;
                case Keys.O | Keys.Control:

                    return true;
                case Keys.P | Keys.Control:

                    return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void ShowGridPreview(GridControl grid)
        {
            if (!grid.IsPrintingAvailable)
            {
                BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "The 'DevExpress.XtraPrinting' library is not found");
                return;
            }

            gridViewWRDataGeneral.OptionsPrint.AutoWidth = true;
            gridViewWRDataGeneral.OptionsPrint.UsePrintStyles = true;
            // Open the Preview window.
            grid.ShowPrintPreview();
        }


        #region #CustomizeSheetFooterEvent
        void options_CustomizeSheetFooter(ContextEventArgs e)
        {
            // Add an empty row to the document's footer.
            e.ExportContext.AddRow();

            // Create a new row.
            var firstRow = new CellObject();
            // Specify row values.
            firstRow.Value = @"Tổng số thùng: " + gridViewWRDataGeneral.RowCount;
            // Specify the cell content alignment and font settings.
            var rowFormatting = CreateXlFormattingObject(true, 16);
            rowFormatting.Font.Size = 14;
            rowFormatting.Font.Bold = false;
            rowFormatting.Font.Italic = true;
            rowFormatting.Alignment.HorizontalAlignment = XlHorizontalAlignment.Left;
            firstRow.Formatting = rowFormatting;
            // Add the created row to the output document. 
            e.ExportContext.AddRow(new[] { firstRow });
        }

        // Specify a cell's alignment and font settings. 
        static XlFormattingObject CreateXlFormattingObject(bool bold, double size)
        {
            var cellFormat = new XlFormattingObject
            {
                Font = new XlCellFont
                {
                    Bold = bold,
                    Size = size
                },
                Alignment = new XlCellAlignment
                {
                    RelativeIndent = 10,
                    HorizontalAlignment = XlHorizontalAlignment.Center,
                    VerticalAlignment = XlVerticalAlignment.Center
                }
            };
            return cellFormat;
        }
        #endregion #CustomizeSheetFooterEvent

        #region #CustomizeCellEvent
        // Specify the value alignment for Discontinued field.
        XlCellAlignment aligmentForDiscontinuedColumn = new XlCellAlignment()
        {
            HorizontalAlignment = XlHorizontalAlignment.Center,
            VerticalAlignment = XlVerticalAlignment.Center
        };

        void options_CustomizeCell(CustomizeCellEventArgs e)
        {

            // Substitute Boolean values within the Discontinued column by special symbols.          
            e.Handled = true;
            e.Formatting.Alignment = aligmentForDiscontinuedColumn;

        }
        #endregion #CustomizeCellEvent




        private void QRCodeGeneral(String code)
        {
            QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.Q;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, eccLevel);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeBitmap = qrCode.GetGraphic(2, System.Drawing.Color.Black, System.Drawing.Color.White, new Bitmap(10, 10));
            pictureBox1.Image = qrCodeBitmap;
            Helpers.ImageHelper.resizeImage(pictureBox1.Image, new Size(100, 100));
            pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
        }

        private async void textEditPalletID_DoubleClick(object sender, EventArgs e)
        {
            frmSearchPalletHeader frmSearch = new frmSearchPalletHeader(this);
            frmSearch.ShowDialog(this);
            frmSearch.Dispose();
            if (!selecttedID.Equals(""))
            {
                textEditPalletID.Text = selecttedID;
                PalletHeader palletHeader = await palletHeaderRepository.GetUnderID(textEditPalletID.Text);
                if (palletHeader != null)
                {
                    dateEditCreatedDate.Text = DateTime.Parse(palletHeader.CreatedDate.ToString()).ToShortDateString();
                    comboBoxEditHandlingStatus.SelectedIndex = palletHeader.HandlingStatusID.Equals("<>") ? 0 : int.Parse(palletHeader.HandlingStatusID) + 1;
                    sbLoadDataForGrid(textEditPalletID.Text);

                    List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(textEditPalletID.Text);
                    if (palletDetails.Count > 0)
                    {
                        var query = palletDetails.GroupBy(g => g.ReceiptDate).Select(s => new PalletDetail
                        {
                            ReceiptDate = s.Key,
                        });
                        foreach (PalletDetail palletDetail in query)
                        {
                            if (!string.IsNullOrEmpty(palletDetail.ReceiptDate.ToString())){
                                if (!comboBoxEditReceiptDate.Properties.Items.Contains(DateTime.Parse(palletDetail.ReceiptDate.ToString()).ToString("yyyy-MM-dd")))
                                {
                                    comboBoxEditReceiptDate.Properties.Items.Add(DateTime.Parse(palletDetail.ReceiptDate.ToString()).ToString("yyyy-MM-dd"));
                                }
                            }                        
                        }
                        comboBoxEditReceiptDate.SelectedIndex = 0;
                    }
                }
                QRCodeGeneral(selecttedID);
            }
        }

        public async Task updatePalletHeader()
        {

            PalletHeader palletHeader = await palletHeaderRepository.GetUnderID(textEditPalletID.Text);
            if (palletHeader != null)
            {
                if (palletHeader.HandlingStatusID == "2")
                {
                    MessageBox.Show("Dữ liệu đuyệt mức 2 không thể chỉnh sửa");
                    return;
                }

                palletHeader.HandlingStatusID = comboBoxEditHandlingStatus.SelectedIndex == 0 ? "<>" : (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
                palletHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
                palletHeader.UpdatedUserID = WMMessage.User.UserID;
                palletHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

                if (await palletHeaderRepository.Update(palletHeader, palletHeader.PalletID, palletHeader.Ordinal) > 0)
                {
                    
                    if (comboBoxEditHandlingStatus.SelectedIndex == 3)
                    {
                        string mCurrentDateTime = DateTime.Now.ToString("MM/yyyy");
                        string mCurrentPeriod = String.Format(mCurrentDateTime, "MM/yyyy");
                        int mYear = int.Parse(mCurrentPeriod.Substring(3, 4));
                        int mMonth = int.Parse(mCurrentPeriod.Substring(0, 2));

                        List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(textEditPalletID.Text);
                        if (palletDetails.Count > 0)
                        {
                            updateInventory(palletDetails);
                            updateInventoryDetail(palletDetails);
                            createOrUpdateGoods(palletDetails);
                        }
                        MessageBox.Show("Cập nhật Pallet thành công-> Đã cộng tồn kho");
                    }
                    else
                    {
                        MessageBox.Show("Cập nhật Pallet thành công");
                    }
                }

            }
        }

        private async void updateInventory(List<PalletDetail> palletDetails)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));

            foreach (PalletDetail palletDetail in palletDetails)
            {
                List<Inventory> inventories = await inventoryRepository.GetUnderMultiPartNumber(mYear, mMonth, "1", palletDetail.PartNumber);
                if (inventories.Count > 0)
                {
                    inventories[0].ReceiptQuantity = inventories[0].ReceiptQuantity + palletDetail.Quantity;
                    inventories[0].ClosingStockQuantity = inventories[0].ClosingStockQuantity + palletDetail.Quantity;
                    await inventoryRepository.Update(inventories[0], mYear, mMonth, "1", palletDetail.PartNumber);
                }
                else
                {
                    Inventory inventory = new Inventory();
                    inventory.Year = mYear;
                    inventory.Month = mMonth;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = palletDetail.PartNumber;
                    inventory.PackingVolume = palletDetail.Quantity;
                    inventory.OpeningStockQuantity = 0;
                    inventory.ReceiptQuantity = palletDetail.Quantity;
                    inventory.IssueQuantity = 0;
                    inventory.ClosingStockQuantity = inventory.OpeningStockQuantity + inventory.ReceiptQuantity;
                    inventory.Unit = palletDetail.Unit;
                    await inventoryRepository.Create(inventory);
                }
            }
        }

        private async void updateInventoryDetail(List<PalletDetail> palletDetails)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            foreach (PalletDetail palletDetail in palletDetails)
            {
                List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams6(mYear, mMonth, "1", palletDetail.PartNumber, palletDetail.PalletID, palletDetail.IDCode);
                if (inventoryDetails.Count == 0)
                {
                    InventoryDetail inventoryDetail = new InventoryDetail();
                    inventoryDetail.Year = mYear;
                    inventoryDetail.Month = mMonth;
                    inventoryDetail.WarehouseID = "1";
                    inventoryDetail.PartNumber = palletDetail.PartNumber;
                    inventoryDetail.LotID = palletDetail.LotID;
                    inventoryDetail.IDCode = palletDetail.IDCode;
                    inventoryDetail.MFGDate = palletDetail.MFGDate;
                    inventoryDetail.EXPDate = palletDetail.EXPDate;
                    inventoryDetail.Quantity = palletDetail.Quantity;
                    inventoryDetail.InputDate = DateTime.Now;
                    inventoryDetail.PalletID = palletDetail.PalletID;
                    await inventoryDetailRepository.Create(inventoryDetail);
                }

            }
        }


        private async void createOrUpdateGoods(List<PalletDetail> palletDetails)
        {
            foreach (PalletDetail palletDetail in palletDetails)
            {
                List<Goods> goodss = await goodsRepository.GetUnderID(palletDetail.PartNumber);
                if (goodss.Count > 0)
                {
                    goodss[0].ProductFamily = palletDetail.ProductFamily;
                    goodss[0].PartName = palletDetail.PartName;
                    goodss[0].Unit = palletDetail.Unit;
                    goodss[0].UpdatedUserID = WMMessage.User.UserID;
                    goodss[0].UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                    await goodsRepository.Update(goodss[0], palletDetail.PartNumber, palletDetail.PalletID);
                }
                else
                {
                    Goods goods = new Goods();
                    goods.PartNumber = palletDetail.PartNumber;
                    goods.PalletID = palletDetail.PalletID;
                    goods.ProductFamily = palletDetail.ProductFamily;
                    goods.Ordinal = goodss.Count + 1;
                    goods.PartName = palletDetail.PartName;
                    goods.Unit = palletDetail.Unit;
                    goods.CreatedUserID = WMMessage.User.UserID;
                    goods.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                    goods.Status = "Created";
                    goods.CurrentIDCode = 0;
                    await goodsRepository.Create(goods);
                }
            }

        }



        private async void updateInventory()
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;

            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }



            List<Inventory> inventories = new List<Inventory>();
            List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(textEditPalletID.Text);
            if (palletDetails.Count > 0)
            {
                foreach (PalletDetail palletDetail in palletDetails)
                {

                    Inventory inventory = new Inventory();
                    inventory.Month = mMonth;
                    inventory.Year = mYear;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = palletDetail.PartNumber;
                    inventory.ReceiptQuantity = palletDetail.Quantity;
                    inventory.IssueQuantity = 0;
                    inventory.Status = "1";
                    inventory.PackingVolume = palletDetail.Quantity;
                    inventory.IDCode = "";
                    inventories.Add(inventory);
                }

            }
            var querys = inventories.GroupBy(x => x.PartNumber)
                       .Select(x => new Inventory
                       {
                           PartNumber = x.Key,
                           Year = x.First().Year,
                           Month = x.First().Month,
                           WarehouseID = x.First().WarehouseID,
                           Status = x.First().Status,
                           ReceiptQuantity = x.Sum(y => y.ReceiptQuantity),
                           IssueQuantity = x.Sum(y => y.IssueQuantity),
                           PackingVolume = x.First().PackingVolume,
                           Material = "",
                           IDCode = "",
                       })
                      .ToList();


            await inventoryRepository.DeleteOldDate("1");
            foreach (Inventory inventorys in querys)
            {
                List<Goods> goodsList = await goodsRepository.GetUnderPartNumber(inventorys.PartNumber);
                if (goodsList.Count > 0)
                {
                    inventorys.Mold = goodsList[0].Mold;
                }

                List<Inventory> inventories1 = await inventoryRepository.GetUnderMultiPartNumber(mYear, mMonth, "1", inventorys.PartNumber);

                List<InventoryOfLastMonths> inventoryOfLastMonths = await inventoryOfLastMonthsRepository.GetByPartNumber("1", inventorys.PartNumber);
                if (inventories1.Count > 0)
                {
                    inventorys.ReceiptQuantity = inventorys.ReceiptQuantity + inventories1[0].ReceiptQuantity;
                    inventorys.IssueQuantity = inventorys.IssueQuantity + inventories1[0].IssueQuantity;
                    inventorys.OpeningStockQuantity = inventoryOfLastMonths.Count > 0 ? inventoryOfLastMonths[0].ClosingStockQuantity : 0;
                    inventorys.ClosingStockQuantity = inventories1[0].OpeningStockQuantity + inventorys.ReceiptQuantity - inventorys.IssueQuantity;
                    await inventoryRepository.Update(inventorys, inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber);
                }
                else
                {
                    inventorys.ReceiptQuantity = inventorys.ReceiptQuantity;
                    inventorys.IssueQuantity = inventorys.IssueQuantity;
                    inventorys.OpeningStockQuantity = inventoryOfLastMonths.Count > 0 ? inventoryOfLastMonths[0].ClosingStockQuantity : 0;
                    inventorys.ClosingStockQuantity = inventorys.OpeningStockQuantity + inventorys.ReceiptQuantity - inventorys.IssueQuantity;
                    await inventoryRepository.Create(inventorys);
                }
            }

        }

        private async void updateInventoryDetail()
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;

            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }

            List<InventoryDetail> inventories = new List<InventoryDetail>();
            List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(textEditPalletID.Text);
            if (palletDetails.Count > 0)
            {
                foreach (PalletDetail palletDetail in palletDetails)
                {
                    InventoryDetail inventory = new InventoryDetail();
                    inventory.Month = mMonth;
                    inventory.Year = mYear;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = palletDetail.PartNumber;
                    inventory.PalletID = palletDetail.IDCode.Substring(0, 5);
                    inventory.LotID = palletDetail.LotID;
                    inventory.IDCode = palletDetail.IDCode;
                    inventory.Quantity = palletDetail.Quantity;
                    inventory.Status = "1";
                    inventory.MFGDate = palletDetail.MFGDate;
                    inventory.EXPDate = palletDetail.EXPDate;
                    inventory.InputDate = dateEditCreatedDate.DateTime;
                    inventories.Add(inventory);

                }


            }
            var querys = inventories.GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID, x.IDCode })
                 .Select(x => new InventoryDetail
                 {
                     PartNumber = x.Key.PartNumber,
                     PalletID = x.Key.PalletID,
                     LotID = x.Key.LotID,
                     IDCode = x.Key.IDCode,
                     Year = x.First().Year,
                     Month = x.First().Month,
                     WarehouseID = x.First().WarehouseID,
                     Status = x.First().Status,
                     Quantity = x.First().Quantity,
                     MFGDate = x.First().MFGDate,
                     EXPDate = x.First().EXPDate,
                     InputDate = x.First().InputDate
                 })
                .ToList();


            await inventoryDetailRepository.DeleteOldDate("1");

            foreach (InventoryDetail inventoryDetail in querys)
            {
                List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams6(inventoryDetail.Year, inventoryDetail.Month, inventoryDetail.WarehouseID,
                    inventoryDetail.PartNumber, inventoryDetail.PalletID, inventoryDetail.IDCode);

                if (inventoryDetails.Count == 0)
                {
                    await inventoryDetailRepository.Create(inventoryDetail);
                }
            }
            BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage, m_MainForm.barStaticItemMessage.Caption + "-> Đã cộng tồn kho");
        }

        private async Task createInventoryOfLastMonth()
        {

            await inventoryOfLastMonthsRepository.DeleteOldDate("1");

            List<Inventory> inventories = await inventoryRepository.GetByWarehouseID("1");
            if (inventories.Count > 0)
            {
                foreach (Inventory inventory in inventories)
                {
                    InventoryOfLastMonths inventorys = new InventoryOfLastMonths();
                    inventorys.Year = inventory.Year;
                    inventorys.Month = inventory.Month;
                    inventorys.WarehouseID = inventory.WarehouseID;
                    inventorys.PartNumber = inventory.PartNumber;
                    inventorys.Status = "0";
                    inventorys.PackingVolume = inventory.PackingVolume;
                    inventorys.Material = inventory.Material;
                    inventorys.Mold = inventory.Mold;
                    inventorys.OpeningStockQuantity = inventory.OpeningStockQuantity;
                    inventorys.ReceiptQuantity = inventory.ReceiptQuantity;
                    inventorys.IssueQuantity = inventory.IssueQuantity;
                    inventorys.ClosingStockQuantity = inventory.ClosingStockQuantity;
                    await inventoryOfLastMonthsRepository.Create(inventorys);
                }
            }



        }

        private async Task createInventoryOfLastMonthDetail()
        {
            await inventoryOfLastMonthDetailRepository.DeleteOldDate("1");
            List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByWarehouseID("1");
            if (inventoryDetails.Count > 0)
            {
                foreach (InventoryDetail inventoryDetail in inventoryDetails)
                {
                    InventoryOfLastMonthDetail inventory = new InventoryOfLastMonthDetail();
                    inventory.Month = inventoryDetail.Month;
                    inventory.Year = inventoryDetail.Year;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = inventoryDetail.PartNumber;
                    inventory.PalletID = inventoryDetail.PalletID;
                    inventory.LotID = inventoryDetail.LotID;
                    inventory.IDCode = inventoryDetail.IDCode;
                    inventory.Quantity = inventoryDetail.Quantity;
                    inventory.Status = "1";
                    inventory.MFGDate = inventoryDetail.MFGDate;
                    inventory.EXPDate = inventoryDetail.EXPDate;
                    inventory.InputDate = inventoryDetail.InputDate;
                    await inventoryOfLastMonthDetailRepository.Create(inventory);
                }


            }

        }


        public async Task updatePalletHeaderSubInventory(string palletID)
        {
            PalletHeader palletHeader = await palletHeaderRepository.GetUnderID(palletID);


            palletHeader.HandlingStatusID = "2";
            palletHeader.HandlingStatusName = "Duyệt mức 2";
            palletHeader.UpdatedUserID = WMMessage.User.UserID;
            palletHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));


            if (await palletHeaderRepository.Update(palletHeader, palletHeader.PalletID, palletHeader.Ordinal) > 0)
            {
                string mCurrentDateTime = DateTime.Now.ToString("MM/yyyy");
                string mCurrentPeriod = String.Format(mCurrentDateTime, "MM/yyyy");
                int mYear = int.Parse(mCurrentPeriod.Substring(3, 4));
                int mMonth = int.Parse(mCurrentPeriod.Substring(0, 2));

                List<Inventory> inventories = await inventoryRepository.GetAll();


                if (inventories.Count > 0)
                {
                    if ((inventories[0].Year == mYear && inventories[0].Month < mMonth) || inventories[0].Year < mYear)
                    {
                        subInventory(palletID);
                        subInventoryDetail(palletID);
                    }
                    else
                    {
                        subInventory(palletID);
                        subInventoryDetail(palletID);
                    }
                }
                else
                {
                    subInventory(palletID);
                    subInventoryDetail(palletID);
                }
            }

        }

        private async void addInventory(string palletID)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;

            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }



            List<Inventory> inventories = new List<Inventory>();
            List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(palletID);
            if (palletDetails.Count > 0)
            {
                foreach (PalletDetail palletDetail in palletDetails)
                {

                    Inventory inventory = new Inventory();
                    inventory.Month = mMonth;
                    inventory.Year = mYear;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = palletDetail.PartNumber;
                    inventory.ReceiptQuantity = palletDetail.Quantity;
                    inventory.IssueQuantity = 0;
                    inventory.Status = "1";
                    inventory.PackingVolume = palletDetail.Quantity;
                    inventory.IDCode = "";
                    inventories.Add(inventory);
                }

            }
            var querys = inventories.GroupBy(x => x.PartNumber)
                       .Select(x => new Inventory
                       {
                           PartNumber = x.Key,
                           Year = x.First().Year,
                           Month = x.First().Month,
                           WarehouseID = x.First().WarehouseID,
                           Status = x.First().Status,
                           ReceiptQuantity = x.Sum(y => y.ReceiptQuantity),
                           IssueQuantity = x.Sum(y => y.IssueQuantity),
                           PackingVolume = x.First().PackingVolume,
                           Material = "",
                           IDCode = "",
                       })
                      .ToList();


            await inventoryRepository.DeleteOldDate("1");
            foreach (Inventory inventorys in querys)
            {
                List<Goods> goodsList = await goodsRepository.GetUnderPartNumber(inventorys.PartNumber);
                if (goodsList.Count > 0)
                {
                    inventorys.Mold = goodsList[0].Mold;
                }

                List<Inventory> inventories1 = await inventoryRepository.GetUnderMultiPartNumber(mYear, mMonth, "1", inventorys.PartNumber);

                List<InventoryOfLastMonths> inventoryOfLastMonths = await inventoryOfLastMonthsRepository.GetByPartNumber("1", inventorys.PartNumber);
                if (inventories1.Count > 0)
                {
                    inventorys.ReceiptQuantity = inventorys.ReceiptQuantity + inventories1[0].ReceiptQuantity;
                    inventorys.IssueQuantity = inventorys.IssueQuantity + inventories1[0].IssueQuantity;
                    inventorys.OpeningStockQuantity = inventoryOfLastMonths.Count > 0 ? inventoryOfLastMonths[0].ClosingStockQuantity : 0;
                    inventorys.ClosingStockQuantity = inventories1[0].OpeningStockQuantity + inventorys.ReceiptQuantity - inventorys.IssueQuantity;
                    await inventoryRepository.Update(inventorys, inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber);
                }
                else
                {
                    inventorys.ReceiptQuantity = inventorys.ReceiptQuantity;
                    inventorys.IssueQuantity = inventorys.IssueQuantity;
                    inventorys.OpeningStockQuantity = inventoryOfLastMonths.Count > 0 ? inventoryOfLastMonths[0].ClosingStockQuantity : 0;
                    inventorys.ClosingStockQuantity = inventorys.OpeningStockQuantity + inventorys.ReceiptQuantity - inventorys.IssueQuantity;
                    await inventoryRepository.Create(inventorys);
                }
            }

        }

        private async void subInventory(string palletID)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;

            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }



            List<Inventory> inventories = new List<Inventory>();
            List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(palletID);
            if (palletDetails.Count > 0)
            {
                foreach (PalletDetail palletDetail in palletDetails)
                {

                    Inventory inventory = new Inventory();
                    inventory.Month = mMonth;
                    inventory.Year = mYear;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = palletDetail.PartNumber;
                    inventory.ReceiptQuantity = 0;
                    inventory.IssueQuantity = palletDetail.Quantity;
                    inventory.Status = "1";
                    inventory.PackingVolume = palletDetail.Quantity;
                    inventory.IDCode = "";
                    inventories.Add(inventory);
                }

            }
            var querys = inventories.GroupBy(x => x.PartNumber)
                       .Select(x => new Inventory
                       {
                           PartNumber = x.Key,
                           Year = x.First().Year,
                           Month = x.First().Month,
                           WarehouseID = x.First().WarehouseID,
                           Status = x.First().Status,
                           ReceiptQuantity = x.Sum(y => y.ReceiptQuantity),
                           IssueQuantity = x.Sum(y => y.IssueQuantity),
                           PackingVolume = x.First().PackingVolume,
                           Material = "",
                           IDCode = "",
                       })
                      .ToList();


            await inventoryRepository.DeleteOldDate("1");
            foreach (Inventory inventorys in querys)
            {
                List<Goods> goodsList = await goodsRepository.GetUnderPartNumber(inventorys.PartNumber);
                if (goodsList.Count > 0)
                {
                    inventorys.Mold = goodsList[0].Mold;
                }

                List<Inventory> inventories1 = await inventoryRepository.GetUnderMultiPartNumber(mYear, mMonth, "1", inventorys.PartNumber);

                List<InventoryOfLastMonths> inventoryOfLastMonths = await inventoryOfLastMonthsRepository.GetByPartNumber("1", inventorys.PartNumber);
                if (inventories1.Count > 0)
                {
                    inventorys.ReceiptQuantity = inventorys.ReceiptQuantity + inventories1[0].ReceiptQuantity;
                    inventorys.IssueQuantity = inventorys.IssueQuantity + inventories1[0].IssueQuantity;
                    inventorys.OpeningStockQuantity = inventoryOfLastMonths.Count > 0 ? inventoryOfLastMonths[0].ClosingStockQuantity : 0;
                    inventorys.ClosingStockQuantity = inventories1[0].OpeningStockQuantity + inventorys.ReceiptQuantity - inventorys.IssueQuantity;
                    await inventoryRepository.Update(inventorys, inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber);
                }
                else
                {
                    inventorys.ReceiptQuantity = inventorys.ReceiptQuantity;
                    inventorys.IssueQuantity = inventorys.IssueQuantity;
                    inventorys.OpeningStockQuantity = inventoryOfLastMonths.Count > 0 ? inventoryOfLastMonths[0].ClosingStockQuantity : 0;
                    inventorys.ClosingStockQuantity = inventorys.OpeningStockQuantity + inventorys.ReceiptQuantity - inventorys.IssueQuantity;
                    await inventoryRepository.Create(inventorys);
                }
            }

        }

        private async void addInventoryDetail(string palletID)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;

            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }

            List<InventoryDetail> inventories = new List<InventoryDetail>();
            List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(palletID);
            if (palletDetails.Count > 0)
            {
                foreach (PalletDetail palletDetail in palletDetails)
                {
                    InventoryDetail inventory = new InventoryDetail();
                    inventory.Month = mMonth;
                    inventory.Year = mYear;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = palletDetail.PartNumber;
                    inventory.PalletID = palletDetail.IDCode.Substring(0, 5);
                    inventory.LotID = palletDetail.LotID;
                    inventory.IDCode = palletDetail.IDCode;
                    inventory.Quantity = palletDetail.Quantity;
                    inventory.Status = "1";
                    inventory.MFGDate = palletDetail.MFGDate;
                    inventory.EXPDate = palletDetail.EXPDate;
                    inventory.InputDate = DateTime.Now;
                    inventories.Add(inventory);

                }


            }
            var querys = inventories.GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID, x.IDCode })
                 .Select(x => new InventoryDetail
                 {
                     PartNumber = x.Key.PartNumber,
                     PalletID = x.Key.PalletID,
                     LotID = x.Key.LotID,
                     IDCode = x.Key.IDCode,
                     Year = x.First().Year,
                     Month = x.First().Month,
                     WarehouseID = x.First().WarehouseID,
                     Status = x.First().Status,
                     Quantity = x.First().Quantity,
                     MFGDate = x.First().MFGDate,
                     EXPDate = x.First().EXPDate,
                     InputDate = x.First().InputDate
                 })
                .ToList();


            await inventoryDetailRepository.DeleteOldDate("1");

            foreach (InventoryDetail inventoryDetail in querys)
            {
                List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams6(inventoryDetail.Year, inventoryDetail.Month, inventoryDetail.WarehouseID,
                    inventoryDetail.PartNumber, inventoryDetail.PalletID, inventoryDetail.IDCode);

                if (inventoryDetails.Count == 0)
                {
                    await inventoryDetailRepository.Create(inventoryDetail);
                }


            }



        }

        private async void subInventoryDetail(string palletID)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;

            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }

            List<InventoryDetail> inventories = new List<InventoryDetail>();
            List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(palletID);
            if (palletDetails.Count > 0)
            {
                foreach (PalletDetail palletDetail in palletDetails)
                {
                    InventoryDetail inventory = new InventoryDetail();
                    inventory.Month = mMonth;
                    inventory.Year = mYear;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = palletDetail.PartNumber;
                    inventory.PalletID = palletDetail.IDCode.Substring(0, 5);
                    inventory.LotID = palletDetail.LotID;
                    inventory.IDCode = palletDetail.IDCode;
                    inventory.Quantity = palletDetail.Quantity;
                    inventory.Status = "1";
                    inventory.MFGDate = palletDetail.MFGDate;
                    inventory.EXPDate = palletDetail.EXPDate;
                    inventory.InputDate = DateTime.Now;
                    inventories.Add(inventory);

                }


            }
            var querys = inventories.GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID })
                 .Select(x => new InventoryDetail
                 {
                     PartNumber = x.Key.PartNumber,
                     PalletID = x.Key.PalletID,
                     LotID = x.Key.LotID,
                     IDCode = string.Join(",", x.Select(ss => ss.IDCode)),
                     Year = x.First().Year,
                     Month = x.First().Month,
                     WarehouseID = x.First().WarehouseID,
                     Status = x.First().Status,
                     Quantity = x.First().Quantity,
                     MFGDate = x.First().MFGDate,
                     EXPDate = x.First().EXPDate,
                     InputDate = x.First().InputDate
                 })
                .ToList();


            await inventoryDetailRepository.DeleteOldDate("1");
            foreach (InventoryDetail inventoryDetail in querys)
            {
                string idCode = !inventoryDetail.IDCode.Equals("") ? inventoryDetail.IDCode : "";

                List<string> idCodes = idCode.Split(',').ToList();

                foreach (string s in idCodes)
                {
                    if (!s.Equals(""))
                    {
                        inventoryDetail.IDCode = s;
                        List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams6(inventoryDetail.Year, inventoryDetail.Month, inventoryDetail.WarehouseID,
                            inventoryDetail.PartNumber, inventoryDetail.PalletID, inventoryDetail.IDCode);

                        if (inventoryDetails.Count > 0)
                        {
                            await inventoryDetailRepository.Delete6(inventoryDetail.Year, inventoryDetail.Month, inventoryDetail.WarehouseID, inventoryDetail.PartNumber,
                                inventoryDetail.PalletID, inventoryDetail.IDCode);
                        }
                    }
                }
            }



        }

        private async Task<List<PalletDetail>> getPalletDataByPO()
        {
            List<PalletDetail> palletDetailsNew = new List<PalletDetail>();
            List<string> listPalletID = new List<string>();

            for (int i = 0; i < gridView1.RowCount; i++)
            {
                int rowHandle = gridView1.GetRowHandle(i);
                string palletID = (string)gridView1.GetRowCellValue(rowHandle, "PalletID");
                listPalletID.Add(palletID);
            }

            foreach (string s in listPalletID)
            {
                List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(s);
                palletDetailsNew.AddRange(palletDetails);
            }

            return palletDetailsNew;
        }


        private async void exportExcelAsync()
        {
            List<PalletDetail> palletDetails = await getPalletDataByPO();

            if (palletDetails.Count == 0)
            {
                BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Chưa chọn pallet nào");
                return;

            }


            var querys1 = palletDetails.GroupBy(x => new { x.PartNumber, x.LotID })
                 .Select(x => new LotInfo
                 {
                     PartNumber = x.Key.PartNumber,
                     Lot = x.Key.LotID,
                     MFGDate = x.First().MFGDate,
                     Qty = x.Sum(y => y.Quantity),
                     QuantityPerlot = x.First().Quantity,
                     Cartons = x.Sum(y => y.Quantity) / x.First().Quantity,
                 })
                .ToList();

            var querys2 = querys1.GroupBy(x => new { x.PartNumber })
                .Select(x => new LotInfo
                {
                    PartNumber = x.Key.PartNumber,
                    Qty = x.Sum(y => y.Qty),
                })
               .ToList();

            string currentDate = DateTime.Now.ToString("yyyy.MM.dd");

            string po = "";
            int index1 = palletDetails[0].PalletID.IndexOf("-");
            int index2 = palletDetails[0].PalletID.IndexOf("-", index1 + 1);
            if (index2 > 0)
            {
                po = palletDetails[0].PalletID.Substring(0, index2);
            }




            using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Excel Workbook|*.xlsx", FileName = "[" + currentDate + "] " + "lot info-" + (po.Length > 0 ? po : palletDetails[0].PalletID) })
            {
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    var fileInfo = new FileInfo(sfd.FileName);
                    using (ExcelPackage excelPackage = new ExcelPackage(fileInfo))
                    {
                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add(po.Length > 0 ? po : palletDetails[0].PalletID);

                        worksheet.TabColor = System.Drawing.Color.Black;
                        worksheet.DefaultRowHeight = 12;
                        //Header of table  
                        //  
                        worksheet.Row(1).Height = 50;
                        worksheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Row(1).Style.Font.Bold = true;
                        worksheet.Cells[1, 1].Value = "PO #";
                        worksheet.Cells[1, 2].Value = "Invoice #";
                        worksheet.Cells[1, 3].Value = "P/N";
                        worksheet.Cells[1, 4].Value = "Ship To";
                        worksheet.Cells[1, 5].Value = "MFG(MM/DD/YY)(Optional)";
                        worksheet.Cells[1, 6].Value = "QTY";
                        worksheet.Cells[1, 7].Value = "LOT";
                        worksheet.Cells[1, 8].Value = "Qty per Lot";
                        worksheet.Cells[1, 9].Value = "Cartons";
                        //Body of table  
                        //  
                        int recordIndex = 2;
                        string str = "";
                        List<int> range = new List<int>();

                        foreach (var lotinfo in querys1)
                        {
                            //worksheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();

                            if (str.Contains(lotinfo.PartNumber))
                            {
                                worksheet.Cells[recordIndex, 3].Value = "";
                                worksheet.Cells[recordIndex, 6].Value = "";

                            }
                            else
                            {
                                worksheet.Cells[recordIndex, 3].Value = lotinfo.PartNumber;
                                worksheet.Cells[recordIndex, 6].Value = (querys2.Where(s => s.PartNumber.Equals(lotinfo.PartNumber)).ToList())[0].Qty;
                                str += lotinfo.PartNumber;
                                range.Add(recordIndex);
                            }

                            worksheet.Cells[recordIndex, 5].Value = lotinfo.MFGDate;

                            worksheet.Cells[recordIndex, 7].Value = lotinfo.Lot;
                            worksheet.Cells[recordIndex, 8].Value = lotinfo.QuantityPerlot;
                            worksheet.Cells[recordIndex, 9].Value = lotinfo.Cartons;
                            worksheet.Cells[recordIndex, 5].Style.Numberformat.Format = "yyyy/MM/dd";

                            recordIndex++;
                        }

                        worksheet.Cells[2, 1, querys1.Count + 1, 1].Merge = true;
                        worksheet.Cells[2, 2, querys1.Count + 1, 2].Merge = true;
                        for (int i = 0; i < range.Count; i++)
                        {
                            if (i == range.Count - 1)
                            {
                                worksheet.Cells[range[i], 3, querys1.Count + 1, 3].Merge = true;
                                worksheet.Cells[range[i], 6, querys1.Count + 1, 6].Merge = true;
                                worksheet.Cells[range[i], 4, querys1.Count + 1, 4].Merge = true;
                            }
                            else
                            {
                                worksheet.Cells[range[i], 3, range[i + 1] - 1, 3].Merge = true;
                                worksheet.Cells[range[i], 6, range[i + 1] - 1, 6].Merge = true;
                                worksheet.Cells[range[i], 4, range[i + 1] - 1, 4].Merge = true;
                            }
                        }

                        worksheet.Cells[querys1.Count + 2, 2].Value = "Total";
                        worksheet.Cells[querys1.Count + 2, 6].Value = querys1.Sum(s => s.Qty);
                        worksheet.Cells[querys1.Count + 2, 9].Value = querys1.Sum(s => s.Cartons);
                        worksheet.Cells[querys1.Count + 2, 1, querys1.Count + 2, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[querys1.Count + 2, 1, querys1.Count + 2, 9].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);

                        worksheet.Column(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(2).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(5).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(6).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(7).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(8).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(9).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Column(3).Width = 30;
                        worksheet.Column(5).Width = 30;
                        worksheet.Column(6).Width = 10;
                        worksheet.Column(7).Width = 20;
                        worksheet.Column(8).Width = 10;





                        //style
                        //worksheet.Column(1).AutoFit();
                        //worksheet.Column(2).AutoFit();
                        //worksheet.Column(3).AutoFit();
                        //worksheet.Column(4).AutoFit();
                        //worksheet.Column(5).AutoFit();
                        //worksheet.Column(6).AutoFit();
                        //worksheet.Column(7).AutoFit();

                        excelPackage.Save();


                    }
                    Process.Start(sfd.InitialDirectory + sfd.FileName);
                }
            }
        }

        private async void exportToExcel2()
        {
            List<PalletDetail> palletDetails = await getPalletDataByPO();

            var querys1 = palletDetails.GroupBy(x => new { x.PartNumber, x.LotID })
                 .Select(x => new LotInfo
                 {
                     PartNumber = x.Key.PartNumber,
                     Lot = x.Key.LotID,
                     MFGDate = x.First().MFGDate,
                     Qty = x.Sum(y => y.Quantity),
                     QuantityPerlot = x.First().Quantity,
                     Cartons = x.Sum(y => y.Quantity) / x.First().Quantity,
                 })
                .ToList();

            var querys2 = querys1.GroupBy(x => new { x.PartNumber })
                .Select(x => new LotInfo
                {
                    PartNumber = x.Key.PartNumber,
                    Qty = x.Sum(y => y.Qty),

                })
               .ToList();

            // Create an exporter instance. 
            IXlExporter exporter = XlExport.CreateExporter(XlDocumentFormat.Xlsx);

            // Create the FileStream object with the specified file path.
            using (FileStream stream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "test.xlsx", FileMode.Create, FileAccess.ReadWrite))
            {
                // Create a new document and begin to write it to the specified stream.
                using (IXlDocument document = exporter.CreateDocument(stream))
                {
                    // Add a new worksheet to the document. 
                    using (IXlSheet sheet = document.CreateSheet())
                    {
                        // Specify the worksheet name.
                        sheet.Name = "Dữ liệu công dân";

                        // Create the first column and set its width. 
                        using (IXlColumn column = sheet.CreateColumn())
                        {
                            column.WidthInPixels = 100;
                        }

                        // Create the second column and set its width.
                        using (IXlColumn column = sheet.CreateColumn())
                        {
                            column.WidthInPixels = 250;
                        }


                        // Specify cell font attributes.
                        XlCellFormatting cellFormatting = new XlCellFormatting();
                        cellFormatting.Font = new XlFont();
                        cellFormatting.Font.Name = "Century Gothic";
                        cellFormatting.Font.SchemeStyle = XlFontSchemeStyles.None;

                        // Specify formatting settings for the header row.
                        XlCellFormatting headerRowFormatting = new XlCellFormatting();
                        headerRowFormatting.CopyFrom(cellFormatting);
                        headerRowFormatting.Font.Bold = true;
                        headerRowFormatting.Font.Color = XlColor.FromTheme(XlThemeColor.Light1, 0.0);
                        headerRowFormatting.Fill = XlFill.SolidFill(XlColor.FromTheme(XlThemeColor.Accent2, 0.0));

                        // Create the header row.
                        using (IXlRow row = sheet.CreateRow())
                        {
                            using (IXlCell cell = row.CreateCell())
                            {
                                cell.Value = "PO #";
                                cell.ApplyFormatting(headerRowFormatting);
                            }
                            using (IXlCell cell = row.CreateCell())
                            {
                                cell.Value = "Invoice #";
                                cell.ApplyFormatting(headerRowFormatting);
                            }
                            using (IXlCell cell = row.CreateCell())
                            {
                                cell.Value = "P/N";
                                cell.ApplyFormatting(headerRowFormatting);
                            }
                            using (IXlCell cell = row.CreateCell())
                            {
                                cell.Value = "MFG(MM/DD/YY)(Optional)";
                                cell.ApplyFormatting(headerRowFormatting);
                            }
                            using (IXlCell cell = row.CreateCell())
                            {
                                cell.Value = "QTY";
                                cell.ApplyFormatting(headerRowFormatting);
                            }
                            using (IXlCell cell = row.CreateCell())
                            {
                                cell.Value = "LOT";
                                cell.ApplyFormatting(headerRowFormatting);
                            }
                            using (IXlCell cell = row.CreateCell())
                            {
                                cell.Value = "Qty per Lot";
                                cell.ApplyFormatting(headerRowFormatting);
                            }
                            using (IXlCell cell = row.CreateCell())
                            {
                                cell.Value = "Cartons";
                                cell.ApplyFormatting(headerRowFormatting);
                            }
                        }

                        // Generate data for the sales report.
                        string[] partNumbers = new string[querys1.Count];
                        string[] mfgDates = new string[querys1.Count];
                        string[] qtys = new string[querys1.Count];
                        string[] lots = new string[querys1.Count];
                        string[] qtyPerLots = new string[querys1.Count];
                        string[] cartons = new string[querys1.Count];

                        foreach (var lotinfo in querys1)
                        {

                            partNumbers.Append(lotinfo.PartNumber);

                        }

                        for (int i = 0; i < partNumbers.Length; i++)
                        {
                            using (IXlRow row = sheet.CreateRow())
                            {
                                using (IXlCell cell = row.CreateCell())
                                {
                                    cell.Value = partNumbers[i];
                                    cell.ApplyFormatting(cellFormatting);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void simpleButtonGetAllPallet_Click(object sender, EventArgs e)
        {
            AllPalletForm allPalletForm = new AllPalletForm();
            allPalletForm.ShowDialog(this);
            allPalletForm.Dispose();
        }



        private async void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (gridView1.RowCount > 0)
            {
                string palletID = (string)(sender as GridView).GetFocusedRowCellValue("PalletID");
                PalletHeader palletHeader = await palletHeaderRepository.GetUnderID(palletID);
                if (palletHeader != null)
                {
                    dateEditCreatedDate.Text = DateTime.Parse(palletHeader.CreatedDate.ToString()).ToShortDateString();
                    comboBoxEditHandlingStatus.SelectedIndex = palletHeader.HandlingStatusID.Equals("<>") ? 0 : int.Parse(palletHeader.HandlingStatusID) + 1;
                    textEditPalletID.Text = palletID;
                    sbLoadDataForGrid(palletID);
                }
                QRCodeGeneral(selecttedID);
            }
        }

        private async void comboBoxEditReceiptDate_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBoxEditReceiptDate.SelectedIndex == 0)
            {
                List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(textEditPalletID.Text);
                gridControlWRDataDetail.DataSource = palletDetails;
                getPackingQuantity();
            }
            else
            {
                List<PalletDetail> palletDetails = (await palletDetailRepository.GetUnderID(textEditPalletID.Text)).Where(w => w.ReceiptDate == DateTime.Parse(comboBoxEditReceiptDate.Text)).ToList();
                gridControlWRDataDetail.DataSource = palletDetails;

                Decimal total = 0;
                total += Convert.ToDecimal(palletDetails.Count);
                textEditTotalIDCode.Text = total.ToString("0.#####");
            }

        }

        private void simpleButtonApproved2AllPallet_Click(object sender, EventArgs e)
        {
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += addDoWork;
            backgroundWorker.RunWorkerCompleted += addRunWokerCompleted;
            backgroundWorker.RunWorkerAsync();
        }

        private async void addDoWork(Object sender, DoWorkEventArgs eventArgs)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;
            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }
            List<PalletHeader> palletHeaders = (await palletHeaderRepository.GetAll()).Where(w => !w.PalletID.Contains("DEP") && !w.PalletID.Contains("DEPO") &&
                    !w.PalletID.Contains("SGPO") && !w.PalletID.Contains("USPO") && !w.PalletID.Contains("PHPO") &&
                 !w.HandlingStatusID.Equals("2") && w.TotalIDCode != 0).ToList();
            if (palletHeaders.Count > 0)
            {
                DialogResult dialogResult = MessageBoxEx.Show("Cảnh báo", "Duyệt mức 2 " + palletHeaders.Count, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    foreach (PalletHeader palletHeader in palletHeaders)
                    {
                        List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(palletHeader.PalletID);
                        if (palletDetails.Count > 0)
                        {
                            foreach (PalletDetail palletDetail in palletDetails)
                            {
                                //cộng tồn kho
                                List<Inventory> inventories = await inventoryRepository.GetUnderMultiPartNumber(mYear, mMonth, "1", palletDetail.PartNumber);
                                if (inventories.Count > 0)
                                {
                                    inventories[0].ReceiptQuantity = inventories[0].ReceiptQuantity + palletDetail.Quantity;
                                    inventories[0].ClosingStockQuantity = inventories[0].ClosingStockQuantity + inventories[0].ReceiptQuantity;
                                    await inventoryRepository.Update(inventories[0], mYear, mMonth, "1", palletDetail.PartNumber);
                                }
                                else
                                {
                                    Inventory inventory = new Inventory();
                                    inventory.Year = mYear;
                                    inventory.Month = mMonth;
                                    inventory.WarehouseID = "1";
                                    inventory.PartNumber = palletDetail.PartNumber;
                                    inventory.PackingVolume = palletDetail.Quantity;
                                    inventory.OpeningStockQuantity = 0;
                                    inventory.ReceiptQuantity = palletDetail.Quantity;
                                    inventory.IssueQuantity = 0;
                                    inventory.ClosingStockQuantity = inventory.OpeningStockQuantity + inventory.ReceiptQuantity;
                                    inventory.Unit = palletDetail.Unit;
                                    await inventoryRepository.Create(inventory);
                                }
                                //cộng tồn kho chi tiết
                                List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams6(mYear, mMonth, "1", palletDetail.PartNumber, palletDetail.PalletID, palletDetail.IDCode);
                                if (inventoryDetails.Count == 0)
                                {
                                    InventoryDetail inventoryDetail = new InventoryDetail();
                                    inventoryDetail.Year = mYear;
                                    inventoryDetail.Month = mMonth;
                                    inventoryDetail.WarehouseID = "1";
                                    inventoryDetail.PartNumber = palletDetail.PartNumber;
                                    inventoryDetail.IDCode = palletDetail.IDCode;
                                    inventoryDetail.MFGDate = palletDetail.MFGDate;
                                    inventoryDetail.EXPDate = palletDetail.EXPDate;
                                    inventoryDetail.Quantity = palletDetail.Quantity;
                                    inventoryDetail.InputDate = DateTime.Now;

                                    await inventoryDetailRepository.Create(inventoryDetail);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void addRunWokerCompleted(Object sender, RunWorkerCompletedEventArgs eventArgs)
        {
            MessageBox.Show("Done");
        }

        private void simpleButtonApproved2AndSubInventory_Click(object sender, EventArgs e)
        {
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += subDoWork;
            backgroundWorker.RunWorkerCompleted += subRunWokerCompleted;
            backgroundWorker.RunWorkerAsync();
        }

        private async void subDoWork(Object sender, DoWorkEventArgs eventArgs)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;
            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }
            List<PalletHeader> palletHeaders = (await palletHeaderRepository.GetAll()).Where(w => (w.PalletID.Contains("DEP") || w.PalletID.Contains("DEPO") ||
                    w.PalletID.Contains("SGPO") || w.PalletID.Contains("USPO") || w.PalletID.Contains("PHPO")) &&
                    !w.HandlingStatusID.Equals("2") && w.TotalIDCode != 0).ToList();
            if (palletHeaders.Count > 0)
            {
                DialogResult dialogResult = MessageBoxEx.Show("Cảnh báo", "Duyệt mức 2 " + palletHeaders.Count, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    foreach (PalletHeader palletHeader in palletHeaders)
                    {
                        List<PalletDetail> palletDetails = await palletDetailRepository.GetUnderID(palletHeader.PalletID);
                        if (palletDetails.Count > 0)
                        {
                            foreach (PalletDetail palletDetail in palletDetails)
                            {
                                //trừ tồn kho
                                List<Inventory> inventories = await inventoryRepository.GetUnderMultiPartNumber(mYear, mMonth, "1", palletDetail.PartNumber);
                                if (inventories.Count > 0)
                                {
                                    inventories[0].IssueQuantity = inventories[0].IssueQuantity + palletDetail.Quantity;
                                    inventories[0].ClosingStockQuantity = inventories[0].ClosingStockQuantity - inventories[0].IssueQuantity;
                                    await inventoryRepository.Update(inventories[0], mYear, mMonth, "1", palletDetail.PartNumber);
                                }
                                else
                                {
                                    Inventory inventory = new Inventory();
                                    inventory.Year = mYear;
                                    inventory.Month = mMonth;
                                    inventory.WarehouseID = "1";
                                    inventory.PartNumber = palletDetail.PartNumber;
                                    inventory.PackingVolume = palletDetail.Quantity;
                                    inventory.OpeningStockQuantity = 0;
                                    inventory.ReceiptQuantity = 0;
                                    inventory.IssueQuantity = palletDetail.Quantity;
                                    inventory.ClosingStockQuantity = inventory.OpeningStockQuantity - inventory.IssueQuantity;
                                    inventory.Unit = palletDetail.Unit;
                                    await inventoryRepository.Create(inventory);
                                }
                                //trừ tồn kho chi tiết
                                List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams6(mYear, mMonth, "1", palletDetail.PartNumber, palletDetail.PalletID, palletDetail.IDCode);
                                if (inventoryDetails.Count > 0)
                                {
                                    await inventoryDetailRepository.Delete6(inventoryDetails[0].Year, inventoryDetails[0].Month, "1", inventoryDetails[0].PartNumber
                                        , inventoryDetails[0].PalletID, inventoryDetails[0].IDCode);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void subRunWokerCompleted(Object sender, RunWorkerCompletedEventArgs eventArgs)
        {
            MessageBox.Show("Done");
        }

        private async void simpleButtonResetAllToApproved1_Click(object sender, EventArgs e)
        {
            List<PalletHeader> palletHeaders = (await palletHeaderRepository.GetAll()).Where(w => w.HandlingStatusID.Equals("2")).ToList();
            if (palletHeaders.Count > 0)
            {
                DialogResult dialogResult = MessageBoxEx.Show("Cảnh báo", "Duyệt mức 1 tất cả pallet " + palletHeaders.Count, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    foreach (PalletHeader palletHeader in palletHeaders)
                    {

                        palletHeader.HandlingStatusID = "1";
                        palletHeader.HandlingStatusName = "Duyệt mức 1";
                        palletHeader.UpdatedUserID = WMMessage.User.UserID;
                        palletHeader.UpdatedDate = DateTime.Now;
                        await palletHeaderRepository.Update(palletHeader, palletHeader.PalletID, palletHeader.Ordinal);
                    }
                    MessageBox.Show("Done");
                }
            }
        }


    }
}
