﻿using DevExpress.XtraBars.Docking2010;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Helpers
{
    class LanguageHelper
    {
        public static void updateLanguageButtonPanell(WindowsUIButtonPanel windowsUIButton)
        {
            ResourceManager rm;
            String cultureName = WMMessage.msgLanguage;
            if (cultureName.Equals("vi-VN"))
            {
                rm = new
                   ResourceManager("WarehouseManager.Resource.vi_local", typeof(UserManagerControl).Assembly);
            }
            else if (cultureName.Equals("en-US"))
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.en_local", typeof(UserManagerControl).Assembly);
            }
            else
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.cn_local", typeof(UserManagerControl).Assembly);
            }

            //WindowsUIButton buttonAdd = windowsUIButton.Buttons[0] as WindowsUIButton;
            ((WindowsUIButton)windowsUIButton.Buttons[0]).Caption = rm.GetString("addnew");
            ((WindowsUIButton)windowsUIButton.Buttons[2]).Caption = rm.GetString("save");
            ((WindowsUIButton)windowsUIButton.Buttons[4]).Caption = rm.GetString("delete");
            ((WindowsUIButton)windowsUIButton.Buttons[6]).Caption = rm.GetString("search");
            ((WindowsUIButton)windowsUIButton.Buttons[8]).Caption = rm.GetString("refesh");
            ((WindowsUIButton)windowsUIButton.Buttons[10]).Caption = rm.GetString("import_data");
            ((WindowsUIButton)windowsUIButton.Buttons[12]).Caption = rm.GetString("export_data");
            ((WindowsUIButton)windowsUIButton.Buttons[14]).Caption = rm.GetString("print");
            ((WindowsUIButton)windowsUIButton.Buttons[16]).Caption = rm.GetString("exit");
        }
    }
}
