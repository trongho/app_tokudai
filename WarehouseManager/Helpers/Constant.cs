﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Models;

namespace WarehouseManager.Helpers
{
    public class Constant
    {
        public static String BaseURL = "http://192.168.1.98:6381/api/";

        //public static String BaseURL = "http://www.wmstokudai.com:6380/api/";
        //public static String BaseURL = "http://113.161.40.179:6380/api/";
        //public static String BaseURL = "http://113.161.187.210:6380/api/";

        public static List<string> units = new List<string> { "PC","RL"};
        public static List<string> molds = new List<string> {"A","B","C"};
        public static List<string> cartonSizes = new List<string> {"340*340*230","340*340*380","340*210*350","370*340*350","430*340*350",
            "560*315*205","500*500*115","500*500*140","390*200*390","580*340*350"};
        public static List<string> pads = new List<string> {};
        public static List<string> nilons = new List<string> { "800*600"};
        public static List<string> labelSizes= new List<string> { "100*100", "100*20"};
        public static List<string> branchs = new List<string> {"<>","TOKUDAI INDUSTRY VIETNAM CO., LTD" };
        public static List<string> handlingStatuss = new List<string> {"<>","Chưa duyệt","Duyệt mức 1","Duyệt mức 2","Hủy"};

        public static string smallDateTimeFormat = "yyyy-MM-dd";
    }
}
