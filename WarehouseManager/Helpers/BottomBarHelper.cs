﻿using DevExpress.XtraBars;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Helpers
{
    class BottomBarHelper
    {
        public static void normalMessage(BarStaticItem barStaticItem, string text)
        {
            barStaticItem.Caption = text;
            barStaticItem.ItemAppearance.Normal.ForeColor = System.Drawing.Color.YellowGreen;
        }
        public static void successMessage(BarStaticItem barStaticItem, string text)
        {
            barStaticItem.Caption = text;
            barStaticItem.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Green;
        }

        public static void errorMessage(BarStaticItem barStaticItem, string text)
        {
            barStaticItem.Caption = text;
            barStaticItem.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Red;
        }
    }
}
