﻿using DevExpress.XtraBars;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Ribbon;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;

namespace WarehouseManager
{
    public partial class MainForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        const int LEADING_SPACE = 12;
        const int CLOSE_SPACE = 15;
        const int CLOSE_AREA = 15;
        String tabUserManagerText = "";
        InventoryRepository inventoryRepository = null;
        InventoryDetailRepository inventoryDetailRepository = null;
        InventoryOfLastMonthsRepository inventoryOfLastMonthsRepository = null;
        InventoryOfLastMonthDetailRepository inventoryOfLastMonthDetailRepository = null;

        public MainForm()
        {
            InitializeComponent();
            this.Load += MainForm_Load;
            updateLanguage();
            this.WindowState = FormWindowState.Maximized;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Dock = DockStyle.Fill;

            ribbon.ItemClick += barButtonItem_ItemClick;
            xtraTabControl1.CloseButtonClick += closeButtonTabpage_Click;
            this.FormClosing += Form1_FormClosing;
            inventoryRepository = new InventoryRepository();
            inventoryDetailRepository = new InventoryDetailRepository();
            inventoryOfLastMonthsRepository = new InventoryOfLastMonthsRepository();
            inventoryOfLastMonthDetailRepository = new InventoryOfLastMonthDetailRepository();
        }

        private void MainForm_Load(Object sender, EventArgs e)
        {
            barHeaderUserInfo.Caption = WMMessage.User.LoginName + "( " + WMMessage.User.Role + ")";
            barButtonItemLogin.Caption = "Đăng xuất";
            barButtonItemLogin.Glyph = imageCollection1.Images[0];
            checkRole();
            //deleteOldInventoryDetailData();
            //deleteOldInventoryData();

            inventoryProcessing();

        }

        private async void inventoryProcessing()
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;
            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }

            List<Inventory> inventories = await inventoryRepository.GetAll();
            if (inventories.Count > 0)
            {
                List<Inventory> tonKhoCacThangTruoc = inventories.Where(w => w.Year < mYear || (w.Year == mYear && w.Month < mMonth)).ToList();
                if (tonKhoCacThangTruoc.Count > 0)
                {
                    foreach (Inventory inventory in tonKhoCacThangTruoc)
                    {
                        Int16 oldYear = inventory.Year;
                        Int16 oldMonth = inventory.Month;
                        List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams4(inventory.Year, inventory.Month, "1", inventory.PartNumber);
                        if (inventoryDetails.Count > 0)
                        {
                            foreach (InventoryDetail inventoryDetail in inventoryDetails)
                            {
                                InventoryOfLastMonthDetail inventoryOfLastMonthDetail = new InventoryOfLastMonthDetail();
                                inventoryOfLastMonthDetail.Year = inventoryDetail.Year;
                                inventoryOfLastMonthDetail.Month = inventoryDetail.Month;
                                inventoryOfLastMonthDetail.WarehouseID = inventoryDetail.WarehouseID;
                                inventoryOfLastMonthDetail.PartNumber = inventoryDetail.PartNumber;
                                inventoryOfLastMonthDetail.LotID = inventoryDetail.LotID;
                                inventoryOfLastMonthDetail.PalletID = inventoryDetail.PalletID;
                                inventoryOfLastMonthDetail.IDCode = inventoryDetail.IDCode;
                                inventoryOfLastMonthDetail.Quantity = inventoryDetail.Quantity;
                                inventoryOfLastMonthDetail.MFGDate = inventoryDetail.MFGDate;
                                inventoryOfLastMonthDetail.EXPDate = inventoryDetail.InputDate;
                                inventoryOfLastMonthDetail.InputDate = inventoryDetail.InputDate;
                                if (await inventoryOfLastMonthDetailRepository.Create(inventoryOfLastMonthDetail) > 0)
                                {
                                    InventoryDetail newInventoryDetail = inventoryDetail;
                                    newInventoryDetail.Year = mYear;
                                    newInventoryDetail.Month = mMonth;
                                    await inventoryDetailRepository.Create(newInventoryDetail);
                                    await inventoryDetailRepository.Delete6(oldYear,oldMonth, "1", inventoryDetail.PartNumber, inventoryDetail.PalletID, inventoryDetail.IDCode);
                                }
                            }
                        }

                        InventoryOfLastMonths inventoryOfLastMonths = new InventoryOfLastMonths();
                        inventoryOfLastMonths.Year = inventory.Year;
                        inventoryOfLastMonths.Month = inventory.Month;
                        inventoryOfLastMonths.WarehouseID = inventory.WarehouseID;
                        inventoryOfLastMonths.PartNumber = inventory.PartNumber;
                        inventoryOfLastMonths.PackingVolume = inventory.PackingVolume;
                        inventoryOfLastMonths.OpeningStockQuantity = inventory.OpeningStockQuantity;
                        inventoryOfLastMonths.ReceiptQuantity = inventory.ReceiptQuantity;
                        inventoryOfLastMonths.IssueQuantity = inventory.IssueQuantity;
                        inventoryOfLastMonths.ClosingStockQuantity = inventory.ClosingStockQuantity;
                        inventoryOfLastMonths.Unit = inventory.Unit;
                        if (await inventoryOfLastMonthsRepository.Create(inventoryOfLastMonths) > 0)
                        {
                            Inventory newInventory = inventory;
                            newInventory.Year = mYear;
                            newInventory.Month = mMonth;
                            await inventoryRepository.Create(newInventory);
                            await inventoryRepository.Delete(oldYear,oldMonth, "1", inventory.PartNumber);
                        }
                    }
                }
            }

            barStaticItemMessage.Caption = "Done";
        }

        private void checkRole()
        {
            if (WMMessage.User.Role.Equals("User"))
            {
                ribbonPageGroupUserManager.Enabled = false;

                ribbonPageGroupGoodsManager.Enabled = false;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private async void deleteOldInventoryData()
        {
            await inventoryRepository.DeleteOldDate("1");
        }

        private async void deleteOldInventoryDetailData()
        {
            await inventoryDetailRepository.DeleteOldDate("1");
        }

        private void updateLanguage()
        {
            ResourceManager rm;
            String cultureName = WMMessage.msgLanguage;
            if (cultureName.Equals("vi-VN"))
            {
                rm = new
                   ResourceManager("WarehouseManager.Resource.vi_local", typeof(MainForm).Assembly);
            }
            else if (cultureName.Equals("en-US"))
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.en_local", typeof(MainForm).Assembly);
            }
            else
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.cn_local", typeof(MainForm).Assembly);
            }


            // LoginForm.ActiveForm.Text = rm.GetString("login_form");
            ribbonPageSystem.Text = rm.GetString("system");
            ribbonPageCategory.Text = rm.GetString("category");
            ribbonPageWarehouse.Text = rm.GetString("warehouse");
            ribbonPageHelp.Text = rm.GetString("help");
            barButtonItemVietnam.Caption = rm.GetString("english");
            barButtonItemEnglish.Caption = rm.GetString("vietnamese");
            barButtonItemChina.Caption = rm.GetString("chinese_simplified");
            ribbonPageGroupUser.Text = rm.GetString("user");
            ribbonPageGroupUserManager.Text = rm.GetString("user_manager");
            ribbonPageGroupGoodsManager.Text = rm.GetString("goods_manager");
            barButtonItemLogin.Caption = rm.GetString("login");
            barButtonItemChangePass.Caption = rm.GetString("change_password");
            barButtonItemAddUser.Caption = rm.GetString("user_role");
            barButtonItemGoods.Caption = rm.GetString("goods");
            barButtonItemMaterial.Caption = rm.GetString("material");
            barButtonItemWarehouseReceipt.Caption = rm.GetString("warehouse_receipt");
            barButtonItemProcessInventory.Caption = rm.GetString("process_inventory");
            barButtonItemIssueOrder.Caption = rm.GetString("issue_order");
            barButtonItemTallySheet.Caption = rm.GetString("tally_sheet");
            barButtonItemProcessTally.Caption = rm.GetString("process_tally");
            barButtonItemReceiptRequisition.Caption = rm.GetString("receipt_requisition");
            barButtonItemReceiptData.Caption = rm.GetString("receipt_data");
            barButtonItemIssueRequisition.Caption = rm.GetString("issue_requisition");
            barButtonItemIssueData.Caption = rm.GetString("issue_data");
            barButtonItemPalletManager.Caption = rm.GetString("pallet_manager");
            barButtonItemTools.Caption = rm.GetString("tools");
        }

        void barButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {

            ResourceManager rm;
            String cultureName = WMMessage.msgLanguage;
            if (cultureName.Equals("vi-VN"))
            {
                rm = new
                   ResourceManager("WarehouseManager.Resource.vi_local", typeof(MainForm).Assembly);
            }
            else if (cultureName.Equals("en-US"))
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.en_local", typeof(MainForm).Assembly);
            }
            else
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.cn_local", typeof(MainForm).Assembly);
            }

            String tag = e.Item.Name;

            switch (tag)
            {
                case "barButtonItemAddUser":
                    /* Navigate to page A */
                    WMPublic.TabCreating(xtraTabControl1, rm.GetString("user_manager"), "tabUserManager", new UserManagerControl(this));
                    break;
                case "barButtonItemLogin":
                    /* Navigate to page B */
                    this.Dispose();
                    if (Application.OpenForms["LoginForm"] != null)
                    {
                        var Main = Application.OpenForms["LoginForm"] as LoginForm;
                        LoginForm loginForm = new LoginForm();
                        loginForm.Show();

                    }
                    break;
                case "barButtonItemGoods":
                    /* Navigate to page D */
                    WMPublic.TabCreating(xtraTabControl1, rm.GetString("goods_manager"), "tabGoods", new GoodManagerControl(this));
                    break;
                case "barButtonItemReceiptRequisition":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, rm.GetString("receipt_requisition"), "tabReceiptRequisition", new ReceiptRequisitionControl());
                    break;
                //case "barButtonItemReceiptData":
                //    /* Navigate to page E */
                //    WMPublic.TabCreating(xtraTabControl1, "Dữ liệu nhập", "tabReceiptData", new ReceiptDataControl());
                //    break;
                case "barButtonItemReceiptData":
                    /* Navigate to page E */
                    ReceiptDataControl receiptDataControl = new ReceiptDataControl();

                    WMPublic.TabCreating2(xtraTabControl1, rm.GetString("receipt_data"), "tabReceiptData", receiptDataControl);
                    receiptDataControl.BringToFront();
                    receiptDataControl.Focus();
                    break;
                case "barButtonItemIssueRequisition":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, rm.GetString("issue_requisition"), "tabIssueRequisition", new IssueRequisitionControl(this));
                    break;
                case "barButtonItemIssueData":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, rm.GetString("issue_data"), "tabIssueData", new IssueDataControl(this));
                    break;
                case "barButtonItemWarehouseReceipt":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, rm.GetString("warehouse_receipt"), "tabWarehouseReceipt", new WarehouseReceiptControl());
                    break;
                case "barButtonItemProcessInventory":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Xử lý tồn kho", "tabProcessInventory", new InventoryProcessControl(this));
                    break;
                case "barButtonItemIssueOrder":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, rm.GetString("issue_order"), "tabIssueOrder", new IssueOrderControl(this));
                    break;
                    break;
                case "barButtonItemReport":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Báo cáo", "tabReport", new ReportControl());
                    break;
                case "barButtonItemColor":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Màu sắc", "tabColor", new ColorControl());
                    break;
                case "barButtonItemUnit":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Đơn vị", "tabUnit", new UnitControl(this));
                    break;
                case "barButtonItemTallySheet":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, rm.GetString("tally_sheet"), "tabTallSheet", new TallySheetControl());
                    break;
                case "barButtonItemProcessTally":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, rm.GetString("process_tally"), "tabProcessTally", new ProcesTallyControl());
                    break;
                //case "barButtonItemBackupRestore":
                //    /* Navigate to page E */
                //    WMPublic.TabCreating(xtraTabControl1, "Lưu trữ và phục hồi", "tabBackupRestore", new BackupRestoreDatabaseControl());
                //    break;
                case "barButtonItemPallet":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Pallet", "tabPallet", new LabelManagerControl());
                    break;
                case "barButtonItemMaterial":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Material", "tabMaterial", new MaterialManagerControl());
                    break;
                case "barButtonItemPalletManager":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, rm.GetString("pallet_manager"), "tabPalletData", new PalletManagerControl(this));
                    break;
            }
        }

        private void closeButtonTabpage_Click(object sender, EventArgs e)
        {
            ClosePageButtonEventArgs EArg = (DevExpress.XtraTab.ViewInfo.ClosePageButtonEventArgs)e;
            string name = EArg.Page.Text;//Get the text of the closed tab
            foreach (XtraTabPage page in xtraTabControl1.TabPages)//Traverse to get the same text as the closed tab
            {
                if (page.Text == name)
                {
                    if (WMPublic.sbMessageCloseTab(this) == true)
                    {
                        xtraTabControl1.TabPages.Remove(page);
                        page.Dispose();
                        return;
                    }
                }
            }
        }

        private void barButtonItemVietnam_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (xtraTabControl1.TabPages.Count > 0)
            {
                WMPublic.sbMessageCloseTabpageRequest(this);
            }
            else
            {
                WMMessage.msgLanguage = "vi-VN";
                ResourceManager resourceManager = new ResourceManager("WarehouseManager.Resource.vi_local", Assembly.GetExecutingAssembly());
                updateLanguage();
            }
        }

        private void barButtonItemEnglish_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (xtraTabControl1.TabPages.Count > 0)
            {
                WMPublic.sbMessageCloseTabpageRequest(this);
            }
            else
            {
                WMMessage.msgLanguage = "en-US";
                ResourceManager resourceManager = new ResourceManager("WarehouseManager.Resource.en_local", Assembly.GetExecutingAssembly());
                updateLanguage();
            }
        }

        private void barButtonItemChina_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (xtraTabControl1.TabPages.Count > 0)
            {
                WMPublic.sbMessageCloseTabpageRequest(this);
            }
            else
            {
                WMMessage.msgLanguage = "zh-CHS";
                ResourceManager resourceManager = new ResourceManager("WarehouseManager.Resource.cn_local", Assembly.GetExecutingAssembly());
                updateLanguage();
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}