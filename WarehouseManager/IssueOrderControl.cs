﻿using DevExpress.Export;
using DevExpress.Export.Xl;
using DevExpress.Printing.ExportHelpers;
using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using WarehouseManager.Helpers;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class IssueOrderControl : DevExpress.XtraEditors.XtraUserControl
    {
        HandlingStatusRepository handlingStatusRepository;
        IOHeaderRepository IOHeaderRepository;
        IODetailRepository IODetailRepository;
        ModalityRepository modalityRepository;
        WarehouseRepository warehouseRepository;
        InventoryRepository inventoryRepository = null;
        InventoryDetailRepository inventoryDetailRepository = null;
        InventoryOfLastMonthsRepository inventoryOfLastMonthsRepository = null;
        InventoryOfLastMonthDetailRepository inventoryOfLastMonthDetailRepository = null;
        GoodsRepository goodsRepository = null;
        public static String selectedIONumber;
        int no = 0;

        internal MainForm m_MainForm;
        public IssueOrderControl(MainForm mainForm)
        {
            InitializeComponent();
            m_MainForm = mainForm;
            BottomBarHelper.normalMessage(m_MainForm.barStaticItemMessage, "!!!");
            handlingStatusRepository = new HandlingStatusRepository();
            IODetailRepository = new IODetailRepository();
            IOHeaderRepository = new IOHeaderRepository();
            modalityRepository = new ModalityRepository();
            warehouseRepository = new WarehouseRepository();
            inventoryRepository = new InventoryRepository();
            inventoryDetailRepository = new InventoryDetailRepository();
            inventoryOfLastMonthsRepository = new InventoryOfLastMonthsRepository();
            inventoryOfLastMonthDetailRepository = new InventoryOfLastMonthDetailRepository();
            goodsRepository = new GoodsRepository();
            popupMenuSelectHandlingStatus();
            popupMenuSelectModality();
            popupMenuSelectWarehouse();
            sbLoadDataForGridIODetailAsync(textEditIONumber.Text);
            this.Load += IssueOrderControl_Load;
        }

        private void IssueOrderControl_Load(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditIONumber.DoubleClick += textEditIONumer_CellDoubleClick;
            gridViewIODetail.CustomColumnDisplayText += GridView_CustomColumnDisplayText;

        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewIODetail.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task popupMenuSelectModality()
        {
            List<Modality> modalities = await modalityRepository.GetAll();
            foreach (Modality modality in modalities)
            {
                comboBoxEditModality.Properties.Items.Add(modality.ModalityName);
            }
            comboBoxEditModality.SelectedIndex = 38;
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

        private async Task popupMenuSelectWarehouse()
        {
            List<Warehouse> warehouses = await warehouseRepository.getAll();
            
            foreach (Warehouse warehouse in warehouses)
            {
                comboBoxEditWarehouse.Properties.Items.Add(warehouse.WarehouseName);
            }
            comboBoxEditWarehouse.SelectedIndex =0;
        }

        private void getTotalQuantity()
        {
            Decimal totalQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewIODetail.GetRowHandle(i);
                if (gridViewIODetail.GetRowCellValue(rowHandle, "PalletID") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewIODetail.GetRowCellValue(rowHandle, "IssueQuantity");
                totalQuantity += quantity;
                i++;
            }
            textEditTotalQuantity.Text = totalQuantity.ToString("0.#####");
        }

        private async Task sbLoadDataForGridIODetailAsync(String IONumber)
        {
            List<IODetail> IODetails = await IODetailRepository.GetUnderID(IONumber);
            int mMaxRow = 100;
            if (IODetails.Count < mMaxRow)
            {
                int num = mMaxRow - IODetails.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    IODetails.Add(new IODetail());
                    i++;
                }
            }
            gridControlIODetail.DataSource = IODetails;
            getTotalQuantity();
        }



        public async Task updateIOHeaderAsync()
        {
            List<IOHeader> iOHeaders= await IOHeaderRepository.GetUnderID(textEditIONumber.Text);
            if (iOHeaders[0].HandlingStatusID == "2")
            {
                MessageBox.Show("Phiếu xuất đã được duyệt mức 2, không thể chỉnh sửa","Alert",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }

            IOHeader IOHeader = iOHeaders[0];
            IOHeader.IONumber = textEditIONumber.Text;
            IOHeader.IODate = DateTime.Parse(dateEditIODate.DateTime.ToString("yyyy-MM-dd"));
            IOHeader.HandlingStatusID =comboBoxEditHandlingStatus.SelectedIndex==0?"<>":(comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            IOHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            IOHeader.ModalityID = $"{int.Parse(comboBoxEditModality.SelectedIndex.ToString()) + 1:D2}";
            IOHeader.ModalityName = comboBoxEditModality.SelectedItem.ToString();
            IOHeader.WarehouseID = comboBoxEditWarehouse.SelectedIndex == 0 ? "<>" : "1";
            IOHeader.WarehouseName = comboBoxEditWarehouse.Text;
            IOHeader.Note = textEditNote.Text;
            IOHeader.UpdatedUserID = WMMessage.User.UserID;
            IOHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            IOHeader.TotalQuantity = decimal.Parse(textEditTotalQuantity.Text);

            if (await IOHeaderRepository.Update(IOHeader, textEditIONumber.Text) > 0)
            {
                updateIODetailAsync();
                


                if (comboBoxEditHandlingStatus.SelectedIndex == 3)
                {
                    string mCurrentDateTime = DateTime.Now.ToString("MM/yyyy");
                    string mCurrentPeriod = String.Format(mCurrentDateTime, "MM/yyyy");
                    int mYear = int.Parse(mCurrentPeriod.Substring(3, 4));
                    int mMonth = int.Parse(mCurrentPeriod.Substring(0, 2));

                    List<IODetail> iODetails = await IODetailRepository.GetUnderID(textEditIONumber.Text);

                    if (iODetails.Count > 0)
                    {
                        updateInventory(iODetails);
                        updateInventoryDetail(iODetails);
                        MessageBox.Show("Điều chỉnh dữ liệu thành cộng-> Đã trừ tồn kho");
                    }
                }
                else
                {
                    MessageBox.Show(WMMessage.msgSaveChangeSuccess);
                }
            }
        }

        private async void updateInventory(List<IODetail> iODetails)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));

            foreach (IODetail iODetail in iODetails)
            {
                List<Inventory> inventories = await inventoryRepository.GetUnderMultiPartNumber(mYear, mMonth, "1",iODetail.PartNumber);
                if (inventories.Count > 0)
                {
                    inventories[0].IssueQuantity = inventories[0].IssueQuantity + iODetail.IssueQuantity;
                    inventories[0].ClosingStockQuantity = inventories[0].ClosingStockQuantity-iODetail.IssueQuantity;
                    await inventoryRepository.Update(inventories[0], mYear, mMonth, "1", iODetail.PartNumber);
                }
                else
                {
                    Inventory inventory = new Inventory();
                    inventory.Year = mYear;
                    inventory.Month = mMonth;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber =iODetail.PartNumber;
                    inventory.PackingVolume =iODetail.IssueQuantity;
                    inventory.OpeningStockQuantity = 0;
                    inventory.ReceiptQuantity =0;
                    inventory.IssueQuantity = iODetail.IssueQuantity;
                    inventory.ClosingStockQuantity = inventory.OpeningStockQuantity -inventory.IssueQuantity;
                    inventory.Unit = iODetail.Unit;
                    await inventoryRepository.Create(inventory);
                }
            }
        }

        private async void updateInventoryDetail(List<IODetail> iODetails)
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            foreach (IODetail iODetail in iODetails)
            {
                List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams6(mYear, mMonth, "1", iODetail.PartNumber,iODetail.PalletID, iODetail.IDCode);
                if (inventoryDetails.Count>0)
                {
                    await inventoryDetailRepository.Delete6(inventoryDetails[0].Year,inventoryDetails[0].Month,"1",inventoryDetails[0].PartNumber
                        ,inventoryDetails[0].PalletID,inventoryDetails[0].IDCode);
                }
            }
        }

        public async Task updateIODetailAsync()
        {
            int num = gridViewIODetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewIODetail.GetRowCellValue(i, gridViewIODetail.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewIODetail.GetRowHandle(i);
                String IONumber = textEditIONumber.Text;
                String PartNumber = (string)gridViewIODetail.GetRowCellValue(rowHandle, "PartNumber");
                String PalletID= (string)gridViewIODetail.GetRowCellValue(rowHandle, "PalletID");
                int Ordinal = (int)gridViewIODetail.GetRowCellValue(rowHandle, "Ordinal");
                List<IODetail> IODetails = await IODetailRepository.GetByParams3(IONumber,PalletID,PartNumber);
                IODetail IODetail = IODetails[0];
                IODetail.IONumber = textEditIONumber.Text;
                IODetail.PartNumber = (string)gridViewIODetail.GetRowCellValue(rowHandle, "PartNumber");
                IODetail.PartName= (string)gridViewIODetail.GetRowCellValue(rowHandle, "PartName");
                IODetail.PalletID = (string)gridViewIODetail.GetRowCellValue(rowHandle, "PalletID");
                IODetail.Unit = (string)gridViewIODetail.GetRowCellValue(rowHandle, "Unit");
                IODetail.Ordinal = i + 1;
                IODetail.IssueQuantity = (Decimal?)gridViewIODetail.GetRowCellValue(rowHandle, "IssueQuantity");
                IODetail.QuantityOrdered = (Decimal?)gridViewIODetail.GetRowCellValue(rowHandle, "QuantityOrdered");
                IODetail.PackingQuantity = (Decimal?)gridViewIODetail.GetRowCellValue(i, "PackingQuantity");
                IODetail.PackingVolume = (Decimal?)gridViewIODetail.GetRowCellValue(i, "PackingVolume");
                //if (((DateTime?)gridViewIODetail.GetRowCellValue(i, "MFGDate")).ToString() != "")
                //{
                //    IODetail.MFGDate = (DateTime?)gridViewIODetail.GetRowCellValue(i, "MFGDate");
                //}
                //if (((DateTime?)gridViewIODetail.GetRowCellValue(i, "EXPDate")).ToString() != "")
                //{
                //    IODetail.EXPDate = (DateTime?)gridViewIODetail.GetRowCellValue(i, "EXPDate");
                //}
                //if (((DateTime?)gridViewIODetail.GetRowCellValue(i, "InputDate")).ToString() != "")
                //{
                //    IODetail.InputDate = (DateTime?)gridViewIODetail.GetRowCellValue(i, "InputDate");
                //}
                if (((DateTime?)gridViewIODetail.GetRowCellValue(i, "OutputDate")).ToString() != "")
                {
                    IODetail.OutputDate = (DateTime?)gridViewIODetail.GetRowCellValue(i, "OutputDate");
                }
                if (await IODetailRepository.Update(IODetail, textEditIONumber.Text,IODetail.PalletID, IODetail.PartNumber, IODetail.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        //private async void updateInventory()
        //{
        //    int num = gridViewIODetail.RowCount - 1;
        //    int i = 0;
        //    while (true)
        //    {
        //        int num2 = i;
        //        int num3 = num;
        //        if (num2 > num3 || gridViewIODetail.GetRowCellValue(i, gridViewIODetail.Columns["colPartNumber"]) == "")
        //        {
        //            break;
        //        }
        //        int rowHandle = gridViewIODetail.GetRowHandle(i);
        //        Inventory inventorys = new Inventory();

        //        inventorys.Year = 2022;
        //        inventorys.Month = 6;
        //        inventorys.WarehouseID = "1";
        //        inventorys.PartNumber = (string)gridViewIODetail.GetRowCellValue(rowHandle, "PartNumber");
        //        inventorys.Status = "1";
        //        List<Inventory> inventories1 = await inventoryRepository.GetByPartNumber("1", (string)gridViewIODetail.GetRowCellValue(rowHandle, "PartNumber"));
        //        if (inventories1.Count > 0)
        //        {
        //            inventorys.OpeningStockQuantity =inventories1[0].OpeningStockQuantity;
        //            inventorys.ReceiptQuantity = inventories1[0].ReceiptQuantity;
        //            inventorys.IssueQuantity = (Decimal?)gridViewIODetail.GetRowCellValue(rowHandle, "IssueQuantity") + inventories1[0].IssueQuantity;
        //            inventorys.ClosingStockQuantity = -(Decimal?)gridViewIODetail.GetRowCellValue(rowHandle, "IssueQuantity")+ inventories1[0].ClosingStockQuantity;
        //            if (await inventoryRepository.Update(inventorys, inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber) > 0)
        //            {
        //                i++;
        //            }
        //        }
        //        else
        //        {
        //            inventorys.OpeningStockQuantity = 0m;
        //            inventorys.ReceiptQuantity = 0m; 
        //            inventorys.IssueQuantity = (Decimal?)gridViewIODetail.GetRowCellValue(rowHandle, "IssueQuantity");
        //            inventorys.ClosingStockQuantity = -(Decimal?)gridViewIODetail.GetRowCellValue(rowHandle, "IssueQuantity");
        //            if (await inventoryRepository.Create(inventorys) > 0)
        //            {
        //                i++;
        //            }
        //        }
        //    }

        //}

        //private async void updateInventoryDetail()
        //{
        //    int num = gridViewIODetail.RowCount - 1;
        //    int i = 0;
        //    while (true)
        //    {
        //        int num2 = i;
        //        int num3 = num;
        //        if (num2 > num3 || gridViewIODetail.GetRowCellValue(i, gridViewIODetail.Columns["colPartNumber"]) == "")
        //        {
        //            break;
        //        }
        //        int rowHandle = gridViewIODetail.GetRowHandle(i);
        //        InventoryDetail inventorys = new InventoryDetail();
        //        inventorys.Year = 2022;
        //        inventorys.Month = 6;
        //        inventorys.WarehouseID = "1";
        //        inventorys.PartNumber = (string)gridViewIODetail.GetRowCellValue(rowHandle, "PartNumber");
        //        inventorys.PalletID = (string)gridViewIODetail.GetRowCellValue(rowHandle, "PalletID");
        //        inventorys.LotID = (string)gridViewIODetail.GetRowCellValue(rowHandle, "LotID");
        //        inventorys.Quantity = (decimal?)gridViewIODetail.GetRowCellValue(rowHandle, "PackingVolume");
        //        inventorys.Status = "1";
        //        if (((DateTime?)gridViewIODetail.GetRowCellValue(i, "MFGDate")).ToString() != "")
        //        {
        //            inventorys.MFGDate = (DateTime?)gridViewIODetail.GetRowCellValue(i, "MFGDate");
        //        }
        //        if (((DateTime?)gridViewIODetail.GetRowCellValue(i, "EXPDate")).ToString() != "")
        //        {
        //            inventorys.EXPDate = (DateTime?)gridViewIODetail.GetRowCellValue(i, "EXPDate");
        //        }
        //        if (((DateTime?)gridViewIODetail.GetRowCellValue(i, "InputDate")).ToString() != "")
        //        {
        //            inventorys.InputDate = (DateTime?)gridViewIODetail.GetRowCellValue(i, "InputDate");
        //        }

        //        string idCode = gridViewIODetail.GetRowCellValue(rowHandle, "IDCode") != null ? gridViewIODetail.GetRowCellValue(rowHandle, "IDCode").ToString() : "";

        //        if (idCode == "")
        //        {
        //            i++;
        //        }
        //        else
        //        {
        //            List<string> idCodes = idCode.Split(',').ToList();

        //            foreach (string s in idCodes)
        //            {
        //                if (!s.Equals(""))
        //                {
        //                    inventorys.IDCode = s;
        //                    List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams6(inventorys.Year, inventorys.Month, inventorys.WarehouseID,
        //                        inventorys.PartNumber, inventorys.PalletID, inventorys.IDCode);

        //                    if (inventoryDetails.Count > 0)
        //                    {
        //                        await inventoryDetailRepository.Delete6(inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber,
        //                        inventorys.PalletID, inventorys.IDCode);
        //                    }
        //                }
        //            }
        //            i++;
        //        }

        //    }
        //    MessageBox.Show("Đã cập nhật tồn kho");
        //}

        private async void updateInventory()
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;

            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }

            List<Inventory> inventories = new List<Inventory>();
            List<IODetail> iODetails = await IODetailRepository.GetUnderID(textEditIONumber.Text);
            if (iODetails.Count > 0)
            {
                foreach (IODetail iODetail in iODetails)
                {
                    List<Inventory> inventories1 = await inventoryRepository.GetByPartNumber("1", iODetail.PartNumber);
                    List<InventoryOfLastMonths> inventoryOfLastMonths = await inventoryOfLastMonthsRepository.GetByPartNumber("1", iODetail.PartNumber);
                    Inventory inventory = new Inventory();
                    inventory.Month = mMonth;
                    inventory.Year = mYear;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = iODetail.PartNumber;
                    inventory.ReceiptQuantity =0;
                    inventory.IssueQuantity =iODetail.IssueQuantity;
                    inventory.Status = "1";
                    inventory.PackingVolume = iODetail.PackingVolume;
                    inventory.IDCode = "";
                    inventories.Add(inventory);
                }


            }
            var querys = inventories.GroupBy(x => x.PartNumber)
                       .Select(x => new Inventory
                       {
                           PartNumber = x.Key,
                           Year = x.First().Year,
                           Month = x.First().Month,
                           WarehouseID = x.First().WarehouseID,
                           Status = x.First().Status,
                           ReceiptQuantity = x.Sum(y => y.ReceiptQuantity),
                           IssueQuantity = x.Sum(y => y.IssueQuantity),
                           PackingVolume = x.First().PackingVolume,
                           Material = "",
                           IDCode = "",
                       })
                      .ToList();

            await inventoryRepository.DeleteOldDate("1");
            foreach (Inventory inventorys in querys)
            {
                List<Goods> goodsList = await goodsRepository.GetUnderPartNumber(inventorys.PartNumber);
                if (goodsList.Count > 0)
                {
                    inventorys.Mold = goodsList[0].Mold;
                }

                List<Inventory> inventories1 = await inventoryRepository.GetUnderMultiPartNumber(mYear, mMonth, "1", inventorys.PartNumber);
                List<InventoryOfLastMonths> inventoryOfLastMonths = await inventoryOfLastMonthsRepository.GetByPartNumber("1", inventorys.PartNumber);

                if (inventories1.Count > 0)
                {
                    inventorys.ReceiptQuantity = inventorys.ReceiptQuantity + inventories1[0].ReceiptQuantity;
                    inventorys.IssueQuantity = inventorys.IssueQuantity + inventories1[0].IssueQuantity;
                    inventorys.OpeningStockQuantity = inventoryOfLastMonths.Count > 0 ? inventoryOfLastMonths[0].ClosingStockQuantity : 0;
                    inventorys.ClosingStockQuantity = inventories1[0].OpeningStockQuantity + inventorys.ReceiptQuantity - inventorys.IssueQuantity;
                    await inventoryRepository.Update(inventorys, inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber);
                }
                else
                {
                    inventorys.ReceiptQuantity = inventorys.ReceiptQuantity;
                    inventorys.IssueQuantity = inventorys.IssueQuantity;
                    inventorys.OpeningStockQuantity = inventoryOfLastMonths.Count > 0 ? inventoryOfLastMonths[0].ClosingStockQuantity : 0;
                    inventorys.ClosingStockQuantity = inventorys.ReceiptQuantity - inventorys.IssueQuantity;
                    await inventoryRepository.Create(inventorys);
                }
            }

        }

        private async void updateInventoryDetail()
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;

            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }

            List<InventoryDetail> inventories = new List<InventoryDetail>();
            List<IODetail> iODetails = await IODetailRepository.GetUnderID(textEditIONumber.Text);
            if (iODetails.Count > 0)
            {
                foreach (IODetail iODetail in iODetails)
                {
                    InventoryDetail inventory = new InventoryDetail();
                    inventory.Month = mMonth;
                    inventory.Year = mYear;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = iODetail.PartNumber;
                    inventory.PalletID = iODetail.PalletID;
                    inventory.LotID = iODetail.LotID;
                    inventory.IDCode = iODetail.IDCode;
                    inventory.Quantity = iODetail.PackingVolume;
                    inventory.Status = "1";
                    inventory.MFGDate = iODetail.MFGDate;
                    inventory.EXPDate = iODetail.EXPDate;
                    inventory.InputDate = iODetail.InputDate;
                    inventories.Add(inventory);

                }


            }
            var querys = inventories.GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID })
                 .Select(x => new InventoryDetail
                 {
                     PartNumber = x.Key.PartNumber,
                     PalletID = x.Key.PalletID,
                     LotID = x.Key.LotID,
                     Year = x.First().Year,
                     Month = x.First().Month,
                     WarehouseID = x.First().WarehouseID,
                     Status = x.First().Status,
                     Quantity = x.Sum(y => y.Quantity),
                     IDCode = string.Join(",", x.Select(ss => ss.IDCode)),
                     MFGDate = x.First().MFGDate,
                     EXPDate = x.First().EXPDate,
                     InputDate = x.First().InputDate
                 })
                .ToList();

            await inventoryDetailRepository.DeleteOldDate("1");
            foreach (InventoryDetail inventoryDetail in querys)
            {
                string idCode = !inventoryDetail.IDCode.Equals("") ? inventoryDetail.IDCode : "";

                List<string> idCodes = idCode.Split(',').ToList();

                foreach (string s in idCodes)
                {
                    if (!s.Equals(""))
                    {
                        inventoryDetail.IDCode = s;
                        List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams6(inventoryDetail.Year, inventoryDetail.Month, inventoryDetail.WarehouseID,
                            inventoryDetail.PartNumber, inventoryDetail.PalletID, inventoryDetail.IDCode);

                        if (inventoryDetails.Count>0)
                        {
                            await inventoryDetailRepository.Delete6(inventoryDetail.Year, inventoryDetail.Month, inventoryDetail.WarehouseID, inventoryDetail.PartNumber,
                                inventoryDetail.PalletID, inventoryDetail.IDCode);
                        }
                    }
                }
            }
            BottomBarHelper.successMessage(m_MainForm.barStaticItemMessage,m_MainForm.barStaticItemMessage.Caption +"-> Đã trừ tồn kho");
        }

        private async void updateInventoryOfLastMonth()
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;

            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }


            await inventoryOfLastMonthsRepository.DeleteOldDate("1");
            if ((await inventoryOfLastMonthsRepository.GetUnderMultiID(mLastYear, mLastMonth, "1")).Count == 0)
            {
                List<Inventory> inventories = await inventoryRepository.GetUnderMultiID(mLastYear, mLastMonth, "1");
                foreach (Inventory inventory in inventories)
                {
                    InventoryOfLastMonths inventorys = new InventoryOfLastMonths();
                    inventorys.Year = inventory.Year;
                    inventorys.Month = inventory.Month;
                    inventorys.WarehouseID = inventory.WarehouseID;
                    inventorys.PartNumber = inventory.PartNumber;
                    inventorys.Status = "0";
                    inventorys.OpeningStockQuantity = inventory.OpeningStockQuantity;
                    inventorys.ReceiptQuantity = inventory.ReceiptQuantity;
                    inventorys.IssueQuantity = inventory.IssueQuantity;
                    inventorys.ClosingStockQuantity = inventory.ClosingStockQuantity;
                    await inventoryOfLastMonthsRepository.Create(inventorys);
                }
            }

        }

        private async Task createInventoryOfLastMonth()
        {

            await inventoryOfLastMonthsRepository.DeleteOldDate("1");

            List<Inventory> inventories = await inventoryRepository.GetByWarehouseID("1");
            if (inventories.Count > 0)
            {
                foreach (Inventory inventory in inventories)
                {
                    InventoryOfLastMonths inventorys = new InventoryOfLastMonths();
                    inventorys.Year = inventory.Year;
                    inventorys.Month = inventory.Month;
                    inventorys.WarehouseID = inventory.WarehouseID;
                    inventorys.PartNumber = inventory.PartNumber;
                    inventorys.Status = "0";
                    inventorys.PackingVolume = inventory.PackingVolume;
                    inventorys.Material = inventory.Material;
                    inventorys.Mold = inventory.Mold;
                    inventorys.OpeningStockQuantity = inventory.OpeningStockQuantity;
                    inventorys.ReceiptQuantity = inventory.ReceiptQuantity;
                    inventorys.IssueQuantity = inventory.IssueQuantity;
                    inventorys.ClosingStockQuantity = inventory.ClosingStockQuantity;
                    await inventoryOfLastMonthsRepository.Create(inventorys);
                }
            }



        }

        private async Task createInventoryOfLastMonthDetail()
        {


            await inventoryOfLastMonthDetailRepository.DeleteOldDate("1");
            List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByWarehouseID("1");
            if (inventoryDetails.Count > 0)
            {
                foreach (InventoryDetail inventoryDetail in inventoryDetails)
                {
                    InventoryOfLastMonthDetail inventory = new InventoryOfLastMonthDetail();
                    inventory.Month = inventoryDetail.Month;
                    inventory.Year = inventoryDetail.Year;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = inventoryDetail.PartNumber;
                    inventory.PalletID = inventoryDetail.PalletID;
                    inventory.LotID = inventoryDetail.LotID;
                    inventory.IDCode = inventoryDetail.IDCode;
                    inventory.Quantity = inventoryDetail.Quantity;
                    inventory.Status = "1";
                    inventory.MFGDate = inventoryDetail.MFGDate;
                    inventory.EXPDate = inventoryDetail.EXPDate;
                    inventory.InputDate = inventoryDetail.InputDate;
                    await inventoryOfLastMonthDetailRepository.Create(inventory);
                }


            }

        }


        private async void IONumber_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private async void textEditIONumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchIO frmSearchIO = new frmSearchIO(this);
            frmSearchIO.ShowDialog(this);
            frmSearchIO.Dispose();
            if (selectedIONumber != null)
            {
                textEditIONumber.Text = selectedIONumber;
                List<IOHeader> iOHeaders = await IOHeaderRepository.GetUnderID(textEditIONumber.Text);
                IOHeader IOHeader = iOHeaders[0];
                dateEditIODate.Text = DateTime.Parse(iOHeaders[0].IODate.ToString()).ToString(Constant.smallDateTimeFormat);
                textEditNote.Text = IOHeader.Note;
                comboBoxEditHandlingStatus.SelectedIndex = iOHeaders[0].HandlingStatusID.Equals("<>") ? 0 : int.Parse(iOHeaders[0].HandlingStatusID)+1;
                comboBoxEditModality.SelectedIndex = int.Parse(iOHeaders[0].ModalityID) - 1;
                comboBoxEditWarehouse.SelectedIndex = iOHeaders[0].WarehouseID.Equals("<>") ? 0 : int.Parse(iOHeaders[0].WarehouseID);
                textEditPONumber.Text = iOHeaders[0].IONumber.Substring(0, iOHeaders[0].IONumber.LastIndexOf("-"));
                await sbLoadDataForGridIODetailAsync(textEditIONumber.Text);
            }

        }

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    break;
                case "save":
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateIOHeaderAsync();
                    }             
                    break;
                case "delete":           
                    break;
                case "search":

                    break;
                case "refesh":
                    sbLoadDataForGridIODetailAsync(textEditIONumber.Text);
                    break;
                case "import":
                    break;
                case "export":
                    using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Excel Workbook|*.xlsx", FileName = textEditIONumber.Text })
                    {
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            string path = sfd.FileName;
                            ExportSettings.DefaultExportType = ExportType.DataAware;
                            // Create a new object defining how a document is exported to the XLSX format.
                            var options = new XlsxExportOptionsEx();
                            // Specify a name of the sheet in the created XLSX file.
                            options.SheetName = textEditIONumber.Text;

                            
                            List<IODetail> iODetails =await IODetailRepository.GetUnderID(textEditIONumber.Text);
                            List<IssueOrderInfo> issueOrderInfos = new List<IssueOrderInfo>();
                            no = 0;
                            foreach(IODetail iODetail in iODetails)
                            {
                                string[] listIDCode = iODetail.IDCode.Split(',');
                                if (listIDCode.Length > 0)
                                {
                                    for(int i = 0; i < listIDCode.Length - 1; i++)
                                    {
                                        issueOrderInfos.Add(new IssueOrderInfo
                                        {
                                            No = no + 1,
                                            IDCode=listIDCode[i],
                                            PartNumber=iODetail.PartNumber,
                                            PartName=iODetail.PartName,
                                            ProductFamily=iODetail.PartName,
                                            LotID=iODetail.LotID,
                                            Unit=iODetail.Unit,
                                            Quantity=iODetail.PackingVolume,
                                            MFGDate=iODetail.MFGDate,
                                            EXPDate=iODetail.EXPDate,
                                        });
                                        no++;
                                    }
                                }
                            }

                            List<Goods> goods = await goodsRepository.getAll();
                            foreach(Goods goods1 in goods)
                            {
                                foreach(IssueOrderInfo issueOrderInfo in issueOrderInfos)
                                {
                                    if (goods1.PartNumber.Equals(issueOrderInfo.PartNumber))
                                    {
                                        issueOrderInfo.ProductFamily = goods1.ProductFamily;
                                    }
                                }
                            }

                            GridControl gridControl = new GridControl();
                            gridControl.Parent = this;
                            gridControl.Dock = DockStyle.Fill;
                            GridView gridView1 = gridControl.MainView as GridView;
                           
                            gridView1.BestFitColumns();
                            gridControl.DataSource = issueOrderInfos;
                            gridView1.Columns["IDCode"].Caption = "Mã hàng";
                            gridView1.Columns["Quantity"].Caption = "Số lượng";
                            gridView1.Columns["Quantity"].DisplayFormat.FormatType = FormatType.Custom;
                            gridView1.Columns["Quantity"].DisplayFormat.FormatString = "G29";
                            gridView1.Columns["IDCode"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;


                            // Subscribe to export customization events. 
                            //options.CustomizeSheetSettings += options_CustomizeSheetSettings;
                            //options.CustomizeSheetHeader += options_CustomizeSheetHeader;
                            options.CustomizeCell += options_CustomizeCell;
                            //options.CustomizeSheetFooter += options_CustomizeSheetFooter;
                            options.CustomizeSheetHeader +=options_CustomizeSheetHeader;
                            //options.AfterAddRow += options_AfterAddRow;
                            options.TextExportMode = TextExportMode.Text;


                            gridControl.ExportToXlsx(path, options);
                           

                            // Open the created XLSX file with the default application.
                            Process.Start(path);
                        }
                    }
                    break;
                case "exportLotInfo":
                    exportPO();
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }

        // Create a new cell with a specified value and format settings.
        static CellObject CreateCell(object value, XlFormattingObject formatCell)
        {
            return new CellObject { Value = value, Formatting = formatCell };
        }

        void options_CustomizeSheetHeader(ContextEventArgs e)
        {
            XlFormattingObject rowFormatting = CreateXlFormattingObject(true, 16);
            rowFormatting.Font.Size = 14;
            rowFormatting.Font.Bold = false;
            rowFormatting.Font.Italic = true;
            rowFormatting.Alignment.HorizontalAlignment = XlHorizontalAlignment.Left;
            var emailCellName = CreateCell("Số phiếu xuất :", rowFormatting);
            var emailCellLocation = CreateCell(textEditIONumber.Text, rowFormatting);
            var TotalCartonCellName = CreateCell("Tổng số thùng :", rowFormatting);
            var TotalCarton = CreateCell(no+"", rowFormatting);
            e.ExportContext.AddRow(new[] { emailCellName,emailCellLocation });
            e.ExportContext.AddRow(new[] {TotalCartonCellName,TotalCarton});
            e.ExportContext.AddRow(new[] {""});

        }

        #region #CustomizeSheetFooterEvent
        void options_CustomizeSheetFooter(ContextEventArgs e)
        {
            // Add an empty row to the document's footer.
            e.ExportContext.AddRow();

            // Create a new row.
            var firstRow = new CellObject();
            // Specify row values.
            firstRow.Value = @"Tổng số thùng: " + gridViewIODetail.RowCount;
            // Specify the cell content alignment and font settings.
            var rowFormatting = CreateXlFormattingObject(true, 16);
            rowFormatting.Font.Size = 14;
            rowFormatting.Font.Bold = false;
            rowFormatting.Font.Italic = true;
            rowFormatting.Alignment.HorizontalAlignment = XlHorizontalAlignment.Left;
            firstRow.Formatting = rowFormatting;
            // Add the created row to the output document. 
            e.ExportContext.AddRow(new[] { firstRow});
        }

        // Specify a cell's alignment and font settings. 
        static XlFormattingObject CreateXlFormattingObject(bool bold, double size)
        {
            var cellFormat = new XlFormattingObject
            {
                Font = new XlCellFont
                {
                    Bold = bold,
                    Size = size
                },
                Alignment = new XlCellAlignment
                {
                    RelativeIndent = 10,
                    HorizontalAlignment = XlHorizontalAlignment.Center,
                    VerticalAlignment = XlVerticalAlignment.Center
                }
            };
            return cellFormat;
        }
        #endregion #CustomizeSheetFooterEvent

        #region #CustomizeCellEvent
        // Specify the value alignment for Discontinued field.
        XlCellAlignment aligmentForDiscontinuedColumn = new XlCellAlignment()
        {
            HorizontalAlignment = XlHorizontalAlignment.Center,
            VerticalAlignment = XlVerticalAlignment.Center
        };

        void options_CustomizeCell(CustomizeCellEventArgs e)
        {

            // Substitute Boolean values within the Discontinued column by special symbols.          
            e.Handled = true;
            e.Formatting.Alignment = aligmentForDiscontinuedColumn;

        }
        #endregion #CustomizeCellEvent


        private async Task<List<IODetail>> getIOByPO()
        {
            List<IODetail> iODetailsNew = new List<IODetail>();
            List<string> listIONumber = new List<string>();

            for (int i = 0; i < gridView1.RowCount; i++)
            {
                int rowHandle = gridView1.GetRowHandle(i);
                string ioNumber = (string)gridView1.GetRowCellValue(rowHandle, "IONumber");
                listIONumber.Add(ioNumber);
            }

            foreach (string s in listIONumber)
            {
                List<IODetail> iODetails = await IODetailRepository.GetUnderID(s);
                iODetailsNew.AddRange(iODetails);
            }

            return iODetailsNew;
        }


        private async void exportPO()
        {
            List<IODetail> ioDetails = await getIOByPO();

            if (ioDetails.Count == 0)
            {
                BottomBarHelper.errorMessage(m_MainForm.barStaticItemMessage, "Không có dữ liệu để xuất ");
                return;

            }


            var querys1 = ioDetails.GroupBy(x => new { x.PartNumber, x.LotID })
                 .Select(x => new LotInfo
                 {
                     PartNumber = x.Key.PartNumber,
                     Lot = x.Key.LotID,
                     MFGDate = x.First().MFGDate,
                     Qty = x.Sum(y =>y.IssueQuantity),
                     QuantityPerlot = x.First().PackingVolume,
                     Cartons = x.Sum(y => y.PackingQuantity),
                 })
                .ToList();

            var querys2 = querys1.GroupBy(x => new { x.PartNumber })
                .Select(x => new LotInfo
                {
                    PartNumber = x.Key.PartNumber,
                    Qty = x.Sum(y => y.Qty),
                })
               .ToList();

            string currentDate = DateTime.Now.ToString("yyyy.MM.dd");

            string po = "";
            int index1 = ioDetails[0].IONumber.IndexOf("-");
            int index2 = ioDetails[0].IONumber.LastIndexOf("-");
            if (index2 > 0)
            {
                po = ioDetails[0].IONumber.Substring(0, index2);
            }




            using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Excel Workbook|*.xlsx", FileName = "[" + currentDate + "] " + "lot info-" + po })
            {
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    var fileInfo = new FileInfo(sfd.FileName);
                    using (ExcelPackage excelPackage = new ExcelPackage(fileInfo))
                    {
                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add(po);

                        worksheet.TabColor = System.Drawing.Color.Black;
                        worksheet.DefaultRowHeight = 12;
                        //Header of table  
                        //  
                        worksheet.Row(1).Height = 50;
                        worksheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Row(1).Style.Font.Bold = true;
                        worksheet.Cells[1, 1].Value = "PO #";
                        worksheet.Cells[1, 2].Value = "Invoice #";
                        worksheet.Cells[1, 3].Value = "P/N";
                        worksheet.Cells[1, 4].Value = "Ship To";
                        worksheet.Cells[1, 5].Value = "MFG(MM/DD/YY)(Optional)";
                        worksheet.Cells[1, 6].Value = "QTY";
                        worksheet.Cells[1, 7].Value = "LOT";
                        worksheet.Cells[1, 8].Value = "Qty per Lot";
                        worksheet.Cells[1, 9].Value = "Cartons";
                        //Body of table  
                        //  
                        int recordIndex = 2;
                        string str = "";
                        List<int> range = new List<int>();

                        foreach (var lotinfo in querys1)
                        {
                            //worksheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();

                            if (str.Contains(lotinfo.PartNumber))
                            {
                                worksheet.Cells[recordIndex, 3].Value = "";
                                worksheet.Cells[recordIndex, 6].Value = "";

                            }
                            else
                            {
                                worksheet.Cells[recordIndex, 3].Value = lotinfo.PartNumber;
                                worksheet.Cells[recordIndex, 6].Value = (querys2.Where(s => s.PartNumber.Equals(lotinfo.PartNumber)).ToList())[0].Qty;
                                str += lotinfo.PartNumber;
                                range.Add(recordIndex);
                            }

                            worksheet.Cells[recordIndex, 5].Value = lotinfo.MFGDate;

                            worksheet.Cells[recordIndex, 7].Value = lotinfo.Lot;
                            worksheet.Cells[recordIndex, 8].Value = lotinfo.QuantityPerlot;
                            worksheet.Cells[recordIndex, 9].Value = lotinfo.Cartons;
                            worksheet.Cells[recordIndex, 5].Style.Numberformat.Format = "yyyy/MM/dd";

                            recordIndex++;
                        }

                        worksheet.Cells[2, 1, querys1.Count + 1, 1].Merge = true;
                        worksheet.Cells[2, 2, querys1.Count + 1, 2].Merge = true;
                        for (int i = 0; i < range.Count; i++)
                        {
                            if (i == range.Count - 1)
                            {
                                worksheet.Cells[range[i], 3, querys1.Count + 1, 3].Merge = true;
                                worksheet.Cells[range[i], 6, querys1.Count + 1, 6].Merge = true;
                                worksheet.Cells[range[i], 4, querys1.Count + 1, 4].Merge = true;
                            }
                            else
                            {
                                worksheet.Cells[range[i], 3, range[i + 1] - 1, 3].Merge = true;
                                worksheet.Cells[range[i], 6, range[i + 1] - 1, 6].Merge = true;
                                worksheet.Cells[range[i], 4, range[i + 1] - 1, 4].Merge = true;
                            }
                        }

                        worksheet.Cells[querys1.Count + 2, 2].Value = "Total:";
                        worksheet.Cells[querys1.Count + 2, 6].Value = querys1.Sum(s => s.Qty);
                        worksheet.Cells[querys1.Count + 2, 9].Value = querys1.Sum(s => s.Cartons);
                        worksheet.Cells[querys1.Count + 2, 1, querys1.Count + 2, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[querys1.Count + 2, 1, querys1.Count + 2, 5].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGreen);
                        worksheet.Cells[querys1.Count + 2, 6, querys1.Count + 2, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[querys1.Count + 2, 6, querys1.Count + 2, 7].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGreen);
                        worksheet.Cells[querys1.Count + 2, 8, querys1.Count + 2, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[querys1.Count + 2, 8, querys1.Count + 2, 9].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGreen);

                        worksheet.Column(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(2).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(5).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(6).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(7).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(8).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Column(9).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Column(3).Width = 30;
                        worksheet.Column(5).Width = 30;
                        worksheet.Column(6).Width = 10;
                        worksheet.Column(7).Width = 20;
                        worksheet.Column(8).Width = 10;





                        //style
                        //worksheet.Column(1).AutoFit();
                        //worksheet.Column(2).AutoFit();
                        //worksheet.Column(3).AutoFit();
                        //worksheet.Column(4).AutoFit();
                        //worksheet.Column(5).AutoFit();
                        //worksheet.Column(6).AutoFit();
                        //worksheet.Column(7).AutoFit();

                        excelPackage.Save();


                    }
                    Process.Start(sfd.InitialDirectory + sfd.FileName);
                }
            }
        }

        private async void gridViewIODetail_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (gridViewIODetail.RowCount > 0)
            {
                string ioNumber = (string)(sender as GridView).GetFocusedRowCellValue("IONumber");
                string partNumber = (string)(sender as GridView).GetFocusedRowCellValue("PartNumber");
                string palletID = (string)(sender as GridView).GetFocusedRowCellValue("PalletID");
                int ordinal = (int)(sender as GridView).GetFocusedRowCellValue("Ordinal");
                List<IODetail> iODetails = await IODetailRepository.GetByParams3(ioNumber,palletID, partNumber);
                if (iODetails.Count > 0)
                {
                    string[] idcodes = iODetails[0].IDCode.Split(',');
                    List<IODetail> iODetails1 = new List<IODetail>();


                    for (int i = 0; i < idcodes.Length - 1; i++)
                    {
                        IODetail iODetail = new IODetail();
                        iODetail.IDCode = idcodes[i];
                        iODetails1.Add(iODetail);
                    }
                    gridControl1.DataSource = iODetails1;
                }
            }
        }
    }
}
