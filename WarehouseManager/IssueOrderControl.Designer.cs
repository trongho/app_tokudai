﻿
namespace WarehouseManager
{
    partial class IssueOrderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IssueOrderControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions10 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDataDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.wRDataDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIDCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlIODetail = new DevExpress.XtraGrid.GridControl();
            this.wRDetailBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewIODetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityOrdered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPalletID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMFGDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXPDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInputDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutputDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLotID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wRDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRDetailTableAdapter();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.label1 = new System.Windows.Forms.Label();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEditIONumber = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditModality = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateEditIODate = new DevExpress.XtraEditors.DateEdit();
            this.comboBoxEditWarehouse = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditTotalQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditNote = new DevExpress.XtraEditors.MemoEdit();
            this.textEditPONumber = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWarehouse = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemModality = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlIODetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewIODetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditIONumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditModality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditIODate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditIODate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPONumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(444, 166, 812, 495);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1298, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("windowsUIButtonImageOptions8.SvgImage")));
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions10.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions10.Image")));
            windowsUIButtonImageOptions10.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Làm mới", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Tìm kiếm", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xuất Lot Info", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "exportLotInfo", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions10, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1294, 32);
            this.windowsUIButtonPanel1.TabIndex = 4;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1298, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1298, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // wRDataDetailBindingSource
            // 
            this.wRDataDetailBindingSource.DataMember = "WRDataDetail";
            this.wRDataDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wRDataDetailTableAdapter
            // 
            this.wRDataDetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControl1);
            this.layoutControl3.Controls.Add(this.gridControlIODetail);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 155);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(481, 337, 650, 400);
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1298, 471);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(1162, 12);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(124, 447);
            this.gridControl1.TabIndex = 6;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIDCode1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colIDCode1
            // 
            this.colIDCode1.Caption = "Mã hàng";
            this.colIDCode1.FieldName = "IDCode";
            this.colIDCode1.Name = "colIDCode1";
            this.colIDCode1.Visible = true;
            this.colIDCode1.VisibleIndex = 0;
            // 
            // gridControlIODetail
            // 
            this.gridControlIODetail.DataSource = this.wRDetailBindingSource1;
            this.gridControlIODetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlIODetail.Location = new System.Drawing.Point(12, 12);
            this.gridControlIODetail.MainView = this.gridViewIODetail;
            this.gridControlIODetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlIODetail.Name = "gridControlIODetail";
            this.gridControlIODetail.Size = new System.Drawing.Size(1146, 447);
            this.gridControlIODetail.TabIndex = 5;
            this.gridControlIODetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewIODetail});
            // 
            // wRDetailBindingSource1
            // 
            this.wRDetailBindingSource1.DataMember = "WRDetail";
            this.wRDetailBindingSource1.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // gridViewIODetail
            // 
            this.gridViewIODetail.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewIODetail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewIODetail.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.gridViewIODetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIONumber,
            this.colOrdinal,
            this.colPartNumber,
            this.colPartName,
            this.colQuantityOrdered,
            this.colIssueQuantity,
            this.colStatus,
            this.colNo,
            this.colPalletID,
            this.colUnit,
            this.colMFGDate,
            this.colEXPDate,
            this.colIDCode,
            this.colInputDate,
            this.colOutputDate,
            this.colPackingVolume,
            this.colPackingQuantity,
            this.colLotID});
            this.gridViewIODetail.DetailHeight = 284;
            this.gridViewIODetail.GridControl = this.gridControlIODetail;
            this.gridViewIODetail.Name = "gridViewIODetail";
            this.gridViewIODetail.OptionsCustomization.AllowSort = false;
            this.gridViewIODetail.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewIODetail.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewIODetail_FocusedRowChanged);
            // 
            // colIONumber
            // 
            this.colIONumber.Caption = "IO Number";
            this.colIONumber.FieldName = "IONumber";
            this.colIONumber.Name = "colIONumber";
            // 
            // colOrdinal
            // 
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.Name = "colOrdinal";
            // 
            // colPartNumber
            // 
            this.colPartNumber.Caption = "Part Number";
            this.colPartNumber.FieldName = "PartNumber";
            this.colPartNumber.MinWidth = 150;
            this.colPartNumber.Name = "colPartNumber";
            this.colPartNumber.Visible = true;
            this.colPartNumber.VisibleIndex = 2;
            this.colPartNumber.Width = 150;
            // 
            // colPartName
            // 
            this.colPartName.Caption = "Part Name";
            this.colPartName.FieldName = "PartName";
            this.colPartName.MinWidth = 200;
            this.colPartName.Name = "colPartName";
            this.colPartName.Visible = true;
            this.colPartName.VisibleIndex = 3;
            this.colPartName.Width = 200;
            // 
            // colQuantityOrdered
            // 
            this.colQuantityOrdered.Caption = "Lượng đặt hàng";
            this.colQuantityOrdered.DisplayFormat.FormatString = "G29";
            this.colQuantityOrdered.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityOrdered.FieldName = "QuantityOrdered";
            this.colQuantityOrdered.MinWidth = 120;
            this.colQuantityOrdered.Name = "colQuantityOrdered";
            this.colQuantityOrdered.Visible = true;
            this.colQuantityOrdered.VisibleIndex = 7;
            this.colQuantityOrdered.Width = 120;
            // 
            // colIssueQuantity
            // 
            this.colIssueQuantity.Caption = "Lượng xuất";
            this.colIssueQuantity.DisplayFormat.FormatString = "G29";
            this.colIssueQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colIssueQuantity.FieldName = "IssueQuantity";
            this.colIssueQuantity.MinWidth = 120;
            this.colIssueQuantity.Name = "colIssueQuantity";
            this.colIssueQuantity.Visible = true;
            this.colIssueQuantity.VisibleIndex = 8;
            this.colIssueQuantity.Width = 120;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            // 
            // colNo
            // 
            this.colNo.Caption = "No.";
            this.colNo.FieldName = "No";
            this.colNo.MinWidth = 43;
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 43;
            // 
            // colPalletID
            // 
            this.colPalletID.Caption = "Pallet Number";
            this.colPalletID.FieldName = "PalletID";
            this.colPalletID.MinWidth = 70;
            this.colPalletID.Name = "colPalletID";
            this.colPalletID.Visible = true;
            this.colPalletID.VisibleIndex = 1;
            this.colPalletID.Width = 70;
            // 
            // colUnit
            // 
            this.colUnit.Caption = "Unit";
            this.colUnit.FieldName = "Unit";
            this.colUnit.MinWidth = 40;
            this.colUnit.Name = "colUnit";
            this.colUnit.Visible = true;
            this.colUnit.VisibleIndex = 5;
            this.colUnit.Width = 40;
            // 
            // colMFGDate
            // 
            this.colMFGDate.Caption = "MFG Date";
            this.colMFGDate.FieldName = "MFGDate";
            this.colMFGDate.MinWidth = 70;
            this.colMFGDate.Name = "colMFGDate";
            this.colMFGDate.Visible = true;
            this.colMFGDate.VisibleIndex = 10;
            this.colMFGDate.Width = 70;
            // 
            // colEXPDate
            // 
            this.colEXPDate.Caption = "EXP Date";
            this.colEXPDate.FieldName = "EXPDate";
            this.colEXPDate.MinWidth = 70;
            this.colEXPDate.Name = "colEXPDate";
            this.colEXPDate.Visible = true;
            this.colEXPDate.VisibleIndex = 11;
            this.colEXPDate.Width = 70;
            // 
            // colIDCode
            // 
            this.colIDCode.Caption = "Mã hàng";
            this.colIDCode.FieldName = "IDCode";
            this.colIDCode.Name = "colIDCode";
            this.colIDCode.Width = 64;
            // 
            // colInputDate
            // 
            this.colInputDate.Caption = "Input Date";
            this.colInputDate.FieldName = "InputDate";
            this.colInputDate.MinWidth = 70;
            this.colInputDate.Name = "colInputDate";
            this.colInputDate.Width = 70;
            // 
            // colOutputDate
            // 
            this.colOutputDate.Caption = "Output Date";
            this.colOutputDate.FieldName = "OutputDate";
            this.colOutputDate.MinWidth = 70;
            this.colOutputDate.Name = "colOutputDate";
            this.colOutputDate.Visible = true;
            this.colOutputDate.VisibleIndex = 12;
            this.colOutputDate.Width = 70;
            // 
            // colPackingVolume
            // 
            this.colPackingVolume.Caption = "Quy cách";
            this.colPackingVolume.DisplayFormat.FormatString = "G29";
            this.colPackingVolume.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPackingVolume.FieldName = "PackingVolume";
            this.colPackingVolume.MinWidth = 50;
            this.colPackingVolume.Name = "colPackingVolume";
            this.colPackingVolume.Visible = true;
            this.colPackingVolume.VisibleIndex = 6;
            this.colPackingVolume.Width = 50;
            // 
            // colPackingQuantity
            // 
            this.colPackingQuantity.Caption = "SL Cartons Xuất";
            this.colPackingQuantity.DisplayFormat.FormatString = "G29";
            this.colPackingQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPackingQuantity.FieldName = "PackingQuantity";
            this.colPackingQuantity.MinWidth = 50;
            this.colPackingQuantity.Name = "colPackingQuantity";
            this.colPackingQuantity.Visible = true;
            this.colPackingQuantity.VisibleIndex = 9;
            this.colPackingQuantity.Width = 50;
            // 
            // colLotID
            // 
            this.colLotID.Caption = "Lot ID";
            this.colLotID.FieldName = "LotID";
            this.colLotID.MinWidth = 70;
            this.colLotID.Name = "colLotID";
            this.colLotID.Visible = true;
            this.colLotID.VisibleIndex = 4;
            this.colLotID.Width = 70;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem4});
            this.layoutControlGroup2.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup2.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 90D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 10D;
            this.layoutControlGroup2.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2});
            rowDefinition1.Height = 100D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup2.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1});
            this.layoutControlGroup2.Size = new System.Drawing.Size(1298, 471);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlIODetail;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1150, 451);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControl1;
            this.layoutControlItem4.Location = new System.Drawing.Point(1150, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem4.Size = new System.Drawing.Size(128, 451);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // wRDetailBindingSource
            // 
            this.wRDetailBindingSource.DataMember = "WRDetail";
            this.wRDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wRDetailTableAdapter
            // 
            this.wRDetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEdit4);
            this.layoutControl2.Controls.Add(this.textEditIONumber);
            this.layoutControl2.Controls.Add(this.comboBoxEditModality);
            this.layoutControl2.Controls.Add(this.dateEditIODate);
            this.layoutControl2.Controls.Add(this.comboBoxEditWarehouse);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantity);
            this.layoutControl2.Controls.Add(this.textEditNote);
            this.layoutControl2.Controls.Add(this.textEditPONumber);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(389, 386, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1298, 118);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(218, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "(*)";
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(2540, 10);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(53, 20);
            this.textEdit4.StyleController = this.layoutControl2;
            this.textEdit4.TabIndex = 11;
            // 
            // textEditIONumber
            // 
            this.textEditIONumber.Location = new System.Drawing.Point(95, 12);
            this.textEditIONumber.Name = "textEditIONumber";
            this.textEditIONumber.Size = new System.Drawing.Size(119, 20);
            this.textEditIONumber.StyleController = this.layoutControl2;
            this.textEditIONumber.TabIndex = 12;
            // 
            // comboBoxEditModality
            // 
            this.comboBoxEditModality.Location = new System.Drawing.Point(754, 12);
            this.comboBoxEditModality.Name = "comboBoxEditModality";
            this.comboBoxEditModality.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditModality.Size = new System.Drawing.Size(119, 20);
            this.comboBoxEditModality.StyleController = this.layoutControl2;
            this.comboBoxEditModality.TabIndex = 13;
            // 
            // dateEditIODate
            // 
            this.dateEditIODate.EditValue = null;
            this.dateEditIODate.Location = new System.Drawing.Point(342, 12);
            this.dateEditIODate.Name = "dateEditIODate";
            this.dateEditIODate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditIODate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditIODate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditIODate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditIODate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditIODate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditIODate.Size = new System.Drawing.Size(119, 20);
            this.dateEditIODate.StyleController = this.layoutControl2;
            this.dateEditIODate.TabIndex = 14;
            // 
            // comboBoxEditWarehouse
            // 
            this.comboBoxEditWarehouse.Location = new System.Drawing.Point(548, 12);
            this.comboBoxEditWarehouse.Name = "comboBoxEditWarehouse";
            this.comboBoxEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditWarehouse.Size = new System.Drawing.Size(119, 20);
            this.comboBoxEditWarehouse.StyleController = this.layoutControl2;
            this.comboBoxEditWarehouse.TabIndex = 17;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(960, 12);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(119, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 25;
            // 
            // textEditTotalQuantity
            // 
            this.textEditTotalQuantity.Location = new System.Drawing.Point(1166, 12);
            this.textEditTotalQuantity.Name = "textEditTotalQuantity";
            this.textEditTotalQuantity.Properties.DisplayFormat.FormatString = "G29";
            this.textEditTotalQuantity.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.textEditTotalQuantity.Size = new System.Drawing.Size(120, 20);
            this.textEditTotalQuantity.StyleController = this.layoutControl2;
            this.textEditTotalQuantity.TabIndex = 27;
            // 
            // textEditNote
            // 
            this.textEditNote.Location = new System.Drawing.Point(95, 36);
            this.textEditNote.Name = "textEditNote";
            this.textEditNote.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textEditNote.Size = new System.Drawing.Size(366, 44);
            this.textEditNote.StyleController = this.layoutControl2;
            this.textEditNote.TabIndex = 16;
            // 
            // textEditPONumber
            // 
            this.textEditPONumber.Location = new System.Drawing.Point(548, 36);
            this.textEditPONumber.Name = "textEditPONumber";
            this.textEditPONumber.Size = new System.Drawing.Size(119, 20);
            this.textEditPONumber.StyleController = this.layoutControl2;
            this.textEditPONumber.TabIndex = 28;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItemWRNumber,
            this.layoutControlItemTotalQuantity,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItemWRDate,
            this.layoutControlItemWarehouse,
            this.layoutControlItemModality,
            this.layoutControlItemNote,
            this.layoutControlItem5});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 16.129032258064516D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 3.225806451612903D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 16.129032258064516D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 16.129032258064516D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 16.129032258064516D;
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 16.129032258064516D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 16.129032258064516D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6,
            columnDefinition7,
            columnDefinition8,
            columnDefinition9});
            rowDefinition2.Height = 25D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 25D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 25D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition5.Height = 25D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1298, 118);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(206, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(41, 24);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemWRNumber
            // 
            this.layoutControlItemWRNumber.Control = this.textEditIONumber;
            this.layoutControlItemWRNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemWRNumber.Name = "layoutControlItemWRNumber";
            this.layoutControlItemWRNumber.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemWRNumber.Text = "Số phiếu xuất";
            this.layoutControlItemWRNumber.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemTotalQuantity
            // 
            this.layoutControlItemTotalQuantity.Control = this.textEditTotalQuantity;
            this.layoutControlItemTotalQuantity.Location = new System.Drawing.Point(1071, 0);
            this.layoutControlItemTotalQuantity.Name = "layoutControlItemTotalQuantity";
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalQuantity.Size = new System.Drawing.Size(207, 24);
            this.layoutControlItemTotalQuantity.Text = "Tổng lượng xuất";
            this.layoutControlItemTotalQuantity.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(865, 0);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemWRDate
            // 
            this.layoutControlItemWRDate.Control = this.dateEditIODate;
            this.layoutControlItemWRDate.Location = new System.Drawing.Point(247, 0);
            this.layoutControlItemWRDate.Name = "layoutControlItemWRDate";
            this.layoutControlItemWRDate.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemWRDate.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemWRDate.Text = "Ngày phiếu xuất";
            this.layoutControlItemWRDate.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemWarehouse
            // 
            this.layoutControlItemWarehouse.Control = this.comboBoxEditWarehouse;
            this.layoutControlItemWarehouse.Location = new System.Drawing.Point(453, 0);
            this.layoutControlItemWarehouse.Name = "layoutControlItemWarehouse";
            this.layoutControlItemWarehouse.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItemWarehouse.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemWarehouse.Text = "Kho xuất hàng";
            this.layoutControlItemWarehouse.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemModality
            // 
            this.layoutControlItemModality.Control = this.comboBoxEditModality;
            this.layoutControlItemModality.Location = new System.Drawing.Point(659, 0);
            this.layoutControlItemModality.Name = "layoutControlItemModality";
            this.layoutControlItemModality.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemModality.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemModality.Text = "Phương thức";
            this.layoutControlItemModality.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemNote
            // 
            this.layoutControlItemNote.Control = this.textEditNote;
            this.layoutControlItemNote.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemNote.Name = "layoutControlItemNote";
            this.layoutControlItemNote.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemNote.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemNote.OptionsTableLayoutItem.RowSpan = 2;
            this.layoutControlItemNote.Size = new System.Drawing.Size(453, 48);
            this.layoutControlItemNote.Text = "Ghi chú";
            this.layoutControlItemNote.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEditPONumber;
            this.layoutControlItem5.Location = new System.Drawing.Point(453, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem5.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItem5.Text = "PO Number";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(79, 13);
            // 
            // IssueOrderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "IssueOrderControl";
            this.Size = new System.Drawing.Size(1298, 626);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlIODetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewIODetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditIONumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditModality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditIODate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditIODate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPONumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource wRDataDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter wRDataDetailTableAdapter;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlIODetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewIODetail;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.BindingSource wRDetailBindingSource1;
        private DevExpress.XtraGrid.Columns.GridColumn colIONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colPartNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPartName;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityOrdered;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private System.Windows.Forms.BindingSource wRDetailBindingSource;
        private WARHOUSE_HPDataSet1TableAdapters.WRDetailTableAdapter wRDetailTableAdapter;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEditIONumber;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditModality;
        private DevExpress.XtraEditors.DateEdit dateEditIODate;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditWarehouse;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantity;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraGrid.Columns.GridColumn colPalletID;
        private DevExpress.XtraGrid.Columns.GridColumn colUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colMFGDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEXPDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIDCode;
        private DevExpress.XtraGrid.Columns.GridColumn colInputDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colLotID;
        private DevExpress.XtraEditors.MemoEdit textEditNote;
        public DevExpress.XtraGrid.GridControl gridControl1;
        public DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colIDCode1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit textEditPONumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWarehouse;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemModality;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
    }
}
