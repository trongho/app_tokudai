﻿using DevExpress.XtraBars.Docking2010;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using OfficeOpenXml;
using LicenseContext = OfficeOpenXml.LicenseContext;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Label = WarehouseManager.Models.Label;
using System.Diagnostics;
using DevExpress.Export;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraPrinting;

namespace WarehouseManager
{
    public partial class MaterialManagerControl : DevExpress.XtraEditors.XtraUserControl
    {

        MaterialRepository materialRepository;
        Int32[] selectedRowHandles = null;
        public MaterialManagerControl()
        {
            InitializeComponent();
            materialRepository = new MaterialRepository();
            this.Load += MaterialManagerControl_Load;

        }

        private void MaterialManagerControl_Load(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_ClickAsync;
            gridView.RowClick += gridView_RowClick;
            sbLoadDataForGrid();
        }

        private void gridView_RowClick(Object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            textEditMaterialName.Text = (string)(sender as GridView).GetFocusedRowCellValue("MaterialName");
        }

        public async Task create()
        {
            Material material = new Material();
            material.MaterialID = ((await materialRepository.getAll()).Count + 1).ToString();
            material.MaterialName = textEditMaterialName.Text.ToString();
            if (WMPublic.sbMessageSaveNewRequest(this) == true)
            {
                if (await materialRepository.Create(material) > 0)
                {
                    WMPublic.sbMessageSaveNewSuccess(this);
                    sbLoadDataForGrid();
                }
            }


        }

        public async Task update()
        {

            Material material = (await materialRepository.GetUnderID(textEditMaterialName.Text))[0];
            material.MaterialName = textEditMaterialName.Text.ToString();
            if (WMPublic.sbMessageSaveChangeRequest(this) == true)
            {

                if (await materialRepository.Update(material, material.MaterialID) > 0)
                {
                    WMPublic.sbMessageSaveChangeSuccess(this);
                    sbLoadDataForGrid();
                }
            }
            
           
        }

        public async Task delete()
        {

            Material material = (await materialRepository.GetUnderID(textEditMaterialName.Text))[0];
            if (WMPublic.sbMessageDeleteRequest(this) == true)
            {

                if (await materialRepository.Delete(material.MaterialID) > 0)
                {
                    WMPublic.sbMessageDeleteSuccess(this);
                    sbLoadDataForGrid();
                }
            }


        }

        public async Task deleteAll()
        {

            if (WMPublic.sbMessageDeleteRequest(this) == true)
            {

                if (await materialRepository.Delete() > 0)
                {
                    WMPublic.sbMessageDeleteSuccess(this);
                    sbLoadDataForGrid();
                }
            }


        }


        private async void sbLoadDataForGrid()
        {
            gridControl.DataSource = await materialRepository.getAll();
        }


        //barbutton click
        public async void button_ClickAsync(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    textEditMaterialName.Text = "";
                    break;
                case "save":
                    if((await materialRepository.GetUnderID(textEditMaterialName.Text)).Count > 0)
                    {
                        await update();
                    }
                    else
                    {
                        await create();
                    }
                    break;
                case "delete":
                    selectedRowHandles = gridView.GetSelectedRows();
                    if (selectedRowHandles.Length > 0)
                    {
                        await delete();
                    }
                    else
                    {
                        await deleteAll();
                    }
                    break;
                case "refesh":
                    sbLoadDataForGrid();
                    break;
                case "export":
                   
                    string path = "d:\\tokudai\\material.xlsx";
                    ExportSettings.DefaultExportType = ExportType.DataAware;
                    XlsxExportOptionsEx advOptions = new XlsxExportOptionsEx();
                    advOptions.AllowGrouping = DevExpress.Utils.DefaultBoolean.False;
                    advOptions.ShowTotalSummaries = DevExpress.Utils.DefaultBoolean.False;
                    advOptions.SheetName = "Material";
                    gridControl.ExportToXlsx(path);
                    // Open the created XLSX file with the default application.
                    Process.Start(path);
                    break;
                case "print":
                    gridView.BeginUpdate();
                    gridView.Columns["MaterialID"].Visible = false;
                    // Check whether the GridControl can be previewed.
                    if (!gridControl.IsPrintingAvailable)
                    {
                        MessageBox.Show("The 'DevExpress.XtraPrinting' library is not found", "Error");
                        return;
                    }

                    // Open the Preview window.
                    gridControl.ShowPrintPreview();

                    // Check whether the GridControl can be printed.
                    if (!gridControl.IsPrintingAvailable)
                    {
                        MessageBox.Show("The 'DevExpress.XtraPrinting' library is not found", "Error");
                        return;
                    }

                    // Print.
                    gridControl.Print();
                    gridView.Columns["MaterialID"].Visible = true;
                    gridView.EndUpdate();
                    break;
                case "close":
                    this.Dispose();
                    break;
            }

        }
    }
}
