﻿using DevExpress.Export;
using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class WarehouseReceiptControl : DevExpress.XtraEditors.XtraUserControl
    {
        HandlingStatusRepository handlingStatusRepository;
        WRHeaderRepository wRHeaderRepository;
        WRDetailRepository wRDetailRepository;
        ModalityRepository modalityRepository;
        WarehouseRepository warehouseRepository;
        InventoryRepository inventoryRepository = null;
        GoodsRepository goodsRepository = null;
        InventoryDetailRepository inventoryDetailRepository = null;
        InventoryOfLastMonthsRepository inventoryOfLastMonthsRepository = null;
        InventoryOfLastMonthDetailRepository inventoryOfLastMonthDetailRepository = null;
        public static String selectedWRNumber;

        public WarehouseReceiptControl()
        {
            InitializeComponent();

            
           

            handlingStatusRepository = new HandlingStatusRepository();
            wRDetailRepository = new WRDetailRepository();
            wRHeaderRepository = new WRHeaderRepository();
            modalityRepository = new ModalityRepository();
            warehouseRepository = new WarehouseRepository();
            inventoryRepository = new InventoryRepository();
            inventoryDetailRepository = new InventoryDetailRepository();
            goodsRepository = new GoodsRepository();
            inventoryOfLastMonthsRepository = new InventoryOfLastMonthsRepository();
            inventoryOfLastMonthDetailRepository = new InventoryOfLastMonthDetailRepository();
            popupMenuSelectHandlingStatus();
            popupMenuSelectModality();
            popupMenuSelectWarehouse();
            sbLoadDataForGridWRDetailAsync(textEditWRNumber.Text);
            this.Load += WarehouseReceiptControl_Load;
        }

        private void WarehouseReceiptControl_Load(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditWRNumber.DoubleClick += textEditWRNumer_CellDoubleClick;
           

        }

      
        private async Task popupMenuSelectModality()
        {
            List<Modality> modalities = await modalityRepository.GetAll();
            foreach (Modality modality in modalities)
            {
                comboBoxEditModality.Properties.Items.Add(modality.ModalityName);
            }
            comboBoxEditModality.SelectedIndex = 8;
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private async Task popupMenuSelectWarehouse()
        {
            List<Warehouse> warehouses = await warehouseRepository.getAll();

            foreach (Warehouse warehouse in warehouses)
            {
                comboBoxEditWarehouse.Properties.Items.Add(warehouse.WarehouseName);
            }
            comboBoxEditWarehouse.SelectedIndex = 1;
        }

        private void getTotalQuantity()
        {
            Decimal totalQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRDetail.GetRowHandle(i);
                if (gridViewWRDetail.GetRowCellValue(rowHandle, "PalletID") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewWRDetail.GetRowCellValue(rowHandle, "ReceiptQuantity");
                totalQuantity += quantity;
                i++;
            }
            textEditTotalQuantity.Text = totalQuantity.ToString("0.#####");
        }

        private async Task sbLoadDataForGridWRDetailAsync(String wRNumber)
        {
            List<WRDetail> wRDetails = await wRDetailRepository.GetUnderID(wRNumber);
            
            gridControlWRDetail.DataSource = wRDetails;
            getTotalQuantity();
        }



        public async Task updateWRHeaderAsync()
        {
            WRHeader WRHeader = await wRHeaderRepository.GetUnderID(textEditWRNumber.Text);
            if (WRHeader.HandlingStatusID == "2")
            {
                MessageBox.Show("Phiếu nhập đã được duyệt mức 2, không thể chỉnh sửa");
                return;
            }

            WRHeader.WRNumber = textEditWRNumber.Text;
            WRHeader.WRDate = DateTime.Parse(dateEditWRDate.DateTime.ToString("yyyy-MM-dd"));
            WRHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            WRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            WRHeader.ModalityID = $"{int.Parse(comboBoxEditModality.SelectedIndex.ToString()) + 1:D2}";
            WRHeader.ModalityName = comboBoxEditModality.SelectedItem.ToString();
            WRHeader.WarehouseID = comboBoxEditWarehouse.SelectedIndex == 0 ? "<>" : "1";
            WRHeader.WarehouseName = comboBoxEditWarehouse.SelectedText;
            WRHeader.Note = textEditNote.Text;
            WRHeader.UpdatedUserID = WMMessage.User.UserID;
            WRHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wRHeaderRepository.Update(WRHeader, textEditWRNumber.Text) > 0)
            {
                updateWRDetailAsync();
                WMPublic.sbMessageSaveChangeSuccess(this);

                if (comboBoxEditHandlingStatus.SelectedIndex == 3)
                {
                    string mCurrentDateTime = DateTime.Now.ToString("MM/yyyy");
                    string mCurrentPeriod = String.Format(mCurrentDateTime, "MM/yyyy");
                    int mYear = int.Parse(mCurrentPeriod.Substring(3, 4));
                    int mMonth = int.Parse(mCurrentPeriod.Substring(0, 2));

                    List<Inventory> inventories = await inventoryRepository.GetAll();
                    MessageBox.Show(inventories.Count + "");

                    if (inventories.Count > 0)
                    {
                        if ((inventories[0].Year == mYear && inventories[0].Month < mMonth) || inventories[0].Year < mYear)
                        {
                            await createInventoryOfLastMonth();
                            await createInventoryOfLastMonthDetail();
                            updateInventory();
                            updateInventoryDetail();
                        }
                        else
                        {
                            updateInventory();
                            updateInventoryDetail();
                        }
                    }
                    else
                    {
                        updateInventory();
                        updateInventoryDetail();
                    }

                }
            }
        }

        public async Task updateWRDetailAsync()
        {
            int num = gridViewWRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRDetail.GetRowCellValue(i, gridViewWRDetail.Columns["colPartNumber"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRDetail.GetRowHandle(i);
                String WRNumber = textEditWRNumber.Text;
                String PartNumber = (string)gridViewWRDetail.GetRowCellValue(rowHandle, "PartNumber");
                int Ordinal = (int)gridViewWRDetail.GetRowCellValue(rowHandle, "Ordinal");
                List<WRDetail> WRDetails = await wRDetailRepository.GetUnderMultiID(WRNumber, PartNumber, Ordinal);
                WRDetail WRDetail = WRDetails[0];
                WRDetail.WRNumber = textEditWRNumber.Text;
                WRDetail.PartNumber = (string)gridViewWRDetail.GetRowCellValue(rowHandle, "PartNumber");
                WRDetail.LotID = (string)gridViewWRDetail.GetRowCellValue(rowHandle, "LotID");
                WRDetail.PartName = (string)gridViewWRDetail.GetRowCellValue(rowHandle, "PartName");
                WRDetail.PalletID = (string)gridViewWRDetail.GetRowCellValue(rowHandle, "PalletID");
                WRDetail.IDCode = (string)gridViewWRDetail.GetRowCellValue(rowHandle, "IDCode");
                WRDetail.Unit = (string)gridViewWRDetail.GetRowCellValue(rowHandle, "Unit");
                WRDetail.Ordinal = i + 1;
                WRDetail.ReceiptQuantity = (Decimal?)gridViewWRDetail.GetRowCellValue(rowHandle, "ReceiptQuantity");
                WRDetail.QuantityReceived = (Decimal?)gridViewWRDetail.GetRowCellValue(rowHandle, "QuantityReceived");
                WRDetail.PackingQuantity = (Decimal?)gridViewWRDetail.GetRowCellValue(i, "PackingQuantity");
                WRDetail.PackingVolume = (Decimal?)gridViewWRDetail.GetRowCellValue(i, "PackingVolume");
                if (((DateTime?)gridViewWRDetail.GetRowCellValue(i, "MFGDate")).ToString() != "")
                {
                    WRDetail.MFGDate = (DateTime?)gridViewWRDetail.GetRowCellValue(i, "MFGDate");
                }
                if (((DateTime?)gridViewWRDetail.GetRowCellValue(i, "EXPDate")).ToString() != "")
                {
                    WRDetail.EXPDate = (DateTime?)gridViewWRDetail.GetRowCellValue(i, "EXPDate");
                }
                if (((DateTime?)gridViewWRDetail.GetRowCellValue(i, "InputDate")).ToString() != "")
                {
                    WRDetail.InputDate = (DateTime?)gridViewWRDetail.GetRowCellValue(i, "InputDate");
                }
                if (await wRDetailRepository.Update(WRDetail, textEditWRNumber.Text, WRDetail.PartNumber, WRDetail.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        private async void updateInventory()
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;

            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }



            List<Inventory> inventories = new List<Inventory>();
            List<WRDetail> wRDetails = await wRDetailRepository.GetUnderID(textEditWRNumber.Text);
            if (wRDetails.Count > 0)
            {
                foreach (WRDetail wRDetail in wRDetails)
                {

                    Inventory inventory = new Inventory();
                    inventory.Month = mMonth;
                    inventory.Year = mYear;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = wRDetail.PartNumber;
                    inventory.ReceiptQuantity = wRDetail.ReceiptQuantity;
                    inventory.IssueQuantity = 0;
                    inventory.Status = "1";
                    inventory.PackingVolume = wRDetail.PackingVolume;
                    inventory.IDCode = "";
                    inventories.Add(inventory);
                }

            }
            var querys = inventories.GroupBy(x => x.PartNumber)
                       .Select(x => new Inventory
                       {
                           PartNumber = x.Key,
                           Year = x.First().Year,
                           Month = x.First().Month,
                           WarehouseID = x.First().WarehouseID,
                           Status = x.First().Status,
                           ReceiptQuantity = x.Sum(y => y.ReceiptQuantity),
                           IssueQuantity = x.Sum(y => y.IssueQuantity),
                           PackingVolume = x.First().PackingVolume,
                           Material = "",
                           IDCode = "",
                       })
                      .ToList();


            await inventoryRepository.DeleteOldDate("1");
            foreach (Inventory inventorys in querys)
            {
                List<Goods> goodsList = await goodsRepository.GetUnderPartNumber(inventorys.PartNumber);
                if (goodsList.Count > 0)
                {
                    inventorys.Mold = goodsList[0].Mold;
                }

                List<Inventory> inventories1 = await inventoryRepository.GetUnderMultiPartNumber(mYear, mMonth, "1", inventorys.PartNumber);
                
                List<InventoryOfLastMonths> inventoryOfLastMonths = await inventoryOfLastMonthsRepository.GetByPartNumber("1", inventorys.PartNumber);
                if (inventories1.Count > 0)
                {
                    inventorys.ReceiptQuantity = inventorys.ReceiptQuantity + inventories1[0].ReceiptQuantity;
                    inventorys.IssueQuantity = inventorys.IssueQuantity + inventories1[0].IssueQuantity;
                    inventorys.OpeningStockQuantity = inventoryOfLastMonths.Count > 0 ? inventoryOfLastMonths[0].ClosingStockQuantity : 0;
                    inventorys.ClosingStockQuantity = inventories1[0].OpeningStockQuantity + inventorys.ReceiptQuantity - inventorys.IssueQuantity;
                    await inventoryRepository.Update(inventorys, inventorys.Year, inventorys.Month, inventorys.WarehouseID, inventorys.PartNumber);
                }
                else
                {
                    inventorys.ReceiptQuantity = inventorys.ReceiptQuantity;
                    inventorys.IssueQuantity = inventorys.IssueQuantity;
                    inventorys.OpeningStockQuantity = inventoryOfLastMonths.Count > 0 ? inventoryOfLastMonths[0].ClosingStockQuantity : 0;
                    inventorys.ClosingStockQuantity =inventorys.OpeningStockQuantity+inventorys.ReceiptQuantity - inventorys.IssueQuantity;
                    await inventoryRepository.Create(inventorys);
                }
            }

        }

        private async void updateInventoryDetail()
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;

            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }

            List<InventoryDetail> inventories = new List<InventoryDetail>();
            List<WRDetail> wRDetails = await wRDetailRepository.GetUnderID(textEditWRNumber.Text);
            if (wRDetails.Count > 0)
            {
                foreach (WRDetail wRDetail in wRDetails)
                {
                    InventoryDetail inventory = new InventoryDetail();
                    inventory.Month = mMonth;
                    inventory.Year = mYear;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = wRDetail.PartNumber;
                    inventory.PalletID = wRDetail.PalletID;
                    inventory.LotID = wRDetail.LotID;
                    inventory.IDCode = wRDetail.IDCode;
                    inventory.Quantity = wRDetail.PackingVolume;
                    inventory.Status = "1";
                    inventory.MFGDate = wRDetail.MFGDate;
                    inventory.EXPDate = wRDetail.EXPDate;
                    inventory.InputDate = wRDetail.InputDate;
                    inventories.Add(inventory);

                }


            }
            var querys = inventories.GroupBy(x => new { x.PartNumber, x.PalletID, x.LotID })
                 .Select(x => new InventoryDetail
                 {
                     PartNumber = x.Key.PartNumber,
                     PalletID = x.Key.PalletID,
                     LotID = x.Key.LotID,
                     Year = x.First().Year,
                     Month = x.First().Month,
                     WarehouseID = x.First().WarehouseID,
                     Status = x.First().Status,
                     Quantity = x.Sum(y => y.Quantity),
                     IDCode = string.Join(",", x.Select(ss => ss.IDCode)),
                     MFGDate = x.First().MFGDate,
                     EXPDate = x.First().EXPDate,
                     InputDate = x.First().InputDate
                 })
                .ToList();


            await inventoryDetailRepository.DeleteOldDate("1");

            foreach (InventoryDetail inventoryDetail in querys)
            {
                string idCode = !inventoryDetail.IDCode.Equals("") ? inventoryDetail.IDCode : "";

                List<string> idCodes = idCode.Split(',').ToList();

                foreach (string s in idCodes)
                {
                    if (!s.Equals(""))
                    {
                        inventoryDetail.IDCode = s;
                        List<InventoryDetail> inventoryDetails = await inventoryDetailRepository.GetByParams6(inventoryDetail.Year, inventoryDetail.Month, inventoryDetail.WarehouseID,
                            inventoryDetail.PartNumber, inventoryDetail.PalletID, inventoryDetail.IDCode);

                        if (inventoryDetails.Count == 0)
                        {
                            await inventoryDetailRepository.Create(inventoryDetail);
                        }
                    }
                }
            }
            MessageBox.Show("Đã cộng tồn kho");
        }

        private async void updateInventoryOfLastMonth()
        {
            String monthYear = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
            Int16 mMonth = Int16.Parse(monthYear.Substring(0, 2));
            Int16 mYear = Int16.Parse(monthYear.Substring(3, 4));
            Int16 mLastMonth;
            Int16 mLastYear;

            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = mYear;
            }


            await inventoryOfLastMonthsRepository.DeleteOldDate("1");
            if ((await inventoryOfLastMonthsRepository.GetUnderMultiID(mLastYear, mLastMonth, "1")).Count == 0)
            {
                List<Inventory> inventories = await inventoryRepository.GetUnderMultiID(mLastYear, mLastMonth, "1");
                foreach (Inventory inventory in inventories)
                {
                    InventoryOfLastMonths inventorys = new InventoryOfLastMonths();
                    inventorys.Year = inventory.Year;
                    inventorys.Month = inventory.Month;
                    inventorys.WarehouseID = inventory.WarehouseID;
                    inventorys.PartNumber = inventory.PartNumber;
                    inventorys.Status = "0";
                    inventorys.OpeningStockQuantity = inventory.OpeningStockQuantity;
                    inventorys.ReceiptQuantity = inventory.ReceiptQuantity;
                    inventorys.IssueQuantity = inventory.IssueQuantity;
                    inventorys.ClosingStockQuantity = inventory.ClosingStockQuantity;
                    await inventoryOfLastMonthsRepository.Create(inventorys);
                }
            }

        }

        private async Task createInventoryOfLastMonth()
        {

            await inventoryOfLastMonthsRepository.DeleteOldDate("1");

            List<Inventory> inventories = await inventoryRepository.GetByWarehouseID("1");
            if (inventories.Count > 0)
            {
                foreach (Inventory inventory in inventories)
                {
                    InventoryOfLastMonths inventorys = new InventoryOfLastMonths();
                    inventorys.Year = inventory.Year;
                    inventorys.Month = inventory.Month;
                    inventorys.WarehouseID = inventory.WarehouseID;
                    inventorys.PartNumber = inventory.PartNumber;
                    inventorys.Status = "0";
                    inventorys.PackingVolume = inventory.PackingVolume;
                    inventorys.Material = inventory.Material;
                    inventorys.Mold = inventory.Mold;
                    inventorys.OpeningStockQuantity = inventory.OpeningStockQuantity;
                    inventorys.ReceiptQuantity = inventory.ReceiptQuantity;
                    inventorys.IssueQuantity = inventory.IssueQuantity;
                    inventorys.ClosingStockQuantity = inventory.ClosingStockQuantity;
                    await inventoryOfLastMonthsRepository.Create(inventorys);
                }
            }

            
           
        }

        private async Task createInventoryOfLastMonthDetail()
        {


            await inventoryOfLastMonthDetailRepository.DeleteOldDate("1");
            List<InventoryDetail> inventoryDetails= await inventoryDetailRepository.GetByWarehouseID("1");
            if (inventoryDetails.Count > 0)
            {
                foreach (InventoryDetail inventoryDetail in inventoryDetails)
                {
                    InventoryOfLastMonthDetail inventory = new InventoryOfLastMonthDetail();
                    inventory.Month = inventoryDetail.Month;
                    inventory.Year = inventoryDetail.Year;
                    inventory.WarehouseID = "1";
                    inventory.PartNumber = inventoryDetail.PartNumber;
                    inventory.PalletID = inventoryDetail.PalletID;
                    inventory.LotID = inventoryDetail.LotID;
                    inventory.IDCode = inventoryDetail.IDCode;
                    inventory.Quantity = inventoryDetail.Quantity;
                    inventory.Status = "1";
                    inventory.MFGDate = inventoryDetail.MFGDate;
                    inventory.EXPDate = inventoryDetail.EXPDate;
                    inventory.InputDate = inventoryDetail.InputDate;
                    await inventoryOfLastMonthDetailRepository.Create(inventory);
                }


            }
    
        }

        private async void WRNumber_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private async void textEditWRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchWR frmSearchWR = new frmSearchWR();
            frmSearchWR.ShowDialog(this);
            frmSearchWR.Dispose();
            if (selectedWRNumber != null)
            {
                textEditWRNumber.Text = selectedWRNumber;
                WRHeader wRHeader = await wRHeaderRepository.GetUnderID(textEditWRNumber.Text);
                dateEditWRDate.Text = wRHeader.WRDate.ToString();
                textEditNote.Text = wRHeader.Note;
                comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wRHeader.HandlingStatusID) + 1;
                comboBoxEditModality.SelectedIndex = int.Parse(wRHeader.ModalityID) - 1;
                comboBoxEditWarehouse.SelectedIndex = int.Parse("000");
                await sbLoadDataForGridWRDetailAsync(textEditWRNumber.Text);
            }

        }

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    break;
                case "save":
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateWRHeaderAsync();
                    }
                    break;
                case "delete":
                    break;
                case "search":

                    break;
                case "refesh":
                    sbLoadDataForGridWRDetailAsync(textEditWRNumber.Text);
                    break;
                case "import":
                    break;
                case "export":
                    export();
                    break;
                case "print":
                    printWarehouseReceipt();
                    break;
                case "close":
                    break;
            }

        }

        private void export()
        {
            var saveDialog = new SaveFileDialog();
            saveDialog.InitialDirectory = "pnk\\";
            saveDialog.DefaultExt = "xlsx";
            saveDialog.FileName = "PNK_"+textEditWRNumber.Text+ ".xlsx";
            saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            saveDialog.ShowDialog();
            string path =saveDialog.FileName;

            gridViewWRDetail.BeginUpdate();
            

            ExportSettings.DefaultExportType = ExportType.DataAware;
            XlsxExportOptionsEx advOptions = new XlsxExportOptionsEx();
            advOptions.AllowGrouping = DevExpress.Utils.DefaultBoolean.False;
            advOptions.ShowTotalSummaries = DevExpress.Utils.DefaultBoolean.False;
            gridControlWRDetail.ExportToXlsx(path);
           
           

            gridViewWRDetail.EndUpdate();
            Process.Start(path);
        }

        private void printWarehouseReceipt()
        {
            gridViewWRDetail.BeginUpdate();
            gridViewWRDetail.Columns["No"].Visible = false;
            // Check whether the GridControl can be previewed.
            if (!gridControlWRDetail.IsPrintingAvailable)
            {
                MessageBox.Show("The 'DevExpress.XtraPrinting' library is not found", "Error");
                return;
            }

            // Open the Preview window.
            gridControlWRDetail.ShowPrintPreview();

            // Check whether the GridControl can be printed.
            if (!gridControlWRDetail.IsPrintingAvailable)
            {
                MessageBox.Show("The 'DevExpress.XtraPrinting' library is not found", "Error");
                return;
            }

            // Print.
            gridControlWRDetail.Print();
            gridViewWRDetail.Columns["No"].Visible = true;
            gridViewWRDetail.EndUpdate();
        }
    }
}
