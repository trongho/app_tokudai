﻿
namespace WarehouseManager
{
    partial class BackupRestoreDatabaseControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition6 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition7 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition10 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition11 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition12 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition8 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition9 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition10 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition11 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition12 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition13 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition14 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageBackup = new DevExpress.XtraTab.XtraTabPage();
            this.dataLayoutControl2 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.simpleButtonBackup = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem4 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem5 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem6 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItemLastBackup = new DevExpress.XtraLayout.SimpleLabelItem();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.dateEditCurentDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditAutoBackUpTimeSpan = new DevExpress.XtraEditors.TextEdit();
            this.textEditFileKeep = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.textEditDatabaseName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItemBackupSetup = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItemCurentDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAutoBackUpTimeSpan = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator4 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator5 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDatabaseName = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPageRestore = new DevExpress.XtraTab.XtraTabPage();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageBackup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl2)).BeginInit();
            this.dataLayoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItemLastBackup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCurentDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCurentDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAutoBackUpTimeSpan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFileKeep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDatabaseName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItemBackupSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCurentDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAutoBackUpTimeSpan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDatabaseName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.xtraTabControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(460, 90, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1167, 656);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Location = new System.Drawing.Point(12, 12);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageBackup;
            this.xtraTabControl1.Size = new System.Drawing.Size(1143, 632);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageBackup,
            this.xtraTabPageRestore});
            // 
            // xtraTabPageBackup
            // 
            this.xtraTabPageBackup.Controls.Add(this.dataLayoutControl2);
            this.xtraTabPageBackup.Controls.Add(this.layoutControl3);
            this.xtraTabPageBackup.Controls.Add(this.layoutControl2);
            this.xtraTabPageBackup.Controls.Add(this.dataLayoutControl1);
            this.xtraTabPageBackup.Name = "xtraTabPageBackup";
            this.xtraTabPageBackup.Size = new System.Drawing.Size(1138, 606);
            this.xtraTabPageBackup.Text = "Lưu trữ cơ sở dữ liệu";
            // 
            // dataLayoutControl2
            // 
            this.dataLayoutControl2.Controls.Add(this.panelControl1);
            this.dataLayoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataLayoutControl2.Location = new System.Drawing.Point(0, 381);
            this.dataLayoutControl2.Name = "dataLayoutControl2";
            this.dataLayoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(716, 103, 650, 400);
            this.dataLayoutControl2.Root = this.layoutControlGroup4;
            this.dataLayoutControl2.Size = new System.Drawing.Size(1138, 89);
            this.dataLayoutControl2.TabIndex = 3;
            this.dataLayoutControl2.Text = "dataLayoutControl2";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup4.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup4.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 100D;
            this.layoutControlGroup4.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1});
            rowDefinition1.Height = 100D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup4.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1});
            this.layoutControlGroup4.Size = new System.Drawing.Size(1138, 89);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.linkLabel3);
            this.layoutControl3.Controls.Add(this.linkLabel2);
            this.layoutControl3.Controls.Add(this.simpleButtonBackup);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl3.Location = new System.Drawing.Point(0, 245);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(529, 192, 650, 400);
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(1138, 136);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // linkLabel3
            // 
            this.linkLabel3.Location = new System.Drawing.Point(236, 70);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(444, 20);
            this.linkLabel3.TabIndex = 6;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "linkLabel3";
            // 
            // linkLabel2
            // 
            this.linkLabel2.Location = new System.Drawing.Point(236, 41);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(444, 20);
            this.linkLabel2.TabIndex = 5;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "linkLabel2";
            // 
            // simpleButtonBackup
            // 
            this.simpleButtonBackup.Location = new System.Drawing.Point(236, 99);
            this.simpleButtonBackup.Name = "simpleButtonBackup";
            this.simpleButtonBackup.Size = new System.Drawing.Size(220, 22);
            this.simpleButtonBackup.StyleController = this.layoutControl3;
            this.simpleButtonBackup.TabIndex = 4;
            this.simpleButtonBackup.Text = "Lưu trữ";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem4,
            this.simpleLabelItem5,
            this.simpleLabelItem6,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8});
            this.layoutControlGroup3.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup3.Name = "Root";
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 33.333333333333329D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 33.333333333333329D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 33.333333333333329D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.AutoSize;
            columnDefinition5.Width = 20D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.AutoSize;
            columnDefinition6.Width = 426D;
            this.layoutControlGroup3.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6});
            rowDefinition2.Height = 25D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 25D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 25D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition5.Height = 25D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup3.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5});
            this.layoutControlGroup3.Size = new System.Drawing.Size(1138, 136);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // simpleLabelItem4
            // 
            this.simpleLabelItem4.AllowHotTrack = false;
            this.simpleLabelItem4.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem4.Name = "simpleLabelItem4";
            this.simpleLabelItem4.Size = new System.Drawing.Size(224, 29);
            this.simpleLabelItem4.Text = "Lưu thủ công";
            this.simpleLabelItem4.TextSize = new System.Drawing.Size(114, 13);
            // 
            // simpleLabelItem5
            // 
            this.simpleLabelItem5.AllowHotTrack = false;
            this.simpleLabelItem5.Location = new System.Drawing.Point(0, 29);
            this.simpleLabelItem5.Name = "simpleLabelItem5";
            this.simpleLabelItem5.OptionsTableLayoutItem.RowIndex = 1;
            this.simpleLabelItem5.Size = new System.Drawing.Size(224, 29);
            this.simpleLabelItem5.Text = "Đường dẫn nơi lưu";
            this.simpleLabelItem5.TextSize = new System.Drawing.Size(114, 13);
            // 
            // simpleLabelItem6
            // 
            this.simpleLabelItem6.AllowHotTrack = false;
            this.simpleLabelItem6.Location = new System.Drawing.Point(0, 58);
            this.simpleLabelItem6.Name = "simpleLabelItem6";
            this.simpleLabelItem6.OptionsTableLayoutItem.RowIndex = 2;
            this.simpleLabelItem6.Size = new System.Drawing.Size(224, 29);
            this.simpleLabelItem6.Text = "Tên cơ sở dữ liệu đã lưu";
            this.simpleLabelItem6.TextSize = new System.Drawing.Size(114, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButtonBackup;
            this.layoutControlItem6.Location = new System.Drawing.Point(224, 87);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem6.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem6.Size = new System.Drawing.Size(224, 29);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.linkLabel2;
            this.layoutControlItem7.Location = new System.Drawing.Point(224, 29);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem7.Size = new System.Drawing.Size(448, 29);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.linkLabel3;
            this.layoutControlItem8.Location = new System.Drawing.Point(224, 58);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem8.Size = new System.Drawing.Size(448, 29);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 164);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(1138, 81);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem2,
            this.simpleLabelItemLastBackup});
            this.layoutControlGroup2.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 100D;
            this.layoutControlGroup2.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition7});
            rowDefinition6.Height = 50D;
            rowDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition7.Height = 50D;
            rowDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup2.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition6,
            rowDefinition7});
            this.layoutControlGroup2.Size = new System.Drawing.Size(1138, 81);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Size = new System.Drawing.Size(1118, 30);
            this.simpleLabelItem2.Text = "Thông tin bản lưu trữ cuối cùng";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(150, 13);
            // 
            // simpleLabelItemLastBackup
            // 
            this.simpleLabelItemLastBackup.AllowHotTrack = false;
            this.simpleLabelItemLastBackup.Location = new System.Drawing.Point(0, 30);
            this.simpleLabelItemLastBackup.Name = "simpleLabelItemLastBackup";
            this.simpleLabelItemLastBackup.OptionsTableLayoutItem.RowIndex = 1;
            this.simpleLabelItemLastBackup.Size = new System.Drawing.Size(1118, 31);
            this.simpleLabelItemLastBackup.Text = "Không có bản lưu trữ nào";
            this.simpleLabelItemLastBackup.TextSize = new System.Drawing.Size(150, 13);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.linkLabel1);
            this.dataLayoutControl1.Controls.Add(this.dateEditCurentDate);
            this.dataLayoutControl1.Controls.Add(this.textEditAutoBackUpTimeSpan);
            this.dataLayoutControl1.Controls.Add(this.textEditFileKeep);
            this.dataLayoutControl1.Controls.Add(this.simpleButtonSave);
            this.dataLayoutControl1.Controls.Add(this.textEditDatabaseName);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(551, 178, 650, 400);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1138, 164);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // linkLabel1
            // 
            this.linkLabel1.Location = new System.Drawing.Point(245, 38);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(229, 20);
            this.linkLabel1.TabIndex = 16;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Chọn đường dẫn";
            // 
            // dateEditCurentDate
            // 
            this.dateEditCurentDate.EditValue = null;
            this.dateEditCurentDate.Location = new System.Drawing.Point(1067, 12);
            this.dateEditCurentDate.Name = "dateEditCurentDate";
            this.dateEditCurentDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditCurentDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditCurentDate.Size = new System.Drawing.Size(59, 20);
            this.dateEditCurentDate.StyleController = this.dataLayoutControl1;
            this.dateEditCurentDate.TabIndex = 8;
            // 
            // textEditAutoBackUpTimeSpan
            // 
            this.textEditAutoBackUpTimeSpan.Location = new System.Drawing.Point(135, 64);
            this.textEditAutoBackUpTimeSpan.Name = "textEditAutoBackUpTimeSpan";
            this.textEditAutoBackUpTimeSpan.Size = new System.Drawing.Size(106, 20);
            this.textEditAutoBackUpTimeSpan.StyleController = this.dataLayoutControl1;
            this.textEditAutoBackUpTimeSpan.TabIndex = 10;
            // 
            // textEditFileKeep
            // 
            this.textEditFileKeep.Location = new System.Drawing.Point(368, 64);
            this.textEditFileKeep.Name = "textEditFileKeep";
            this.textEditFileKeep.Size = new System.Drawing.Size(106, 20);
            this.textEditFileKeep.StyleController = this.dataLayoutControl1;
            this.textEditFileKeep.TabIndex = 12;
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Location = new System.Drawing.Point(245, 116);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(229, 22);
            this.simpleButtonSave.StyleController = this.dataLayoutControl1;
            this.simpleButtonSave.TabIndex = 15;
            this.simpleButtonSave.Text = "Lưu";
            // 
            // textEditDatabaseName
            // 
            this.textEditDatabaseName.Location = new System.Drawing.Point(135, 90);
            this.textEditDatabaseName.Name = "textEditDatabaseName";
            this.textEditDatabaseName.Size = new System.Drawing.Size(339, 20);
            this.textEditDatabaseName.StyleController = this.dataLayoutControl1;
            this.textEditDatabaseName.TabIndex = 17;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItemBackupSetup,
            this.layoutControlItemCurentDate,
            this.layoutControlItemAutoBackUpTimeSpan,
            this.layoutControlItem4,
            this.simpleLabelItem1,
            this.layoutControlItem5,
            this.simpleSeparator1,
            this.simpleSeparator2,
            this.simpleSeparator3,
            this.simpleSeparator4,
            this.simpleSeparator5,
            this.layoutControlItem2,
            this.layoutControlItemDatabaseName});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 20.833333333333336D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 20.833333333333336D;
            columnDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition10.Width = 20.833333333333336D;
            columnDefinition11.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition11.Width = 20.833333333333336D;
            columnDefinition12.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition12.Width = 16.666666666666668D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition8,
            columnDefinition9,
            columnDefinition10,
            columnDefinition11,
            columnDefinition12});
            rowDefinition8.Height = 18.140589569160998D;
            rowDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition9.Height = 18.140589569160998D;
            rowDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition10.Height = 18.140589569160998D;
            rowDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition11.Height = 18.140589569160998D;
            rowDefinition11.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition12.Height = 18.140589569160998D;
            rowDefinition12.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition13.Height = 4.5351473922902494D;
            rowDefinition13.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition14.Height = 4.7619047619047619D;
            rowDefinition14.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition8,
            rowDefinition9,
            rowDefinition10,
            rowDefinition11,
            rowDefinition12,
            rowDefinition13,
            rowDefinition14});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1138, 164);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // simpleLabelItemBackupSetup
            // 
            this.simpleLabelItemBackupSetup.AllowHotTrack = false;
            this.simpleLabelItemBackupSetup.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItemBackupSetup.Name = "simpleLabelItemBackupSetup";
            this.simpleLabelItemBackupSetup.Size = new System.Drawing.Size(233, 26);
            this.simpleLabelItemBackupSetup.Text = "Cài đặt tự động lưu trữ";
            this.simpleLabelItemBackupSetup.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutControlItemCurentDate
            // 
            this.layoutControlItemCurentDate.Control = this.dateEditCurentDate;
            this.layoutControlItemCurentDate.Location = new System.Drawing.Point(932, 0);
            this.layoutControlItemCurentDate.Name = "layoutControlItemCurentDate";
            this.layoutControlItemCurentDate.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemCurentDate.Size = new System.Drawing.Size(186, 26);
            this.layoutControlItemCurentDate.Text = "Ngày hiện tại";
            this.layoutControlItemCurentDate.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutControlItemAutoBackUpTimeSpan
            // 
            this.layoutControlItemAutoBackUpTimeSpan.Control = this.textEditAutoBackUpTimeSpan;
            this.layoutControlItemAutoBackUpTimeSpan.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItemAutoBackUpTimeSpan.Name = "layoutControlItemAutoBackUpTimeSpan";
            this.layoutControlItemAutoBackUpTimeSpan.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemAutoBackUpTimeSpan.Size = new System.Drawing.Size(233, 26);
            this.layoutControlItemAutoBackUpTimeSpan.Text = "Thời gian tự động lưu trữ";
            this.layoutControlItemAutoBackUpTimeSpan.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEditFileKeep;
            this.layoutControlItem4.Location = new System.Drawing.Point(233, 52);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem4.Size = new System.Drawing.Size(233, 26);
            this.layoutControlItem4.Text = "Số lượng file giữ lại";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(120, 13);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 26);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.OptionsTableLayoutItem.RowIndex = 1;
            this.simpleLabelItem1.Size = new System.Drawing.Size(233, 26);
            this.simpleLabelItem1.Text = "Chọn nơi lưu trữ";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButtonSave;
            this.layoutControlItem5.Location = new System.Drawing.Point(233, 104);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem5.Size = new System.Drawing.Size(233, 26);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 137);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.OptionsTableLayoutItem.RowIndex = 6;
            this.simpleSeparator1.Size = new System.Drawing.Size(233, 7);
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.Location = new System.Drawing.Point(233, 137);
            this.simpleSeparator2.Name = "simpleSeparator2";
            this.simpleSeparator2.OptionsTableLayoutItem.ColumnIndex = 1;
            this.simpleSeparator2.OptionsTableLayoutItem.RowIndex = 6;
            this.simpleSeparator2.Size = new System.Drawing.Size(233, 7);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.Location = new System.Drawing.Point(466, 137);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.OptionsTableLayoutItem.ColumnIndex = 2;
            this.simpleSeparator3.OptionsTableLayoutItem.RowIndex = 6;
            this.simpleSeparator3.Size = new System.Drawing.Size(233, 7);
            // 
            // simpleSeparator4
            // 
            this.simpleSeparator4.AllowHotTrack = false;
            this.simpleSeparator4.Location = new System.Drawing.Point(699, 137);
            this.simpleSeparator4.Name = "simpleSeparator4";
            this.simpleSeparator4.OptionsTableLayoutItem.ColumnIndex = 3;
            this.simpleSeparator4.OptionsTableLayoutItem.RowIndex = 6;
            this.simpleSeparator4.Size = new System.Drawing.Size(233, 7);
            // 
            // simpleSeparator5
            // 
            this.simpleSeparator5.AllowHotTrack = false;
            this.simpleSeparator5.Location = new System.Drawing.Point(932, 137);
            this.simpleSeparator5.Name = "simpleSeparator5";
            this.simpleSeparator5.OptionsTableLayoutItem.ColumnIndex = 4;
            this.simpleSeparator5.OptionsTableLayoutItem.RowIndex = 6;
            this.simpleSeparator5.Size = new System.Drawing.Size(186, 7);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.linkLabel1;
            this.layoutControlItem2.Location = new System.Drawing.Point(233, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem2.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem2.Size = new System.Drawing.Size(233, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItemDatabaseName
            // 
            this.layoutControlItemDatabaseName.Control = this.textEditDatabaseName;
            this.layoutControlItemDatabaseName.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItemDatabaseName.Name = "layoutControlItemDatabaseName";
            this.layoutControlItemDatabaseName.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemDatabaseName.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItemDatabaseName.Size = new System.Drawing.Size(466, 26);
            this.layoutControlItemDatabaseName.Text = "Tên cơ sở dữ liệu";
            this.layoutControlItemDatabaseName.TextSize = new System.Drawing.Size(120, 13);
            // 
            // xtraTabPageRestore
            // 
            this.xtraTabPageRestore.Name = "xtraTabPageRestore";
            this.xtraTabPageRestore.Size = new System.Drawing.Size(1138, 606);
            this.xtraTabPageRestore.Text = "Hồi phục cơ sở dữ liệu";
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1167, 656);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.xtraTabControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1147, 636);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Location = new System.Drawing.Point(156, 22);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Size = new System.Drawing.Size(344, 20);
            this.pictureEdit1.TabIndex = 0;
            this.pictureEdit1.Visible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.panelControl1;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1118, 69);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pictureEdit1);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1114, 65);
            this.panelControl1.TabIndex = 4;
            this.panelControl1.Visible = false;
            // 
            // BackupRestoreDatabaseControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "BackupRestoreDatabaseControl";
            this.Size = new System.Drawing.Size(1167, 656);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageBackup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl2)).EndInit();
            this.dataLayoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItemLastBackup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCurentDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCurentDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAutoBackUpTimeSpan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFileKeep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDatabaseName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItemBackupSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCurentDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAutoBackUpTimeSpan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDatabaseName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageBackup;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageRestore;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButtonBackup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem4;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem5;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItemLastBackup;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.DateEdit dateEditCurentDate;
        private DevExpress.XtraEditors.TextEdit textEditAutoBackUpTimeSpan;
        private DevExpress.XtraEditors.TextEdit textEditFileKeep;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItemBackupSetup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCurentDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAutoBackUpTimeSpan;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator4;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator5;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit textEditDatabaseName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDatabaseName;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}
